﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Enums;

namespace SharpHmiAndroid
{
    public class LanguagesAdapter : BaseAdapter<Language>
    {
        List<Language> languageList;
		Activity context;

        public LanguagesAdapter(Activity cntxt, List<Language> list)
		{
			context = cntxt;
            languageList = list;
		}

        public override Language this[int position] => languageList[position];

        public override int Count => languageList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
			var view = convertView ?? context.LayoutInflater.Inflate(Resource.Layout.touch_event_item_adapter, parent, false);

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = languageList[position].ToString();
			return view;
        }
    }
}
