﻿using System;
using static SharpHmiAndroid.AppInstanceManager;

namespace SharpHmiAndroid
{
	public class Const
	{
		public Const()
		{
		}

		// Key to pass a FileChooser filename via IntentHelper
		public static string INTENTHELPER_KEY_FILECHOOSER_FILE = "IntentFileChooserFile";

		// Request id for FileChooserActivity
		public static readonly int REQUEST_FILE_CHOOSER = 45;

		public static int JSON_REQUEST_WRITE_STORAGE = 11;

		public static string PREFS_KEY_TRANSPORT_PORT = "TCPPort";
		public static string PREFS_KEY_TRANSPORT_IP = "IPAddress";
        public static string PREFS_KEY_INITIAL_CONFIG = "initial_config";

        public static string PREFS_KEY_BC_MIX_AUDIO_SUPPORTED = "bc_mix_audio_supported";
        public static string PREFS_KEY_BUTTONS_GET_CAPABILITIES = "buttons_get_capabilities";
        public static string PREFS_KEY_NAVIGATION_IS_READY = "nav_is_ready";
		public static string PREFS_KEY_RC_GET_CAPABILITIES = "rc_get_capabilities";
		public static string PREFS_KEY_RC_IS_READY = "rc_is_ready";
        public static string PREFS_KEY_TTS_GET_CAPABILITIES = "tts_get_capabilities";
        public static string PREFS_KEY_TTS_GET_LANGUAGE = "tts_get_language";
        public static string PREFS_KEY_TTS_GET_SUPPORTED_LANGUAGE = "tts_get_supported_language";
        public static string PREFS_KEY_TTS_IS_READY = "tts_is_ready";
		public static string PREFS_KEY_UI_GET_CAPABILITIES = "ui_get_capabilities";
		public static string PREFS_KEY_UI_GET_LANGUAGE = "ui_get_language";
		public static string PREFS_KEY_UI_GET_SUPPORTED_LANGUAGE = "ui_get_supported_language";
		public static string PREFS_KEY_UI_IS_READY = "ui_is_ready";
        public static string PREFS_KEY_VI_GET_VEHICLE_DATA = "vi_get_vehicle_data";
        public static string PREFS_KEY_VI_GET_VEHICLE_TYPE = "vi_get_vehicle_type";
        public static string PREFS_KEY_VI_IS_READY = "vi_is_ready";
		public static string PREFS_KEY_VR_GET_CAPABILITIES = "vr_get_capabilities";
		public static string PREFS_KEY_VR_GET_LANGUAGE = "vr_get_language";
		public static string PREFS_KEY_VR_GET_SUPPORTED_LANGUAGE = "vr_get_supported_language";
		public static string PREFS_KEY_VR_IS_READY = "vr_is_ready";
        public static string PREFS_KEY_SELECTION_MODE = "selection_mode";
        public static string PREFS_KEY_BC_GET_SYSTEM_INFO = "bc_get_get_system_info";

        public static int PREFS_DEFAULT_TRANSPORT_PORT = 8087;
		public static string PREFS_DEFAULT_TRANSPORT_IP = "127.0.0.1";
        public static Boolean PREFS_DEFAULT_BC_MIX_AUDIO_SUPPORTED = true;
		public static Boolean PREFS_DEFAULT_BUTTONS_GET_CAPABILITIES = true;
		public static Boolean PREFS_DEFAULT_NAVIGATION_IS_READY = true;
		public static Boolean PREFS_DEFAULT_RC_GET_CAPABILITIES = true;
		public static Boolean PREFS_DEFAULT_RC_IS_READY = true;
		public static Boolean PREFS_DEFAULT_TTS_GET_CAPABILITIES = true;
		public static Boolean PREFS_DEFAULT_TTS_GET_LANGUAGE = true;
		public static Boolean PREFS_DEFAULT_TTS_GET_SUPPORTED_LANGUAGE = true;
		public static Boolean PREFS_DEFAULT_TTS_IS_READY = true;
		public static Boolean PREFS_DEFAULT_UI_GET_CAPABILITIES = true;
		public static Boolean PREFS_DEFAULT_UI_GET_LANGUAGE = true;
		public static Boolean PREFS_DEFAULT_UI_GET_SUPPORTED_LANGUAGE = true;
		public static Boolean PREFS_DEFAULT_UI_IS_READY = true;
		public static Boolean PREFS_DEFAULT_VI_GET_VEHICLE_DATA = true;
		public static Boolean PREFS_DEFAULT_VI_GET_VEHICLE_TYPE = true;
		public static Boolean PREFS_DEFAULT_VI_IS_READY = true;
		public static Boolean PREFS_DEFAULT_VR_GET_CAPABILITIES = true;
		public static Boolean PREFS_DEFAULT_VR_GET_LANGUAGE = true;
		public static Boolean PREFS_DEFAULT_VR_GET_SUPPORTED_LANGUAGE = true;
		public static Boolean PREFS_DEFAULT_VR_IS_READY = true;
        public static Boolean PREFS_DEFAULT_BC_GET_SYSTEM_INFO = true;
        public static int PREFS_DEFAULT_SELECTION_MODE = (int)SelectionMode.NONE;
	}
}