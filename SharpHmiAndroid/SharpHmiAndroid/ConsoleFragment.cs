﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using HmiApiLib;
using HmiApiLib.Base;
using HmiApiLib.Builder;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using Newtonsoft.Json;

namespace SharpHmiAndroid
{
	public class ConsoleFragment : Fragment
	{
        private string CategoryAll = "All";
        private string CategoryBasicCommunication = "Basic Communication";
        private string CategoryButtons = "Buttons";
        private string CategoryNavigation = "Navigation";
        private string CategoryRC = "RC";
        private string CategorySDL = "SDL";
        private string CategoryTTS = "TTS";
        private string CategoryUI = "UI";
        private string CategoryVehicleInfo = "Vehicle Info";
        private string CategoryVR = "VR";

        private string TX_LATER = "Tx Later";
        private string TX_NOW = "Tx Now";
        private string RESET = "Reset";
        private string CANCEL = "Cancel";

        // Basic Communication Outgoing Response
        private string BCResponseActivateApp = "BC.ActivateApp";
		private string BCResponseAllowDeviceToConnect = "BC.AllowDeviceToConnect";
        private string BCResponseDecryptCertificate = "BC.DecryptCertificate";
		private string BCResponseDialNumber = "BC.DialNumber";
		private string BCResponseGetSystemInfo = "BC.GetSystemInfo";
		private string BCResponseMixingAudioSupported = "BC.MixingAudioSupported";
		private string BCResponsePolicyUpdate = "BC.PolicyUpdate";
		private string BCResponseSystemRequest = "BC.SystemRequest";
		private string BCResponseUpdateAppList = "BC.UpdateAppList";
		private string BCResponseUpdateDeviceList = "BC.UpdateDeviceList";

		// Basic Communication Outgoing Notification
		private string BCNotificationOnAppActivated = "BC.OnAppActivated";
		private string BCNotificationOnAppDeactivated = "BC.OnAppDeactivated";
		private string BCNotificationOnAwakeSDL = "BC.OnAwakeSDL";
		private string BCNotificationOnDeactivateHMI = "BC.OnDeactivateHMI";
		private string BCNotificationOnDeviceChosen = "BC.OnDeviceChosen";
		private string BCNotificationOnEmergencyEvent = "BC.OnEmergencyEvent";
		private string BCNotificationOnEventChanged = "BC.OnEventChanged";
		private string BCNotificationOnExitAllApplications = "BC.OnExitAllApplications";
		private string BCNotificationOnExitApplication = "BC.OnExitApplication";
		private string BCNotificationOnFindApplications = "BC.OnFindApplications";
		private string BCNotificationOnIgnitionCycleOver = "BC.OnIgnitionCycleOver";
		private string BCNotificationOnPhoneCall = "BC.OnPhoneCall";
		private string BCNotificationOnReady = "BC.OnReady";
		private string BCNotificationOnStartDeviceDiscovery = "BC.OnStartDeviceDiscovery";
		private string BCNotificationOnSystemInfoChanged = "BC.OnSystemInfoChanged";
		private string BCNotificationOnSystemRequest = "BC.OnSystemRequest";
		private string BCNotificationOnUpdateDeviceList = "BC.OnUpdateDeviceList";

		// Button Outgoing Response
		private string ButtonsResponseGetCapabilities = "Buttons.GetCapabilities";
        private string ButtonsResponseButtonPress = "Buttons.ButtonPress";

		// Button Outgoing Notifications
		private string ButtonsNotificationOnButtonEvent = "Buttons.OnButtonEvent";
		private string ButtonsNotificationOnButtonPress = "Buttons.OnButtonPress";

		// Navigation Outgoing Response
		private string NavigationResponseAlertManeuver = "Navigation.AlertManeuver";
		private string NavigationResponseGetWayPoints = "Navigation.GetWayPoints";
		private string NavigationResponseIsReady = "Navigation.IsReady";
		private string NavigationResponseSendLocation = "Navigation.SendLocation";
		private string NavigationResponseShowConstantTBT = "Navigation.ShowConstantTBT";
		private string NavigationResponseStartAudioStream = "Navigation.StartAudioStream";
		private string NavigationResponseStartStream = "Navigation.StartStream";
		private string NavigationResponseStopAudioStream = "Navigation.StopAudioStream";
		private string NavigationResponseStopStream = "Navigation.StopStream";
		private string NavigationResponseSubscribeWayPoints = "Navigation.SubscribeWayPoints";
		private string NavigationResponseUnsubscribeWayPoints = "Navigation.UnsubscribeWayPoints";
		private string NavigationResponseUpdateTurnList = "Navigation.UpdateTurnList";

		// Navigation Outgoing Notifications
		private string NavigationNotificationOnTBTClientState = "Navigation.OnTBTClientState";

        // RC Outgoing Response
        private string RCResponseGetCapabilities = "RC.GetCapabilities";
        private string RCResponseGetInteriorVehicleData = "RC.GetInteriorVehicleData";
        private string RCResponseGetInteriorVehicleDataConsent = "RC.GetInteriorVehicleDataConsent";
        private string RCResponseIsReady = "RC.IsReady";
        private string RCResponseSetInteriorVehicleData = "RC.SetInteriorVehicleData";

        // RC Outgoing Notifications
        private string RCNotificationOnInteriorVehicleData = "RC.OnInteriorVehicleData";
        private string RCNotificationOnRemoteControlSettings = "RC.OnRemoteControlSettings";

		// SDL Outgoing Request
		private string SDLRequestActivateApp = "SDL.ActivateApp";
		private string SDLRequestGetListOfPermissions = "SDL.GetListOfPermissions";
		private string SDLRequestGetStatusUpdate = "SDL.GetStatusUpdate";
		private string SDLRequestGetURLS = "SDL.GetURLS";
		private string SDLRequestGetUserFriendlyMessage = "SDL.GetUserFriendlyMessage";
		private string SDLRequestUpdateSDL = "SDL.UpdateSDL";

		// SDL Outgoing Notifications
		private string SDLNotificationOnAllowSDLFunctionality = "SDL.OnAllowSDLFunctionality";
		private string SDLNotificationOnAppPermissionConsent = "SDL.OnAppPermissionConsent";
		private string SDLNotificationOnPolicyUpdate = "SDL.OnPolicyUpdate";
		private string SDLNotificationOnReceivedPolicyUpdate = "SDL.OnReceivedPolicyUpdate";

		// TTS Outgoing Response
		private string TTSResponseChangeRegistration = "TTS.ChangeRegistration";
		private string TTSResponseGetCapabilities = "TTS.GetCapabilities";
		private string TTSResponseGetLanguage = "TTS.GetLanguage";
		private string TTSResponseGetSupportedLanguages = "TTS.GetSupportedLanguages";
		private string TTSResponseIsReady = "TTS.IsReady";
		private string TTSResponseSetGlobalProperties = "TTS.SetGlobalProperties";
		private string TTSResponseSpeak = "TTS.Speak";
		private string TTSResponseStopSpeaking = "TTS.StopSpeaking";

		// TTS Outgoing Notifications
		private string TTSNotificationOnLanguageChange = "TTS.OnLanguageChange";
		private string TTSNotificationOnResetTimeout = "TTS.OnResetTimeout";
		private string TTSNotificationStarted = "TTS.Started";
		private string TTSNotificationStopped = "TTS.Stopped";

		// UI Outgoing Response
		private string UIResponseAddCommand = "UI.AddCommand";
		private string UIResponseAddSubMenu = "UI.AddSubMenu";
		private string UIResponseAlert = "UI.Alert";
		private string UIResponseChangeRegistration = "UI.ChangeRegistration";
		private string UIResponseClosePopUp = "UI.ClosePopUp";
		private string UIResponseDeleteCommand = "UI.DeleteCommand";
		private string UIResponseDeleteSubMenu = "UI.DeleteSubMenu";
		private string UIResponseEndAudioPassThru = "UI.EndAudioPassThru";
		private string UIResponseGetCapabilities = "UI.GetCapabilities";
		private string UIResponseGetLanguage = "UI.GetLanguage";
		private string UIResponseGetSupportedLanguages = "UI.GetSupportedLanguages";
		private string UIResponseIsReady = "UI.IsReady";
		private string UIResponsePerformAudioPassThru = "UI.PerformAudioPassThru";
		private string UIResponsePerformInteraction = "UI.PerformInteraction";
		private string UIResponseScrollableMessage = "UI.ScrollableMessage";
		private string UIResponseSetAppIcon = "UI.SetAppIcon";
		private string UIResponseSetDisplayLayout = "UI.SetDisplayLayout";
		private string UIResponseSetGlobalProperties = "UI.SetGlobalProperties";
		private string UIResponseSetMediaClockTimer = "UI.SetMediaClockTimer";
		private string UIResponseShow = "UI.Show";
		private string UIResponseShowCustomForm = "UI.ShowCustomForm";
		private string UIResponseSlider = "UI.Slider";

		// UI Outgoing Notifications
		private string UINotificationOnCommand = "UI.OnCommand";
		private string UINotificationOnDriverDistraction = "UI.OnDriverDistraction";
		private string UINotificationOnKeyboardInput = "UI.OnKeyboardInput";
		private string UINotificationOnLanguageChange = "UI.OnLanguageChange";
		private string UINotificationOnRecordStart = "UI.OnRecordStart";
		private string UINotificationOnResetTimeout = "UI.OnResetTimeout";
		private string UINotificationOnSystemContext = "UI.OnSystemContext";
		private string UINotificationOnTouchEvent = "UI.OnTouchEvent";

		// VehicleInfo Outgoing Response
		private string VIResponseDiagnosticMessage = "VI.DiagnosticMessage";
		private string VIResponseGetDTCs = "VI.GetDTCs";
		private string VIResponseGetVehicleData = "VI.GetVehicleData";
		private string VIResponseGetVehicleType = "VI.GetVehicleType";
		private string VIResponseIsReady = "VI.IsReady";
		private string VIResponseReadDID = "VI.ReadDID";
		private string VIResponseSubscribeVehicleData = "VI.SubscribeVehicleData";
		private string VIResponseUnsubscribeVehicleData = "VI.UnsubscribeVehicleData";

		// VehicleInfo Outgoing Notifications
		private string VINotificationOnVehicleData = "VI.OnVehicleData";

		// VR Outgoing Response
		private string VRResponseAddCommand = "VR.AddCommand";
		private string VRResponseChangeRegistration = "VR.ChangeRegistration";
		private string VRResponseDeleteCommand = "VR.DeleteCommand";
		private string VRResponseGetCapabilities = "VR.GetCapabilities";
		private string VRResponseGetLanguage = "VR.GetLanguage";
		private string VRResponseGetSupportedLanguages = "VR.GetSupportedLanguages";
		private string VRResponseIsReady = "VR.IsReady";
		private string VRResponsePerformInteraction = "VR.PerformInteraction";

		// VR Outgoing Notifications
		private string VRNotificationOnCommand = "VR.OnCommand";
		private string VRNotificationOnLanguageChange = "VR.OnLanguageChange";
		private string VRNotificationStarted = "VR.Started";
		private string VRNotificationStopped = "VR.Stopped";



		private ListView _listview = null;
		public MessageAdapter _msgAdapter = null;
		private List<LogMessage> _logMessages = new List<LogMessage>();

		//int appID;
		////public static readonly String sClickedAppID = "APP_ID";

		LayoutInflater layoutInflater;


		string[] resultCode = Enum.GetNames(typeof(HmiApiLib.Common.Enums.Result));
		string[] vehicleDataType = Enum.GetNames(typeof(VehicleDataType));
        string[] vehicleDataResultCode = Enum.GetNames(typeof(VehicleDataResultCode));
		string[] languages = Enum.GetNames(typeof(Language));
		string[] transportType = Enum.GetNames(typeof(TransportType));
		string[] eventTypes = Enum.GetNames(typeof(EventTypes));
		string[] fileType = Enum.GetNames(typeof(FileType));
		string[] requestType = Enum.GetNames(typeof(RequestType));
		string[] appsCloseReason = Enum.GetNames(typeof(ApplicationsCloseReason));
		string[] appsExitReason = Enum.GetNames(typeof(ApplicationExitReason));
		string[] buttonNames = Enum.GetNames(typeof(ButtonName));
		string[] buttonEventMode = Enum.GetNames(typeof(ButtonEventMode));
		string[] buttonPressMode = Enum.GetNames(typeof(ButtonPressMode));
		string[] imageType = Enum.GetNames(typeof(ImageType));
		string[] driverDistractionState = Enum.GetNames(typeof(DriverDistractionState));
		string[] keyBoardEvent = Enum.GetNames(typeof(KeyboardEvent));
		string[] vrCapabilities = Enum.GetNames(typeof(VrCapabilities));
		string[] speechCapabilities = Enum.GetNames(typeof(SpeechCapabilities));
        string[] prerecordedSpeech = Enum.GetNames(typeof(PrerecordedSpeech));
        string[] tbtStateArray = Enum.GetNames(typeof(TBTState));
		String[] PowerModeQualificationStatusArray = Enum.GetNames(typeof(PowerModeQualificationStatus));
		String[] CarModeStatusArray = Enum.GetNames(typeof(CarModeStatus));
		String[] PowerModeStatusArray = Enum.GetNames(typeof(PowerModeStatus));
		String[] EmergencyEventTypeArray = Enum.GetNames(typeof(EmergencyEventType));
		String[] FuelCutoffStatusArray = Enum.GetNames(typeof(FuelCutoffStatus));
		string[] vehicleDataNotificationStatusArray = Enum.GetNames(typeof(VehicleDataNotificationStatus));
		string[] ECallConfirmationStatusArray = Enum.GetNames(typeof(ECallConfirmationStatus));
        string[] AmbientLightStatusArray = Enum.GetNames(typeof(AmbientLightStatus));
		String[] DeviceLevelStatusArray = Enum.GetNames(typeof(DeviceLevelStatus));
		String[] PrimaryAudioSourceArray = Enum.GetNames(typeof(PrimaryAudioSource));
		String[] IgnitionStableStatusArray = Enum.GetNames(typeof(IgnitionStableStatus));
		String[] IgnitionStatusArray = Enum.GetNames(typeof(IgnitionStatus));
		String[] WarningLightStatusArray = Enum.GetNames(typeof(WarningLightStatus));
		String[] SingleTireStatusArray = Enum.GetNames(typeof(ComponentVolumeStatus));
		String[] CompassDirectionArray = Enum.GetNames(typeof(CompassDirection));
		String[] DimensionArray = Enum.GetNames(typeof(Dimension));
		String[] PRNDLArray = Enum.GetNames(typeof(PRNDL));
		String[] WiperStatusArray = Enum.GetNames(typeof(WiperStatus));
		String[] VehicleDataStatusArray = Enum.GetNames(typeof(VehicleDataStatus));
        String[] ComponentVolumeStatusArray = Enum.GetNames(typeof(ComponentVolumeStatus));
        String[] VehicleDataEventStatusArray = Enum.GetNames(typeof(VehicleDataEventStatus));
        String[] RCAccessModeArray = Enum.GetNames(typeof(RCAccessMode));
        String[] RCModuleTypeArray = Enum.GetNames(typeof(ModuleType));
        String[] RCRadioBandArray = Enum.GetNames(typeof(RadioBand));
        String[] RCRadioStateArray = Enum.GetNames(typeof(RadioState));
        String[] RCTempUnitArray= Enum.GetNames(typeof(TemperatureUnit));
        String[] RCDefrostZoneArray= Enum.GetNames(typeof(DefrostZone));
        String[] RCVentilationModeArray= Enum.GetNames(typeof(VentilationMode));
        String[] VIElectronicParkBrakeStatusArray = Enum.GetNames(typeof(ElectronicParkBrakeStatus));

		string[] systemContext = Enum.GetNames(typeof(SystemContext));

		public ConsoleFragment()
		{
			
		}

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			if (AppInstanceManager.Instance.getMsgAdapter() == null)
			{
				_msgAdapter = new MessageAdapter(this.Activity, _logMessages);
				AppInstanceManager.Instance.setMsgAdapter(_msgAdapter);
			}
			else
			{
				_msgAdapter = AppInstanceManager.Instance.getMsgAdapter();
				_msgAdapter.updateActivity(this.Activity);
			}

			if (SdlService.instance == null)
			{
				var intent = new Intent((MainActivity)this.Activity, typeof(SdlService));
				((MainActivity)this.Activity).StartService(intent);
			}
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			View rootView = inflater.Inflate(Resource.Layout.console_fragment, container,
											 false);
			layoutInflater = inflater;
			// appID = Arguments.GetIntArray(sClickedAppID);

			_listview = (ListView)rootView.FindViewById(Resource.Id.messageList);
			_listview.Clickable = true;

			_listview.Adapter = _msgAdapter;
			_listview.TranscriptMode = TranscriptMode.AlwaysScroll;

			_listview.ItemClick += listView_ItemClick;

			if (_listview.Adapter.Count > 10)
			{
				_listview.StackFromBottom = true;
			}

            Button sendRPCButton = (Button)rootView.FindViewById(Resource.Id.send_rpc);
            sendRPCButton.Click += (object sender, EventArgs e) => showRPCListDialog();

			return rootView;
		}

		private void showDialogWithBack(string sTitle, string sBody, Boolean isRpcResendAllowed, AlertDialog.Builder builder, View jsonLayout)
		{
			AlertDialog.Builder dialogNew = new AlertDialog.Builder(this.Activity);
			dialogNew.SetMessage(sBody);
			dialogNew.SetTitle("Show Getter Methods");
			dialogNew.SetPositiveButton("Back", (senderAlert, args) =>
				{

					if (jsonLayout != null)
					{
						ViewGroup parent = (ViewGroup)jsonLayout.Parent;
						if (parent != null)
						{
							parent.RemoveView(jsonLayout);
						}
					}
					AlertDialog alertDlg = builder.Create();
					alertDlg.Show();

					Button resendRpcAllowed = alertDlg.GetButton((int)DialogButtonType.Neutral);
					if (isRpcResendAllowed)
					{
						resendRpcAllowed.Enabled = true;
					}
					else
					{
						resendRpcAllowed.Enabled = false;
					}

				});
			AlertDialog ad = dialogNew.Create();
			ad.Show();
		}

		void listView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			Object listObj = _msgAdapter[e.Position];

			if (listObj is RpcLogMessage)
			{
				LayoutInflater requestJSON = (LayoutInflater)this.Activity.GetSystemService(Context.LayoutInflaterService);
				View jsonLayout = requestJSON.Inflate(Resource.Layout.consolelogpreview, null);
				EditText jsonText = (EditText)jsonLayout.FindViewById(Resource.Id.consoleLogPreview_jsonContent);

				RpcMessage message = ((RpcLogMessage)listObj).getMessage();
				AlertDialog.Builder builder = new AlertDialog.Builder(this.Activity);

				string rawJSON = "";
				int corrId = -1;
				string methodName = "";

				jsonText.Focusable = false;

				if (message is RpcRequest)
				{
					corrId = ((RpcRequest)message).getId();
					methodName = ((RpcRequest)message).method;
				}
				else if (message is RpcResponse)
				{
					corrId = ((RpcResponse)message).getId();
                    methodName = ((RpcResponse)message).getMethod();
				}
				else if (message is RpcNotification)
				{
					methodName = ((RpcNotification)message).method;
				}
				else if (message is RequestNotifyMessage)
				{
					methodName = ((RequestNotifyMessage)message).method;
				}

				try
				{
					rawJSON = JsonConvert.SerializeObject(message, Formatting.Indented, new JsonSerializerSettings
					{
						NullValueHandling = NullValueHandling.Ignore
					});
					builder.SetTitle("Raw JSON" + (corrId != -1 ? " (Corr ID " + corrId + ")" : ""));
				}
				catch (Exception ex)
				{
					try
					{
						rawJSON = methodName +
							" (" + message.getRpcMessageFlow().ToString().ToLower() + " " + message.getRpcMessageType().ToString().ToLower() + ")";
					}
					catch (Exception e1)
					{
						rawJSON = "Undefined";
					}
				}

				string finalJSON = rawJSON;

				jsonText.Text = finalJSON;

				builder.SetView(jsonLayout);

				builder.SetPositiveButton("Getters", (senderAlert, args) =>
				{
					string sInfo = RpcMessageGetterInfo.viewDetails(message, false, 0);
					Boolean isRpcResendAllowed = false;

					if (message.rpcMessageFlow == HmiApiLib.Common.Enums.RpcMessageFlow.OUTGOING)
					{
						isRpcResendAllowed = true;
					}

					showDialogWithBack("GetterInfo", sInfo, isRpcResendAllowed, builder, jsonLayout);
				});

				builder.SetNeutralButton("Resend", (senderAlert, args) =>
				{
					AppInstanceManager.Instance.sendRpc(message);
				});

				builder.SetNegativeButton(CANCEL, (senderAlert, args) =>
				{
					builder.Dispose();
				});

				AlertDialog ad = builder.Create();
				ad.Show();

				Button resendRpc = ad.GetButton((int)DialogButtonType.Neutral);
				if (message.rpcMessageFlow == HmiApiLib.Common.Enums.RpcMessageFlow.OUTGOING)
				{
					resendRpc.Enabled = true;
				}
				else
				{
					resendRpc.Enabled = false;
				}

			}

			else if (listObj is StringLogMessage)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(this.Activity);
				string sMessageText = ((StringLogMessage)listObj).getData();
				if (sMessageText == "")
				{
					sMessageText = ((StringLogMessage)listObj).getMessage();
				}
				builder.SetMessage(sMessageText);
				builder.SetPositiveButton("OK", (senderAlert, args) =>
				{
					builder.Dispose();
				});
				AlertDialog ad = builder.Create();
				ad.Show();
			}
		}

		public void showRPCListDialog()
		{
            string[] categoryListArray = {CategoryAll, CategoryBasicCommunication, CategoryButtons, 
                CategoryNavigation, CategoryRC, CategorySDL, CategoryTTS, CategoryUI, CategoryVehicleInfo, CategoryVR};
            
			string[] BasicCommunicationArray = {BCResponseActivateApp, BCResponseAllowDeviceToConnect, BCResponseDecryptCertificate, BCResponseDialNumber,
				BCResponseGetSystemInfo, BCResponseMixingAudioSupported, BCResponsePolicyUpdate, BCResponseSystemRequest,
				BCResponseUpdateAppList, BCResponseUpdateDeviceList, BCNotificationOnAppActivated, BCNotificationOnAppDeactivated,
				BCNotificationOnAwakeSDL, BCNotificationOnDeactivateHMI, BCNotificationOnDeviceChosen,
				BCNotificationOnEmergencyEvent, BCNotificationOnEventChanged, BCNotificationOnExitAllApplications,
				BCNotificationOnExitApplication, BCNotificationOnFindApplications, BCNotificationOnIgnitionCycleOver,
				BCNotificationOnPhoneCall, BCNotificationOnReady, BCNotificationOnStartDeviceDiscovery,
				BCNotificationOnSystemInfoChanged, BCNotificationOnSystemRequest, BCNotificationOnUpdateDeviceList};
            
            string[] ButtonsArray = {ButtonsResponseGetCapabilities, ButtonsResponseButtonPress, ButtonsNotificationOnButtonEvent, ButtonsNotificationOnButtonPress};

			string[] NavigationArray = {NavigationResponseAlertManeuver, NavigationResponseGetWayPoints, NavigationResponseIsReady,
				NavigationResponseSendLocation, NavigationResponseShowConstantTBT, NavigationResponseStartAudioStream,
				NavigationResponseStartStream, NavigationResponseStopAudioStream, NavigationResponseStopStream,
				NavigationResponseSubscribeWayPoints, NavigationResponseUnsubscribeWayPoints, NavigationResponseUpdateTurnList,
				NavigationNotificationOnTBTClientState};

            string[] RCArray = {RCResponseGetCapabilities, RCResponseGetInteriorVehicleData, RCResponseGetInteriorVehicleDataConsent, RCResponseIsReady,
                RCResponseSetInteriorVehicleData, RCNotificationOnInteriorVehicleData, RCNotificationOnRemoteControlSettings};

			string[] SDLArray = {SDLRequestActivateApp, SDLRequestGetListOfPermissions,
				SDLRequestGetStatusUpdate, SDLRequestGetURLS, SDLRequestGetUserFriendlyMessage, SDLRequestUpdateSDL,
				SDLNotificationOnAllowSDLFunctionality, SDLNotificationOnAppPermissionConsent, SDLNotificationOnPolicyUpdate,
				SDLNotificationOnReceivedPolicyUpdate};

			string[] TTSArray = {TTSResponseChangeRegistration, TTSResponseGetCapabilities, TTSResponseGetLanguage, 
                TTSResponseGetSupportedLanguages, TTSResponseIsReady, TTSResponseSetGlobalProperties,
				TTSResponseSpeak, TTSResponseStopSpeaking, TTSNotificationOnLanguageChange, TTSNotificationOnResetTimeout,
				TTSNotificationStarted, TTSNotificationStopped};

			string[] UIArray = {UIResponseAddCommand, UIResponseAddSubMenu, UIResponseAlert, UIResponseChangeRegistration, 
                UIResponseClosePopUp, UIResponseDeleteCommand, UIResponseDeleteSubMenu, UIResponseEndAudioPassThru, 
                UIResponseGetCapabilities, UIResponseGetLanguage, UIResponseGetSupportedLanguages,
				UIResponseIsReady, UIResponsePerformAudioPassThru, UIResponsePerformInteraction, UIResponseScrollableMessage,
				UIResponseSetAppIcon, UIResponseSetDisplayLayout, UIResponseSetGlobalProperties, UIResponseSetMediaClockTimer,
				UIResponseShow, UIResponseShowCustomForm, UIResponseSlider, UINotificationOnCommand, UINotificationOnDriverDistraction,
				UINotificationOnKeyboardInput, UINotificationOnLanguageChange, UINotificationOnRecordStart, UINotificationOnResetTimeout,
				UINotificationOnSystemContext, UINotificationOnTouchEvent};

			string[] VehicleInfoArray = {VIResponseDiagnosticMessage, VIResponseGetDTCs,
				VIResponseGetVehicleData, VIResponseGetVehicleType, VIResponseIsReady, VIResponseReadDID,
				VIResponseSubscribeVehicleData, VIResponseUnsubscribeVehicleData, VINotificationOnVehicleData};

			string[] VRArray = {VRResponseAddCommand, VRResponseChangeRegistration, VRResponseDeleteCommand, 
                VRResponseGetCapabilities, VRResponseGetLanguage, VRResponseGetSupportedLanguages, VRResponseIsReady, 
                VRResponsePerformInteraction, VRNotificationOnCommand, VRNotificationOnLanguageChange, 
                VRNotificationStarted, VRNotificationStopped};

            String[] allRpcListArray = { BCResponseActivateApp, BCResponseAllowDeviceToConnect, BCResponseDecryptCertificate, BCResponseDialNumber,
				BCResponseGetSystemInfo, BCResponseMixingAudioSupported, BCResponsePolicyUpdate, BCResponseSystemRequest,
				BCResponseUpdateAppList, BCResponseUpdateDeviceList, BCNotificationOnAppActivated, BCNotificationOnAppDeactivated,
				BCNotificationOnAwakeSDL, BCNotificationOnDeactivateHMI, BCNotificationOnDeviceChosen,
				BCNotificationOnEmergencyEvent, BCNotificationOnEventChanged, BCNotificationOnExitAllApplications,
				BCNotificationOnExitApplication, BCNotificationOnFindApplications, BCNotificationOnIgnitionCycleOver,
				BCNotificationOnPhoneCall, BCNotificationOnReady, BCNotificationOnStartDeviceDiscovery,
				BCNotificationOnSystemInfoChanged, BCNotificationOnSystemRequest, BCNotificationOnUpdateDeviceList,
                ButtonsResponseGetCapabilities, ButtonsResponseButtonPress,ButtonsNotificationOnButtonEvent, ButtonsNotificationOnButtonPress,
				NavigationResponseAlertManeuver, NavigationResponseGetWayPoints, NavigationResponseIsReady,
				NavigationResponseSendLocation, NavigationResponseShowConstantTBT, NavigationResponseStartAudioStream,
				NavigationResponseStartStream, NavigationResponseStopAudioStream, NavigationResponseStopStream,
				NavigationResponseSubscribeWayPoints, NavigationResponseUnsubscribeWayPoints, NavigationResponseUpdateTurnList,
                NavigationNotificationOnTBTClientState, RCResponseGetCapabilities, RCResponseGetInteriorVehicleData, 
                RCResponseGetInteriorVehicleDataConsent, RCResponseIsReady, RCResponseSetInteriorVehicleData, 
                RCNotificationOnInteriorVehicleData, RCNotificationOnRemoteControlSettings, SDLRequestActivateApp, SDLRequestGetListOfPermissions,
				SDLRequestGetStatusUpdate, SDLRequestGetURLS, SDLRequestGetUserFriendlyMessage, SDLRequestUpdateSDL,
				SDLNotificationOnAllowSDLFunctionality, SDLNotificationOnAppPermissionConsent, SDLNotificationOnPolicyUpdate,
				SDLNotificationOnReceivedPolicyUpdate, TTSResponseChangeRegistration, TTSResponseGetCapabilities,
				TTSResponseGetLanguage, TTSResponseGetSupportedLanguages, TTSResponseIsReady, TTSResponseSetGlobalProperties,
				TTSResponseSpeak, TTSResponseStopSpeaking, TTSNotificationOnLanguageChange, TTSNotificationOnResetTimeout,
				TTSNotificationStarted, TTSNotificationStopped, UIResponseAddCommand, UIResponseAddSubMenu, UIResponseAlert,
				UIResponseChangeRegistration, UIResponseClosePopUp, UIResponseDeleteCommand, UIResponseDeleteSubMenu,
				UIResponseEndAudioPassThru, UIResponseGetCapabilities, UIResponseGetLanguage, UIResponseGetSupportedLanguages,
				UIResponseIsReady, UIResponsePerformAudioPassThru, UIResponsePerformInteraction, UIResponseScrollableMessage,
				UIResponseSetAppIcon, UIResponseSetDisplayLayout, UIResponseSetGlobalProperties, UIResponseSetMediaClockTimer,
				UIResponseShow, UIResponseShowCustomForm, UIResponseSlider, UINotificationOnCommand, UINotificationOnDriverDistraction,
				UINotificationOnKeyboardInput, UINotificationOnLanguageChange, UINotificationOnRecordStart, UINotificationOnResetTimeout,
				UINotificationOnSystemContext, UINotificationOnTouchEvent, VIResponseDiagnosticMessage, VIResponseGetDTCs,
				VIResponseGetVehicleData, VIResponseGetVehicleType, VIResponseIsReady, VIResponseReadDID,
				VIResponseSubscribeVehicleData, VIResponseUnsubscribeVehicleData, VINotificationOnVehicleData, VRResponseAddCommand,
				VRResponseChangeRegistration, VRResponseDeleteCommand, VRResponseGetCapabilities,
				VRResponseGetLanguage, VRResponseGetSupportedLanguages, VRResponseIsReady, VRResponsePerformInteraction,
				VRNotificationOnCommand, VRNotificationOnLanguageChange, VRNotificationStarted, VRNotificationStopped};

			AlertDialog.Builder rpcListAlertDialog = new AlertDialog.Builder(Activity);
			View layout = (View)layoutInflater.Inflate(Resource.Layout.rpclistLayout, null);
			rpcListAlertDialog.SetView(layout);
			rpcListAlertDialog.SetTitle("Pick an RPC");

			rpcListAlertDialog.SetNegativeButton("Done", (senderAlert, args) =>
			{
				rpcListAlertDialog.Dispose();
			});

            ArrayAdapter<String> selectionSpinnerAdapter = new ArrayAdapter<string>(Activity, Android.Resource.Layout.SimpleListItem1, categoryListArray);
            Spinner rpcSelectionSpinner = layout.FindViewById<Spinner>(Resource.Id.rpc_list_selection_spinner);
            rpcSelectionSpinner.Adapter = selectionSpinnerAdapter;

            String[] selectedRPC = allRpcListArray;

			ListView rpcListView = (ListView)layout.FindViewById(Resource.Id.rpc_list_view);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleListItem1, selectedRPC);
			rpcListView.Adapter = adapter;

			rpcSelectionSpinner.ItemSelected += (s,e) =>
			{
				switch (e.Position)
				{
					case 0:
						selectedRPC = allRpcListArray;
						break;
					case 1:
						selectedRPC = BasicCommunicationArray;
						break;
					case 2:
						selectedRPC = ButtonsArray;
						break;
					case 3:
						selectedRPC = NavigationArray;
						break;
					case 4:
                        selectedRPC = RCArray;
                        break;
					case 5:
						selectedRPC = SDLArray;
						break;
					case 6:
						selectedRPC = TTSArray;
						break;
					case 7:
						selectedRPC = UIArray;
						break;
					case 8:
						selectedRPC = VehicleInfoArray;
						break;
					case 9:
						selectedRPC = VRArray;
						break;
				}
                Activity.RunOnUiThread(() => 
                {
					adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleListItem1, selectedRPC);
					rpcListView.Adapter = adapter;
					adapter.NotifyDataSetChanged();
                });
			};

			rpcListView.ItemClick += (object sender, Android.Widget.AdapterView.ItemClickEventArgs e) =>
			 {
				 string clickedItem = rpcListView.GetItemAtPosition(e.Position).ToString();


				 if (clickedItem.Equals(BCResponseActivateApp))
				 {
					 CreateBCResponseActivateApp();
				 }
				 if (clickedItem.Equals(BCResponseAllowDeviceToConnect))
				 {
					 CreateBCResponseAllowDeviceToConnect();
				 }
				 if (clickedItem.Equals(BCResponseDecryptCertificate))
				 {
					 CreateBCResponseDecryptCertificate();
				 }
				 else if (clickedItem.Equals(BCResponseDialNumber))
				 {
					 CreateBCResponseDialNumber();
				 }
				 else if (clickedItem.Equals(BCResponseGetSystemInfo))
				 {
                    ((MainActivity)Activity).CreateBCResponseGetSystemInfo();
				 }
				 else if (clickedItem.Equals(BCResponseMixingAudioSupported))
				 {
                    ((MainActivity) Activity).CreateBCResponseMixingAudioSupported();
				 }
				 else if (clickedItem.Equals(BCResponsePolicyUpdate))
				 {
					 CreateBCResponsePolicyUpdate();
				 }
				 else if (clickedItem.Equals(BCResponseSystemRequest))
				 {
					 CreateBCResponseSystemRequest();
				 }
				 else if (clickedItem.Equals(BCResponseUpdateAppList))
				 {
					 CreateBCResponseUpdateAppList();
				 }
				 else if (clickedItem.Equals(BCResponseUpdateDeviceList))
				 {
					 CreateBCResponseUpdateDeviceList();
				 }
				 else if (clickedItem.Equals(BCNotificationOnAppActivated))
				 {
					 CreateBCNotificationOnAppActivated();
				 }
				 else if (clickedItem.Equals(BCNotificationOnAppDeactivated))
				 {
					 CreateBCNotificationOnAppDeactivated();
				 }
				 else if (clickedItem.Equals(BCNotificationOnAwakeSDL))
				 {
					 CreateBCNotificationOnAwakeSDL();
				 }
				 else if (clickedItem.Equals(BCNotificationOnDeactivateHMI))
				 {
					 CreateBCNotificationOnDeactivateHMI();
				 }
				 else if (clickedItem.Equals(BCNotificationOnDeviceChosen))
				 {
					 CreateBCNotificationOnDeviceChosen();
				 }
				 else if (clickedItem.Equals(BCNotificationOnEmergencyEvent))
				 {
					 CreateBCNotificationOnEmergencyEvent();
				 }
				 else if (clickedItem.Equals(BCNotificationOnEventChanged))
				 {
					 CreateBCNotificationOnEventChanged();
				 }
				 else if (clickedItem.Equals(BCNotificationOnExitAllApplications))
				 {
					 CreateBCNotificationOnExitAllApplications();
				 }
				 else if (clickedItem.Equals(BCNotificationOnExitApplication))
				 {
					 CreateBCNotificationOnExitApplication();
				 }
				 else if (clickedItem.Equals(BCNotificationOnFindApplications))
				 {
					 CreateBCNotificationOnFindApplications();
				 }
				 else if (clickedItem.Equals(BCNotificationOnIgnitionCycleOver))
				 {
					 CreateBCNotificationOnIgnitionCycleOver();
				 }
				 else if (clickedItem.Equals(BCNotificationOnPhoneCall))
				 {
					 CreateBCNotificationOnPhoneCall();
				 }
				 else if (clickedItem.Equals(BCNotificationOnReady))
				 {
					 CreateBCNotificationOnReady();
				 }
				 else if (clickedItem.Equals(BCNotificationOnStartDeviceDiscovery))
				 {
					 CreateBCNotificationOnStartDeviceDiscovery();
				 }
				 else if (clickedItem.Equals(BCNotificationOnSystemInfoChanged))
				 {
					 CreateBCNotificationOnSystemInfoChanged();
				 }
				 else if (clickedItem.Equals(BCNotificationOnSystemRequest))
				 {
					 CreateBCNotificationOnSystemRequest();
				 }
				 else if (clickedItem.Equals(BCNotificationOnUpdateDeviceList))
				 {
					 CreateBCNotificationOnUpdateDeviceList();
				 }
				 else if (clickedItem.Equals(ButtonsResponseGetCapabilities))
				 {
					 ((MainActivity)Activity).CreateButtonsGetCapabilities();
				 }
                else if (clickedItem.Equals(ButtonsResponseButtonPress))
				 {
                    CreateButtonsResponseButtonPress();
				 }
				 else if (clickedItem.Equals(ButtonsNotificationOnButtonEvent))
				 {
					 CreateButtonsNotificationOnButtonEvent();
				 }
				 else if (clickedItem.Equals(ButtonsNotificationOnButtonPress))
				 {
					 CreateButtonsNotificationOnButtonPress();
				 }
				 else if (clickedItem.Equals(NavigationResponseAlertManeuver))
				 {
					 CreateNavigationResponseAlertManeuver();
				 }
				 else if (clickedItem.Equals(NavigationResponseGetWayPoints))
				 {
					 CreateNavigationResponseGetWayPoints();
				 }
				 else if (clickedItem.Equals(NavigationResponseIsReady))
				 {
					 ((MainActivity)Activity).CreateNavigationResponseIsReady();
				 }
				 else if (clickedItem.Equals(NavigationResponseSendLocation))
				 {
					 CreateNavigationResponseSendLocation();
				 }
				 else if (clickedItem.Equals(NavigationResponseShowConstantTBT))
				 {
					 CreateNavigationResponseShowConstantTBT();
				 }
				 else if (clickedItem.Equals(NavigationResponseStartAudioStream))
				 {
					 CreateNavigationResponseStartAudioStream();
				 }
				 else if (clickedItem.Equals(NavigationResponseStartStream))
				 {
					 CreateNavigationResponseStartStream();
				 }
				 else if (clickedItem.Equals(NavigationResponseStopAudioStream))
				 {
					 CreateNavigationResponseStopAudioStream();
				 }
				 else if (clickedItem.Equals(NavigationResponseStopStream))
				 {
					 CreateNavigationResponseStopStream();
				 }
				 else if (clickedItem.Equals(NavigationResponseSubscribeWayPoints))
				 {
					 CreateNavigationResponseSubscribeWayPoints();
				 }
				 else if (clickedItem.Equals(NavigationResponseUnsubscribeWayPoints))
				 {
					 CreateNavigationResponseUnsubscribeWayPoints();
				 }
				 else if (clickedItem.Equals(NavigationResponseUpdateTurnList))
				 {
					 CreateNavigationResponseUpdateTurnList();
				 }
				 else if (clickedItem.Equals(NavigationNotificationOnTBTClientState))
				 {
					 CreateNavigationNotificationOnTBTClientState();
				 }
				 else if (clickedItem.Equals(RCResponseGetCapabilities))
				 {
					 ((MainActivity)Activity).CreateRCResponseGetCapabilities();
				 }
                else if (clickedItem.Equals(RCResponseGetInteriorVehicleData))
				 {
					 CreateRCResponseGetInteriorVehicleData();
				 }
                else if (clickedItem.Equals(RCResponseGetInteriorVehicleDataConsent))
				 {
					 CreateRCResponseGetInteriorVehicleDataConsent();
				 }
				 else if (clickedItem.Equals(RCResponseIsReady))
				 {
					 ((MainActivity)Activity).CreateRCResponseIsReady();
				 }
                else if (clickedItem.Equals(RCResponseSetInteriorVehicleData))
				 {
					 CreateRCResponseSetInteriorVehicleData();
				 }
                else if (clickedItem.Equals(RCNotificationOnInteriorVehicleData))
				 {
					 CreateRCNotificationOnInteriorVehicleData();
				 }
                else if (clickedItem.Equals(RCNotificationOnRemoteControlSettings))
				 {
					 CreateRCNotificationOnRemoteControlSettings();
				 }
				 else if (clickedItem.Equals(SDLRequestActivateApp))
				 {
					 CreateSDLRequestActivateApp();
				 }
				 else if (clickedItem.Equals(SDLRequestGetListOfPermissions))
				 {
					 CreateSDLRequestGetListOfPermissions();
				 }
				 else if (clickedItem.Equals(SDLRequestGetStatusUpdate))
				 {
					 CreateSDLRequestGetStatusUpdate();
				 }
				 else if (clickedItem.Equals(SDLRequestGetURLS))
				 {
					 CreateSDLRequestGetURLS();
				 }
				 else if (clickedItem.Equals(SDLRequestGetUserFriendlyMessage))
				 {
					 CreateSDLRequestGetUserFriendlyMessage();
				 }
				 else if (clickedItem.Equals(SDLRequestUpdateSDL))
				 {
					 CreateSDLRequestUpdateSDL();
				 }
				 else if (clickedItem.Equals(SDLNotificationOnAllowSDLFunctionality))
				 {
					 CreateSDLNotificationOnAllowSDLFunctionality();
				 }
				 else if (clickedItem.Equals(SDLNotificationOnAppPermissionConsent))
				 {
					 CreateSDLNotificationOnAppPermissionConsent();
				 }
				 else if (clickedItem.Equals(SDLNotificationOnPolicyUpdate))
				 {
					 CreateSDLNotificationOnPolicyUpdate();
				 }
				 else if (clickedItem.Equals(SDLNotificationOnReceivedPolicyUpdate))
				 {
					 CreateSDLNotificationOnReceivedPolicyUpdate();
				 }
				 else if (clickedItem.Equals(TTSResponseChangeRegistration))
				 {
					 CreateTTSResponseChangeRegistration();
				 }
				 else if (clickedItem.Equals(TTSResponseGetCapabilities))
				 {
					 ((MainActivity)Activity).CreateTTSResponseGetCapabilities();
				 }
				 else if (clickedItem.Equals(TTSResponseGetLanguage))
				 {
					 ((MainActivity)Activity).CreateTTSResponseGetLanguage();
				 }
				 else if (clickedItem.Equals(TTSResponseGetSupportedLanguages))
				 {
					 ((MainActivity)Activity).CreateTTSResponseGetSupportedLanguages();
				 }
				 else if (clickedItem.Equals(TTSResponseIsReady))
				 {
					 ((MainActivity)Activity).CreateTTSResponseIsReady();
				 }
				 else if (clickedItem.Equals(TTSResponseSetGlobalProperties))
				 {
					 CreateTTSResponseSetGlobalProperties();
				 }
				 else if (clickedItem.Equals(TTSResponseSpeak))
				 {
					 CreateTTSResponseSpeak();
				 }
				 else if (clickedItem.Equals(TTSResponseStopSpeaking))
				 {
					 CreateTTSResponseStopSpeaking();
				 }
				 else if (clickedItem.Equals(TTSNotificationOnLanguageChange))
				 {
					 CreateTTSNotificationOnLanguageChange();
				 }
				 else if (clickedItem.Equals(TTSNotificationOnResetTimeout))
				 {
					 CreateTTSNotificationOnResetTimeout();
				 }
				 else if (clickedItem.Equals(TTSNotificationStarted))
				 {
					 CreateTTSNotificationStarted();
				 }
				 else if (clickedItem.Equals(TTSNotificationStopped))
				 {
					 CreateTTSNotificationStopped();
				 }
				 else if (clickedItem.Equals(UIResponseAddCommand))
				 {
					 CreateUIResponseAddCommand();
				 }
				 else if (clickedItem.Equals(UIResponseAddSubMenu))
				 {
					 CreateUIResponseAddSubMenu();
				 }
				 else if (clickedItem.Equals(UIResponseAlert))
				 {
					 CreateUIResponseAlert();
				 }
				 else if (clickedItem.Equals(UIResponseChangeRegistration))
				 {
					 CreateUIResponseChangeRegistration();
				 }
				 else if (clickedItem.Equals(UIResponseClosePopUp))
				 {
					 CreateUIResponseClosePopUp();
				 }
				 else if (clickedItem.Equals(UIResponseDeleteCommand))
				 {
					 CreateUIResponseDeleteCommand();
				 }
				 else if (clickedItem.Equals(UIResponseDeleteSubMenu))
				 {
					 CreateUIResponseDeleteSubMenu();
				 }
				 else if (clickedItem.Equals(UIResponseEndAudioPassThru))
				 {
					 CreateUIResponseEndAudioPassThru();
				 }
				 else if (clickedItem.Equals(UIResponseGetCapabilities))
				 {
					 ((MainActivity)Activity).CreateUIResponseGetCapabilities();
				 }
				 else if (clickedItem.Equals(UIResponseGetLanguage))
				 {
					 ((MainActivity)Activity).CreateUIResponseGetLanguage();
				 }
				 else if (clickedItem.Equals(UIResponseGetSupportedLanguages))
				 {
					 ((MainActivity)Activity).CreateUIResponseGetSupportedLanguages();
				 }
				 else if (clickedItem.Equals(UIResponseIsReady))
				 {
					 ((MainActivity)Activity).CreateUIResponseIsReady();
				 }
				 else if (clickedItem.Equals(UIResponsePerformAudioPassThru))
				 {
					 CreateUIResponsePerformAudioPassThru();
				 }
				 else if (clickedItem.Equals(UIResponsePerformInteraction))
				 {
					 CreateUIResponsePerformInteraction();
				 }
				 else if (clickedItem.Equals(UIResponseScrollableMessage))
				 {
					 CreateUIResponseScrollableMessage();
				 }
				 else if (clickedItem.Equals(UIResponseSetAppIcon))
				 {
					 CreateUIResponseSetAppIcon();
				 }
				 else if (clickedItem.Equals(UIResponseSetDisplayLayout))
				 {
					 CreateUIResponseSetDisplayLayout();
				 }
				 else if (clickedItem.Equals(UIResponseSetGlobalProperties))
				 {
					 CreateUIResponseSetGlobalProperties();
				 }
				 else if (clickedItem.Equals(UIResponseSetMediaClockTimer))
				 {
					 CreateUIResponseSetMediaClockTimer();
				 }
				 else if (clickedItem.Equals(UIResponseShow))
				 {
					 CreateUIResponseShow();
				 }
				 else if (clickedItem.Equals(UIResponseShowCustomForm))
				 {
					 CreateUIResponseShowCustomForm();
				 }
				 else if (clickedItem.Equals(UIResponseSlider))
				 {
					 CreateUIResponseSlider();
				 }
				 else if (clickedItem.Equals(UINotificationOnCommand))
				 {
					 CreateUINotificationOnCommand();
				 }
				 else if (clickedItem.Equals(UINotificationOnDriverDistraction))
				 {
					 CreateUINotificationOnDriverDistraction();
				 }
				 else if (clickedItem.Equals(UINotificationOnKeyboardInput))
				 {
					 CreateUINotificationOnKeyboardInput();
				 }
				 else if (clickedItem.Equals(UINotificationOnLanguageChange))
				 {
					 CreateUINotificationOnLanguageChange();
				 }
				 else if (clickedItem.Equals(UINotificationOnRecordStart))
				 {
					 CreateUINotificationOnRecordStart();
				 }
				 else if (clickedItem.Equals(UINotificationOnResetTimeout))
				 {
					 CreateUINotificationOnResetTimeout();
				 }
				 else if (clickedItem.Equals(UINotificationOnSystemContext))
				 {
					 CreateUINotificationOnSystemContext();
				 }
				 else if (clickedItem.Equals(UINotificationOnTouchEvent))
				 {
					 CreateUINotificationOnTouchEvent();
				 }
				 else if (clickedItem.Equals(VIResponseDiagnosticMessage))
				 {
					 CreateVIResponseDiagnosticMessage();
				 }
				 else if (clickedItem.Equals(VIResponseGetDTCs))
				 {
					 CreateVIResponseGetDTCs();
				 }
				 else if (clickedItem.Equals(VIResponseGetVehicleData))
				 {
					 ((MainActivity)Activity).CreateVIResponseGetVehicleData();
				 }
				 else if (clickedItem.Equals(VIResponseGetVehicleType))
				 {
					 ((MainActivity)Activity).CreateVIResponseGetVehicleType();
				 }
				 else if (clickedItem.Equals(VIResponseIsReady))
				 {
					 ((MainActivity)Activity).CreateVIResponseIsReady();
				 }
				 else if (clickedItem.Equals(VIResponseReadDID))
				 {
					 CreateVIResponseReadDID();
				 }
				 else if (clickedItem.Equals(VIResponseSubscribeVehicleData))
				 {
					 CreateVIResponseSubscribeVehicleData();
				 }
				 else if (clickedItem.Equals(VIResponseUnsubscribeVehicleData))
				 {
					 CreateVIResponseUnSubscribeVehicleData();
				 }
                 else if (clickedItem.Equals(VINotificationOnVehicleData))
                {
                     CreateVINotificationOnVehicleData();
                }
				 else if (clickedItem.Equals(VRResponseAddCommand))
				 {
					 CreateVRResponseAddCommand();
				 }
				 else if (clickedItem.Equals(VRResponseChangeRegistration))
				 {
					 CreateVRResponseChangeRegistration();
				 }
				 else if (clickedItem.Equals(VRResponseDeleteCommand))
				 {
					 CreateVRResponseDeleteCommand();
				 }
				 else if (clickedItem.Equals(VRResponseGetCapabilities))
				 {
					 ((MainActivity)Activity).CreateVRResponseGetCapabilities();
				 }
				 else if (clickedItem.Equals(VRResponseGetLanguage))
				 {
					 ((MainActivity)Activity).CreateVRResponseGetLanguage();
				 }
				 else if (clickedItem.Equals(VRResponseGetSupportedLanguages))
				 {
					 ((MainActivity)Activity).CreateVRResponseGetSupportedLanguages();
				 }
				 else if (clickedItem.Equals(VRResponseIsReady))
				 {
					 ((MainActivity)Activity).CreateVRResponseIsReady();
				 }
				 else if (clickedItem.Equals(VRResponsePerformInteraction))
				 {
					 CreateVRResponsePerformInteraction();
				 }
				 else if (clickedItem.Equals(VRNotificationOnCommand))
				 {
					 CreateVRNotificationOnCommand();
				 }
				 else if (clickedItem.Equals(VRNotificationOnLanguageChange))
				 {
					 CreateVRNotificationOnLanguageChange();
				 }
				 else if (clickedItem.Equals(VRNotificationStarted))
				 {
					 CreateVRNotificationStarted();
				 }
				 else if (clickedItem.Equals(VRNotificationStopped))
				 {
					 CreateVRNotificationStopped();
				 }
			 };

			rpcListAlertDialog.Show();
		}


		private void CreateRCNotificationOnRemoteControlSettings()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(RCNotificationOnRemoteControlSettings);

			CheckBox allowedCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.allow);

			CheckBox rcAccessModeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
			Spinner rcAccessModeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

			rcAccessModeCheckBox.CheckedChange += (s, e) => rcAccessModeSpinner.Enabled = e.IsChecked;

			var adapter1 = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCAccessModeArray);
			rcAccessModeSpinner.Adapter = adapter1;

			HmiApiLib.Controllers.RC.OutGoingNotifications.OnRemoteControlSettings tmpObj = new HmiApiLib.Controllers.RC.OutGoingNotifications.OnRemoteControlSettings();
			tmpObj = (HmiApiLib.Controllers.RC.OutGoingNotifications.OnRemoteControlSettings)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutGoingNotifications.OnRemoteControlSettings>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				allowedCheckBox.Checked = (bool)tmpObj.getAllowed();

				if (tmpObj.getAccessMode() != null)
					rcAccessModeSpinner.SetSelection((int)tmpObj.getAccessMode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			allowedCheckBox.Text = "Allowed";

			rcAccessModeCheckBox.Text = "RCAccessMode";

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				RCAccessMode? rcAccessMode = null;
				if (rcAccessModeCheckBox.Checked)
					rcAccessMode = (RCAccessMode)rcAccessModeSpinner.SelectedItemPosition;

				RequestNotifyMessage rpcResponse = BuildRpc.buildRcOnRemoteControlSettings(allowedCheckBox.Checked, rcAccessMode);
				AppUtils.savePreferenceValueForRpc(adapter1.Context, rpcResponse.getMethod(), rpcResponse);
				AppInstanceManager.Instance.sendRpc(rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter1.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}


		private void CreateRCNotificationOnInteriorVehicleData()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_interior_vehicle_data, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(RCNotificationOnInteriorVehicleData);

			CheckBox moduleDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_data_cb);

			CheckBox moduleTypeCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_type_cb);
			Spinner moduleTypeSpn = (Spinner)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_type_spn);

			CheckBox addRadioControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_radio_control_data_chk);
			Button addRadioControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_radio_control_data_btn);

			CheckBox addClimateControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_climate_control_data_chk);
			Button addClimateControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_climate_control_data_btn);

			moduleDataCb.CheckedChange += (s, e) =>
			{
				moduleTypeCb.Enabled = e.IsChecked;
				addRadioControlDataCb.Enabled = e.IsChecked;
				addClimateControlDataCb.Enabled = e.IsChecked;

				moduleTypeCb.Checked = e.IsChecked;
				addRadioControlDataCb.Checked = e.IsChecked;
				addClimateControlDataCb.Checked = e.IsChecked;
			};

			moduleTypeCb.CheckedChange += (s, e) => moduleTypeSpn.Enabled = e.IsChecked;
			addRadioControlDataCb.CheckedChange += (s, e) => addRadioControlDataButton.Enabled = e.IsChecked;
			addClimateControlDataCb.CheckedChange += (s, e) => addClimateControlDataButton.Enabled = e.IsChecked;

			moduleTypeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCModuleTypeArray);

			RadioControlData radioControlData = null;
			ClimateControlData climateControlData = null;

			HmiApiLib.Controllers.RC.OutGoingNotifications.OnInteriorVehicleData tmpObj = new HmiApiLib.Controllers.RC.OutGoingNotifications.OnInteriorVehicleData();
			tmpObj = (HmiApiLib.Controllers.RC.OutGoingNotifications.OnInteriorVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutGoingNotifications.OnInteriorVehicleData>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				if (tmpObj.getModuleData() != null)
				{
					moduleTypeSpn.SetSelection((int)tmpObj.getModuleData().getModuleType());
                    radioControlData = tmpObj.getModuleData().getRadioControlData();
                    climateControlData = tmpObj.getModuleData().getClimateControlData();
				}
			}

			if (radioControlData == null)
				radioControlData = new RadioControlData();

			if (climateControlData == null)
				climateControlData = new ClimateControlData();

			addRadioControlDataButton.Click += delegate
			{
				AlertDialog.Builder radioControlDataAlertDialogue = new AlertDialog.Builder(Activity);
				View radioControlDataView = layoutInflater.Inflate(Resource.Layout.radio_control_data, null);
				radioControlDataAlertDialogue.SetView(radioControlDataView);
				radioControlDataAlertDialogue.SetTitle("RadioControlData");

				CheckBox frequencyIntegerCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_integer_cb);
				EditText frequencyIntegerEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_integer_et);

				CheckBox frequencyFractionCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_fraction_cb);
				EditText frequencyFractionEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_fraction_et);

				CheckBox radioBandCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_band_cb);
				Spinner radioBandSpn = (Spinner)radioControlDataView.FindViewById(Resource.Id.radio_control_data_band_spn);


				CheckBox rdsDataCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_rds_data_cb);

				CheckBox rdsDataPsCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_ps_cb);
				EditText rdsDataPsEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_ps_et);

				CheckBox rdsDataRtCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_rt_cb);
				EditText rdsDataRtEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_rt_et);

				CheckBox rdsDataCtCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_ct_cb);
				EditText rdsDataCtEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_ct_et);

				CheckBox rdsDataPiCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_pi_cb);
				EditText rdsDataPiEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_pi_et);

				CheckBox rdsDataPtyCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_pty_cb);
				EditText rdsDataPtyEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_pty_et);

				CheckBox rdsDataTpCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_tp_cb);

				CheckBox rdsDataTaCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_ta_cb);

				CheckBox rdsDataRegCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_reg_cb);
				EditText rdsDataRegEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_reg_et);


				CheckBox availableHDsCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_available_hds_cb);
				EditText availableHDsEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_available_hds_et);

				CheckBox hdChannelCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_hd_channel_cb);
				EditText hdChannelEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_hd_channel_et);

				CheckBox signalStrengthCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_strength_cb);
				EditText signalStrengthEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_strength_et);

				CheckBox signalChangeThresholdCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_change_threshold_cb);
				EditText signalChangeThresholdEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_change_threshold_et);

				CheckBox radioEnableCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_radio_enable_cb);

				CheckBox radioStateCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_radio_state_cb);
				Spinner radioStateSpn = (Spinner)radioControlDataView.FindViewById(Resource.Id.radio_control_data_radio_state_spn);

				radioBandSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCRadioBandArray);
				radioStateSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCRadioStateArray);

				frequencyIntegerEt.Text = radioControlData.getFrequencyInteger().ToString();
				frequencyFractionEt.Text = radioControlData.getFrequencyFraction().ToString();
				radioBandSpn.SetSelection((int)radioControlData.getBand());

				if (radioControlData.getRdsData() != null)
				{
					rdsDataPsEt.Text = radioControlData.getRdsData().PS;
					rdsDataRtEt.Text = radioControlData.getRdsData().RT;
					rdsDataCtEt.Text = radioControlData.getRdsData().CT;
					rdsDataPiEt.Text = radioControlData.getRdsData().PI;
					rdsDataPtyEt.Text = radioControlData.getRdsData().PTY.ToString();

					rdsDataTpCb.Checked = radioControlData.getRdsData().TP;
					rdsDataTaCb.Checked = radioControlData.getRdsData().TA;
					rdsDataRegEt.Text = radioControlData.getRdsData().REG;
				}

				availableHDsEt.Text = radioControlData.getAvailableHDs().ToString();
				hdChannelEt.Text = radioControlData.getHdChannel().ToString();
				signalStrengthEt.Text = radioControlData.getSignalStrength().ToString();
				signalChangeThresholdEt.Text = radioControlData.getSignalChangeThreshold().ToString();
				radioEnableCb.Checked = radioControlData.getRadioEnable();
				radioStateSpn.SetSelection((int)radioControlData.getState());

				frequencyIntegerCb.CheckedChange += (s, e) => frequencyIntegerEt.Enabled = e.IsChecked;
				frequencyFractionCb.CheckedChange += (s, e) => frequencyFractionEt.Enabled = e.IsChecked;
				radioBandCb.CheckedChange += (s, e) => radioBandSpn.Enabled = e.IsChecked;

				rdsDataCb.CheckedChange += (s, e) =>
				{
					rdsDataPsCb.Enabled = e.IsChecked;
					rdsDataRtCb.Enabled = e.IsChecked;
					rdsDataCtCb.Enabled = e.IsChecked;
					rdsDataPiCb.Enabled = e.IsChecked;
					rdsDataPtyCb.Enabled = e.IsChecked;
					rdsDataTpCb.Enabled = e.IsChecked;
					rdsDataTaCb.Enabled = e.IsChecked;
					rdsDataRegCb.Enabled = e.IsChecked;

					rdsDataPsEt.Enabled = e.IsChecked;
					rdsDataRtEt.Enabled = e.IsChecked;
					rdsDataCtEt.Enabled = e.IsChecked;
					rdsDataPiEt.Enabled = e.IsChecked;
					rdsDataPtyEt.Enabled = e.IsChecked;
					rdsDataRegEt.Enabled = e.IsChecked;
				};
				rdsDataPsCb.CheckedChange += (s, e) => rdsDataPsEt.Enabled = e.IsChecked;
				rdsDataRtCb.CheckedChange += (s, e) => rdsDataRtEt.Enabled = e.IsChecked;
				rdsDataCtCb.CheckedChange += (s, e) => rdsDataCtEt.Enabled = e.IsChecked;
				rdsDataPiCb.CheckedChange += (s, e) => rdsDataPiEt.Enabled = e.IsChecked;
				rdsDataPtyCb.CheckedChange += (s, e) => rdsDataPtyEt.Enabled = e.IsChecked;
				rdsDataRegCb.CheckedChange += (s, e) => rdsDataRegEt.Enabled = e.IsChecked;

				availableHDsCb.CheckedChange += (s, e) => availableHDsEt.Enabled = e.IsChecked;
				hdChannelCb.CheckedChange += (s, e) => hdChannelEt.Enabled = e.IsChecked;
				signalStrengthCb.CheckedChange += (s, e) => signalStrengthEt.Enabled = e.IsChecked;
				signalChangeThresholdCb.CheckedChange += (s, e) => signalChangeThresholdEt.Enabled = e.IsChecked;
				radioStateCb.CheckedChange += (s, e) => radioStateSpn.Enabled = e.IsChecked;


				radioControlDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
				{
					radioControlDataAlertDialogue.Dispose();
				});

				radioControlDataAlertDialogue.SetPositiveButton("ADD", (senderAlert, evn) =>
				{
					if (frequencyIntegerCb.Checked && frequencyIntegerEt.Text != null && frequencyIntegerEt.Text.Length > 0)
						radioControlData.frequencyInteger = Java.Lang.Integer.ParseInt(frequencyIntegerEt.Text);
					else
						radioControlData.frequencyInteger = 0;

					if (frequencyFractionCb.Checked && frequencyFractionEt.Text != null && frequencyFractionEt.Text.Length > 0)
						radioControlData.frequencyFraction = Java.Lang.Integer.ParseInt(frequencyFractionEt.Text);
					else
						radioControlData.frequencyFraction = 0;

					if (radioBandCb.Checked)
						radioControlData.band = (RadioBand)radioBandSpn.SelectedItemPosition;
					else
						radioControlData.band = RadioBand.AM;


					radioControlData.rdsData = null;
					if (rdsDataCb.Checked)
					{
						radioControlData.rdsData = new RdsData();

						if (rdsDataPsCb.Checked)
							radioControlData.rdsData.PS = rdsDataPsEt.Text;

						if (rdsDataRtCb.Checked)
							radioControlData.rdsData.RT = rdsDataRtEt.Text;

						if (rdsDataCtCb.Checked)
							radioControlData.rdsData.CT = rdsDataCtEt.Text;

						if (rdsDataPiCb.Checked)
							radioControlData.rdsData.PI = rdsDataPiEt.Text;

						if (rdsDataPtyCb.Checked && rdsDataPtyEt.Text != null && rdsDataPtyEt.Text.Length > 0)
							radioControlData.rdsData.PTY = Java.Lang.Integer.ParseInt(rdsDataPtyEt.Text);

						if (rdsDataRegCb.Checked)
							radioControlData.rdsData.REG = rdsDataRegEt.Text;

						radioControlData.rdsData.TP = rdsDataTpCb.Checked;
						radioControlData.rdsData.TA = rdsDataTaCb.Checked;
					}

					if (availableHDsCb.Checked && availableHDsEt.Text != null && availableHDsEt.Text.Length > 0)
						radioControlData.availableHDs = Java.Lang.Integer.ParseInt(availableHDsEt.Text);
					else
						radioControlData.availableHDs = 0;

					if (hdChannelCb.Checked && hdChannelEt.Text != null && hdChannelEt.Text.Length > 0)
						radioControlData.hdChannel = Java.Lang.Integer.ParseInt(hdChannelEt.Text);
					else
						radioControlData.hdChannel = 0;

					if (signalStrengthCb.Checked && signalStrengthEt.Text != null && signalStrengthEt.Text.Length > 0)
						radioControlData.signalStrength = Java.Lang.Integer.ParseInt(signalStrengthEt.Text);
					else
						radioControlData.signalStrength = 0;

					if (signalChangeThresholdCb.Checked && signalChangeThresholdEt.Text != null && signalChangeThresholdEt.Text.Length > 0)
						radioControlData.signalChangeThreshold = Java.Lang.Integer.ParseInt(signalChangeThresholdEt.Text);
					else
						radioControlData.signalChangeThreshold = 0;

					radioControlData.radioEnable = radioEnableCb.Checked;

					if (radioStateCb.Checked)
						radioControlData.state = (RadioState)radioStateSpn.SelectedItemPosition;
					else
						radioControlData.state = RadioState.ACQUIRING;
				});

				radioControlDataAlertDialogue.Show();
			};


			addClimateControlDataButton.Click += delegate
			{
				AlertDialog.Builder climateControlDataAlertDialogue = new AlertDialog.Builder(Activity);
				View climateControlDataView = layoutInflater.Inflate(Resource.Layout.climate_control_data, null);
				climateControlDataAlertDialogue.SetView(climateControlDataView);
				climateControlDataAlertDialogue.SetTitle("ClimateControlData");

				CheckBox fanSpeedCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_fan_speed_cb);
				EditText fanSpeedEt = (EditText)climateControlDataView.FindViewById(Resource.Id.climate_control_data_fan_speed_et);


				CheckBox currentTempCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_current_temp_cb);

				CheckBox currentTempUnitCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.current_temp_temperature_unit_cb);
				Spinner currentTempUnitSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.current_temp_temperature_unit_spn);

				CheckBox currentTempValueCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.current_temp_value_cb);
				EditText currentTempValueEt = (EditText)climateControlDataView.FindViewById(Resource.Id.current_temp_value_et);


				CheckBox desiredTempCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_desired_temp_cb);

				CheckBox desiredTempUnitCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.desired_temp_temperature_unit_cb);
				Spinner desiredTempUnitSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.desired_temp_temperature_unit_spn);

				CheckBox desiredTempValueCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.desired_temp_value_cb);
				EditText desiredTempValueEt = (EditText)climateControlDataView.FindViewById(Resource.Id.desired_temp_value_et);


				CheckBox acEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ac_enable_cb);
				CheckBox circulatedAirEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_circulated_air_enable_cb);
				CheckBox autoModeEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_auto_mode_enable_cb);

				CheckBox defrostZoneEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_defrost_zone_cb);
				Spinner defrostZoneEnableSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.climate_control_data_defrost_zone_spn);

				CheckBox dualModeEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_dual_mode_enable_cb);
				CheckBox acMaxEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ac_max_enable_cb);

				CheckBox dataVentilationModeCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ventilation_mode_cb);
				Spinner dataVentilationModeSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ventilation_mode_spn);

				currentTempUnitSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCTempUnitArray);
				desiredTempUnitSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCTempUnitArray);
				defrostZoneEnableSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCDefrostZoneArray);
				dataVentilationModeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCVentilationModeArray);

				fanSpeedCb.CheckedChange += (s, e) => fanSpeedEt.Enabled = e.IsChecked;

				currentTempCb.CheckedChange += (s, e) =>
				{
					currentTempUnitCb.Enabled = e.IsChecked;
					currentTempValueCb.Enabled = e.IsChecked;
				};

				currentTempUnitCb.CheckedChange += (s, e) => currentTempUnitSpn.Enabled = e.IsChecked;
				currentTempValueCb.CheckedChange += (s, e) => currentTempValueEt.Enabled = e.IsChecked;

				desiredTempCb.CheckedChange += (s, e) =>
				{
					desiredTempUnitCb.Enabled = e.IsChecked;
					desiredTempValueCb.Enabled = e.IsChecked;
				};

				desiredTempUnitCb.CheckedChange += (s, e) => desiredTempUnitSpn.Enabled = e.IsChecked;
				desiredTempValueCb.CheckedChange += (s, e) => desiredTempValueEt.Enabled = e.IsChecked;


				defrostZoneEnableCb.CheckedChange += (s, e) => defrostZoneEnableSpn.Enabled = e.IsChecked;
				dataVentilationModeCb.CheckedChange += (s, e) => dataVentilationModeSpn.Enabled = e.IsChecked;

				fanSpeedEt.Text = climateControlData.getFanSpeed().ToString();

				if (climateControlData.getCurrentTemperature() != null)
				{
					currentTempUnitSpn.SetSelection((int)climateControlData.getCurrentTemperature().getUnit());
					currentTempValueEt.Text = climateControlData.getCurrentTemperature().getValue().ToString();
				}

				if (climateControlData.getDesiredTemperature() != null)
				{
					desiredTempUnitSpn.SetSelection((int)climateControlData.getDesiredTemperature().getUnit());
					desiredTempValueEt.Text = climateControlData.getDesiredTemperature().getValue().ToString();
				}
				acEnableCb.Checked = climateControlData.getAcEnable();
				circulatedAirEnableCb.Checked = climateControlData.getCirculateAirEnable();
				autoModeEnableCb.Checked = climateControlData.getAutoModeEnable();

				defrostZoneEnableSpn.SetSelection((int)climateControlData.getDefrostZone());
				dualModeEnableCb.Checked = climateControlData.getDualModeEnable();
				acMaxEnableCb.Checked = climateControlData.getAcMaxEnable();
				dataVentilationModeSpn.SetSelection((int)climateControlData.getVentilationMode());

				climateControlDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
				 {
					 climateControlDataAlertDialogue.Dispose();
				 });

				climateControlDataAlertDialogue.SetPositiveButton("ADD", (senderAlert, evn) =>
				{
					if (fanSpeedCb.Checked && fanSpeedEt.Text != null && fanSpeedEt.Text.Length > 0)
						climateControlData.fanSpeed = Java.Lang.Integer.ParseInt(fanSpeedEt.Text);

					climateControlData.currentTemperature = null;
					if (currentTempCb.Checked)
					{
                        climateControlData.currentTemperature = new Temperature();
						if (currentTempUnitCb.Checked)
							climateControlData.currentTemperature.unit = (TemperatureUnit)currentTempUnitSpn.SelectedItemPosition;

						if (currentTempValueCb.Checked && currentTempValueEt.Text != null && currentTempValueEt.Text.Length > 0)
							climateControlData.currentTemperature.value = Java.Lang.Integer.ParseInt(currentTempValueEt.Text);
					}

					climateControlData.desiredTemperature = null;
					if (desiredTempCb.Checked)
					{
                        climateControlData.desiredTemperature = new Temperature();
						if (desiredTempUnitCb.Checked)
							climateControlData.desiredTemperature.unit = (TemperatureUnit)desiredTempUnitSpn.SelectedItemPosition;

						if (desiredTempValueCb.Checked && desiredTempValueEt.Text != null && desiredTempValueEt.Text.Length > 0)
							climateControlData.desiredTemperature.value = Java.Lang.Integer.ParseInt(desiredTempValueEt.Text);
					}

					climateControlData.acEnable = acEnableCb.Checked;
					climateControlData.circulateAirEnable = circulatedAirEnableCb.Checked;
					climateControlData.autoModeEnable = autoModeEnableCb.Checked;
					climateControlData.dualModeEnable = dualModeEnableCb.Checked;
					climateControlData.acMaxEnable = acMaxEnableCb.Checked;


					if (defrostZoneEnableCb.Checked)
					{
						climateControlData.defrostZone = (DefrostZone)defrostZoneEnableSpn.SelectedItemPosition;
					}

					if (dataVentilationModeCb.Checked)
					{
						climateControlData.ventilationMode = (VentilationMode)dataVentilationModeSpn.SelectedItemPosition;
					}
				});

				climateControlDataAlertDialogue.Show();
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, evn) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, evn) =>
			{
                ModuleData moduleData = null;
				if (moduleDataCb.Checked)
				{
                    moduleData = new ModuleData();
					if (moduleTypeCb.Checked)
						moduleData.moduleType = (ModuleType)moduleTypeSpn.SelectedItemPosition;

					if (addRadioControlDataCb.Checked)
						moduleData.radioControlData = radioControlData;

					if (addClimateControlDataCb.Checked)
						moduleData.climateControlData = climateControlData;
				}

				RequestNotifyMessage rpcResponse = BuildRpc.buildRcOnInteriorVehicleData(moduleData);
				AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
				AppInstanceManager.Instance.sendRpc(rpcResponse);

			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, even) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}


		private void CreateRCResponseSetInteriorVehicleData()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.set_interior_vehicle_data, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(RCResponseSetInteriorVehicleData);

			CheckBox moduleDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_data_cb);

			CheckBox moduleTypeCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_type_cb);
			Spinner moduleTypeSpn = (Spinner)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_module_type_spn);

			CheckBox addRadioControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_radio_control_data_chk);
			Button addRadioControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_radio_control_data_btn);

			CheckBox addClimateControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_climate_control_data_chk);
			Button addClimateControlDataButton = (Button)rpcView.FindViewById(Resource.Id.on_interior_vehicle_data_add_climate_control_data_btn);

			CheckBox resultCodeCb = (CheckBox)rpcView.FindViewById(Resource.Id.set_interior_vehicle_data_result_code_cb);
			Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.set_interior_vehicle_data_result_code_spn);

			moduleDataCb.CheckedChange += (s, e) =>
			{
				moduleTypeCb.Enabled = e.IsChecked;
				addRadioControlDataCb.Enabled = e.IsChecked;
				addClimateControlDataCb.Enabled = e.IsChecked;

				moduleTypeCb.Checked = e.IsChecked;
				addRadioControlDataCb.Checked = e.IsChecked;
				addClimateControlDataCb.Checked = e.IsChecked;
			};

			moduleTypeCb.CheckedChange += (s, e) => moduleTypeSpn.Enabled = e.IsChecked;
			addRadioControlDataCb.CheckedChange += (s, e) => addRadioControlDataButton.Enabled = e.IsChecked;
			addClimateControlDataCb.CheckedChange += (s, e) => addClimateControlDataButton.Enabled = e.IsChecked;
			resultCodeCb.CheckedChange += (s, e) => resultCodeSpn.Enabled = e.IsChecked;

			moduleTypeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCModuleTypeArray);
			resultCodeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);

			RadioControlData radioControlData = null;
			ClimateControlData climateControlData = null;

			HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.SetInteriorVehicleData>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				if (tmpObj.getModuleData() != null)
				{
					moduleTypeSpn.SetSelection((int)tmpObj.getModuleData().getModuleType());
                    radioControlData = tmpObj.getModuleData().getRadioControlData();
                    climateControlData = tmpObj.getModuleData().getClimateControlData();
				}
				resultCodeSpn.SetSelection((int)tmpObj.getResultCode());
			}

			if (radioControlData == null)
				radioControlData = new RadioControlData();

			if (climateControlData == null)
				climateControlData = new ClimateControlData();

			addRadioControlDataButton.Click += delegate
			{
				AlertDialog.Builder radioControlDataAlertDialogue = new AlertDialog.Builder(Activity);
				View radioControlDataView = layoutInflater.Inflate(Resource.Layout.radio_control_data, null);
				radioControlDataAlertDialogue.SetView(radioControlDataView);
				radioControlDataAlertDialogue.SetTitle("RadioControlData");

				CheckBox frequencyIntegerCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_integer_cb);
				EditText frequencyIntegerEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_integer_et);

				CheckBox frequencyFractionCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_fraction_cb);
				EditText frequencyFractionEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_fraction_et);

				CheckBox radioBandCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_band_cb);
				Spinner radioBandSpn = (Spinner)radioControlDataView.FindViewById(Resource.Id.radio_control_data_band_spn);


				CheckBox rdsDataCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_rds_data_cb);

				CheckBox rdsDataPsCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_ps_cb);
				EditText rdsDataPsEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_ps_et);

				CheckBox rdsDataRtCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_rt_cb);
				EditText rdsDataRtEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_rt_et);

				CheckBox rdsDataCtCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_ct_cb);
				EditText rdsDataCtEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_ct_et);

				CheckBox rdsDataPiCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_pi_cb);
				EditText rdsDataPiEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_pi_et);

				CheckBox rdsDataPtyCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_pty_cb);
				EditText rdsDataPtyEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_pty_et);

				CheckBox rdsDataTpCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_tp_cb);

				CheckBox rdsDataTaCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_ta_cb);

				CheckBox rdsDataRegCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_reg_cb);
				EditText rdsDataRegEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_reg_et);


				CheckBox availableHDsCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_available_hds_cb);
				EditText availableHDsEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_available_hds_et);

				CheckBox hdChannelCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_hd_channel_cb);
				EditText hdChannelEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_hd_channel_et);

				CheckBox signalStrengthCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_strength_cb);
				EditText signalStrengthEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_strength_et);

				CheckBox signalChangeThresholdCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_change_threshold_cb);
				EditText signalChangeThresholdEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_change_threshold_et);

				CheckBox radioEnableCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_radio_enable_cb);

				CheckBox radioStateCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_radio_state_cb);
				Spinner radioStateSpn = (Spinner)radioControlDataView.FindViewById(Resource.Id.radio_control_data_radio_state_spn);

				radioBandSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCRadioBandArray);
				radioStateSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCRadioStateArray);

				frequencyIntegerEt.Text = radioControlData.getFrequencyInteger().ToString();
				frequencyFractionEt.Text = radioControlData.getFrequencyFraction().ToString();
				radioBandSpn.SetSelection((int)radioControlData.getBand());

				if (radioControlData.getRdsData() != null)
				{
					rdsDataPsEt.Text = radioControlData.getRdsData().PS;
					rdsDataRtEt.Text = radioControlData.getRdsData().RT;
					rdsDataCtEt.Text = radioControlData.getRdsData().CT;
					rdsDataPiEt.Text = radioControlData.getRdsData().PI;
					rdsDataPtyEt.Text = radioControlData.getRdsData().PTY.ToString();

					rdsDataTpCb.Checked = radioControlData.getRdsData().TP;
					rdsDataTaCb.Checked = radioControlData.getRdsData().TA;
					rdsDataRegEt.Text = radioControlData.getRdsData().REG;
				}

				availableHDsEt.Text = radioControlData.getAvailableHDs().ToString();
				hdChannelEt.Text = radioControlData.getHdChannel().ToString();
				signalStrengthEt.Text = radioControlData.getSignalStrength().ToString();
				signalChangeThresholdEt.Text = radioControlData.getSignalChangeThreshold().ToString();
				radioEnableCb.Checked = radioControlData.getRadioEnable();
				radioStateSpn.SetSelection((int)radioControlData.getState());

				frequencyIntegerCb.CheckedChange += (s, e) => frequencyIntegerEt.Enabled = e.IsChecked;
				frequencyFractionCb.CheckedChange += (s, e) => frequencyFractionEt.Enabled = e.IsChecked;
				radioBandCb.CheckedChange += (s, e) => radioBandSpn.Enabled = e.IsChecked;

				rdsDataCb.CheckedChange += (s, e) =>
				{
					rdsDataPsCb.Enabled = e.IsChecked;
					rdsDataRtCb.Enabled = e.IsChecked;
					rdsDataCtCb.Enabled = e.IsChecked;
					rdsDataPiCb.Enabled = e.IsChecked;
					rdsDataPtyCb.Enabled = e.IsChecked;
					rdsDataTpCb.Enabled = e.IsChecked;
					rdsDataTaCb.Enabled = e.IsChecked;
					rdsDataRegCb.Enabled = e.IsChecked;

					rdsDataPsEt.Enabled = e.IsChecked;
					rdsDataRtEt.Enabled = e.IsChecked;
					rdsDataCtEt.Enabled = e.IsChecked;
					rdsDataPiEt.Enabled = e.IsChecked;
					rdsDataPtyEt.Enabled = e.IsChecked;
					rdsDataRegEt.Enabled = e.IsChecked;
				};
				rdsDataPsCb.CheckedChange += (s, e) => rdsDataPsEt.Enabled = e.IsChecked;
				rdsDataRtCb.CheckedChange += (s, e) => rdsDataRtEt.Enabled = e.IsChecked;
				rdsDataCtCb.CheckedChange += (s, e) => rdsDataCtEt.Enabled = e.IsChecked;
				rdsDataPiCb.CheckedChange += (s, e) => rdsDataPiEt.Enabled = e.IsChecked;
				rdsDataPtyCb.CheckedChange += (s, e) => rdsDataPtyEt.Enabled = e.IsChecked;
				rdsDataRegCb.CheckedChange += (s, e) => rdsDataRegEt.Enabled = e.IsChecked;

				availableHDsCb.CheckedChange += (s, e) => availableHDsEt.Enabled = e.IsChecked;
				hdChannelCb.CheckedChange += (s, e) => hdChannelEt.Enabled = e.IsChecked;
				signalStrengthCb.CheckedChange += (s, e) => signalStrengthEt.Enabled = e.IsChecked;
				signalChangeThresholdCb.CheckedChange += (s, e) => signalChangeThresholdEt.Enabled = e.IsChecked;
				radioStateCb.CheckedChange += (s, e) => radioStateSpn.Enabled = e.IsChecked;


				radioControlDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
				{
					radioControlDataAlertDialogue.Dispose();
				});

				radioControlDataAlertDialogue.SetPositiveButton("ADD", (senderAlert, evn) =>
				{
                    if (frequencyIntegerCb.Checked && frequencyIntegerEt.Text != null && frequencyIntegerEt.Text.Length > 0)
                        radioControlData.frequencyInteger = Java.Lang.Integer.ParseInt(frequencyIntegerEt.Text);
                    else
                        radioControlData.frequencyInteger = 0;

					if (frequencyFractionCb.Checked && frequencyFractionEt.Text != null && frequencyFractionEt.Text.Length > 0)
						radioControlData.frequencyFraction = Java.Lang.Integer.ParseInt(frequencyFractionEt.Text);
                    else
						radioControlData.frequencyFraction = 0;

					if (radioBandCb.Checked)
						radioControlData.band = (RadioBand)radioBandSpn.SelectedItemPosition;
                    else
                        radioControlData.band = RadioBand.AM;


					radioControlData.rdsData = null;
					if (rdsDataCb.Checked)
					{
                        radioControlData.rdsData = new RdsData();

						if (rdsDataPsCb.Checked)
							radioControlData.rdsData.PS = rdsDataPsEt.Text;  

						if (rdsDataRtCb.Checked)
							radioControlData.rdsData.RT = rdsDataRtEt.Text;
                                
						if (rdsDataCtCb.Checked)
							radioControlData.rdsData.CT = rdsDataCtEt.Text;
                            
						if (rdsDataPiCb.Checked)
							radioControlData.rdsData.PI = rdsDataPiEt.Text;
                            
						if (rdsDataPtyCb.Checked && rdsDataPtyEt.Text != null && rdsDataPtyEt.Text.Length > 0)
							radioControlData.rdsData.PTY = Java.Lang.Integer.ParseInt(rdsDataPtyEt.Text);

						if (rdsDataRegCb.Checked)
							radioControlData.rdsData.REG = rdsDataRegEt.Text;
                            
						radioControlData.rdsData.TP = rdsDataTpCb.Checked;
                        radioControlData.rdsData.TA = rdsDataTaCb.Checked;
					}
                        

					if (availableHDsCb.Checked && availableHDsEt.Text != null && availableHDsEt.Text.Length > 0)
						radioControlData.availableHDs = Java.Lang.Integer.ParseInt(availableHDsEt.Text);
                    else
						radioControlData.availableHDs = 0;
                        
					if (hdChannelCb.Checked && hdChannelEt.Text != null && hdChannelEt.Text.Length > 0)
						radioControlData.hdChannel = Java.Lang.Integer.ParseInt(hdChannelEt.Text);
                    else
						radioControlData.hdChannel = 0;
                        
					if (signalStrengthCb.Checked && signalStrengthEt.Text != null && signalStrengthEt.Text.Length > 0)
						radioControlData.signalStrength = Java.Lang.Integer.ParseInt(signalStrengthEt.Text);
                    else
						radioControlData.signalStrength = 0;

					if (signalChangeThresholdCb.Checked && signalChangeThresholdEt.Text != null && signalChangeThresholdEt.Text.Length > 0)
					    radioControlData.signalChangeThreshold = Java.Lang.Integer.ParseInt(signalChangeThresholdEt.Text);
                    else
						radioControlData.signalChangeThreshold = 0;

						radioControlData.radioEnable = radioEnableCb.Checked;

					if (radioStateCb.Checked)
						radioControlData.state = (RadioState)radioStateSpn.SelectedItemPosition;
                    else
                        radioControlData.state = RadioState.ACQUIRING;
				});

				radioControlDataAlertDialogue.Show();
			};

			addClimateControlDataButton.Click += delegate
			{
				AlertDialog.Builder climateControlDataAlertDialogue = new AlertDialog.Builder(Activity);
				View climateControlDataView = layoutInflater.Inflate(Resource.Layout.climate_control_data, null);
				climateControlDataAlertDialogue.SetView(climateControlDataView);
				climateControlDataAlertDialogue.SetTitle("ClimateControlData");

				CheckBox fanSpeedCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_fan_speed_cb);
				EditText fanSpeedEt = (EditText)climateControlDataView.FindViewById(Resource.Id.climate_control_data_fan_speed_et);

				CheckBox currentTempCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_current_temp_cb);

				CheckBox currentTempUnitCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.current_temp_temperature_unit_cb);
				Spinner currentTempUnitSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.current_temp_temperature_unit_spn);

				CheckBox currentTempValueCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.current_temp_value_cb);
				EditText currentTempValueEt = (EditText)climateControlDataView.FindViewById(Resource.Id.current_temp_value_et);

				CheckBox desiredTempCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_desired_temp_cb);

				CheckBox desiredTempUnitCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.desired_temp_temperature_unit_cb);
				Spinner desiredTempUnitSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.desired_temp_temperature_unit_spn);

				CheckBox desiredTempValueCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.desired_temp_value_cb);
				EditText desiredTempValueEt = (EditText)climateControlDataView.FindViewById(Resource.Id.desired_temp_value_et);

				CheckBox acEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ac_enable_cb);
				CheckBox circulatedAirEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_circulated_air_enable_cb);
				CheckBox autoModeEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_auto_mode_enable_cb);

				CheckBox defrostZoneEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_defrost_zone_cb);
				Spinner defrostZoneEnableSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.climate_control_data_defrost_zone_spn);

				CheckBox dualModeEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_dual_mode_enable_cb);
				CheckBox acMaxEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ac_max_enable_cb);

				CheckBox dataVentilationModeCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ventilation_mode_cb);
				Spinner dataVentilationModeSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ventilation_mode_spn);

				currentTempUnitSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCTempUnitArray);
				desiredTempUnitSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCTempUnitArray);
				defrostZoneEnableSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCDefrostZoneArray);
				dataVentilationModeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCVentilationModeArray);

				fanSpeedCb.CheckedChange += (s, e) => fanSpeedEt.Enabled = e.IsChecked;

				currentTempCb.CheckedChange += (s, e) =>
				{
					currentTempUnitCb.Enabled = e.IsChecked;
					currentTempUnitCb.Checked = e.IsChecked;

					currentTempValueCb.Enabled = e.IsChecked;
					currentTempValueCb.Checked = e.IsChecked;
				};

				currentTempUnitCb.CheckedChange += (s, e) => currentTempUnitSpn.Enabled = e.IsChecked;
				currentTempValueCb.CheckedChange += (s, e) => currentTempValueEt.Enabled = e.IsChecked;

				desiredTempCb.CheckedChange += (s, e) =>
				{
					desiredTempUnitCb.Enabled = e.IsChecked;
					desiredTempUnitCb.Checked = e.IsChecked;

					desiredTempValueCb.Enabled = e.IsChecked;
					desiredTempValueCb.Checked = e.IsChecked;
				};

				desiredTempUnitCb.CheckedChange += (s, e) => desiredTempUnitSpn.Enabled = e.IsChecked;
				desiredTempValueCb.CheckedChange += (s, e) => desiredTempValueEt.Enabled = e.IsChecked;

				defrostZoneEnableCb.CheckedChange += (s, e) => defrostZoneEnableSpn.Enabled = e.IsChecked;
				dataVentilationModeCb.CheckedChange += (s, e) => dataVentilationModeSpn.Enabled = e.IsChecked;

				fanSpeedEt.Text = climateControlData.getFanSpeed().ToString();

				if (climateControlData.getCurrentTemperature() != null)
				{
					currentTempUnitSpn.SetSelection((int)climateControlData.getCurrentTemperature().getUnit());
					currentTempValueEt.Text = climateControlData.getCurrentTemperature().getValue().ToString();
				}

				if (climateControlData.getDesiredTemperature() != null)
				{
					desiredTempUnitSpn.SetSelection((int)climateControlData.getDesiredTemperature().getUnit());
					desiredTempValueEt.Text = climateControlData.getDesiredTemperature().getValue().ToString();
				}
				acEnableCb.Checked = climateControlData.getAcEnable();
				circulatedAirEnableCb.Checked = climateControlData.getCirculateAirEnable();
				autoModeEnableCb.Checked = climateControlData.getAutoModeEnable();

				defrostZoneEnableSpn.SetSelection((int)climateControlData.getDefrostZone());
				dualModeEnableCb.Checked = climateControlData.getDualModeEnable();
				acMaxEnableCb.Checked = climateControlData.getAcMaxEnable();
				dataVentilationModeSpn.SetSelection((int)climateControlData.getVentilationMode());


				climateControlDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
				 {
					 climateControlDataAlertDialogue.Dispose();
				 });

				climateControlDataAlertDialogue.SetPositiveButton("ADD", (senderAlert, evn) =>
				{
					if (fanSpeedCb.Checked && fanSpeedEt.Text != null && fanSpeedEt.Text.Length > 0)
						climateControlData.fanSpeed = Java.Lang.Integer.ParseInt(fanSpeedEt.Text);

                    climateControlData.currentTemperature = null;
					if (currentTempCb.Checked)
					{
                        climateControlData.currentTemperature = new Temperature();
						if (currentTempUnitCb.Checked)
							climateControlData.currentTemperature.unit = (TemperatureUnit)currentTempUnitSpn.SelectedItemPosition;

						if (currentTempValueCb.Checked && currentTempValueEt.Text != null && currentTempValueEt.Text.Length > 0)
							climateControlData.currentTemperature.value = Java.Lang.Integer.ParseInt(currentTempValueEt.Text);
					}

					climateControlData.desiredTemperature = null;
					if (desiredTempCb.Checked)
					{
                        climateControlData.desiredTemperature = new Temperature();
						if (desiredTempUnitCb.Checked)
							climateControlData.desiredTemperature.unit = (TemperatureUnit)desiredTempUnitSpn.SelectedItemPosition;

						if (desiredTempValueCb.Checked && desiredTempValueEt.Text != null && desiredTempValueEt.Text.Length > 0)
							climateControlData.desiredTemperature.value = Java.Lang.Integer.ParseInt(desiredTempValueEt.Text);
					}

					climateControlData.acEnable = acEnableCb.Checked;
					climateControlData.circulateAirEnable = circulatedAirEnableCb.Checked;
					climateControlData.autoModeEnable = autoModeEnableCb.Checked;
					climateControlData.dualModeEnable = dualModeEnableCb.Checked;
					climateControlData.acMaxEnable = acMaxEnableCb.Checked;


					if (defrostZoneEnableCb.Checked)
					{
						climateControlData.defrostZone = (DefrostZone)defrostZoneEnableSpn.SelectedItemPosition;
					}

					if (dataVentilationModeCb.Checked)
					{
						climateControlData.ventilationMode = (VentilationMode)dataVentilationModeSpn.SelectedItemPosition;
					}
				});

				climateControlDataAlertDialogue.Show();
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, evn) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, evn) =>
			{
                ModuleData moduleData = null;
				if (moduleDataCb.Checked)
				{
                    moduleData = new ModuleData();
					if (moduleTypeCb.Checked)
						moduleData.moduleType = (ModuleType)moduleTypeSpn.SelectedItemPosition;

					if (addRadioControlDataCb.Checked)
						moduleData.radioControlData = radioControlData;

					if (addClimateControlDataCb.Checked)
						moduleData.climateControlData = climateControlData;
				}

				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCb.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildRcSetInteriorVehicleDataResponse(BuildRpc.getNextId(), rsltCode, moduleData);
				AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, even) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateRCResponseGetInteriorVehicleDataConsent()
		{
			Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(RCResponseGetInteriorVehicleDataConsent);

			CheckBox checkBoxAllow = (CheckBox)rpcView.FindViewById(Resource.Id.allow);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = adapter;

			HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				checkBoxAllow.Checked = (bool)tmpObj.getAllowed();
				spnResultCode.SetSelection((int)tmpObj.getResultCode());
			}

			rsltCode.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			checkBoxAllow.Text = ("Allow");
			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildRcGetInteriorVehicleDataConsentResponse(BuildRpc.getNextId(), selectedResultCode, checkBoxAllow.Checked);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateRCResponseGetInteriorVehicleData()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.get_interior_vehicle_data, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(RCResponseGetInteriorVehicleData);

			CheckBox moduleDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_module_data_cb);

			CheckBox moduleTypeCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_module_type_cb);
			Spinner moduleTypeSpn = (Spinner)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_module_type_spn);

			CheckBox addRadioControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_radio_control_data_chk);
			Button addRadioControlDataButton = (Button)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_radio_control_data_btn);

			CheckBox addClimateControlDataCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_climate_control_data_chk);
			Button addClimateControlDataButton = (Button)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_add_climate_control_data_btn);

			CheckBox isSubscribedCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_is_subscribed_cb);

			CheckBox resultCodeCb = (CheckBox)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_result_code_cb);
			Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.get_interior_vehicle_data_result_code_spn);

			moduleDataCb.CheckedChange += (s, e) =>
			{
				moduleTypeCb.Enabled = e.IsChecked;
				addRadioControlDataCb.Enabled = e.IsChecked;
				addClimateControlDataCb.Enabled = e.IsChecked;

				moduleTypeCb.Checked = e.IsChecked;
				addRadioControlDataCb.Checked = e.IsChecked;
				addClimateControlDataCb.Checked = e.IsChecked;
			};

			moduleTypeCb.CheckedChange += (s, e) => moduleTypeSpn.Enabled = e.IsChecked;
			addRadioControlDataCb.CheckedChange += (s, e) => addRadioControlDataButton.Enabled = e.IsChecked;
			addClimateControlDataCb.CheckedChange += (s, e) => addClimateControlDataButton.Enabled = e.IsChecked;
			resultCodeCb.CheckedChange += (s, e) => resultCodeSpn.Enabled = e.IsChecked;

			moduleTypeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCModuleTypeArray);
			resultCodeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);


			RadioControlData radioControlData = null;
			ClimateControlData climateControlData = null;

			HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData tmpObj = new HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData();
			tmpObj = (HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.RC.OutgoingResponses.GetInteriorVehicleData>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				if (tmpObj.getModuleData() != null)
				{
					moduleTypeSpn.SetSelection((int)tmpObj.getModuleData().getModuleType());
					radioControlData = tmpObj.getModuleData().getRadioControlData();
					climateControlData = tmpObj.getModuleData().getClimateControlData();
				}

				if (tmpObj.getIsSubscribed() != null)
				{
					isSubscribedCb.Checked = (bool)tmpObj.getIsSubscribed();
				}

				resultCodeSpn.SetSelection((int)tmpObj.getResultCode());
			}

			if (radioControlData == null)
				radioControlData = new RadioControlData();

			if (climateControlData == null)
				climateControlData = new ClimateControlData();

			addRadioControlDataButton.Click += delegate
			{
				AlertDialog.Builder radioControlDataAlertDialogue = new AlertDialog.Builder(Activity);
				View radioControlDataView = layoutInflater.Inflate(Resource.Layout.radio_control_data, null);
				radioControlDataAlertDialogue.SetView(radioControlDataView);
				radioControlDataAlertDialogue.SetTitle("RadioControlData");

				CheckBox frequencyIntegerCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_integer_cb);
				EditText frequencyIntegerEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_integer_et);

				CheckBox frequencyFractionCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_fraction_cb);
				EditText frequencyFractionEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_frequency_fraction_et);

				CheckBox radioBandCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_band_cb);
				Spinner radioBandSpn = (Spinner)radioControlDataView.FindViewById(Resource.Id.radio_control_data_band_spn);


				CheckBox rdsDataCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_rds_data_cb);

				CheckBox rdsDataPsCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_ps_cb);
				EditText rdsDataPsEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_ps_et);

				CheckBox rdsDataRtCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_rt_cb);
				EditText rdsDataRtEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_rt_et);

				CheckBox rdsDataCtCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_ct_cb);
				EditText rdsDataCtEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_ct_et);

				CheckBox rdsDataPiCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_pi_cb);
				EditText rdsDataPiEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_pi_et);

				CheckBox rdsDataPtyCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_pty_cb);
				EditText rdsDataPtyEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_pty_et);

				CheckBox rdsDataTpCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_tp_cb);

				CheckBox rdsDataTaCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_ta_cb);

				CheckBox rdsDataRegCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_reg_cb);
				EditText rdsDataRegEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_reg_et);


				CheckBox availableHDsCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_available_hds_cb);
				EditText availableHDsEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_available_hds_et);

				CheckBox hdChannelCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_hd_channel_cb);
				EditText hdChannelEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_hd_channel_et);

				CheckBox signalStrengthCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_strength_cb);
				EditText signalStrengthEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_strength_et);

				CheckBox signalChangeThresholdCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_change_threshold_cb);
				EditText signalChangeThresholdEt = (EditText)radioControlDataView.FindViewById(Resource.Id.radio_control_data_signal_change_threshold_et);

				CheckBox radioEnableCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_radio_enable_cb);

				CheckBox radioStateCb = (CheckBox)radioControlDataView.FindViewById(Resource.Id.radio_control_data_radio_state_cb);
				Spinner radioStateSpn = (Spinner)radioControlDataView.FindViewById(Resource.Id.radio_control_data_radio_state_spn);

				radioBandSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCRadioBandArray);
				radioStateSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCRadioStateArray);

				frequencyIntegerEt.Text = radioControlData.getFrequencyInteger().ToString();
				frequencyFractionEt.Text = radioControlData.getFrequencyFraction().ToString();
				radioBandSpn.SetSelection((int)radioControlData.getBand());

				if (radioControlData.getRdsData() != null)
				{
					rdsDataPsEt.Text = radioControlData.getRdsData().PS;
					rdsDataRtEt.Text = radioControlData.getRdsData().RT;
					rdsDataCtEt.Text = radioControlData.getRdsData().CT;
					rdsDataPiEt.Text = radioControlData.getRdsData().PI;
					rdsDataPtyEt.Text = radioControlData.getRdsData().PTY.ToString();

					rdsDataTpCb.Checked = radioControlData.getRdsData().TP;
					rdsDataTaCb.Checked = radioControlData.getRdsData().TA;
					rdsDataRegEt.Text = radioControlData.getRdsData().REG;
				}

				availableHDsEt.Text = radioControlData.getAvailableHDs().ToString();
				hdChannelEt.Text = radioControlData.getHdChannel().ToString();
				signalStrengthEt.Text = radioControlData.getSignalStrength().ToString();
				signalChangeThresholdEt.Text = radioControlData.getSignalChangeThreshold().ToString();
				radioEnableCb.Checked = radioControlData.getRadioEnable();
				radioStateSpn.SetSelection((int)radioControlData.getState());

				frequencyIntegerCb.CheckedChange += (s, e) => frequencyIntegerEt.Enabled = e.IsChecked;
				frequencyFractionCb.CheckedChange += (s, e) => frequencyFractionEt.Enabled = e.IsChecked;
				radioBandCb.CheckedChange += (s, e) => radioBandSpn.Enabled = e.IsChecked;

				rdsDataCb.CheckedChange += (s, e) =>
				{
					rdsDataPsCb.Enabled = e.IsChecked;
					rdsDataRtCb.Enabled = e.IsChecked;
					rdsDataCtCb.Enabled = e.IsChecked;
					rdsDataPiCb.Enabled = e.IsChecked;
					rdsDataPtyCb.Enabled = e.IsChecked;
					rdsDataTpCb.Enabled = e.IsChecked;
					rdsDataTaCb.Enabled = e.IsChecked;
					rdsDataRegCb.Enabled = e.IsChecked;

					rdsDataPsEt.Enabled = e.IsChecked;
					rdsDataRtEt.Enabled = e.IsChecked;
					rdsDataCtEt.Enabled = e.IsChecked;
					rdsDataPiEt.Enabled = e.IsChecked;
					rdsDataPtyEt.Enabled = e.IsChecked;
					rdsDataRegEt.Enabled = e.IsChecked;
				};
				rdsDataPsCb.CheckedChange += (s, e) => rdsDataPsEt.Enabled = e.IsChecked;
				rdsDataRtCb.CheckedChange += (s, e) => rdsDataRtEt.Enabled = e.IsChecked;
				rdsDataCtCb.CheckedChange += (s, e) => rdsDataCtEt.Enabled = e.IsChecked;
				rdsDataPiCb.CheckedChange += (s, e) => rdsDataPiEt.Enabled = e.IsChecked;
				rdsDataPtyCb.CheckedChange += (s, e) => rdsDataPtyEt.Enabled = e.IsChecked;
				rdsDataRegCb.CheckedChange += (s, e) => rdsDataRegEt.Enabled = e.IsChecked;

				availableHDsCb.CheckedChange += (s, e) => availableHDsEt.Enabled = e.IsChecked;
				hdChannelCb.CheckedChange += (s, e) => hdChannelEt.Enabled = e.IsChecked;
				signalStrengthCb.CheckedChange += (s, e) => signalStrengthEt.Enabled = e.IsChecked;
				signalChangeThresholdCb.CheckedChange += (s, e) => signalChangeThresholdEt.Enabled = e.IsChecked;
				radioStateCb.CheckedChange += (s, e) => radioStateSpn.Enabled = e.IsChecked;

				radioControlDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
				{
					radioControlDataAlertDialogue.Dispose();
				});

				radioControlDataAlertDialogue.SetPositiveButton("ADD", (senderAlert, evn) =>
				{
					if (frequencyIntegerCb.Checked && frequencyIntegerEt.Text != null && frequencyIntegerEt.Text.Length > 0)
						radioControlData.frequencyInteger = Java.Lang.Integer.ParseInt(frequencyIntegerEt.Text);
					else
						radioControlData.frequencyInteger = 0;

					if (frequencyFractionCb.Checked && frequencyFractionEt.Text != null && frequencyFractionEt.Text.Length > 0)
						radioControlData.frequencyFraction = Java.Lang.Integer.ParseInt(frequencyFractionEt.Text);
					else
						radioControlData.frequencyFraction = 0;

					if (radioBandCb.Checked)
						radioControlData.band = (RadioBand)radioBandSpn.SelectedItemPosition;
					else
						radioControlData.band = RadioBand.AM;


					radioControlData.rdsData = null;
					if (rdsDataCb.Checked)
					{
						radioControlData.rdsData = new RdsData();

						if (rdsDataPsCb.Checked)
							radioControlData.rdsData.PS = rdsDataPsEt.Text;

						if (rdsDataRtCb.Checked)
							radioControlData.rdsData.RT = rdsDataRtEt.Text;

						if (rdsDataCtCb.Checked)
							radioControlData.rdsData.CT = rdsDataCtEt.Text;

						if (rdsDataPiCb.Checked)
							radioControlData.rdsData.PI = rdsDataPiEt.Text;

						if (rdsDataPtyCb.Checked && rdsDataPtyEt.Text != null && rdsDataPtyEt.Text.Length > 0)
							radioControlData.rdsData.PTY = Java.Lang.Integer.ParseInt(rdsDataPtyEt.Text);

						if (rdsDataRegCb.Checked)
							radioControlData.rdsData.REG = rdsDataRegEt.Text;

						radioControlData.rdsData.TP = rdsDataTpCb.Checked;
						radioControlData.rdsData.TA = rdsDataTaCb.Checked;
					}

					if (availableHDsCb.Checked && availableHDsEt.Text != null && availableHDsEt.Text.Length > 0)
						radioControlData.availableHDs = Java.Lang.Integer.ParseInt(availableHDsEt.Text);
					else
						radioControlData.availableHDs = 0;

					if (hdChannelCb.Checked && hdChannelEt.Text != null && hdChannelEt.Text.Length > 0)
						radioControlData.hdChannel = Java.Lang.Integer.ParseInt(hdChannelEt.Text);
					else
						radioControlData.hdChannel = 0;

					if (signalStrengthCb.Checked && signalStrengthEt.Text != null && signalStrengthEt.Text.Length > 0)
						radioControlData.signalStrength = Java.Lang.Integer.ParseInt(signalStrengthEt.Text);
					else
						radioControlData.signalStrength = 0;

					if (signalChangeThresholdCb.Checked && signalChangeThresholdEt.Text != null && signalChangeThresholdEt.Text.Length > 0)
						radioControlData.signalChangeThreshold = Java.Lang.Integer.ParseInt(signalChangeThresholdEt.Text);
					else
						radioControlData.signalChangeThreshold = 0;

					radioControlData.radioEnable = radioEnableCb.Checked;

					if (radioStateCb.Checked)
						radioControlData.state = (RadioState)radioStateSpn.SelectedItemPosition;
					else
						radioControlData.state = RadioState.ACQUIRING;
				});

				radioControlDataAlertDialogue.Show();
			};

			addClimateControlDataButton.Click += delegate
			{
				AlertDialog.Builder climateControlDataAlertDialogue = new AlertDialog.Builder(Activity);
				View climateControlDataView = layoutInflater.Inflate(Resource.Layout.climate_control_data, null);
				climateControlDataAlertDialogue.SetView(climateControlDataView);
				climateControlDataAlertDialogue.SetTitle("ClimateControlData");

				CheckBox fanSpeedCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_fan_speed_cb);
				EditText fanSpeedEt = (EditText)climateControlDataView.FindViewById(Resource.Id.climate_control_data_fan_speed_et);


				CheckBox currentTempCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_current_temp_cb);

				CheckBox currentTempUnitCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.current_temp_temperature_unit_cb);
				Spinner currentTempUnitSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.current_temp_temperature_unit_spn);

				CheckBox currentTempValueCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.current_temp_value_cb);
				EditText currentTempValueEt = (EditText)climateControlDataView.FindViewById(Resource.Id.current_temp_value_et);


				CheckBox desiredTempCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_desired_temp_cb);

				CheckBox desiredTempUnitCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.desired_temp_temperature_unit_cb);
				Spinner desiredTempUnitSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.desired_temp_temperature_unit_spn);

				CheckBox desiredTempValueCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.desired_temp_value_cb);
				EditText desiredTempValueEt = (EditText)climateControlDataView.FindViewById(Resource.Id.desired_temp_value_et);


				CheckBox acEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ac_enable_cb);
				CheckBox circulatedAirEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_circulated_air_enable_cb);
				CheckBox autoModeEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_auto_mode_enable_cb);

				CheckBox defrostZoneEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_defrost_zone_cb);
				Spinner defrostZoneEnableSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.climate_control_data_defrost_zone_spn);

				CheckBox dualModeEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_dual_mode_enable_cb);
				CheckBox acMaxEnableCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ac_max_enable_cb);

				CheckBox dataVentilationModeCb = (CheckBox)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ventilation_mode_cb);
				Spinner dataVentilationModeSpn = (Spinner)climateControlDataView.FindViewById(Resource.Id.climate_control_data_ventilation_mode_spn);


				currentTempUnitSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCTempUnitArray);
				desiredTempUnitSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCTempUnitArray);
				defrostZoneEnableSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCDefrostZoneArray);
				dataVentilationModeSpn.Adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, RCVentilationModeArray);


				fanSpeedCb.CheckedChange += (s, e) => fanSpeedEt.Enabled = e.IsChecked;

				currentTempCb.CheckedChange += (s, e) =>
				{
					currentTempUnitCb.Enabled = e.IsChecked;
					currentTempValueCb.Enabled = e.IsChecked;
				};

				currentTempUnitCb.CheckedChange += (s, e) => currentTempUnitSpn.Enabled = e.IsChecked;
				currentTempValueCb.CheckedChange += (s, e) => currentTempValueEt.Enabled = e.IsChecked;

				desiredTempCb.CheckedChange += (s, e) =>
				{
					desiredTempUnitCb.Enabled = e.IsChecked;
					desiredTempValueCb.Enabled = e.IsChecked;
				};

				desiredTempUnitCb.CheckedChange += (s, e) => desiredTempUnitSpn.Enabled = e.IsChecked;
				desiredTempValueCb.CheckedChange += (s, e) => desiredTempValueEt.Enabled = e.IsChecked;

				defrostZoneEnableCb.CheckedChange += (s, e) => defrostZoneEnableSpn.Enabled = e.IsChecked;
				dataVentilationModeCb.CheckedChange += (s, e) => dataVentilationModeSpn.Enabled = e.IsChecked;

				fanSpeedEt.Text = climateControlData.getFanSpeed().ToString();

				if (climateControlData.getCurrentTemperature() != null)
				{
					currentTempUnitSpn.SetSelection((int)climateControlData.getCurrentTemperature().getUnit());
					currentTempValueEt.Text = climateControlData.getCurrentTemperature().getValue().ToString();
				}

				if (climateControlData.getDesiredTemperature() != null)
				{
					desiredTempUnitSpn.SetSelection((int)climateControlData.getDesiredTemperature().getUnit());
					desiredTempValueEt.Text = climateControlData.getDesiredTemperature().getValue().ToString();
				}
				acEnableCb.Checked = climateControlData.getAcEnable();
				circulatedAirEnableCb.Checked = climateControlData.getCirculateAirEnable();
				autoModeEnableCb.Checked = climateControlData.getAutoModeEnable();

				defrostZoneEnableSpn.SetSelection((int)climateControlData.getDefrostZone());
				dualModeEnableCb.Checked = climateControlData.getDualModeEnable();
				acMaxEnableCb.Checked = climateControlData.getAcMaxEnable();
				dataVentilationModeSpn.SetSelection((int)climateControlData.getVentilationMode());


				climateControlDataAlertDialogue.SetNegativeButton(CANCEL, (senderAlert, evn) =>
				 {
					 climateControlDataAlertDialogue.Dispose();
				 });

				climateControlDataAlertDialogue.SetPositiveButton("ADD", (senderAlert, evn) =>
				{
					if (fanSpeedCb.Checked && fanSpeedEt.Text != null && fanSpeedEt.Text.Length > 0)
						climateControlData.fanSpeed = Java.Lang.Integer.ParseInt(fanSpeedEt.Text);

					climateControlData.currentTemperature = null;
					if (currentTempCb.Checked)
					{
                        climateControlData.currentTemperature = new Temperature();
						if (currentTempUnitCb.Checked)
							climateControlData.currentTemperature.unit = (TemperatureUnit)currentTempUnitSpn.SelectedItemPosition;

						if (currentTempValueCb.Checked && currentTempValueEt.Text != null && currentTempValueEt.Text.Length > 0)
							climateControlData.currentTemperature.value = Java.Lang.Integer.ParseInt(currentTempValueEt.Text);
					}

					climateControlData.desiredTemperature = null;
					if (desiredTempCb.Checked)
					{
                        climateControlData.desiredTemperature = new Temperature();
						if (desiredTempUnitCb.Checked)
							climateControlData.desiredTemperature.unit = (TemperatureUnit)desiredTempUnitSpn.SelectedItemPosition;

						if (desiredTempValueCb.Checked && desiredTempValueEt.Text != null && desiredTempValueEt.Text.Length > 0)
							climateControlData.desiredTemperature.value = Java.Lang.Integer.ParseInt(desiredTempValueEt.Text);
					}

					climateControlData.acEnable = acEnableCb.Checked;
					climateControlData.circulateAirEnable = circulatedAirEnableCb.Checked;
					climateControlData.autoModeEnable = autoModeEnableCb.Checked;
					climateControlData.dualModeEnable = dualModeEnableCb.Checked;
					climateControlData.acMaxEnable = acMaxEnableCb.Checked;

					if (defrostZoneEnableCb.Checked)
					{
						climateControlData.defrostZone = (DefrostZone)defrostZoneEnableSpn.SelectedItemPosition;
					}

					if (dataVentilationModeCb.Checked)
					{
						climateControlData.ventilationMode = (VentilationMode)dataVentilationModeSpn.SelectedItemPosition;
					}
				});

				climateControlDataAlertDialogue.Show();
			};

			ModuleData moduleData = new ModuleData();

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, evn) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, evn) =>
			{
				if (moduleDataCb.Checked)
				{
					if (moduleTypeCb.Checked)
						moduleData.moduleType = (ModuleType)moduleTypeSpn.SelectedItemPosition;

					if (addRadioControlDataCb.Checked)
						moduleData.radioControlData = radioControlData;

					if (addClimateControlDataCb.Checked)
						moduleData.climateControlData = climateControlData;
				}

				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCb.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildRcGetInteriorVehicleDataResponse(BuildRpc.getNextId(), rsltCode, moduleData, isSubscribedCb.Checked);
				AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, even) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateVRNotificationStopped()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			rpcAlertDialog.SetTitle(VRNotificationStopped);

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
			    AppInstanceManager.Instance.sendRpc(BuildRpc.buildVRStoppedNotification());
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{

			});

			rpcAlertDialog.Show();
		}

		private void CreateVRNotificationStarted()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			rpcAlertDialog.SetTitle(VRNotificationStarted);

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
			    AppInstanceManager.Instance.sendRpc(BuildRpc.buildVRStartedNotification());
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{

			});

			rpcAlertDialog.Show();
		}

		private void CreateVRNotificationOnLanguageChange()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(VRNotificationOnLanguageChange);

			CheckBox languageCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			languageCheckBox.Text = "Language";
			Spinner spnLanguage = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            languageCheckBox.CheckedChange += (sender, e) => spnLanguage.Enabled = e.IsChecked;
            string[] language = Enum.GetNames(typeof(Language));
			var languageAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, language);
			spnLanguage.Adapter = languageAdapter;

            HmiApiLib.Controllers.VR.OutGoingNotifications.OnLanguageChange tmpObj = new HmiApiLib.Controllers.VR.OutGoingNotifications.OnLanguageChange();
            tmpObj = (HmiApiLib.Controllers.VR.OutGoingNotifications.OnLanguageChange)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutGoingNotifications.OnLanguageChange>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getLanguage() != null)
                    spnLanguage.SetSelection((int)tmpObj.getLanguage());
            }


            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
                Language? lang = null;
                if (languageCheckBox.Checked)
                {
                    lang = (Language)spnLanguage.SelectedItemPosition;
                }

                RequestNotifyMessage rpcMessage = null;
                rpcMessage = BuildRpc.buildVROnLanguageChange(lang);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
			   if (tmpObj != null)
			   {
			       AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
			   }
			});

			rpcAlertDialog.Show();
		}

		private void CreateVRNotificationOnCommand()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_command_notification, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(VRNotificationOnCommand);

            CheckBox cmdIdCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_command_cmd_id_check);
            Spinner cmdIdSpinner = (Spinner)rpcView.FindViewById(Resource.Id.on_command_cmd_id_spinner);

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_check);

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.sdl_on_command_app_id_manual_radioGroup);

			RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_command_app_id_manual);
			EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_et);

            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_command_app_id_registered_apps);
			Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_spinner);
            registerdAppIdSpn.Enabled = false;


            List<int?> cmdIDList = new List<int?>();

            List<int> appIds = new List<int>();
            foreach (AppItem item in AppInstanceManager.appList)
            {
                appIds.Add(item.getAppID());
            }

            int[] appIDArray = appIds.ToArray();

            for (int i = 0; i < AppInstanceManager.commandIdList.Count; i++)
            {
                List<int?> commandIdList = AppInstanceManager.commandIdList[appIDArray[i]];
                cmdIDList.AddRange(commandIdList);
            }

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

			var cmdIdAdapter = new ArrayAdapter<int?>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, cmdIDList);
			cmdIdSpinner.Adapter = cmdIdAdapter;


            cmdIdCheck.CheckedChange += (sender, e) => cmdIdSpinner.Enabled = e.IsChecked;
            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
				manualAppIdRadioButton.Enabled = e.IsChecked;
				registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };

            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };			


            HmiApiLib.Controllers.VR.OutGoingNotifications.OnCommand tmpObj = new HmiApiLib.Controllers.VR.OutGoingNotifications.OnCommand();
            tmpObj = (HmiApiLib.Controllers.VR.OutGoingNotifications.OnCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutGoingNotifications.OnCommand>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getAppId() != null)
                    manualAppIdEditText.Text = tmpObj.getAppId().ToString();

                if (tmpObj.getCmdID() != null)
                    cmdIdSpinner.SetSelection((int)tmpObj.getCmdID());
            }

			
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))

                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = appIds[registerdAppIdSpn.SelectedItemPosition];

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }

                 int? commandId = null;

                 if (cmdIdCheck.Checked)
                     commandId = (int)cmdIdSpinner.SelectedItem;

                 RequestNotifyMessage rpcMessage = BuildRpc.buildVROnCommand(commandId, selectedAppID);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
				    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateVRResponsePerformInteraction()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.vr_perform_interaction_response, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(VRResponsePerformInteraction);

			CheckBox checkBoxChoiceID = (CheckBox)rpcView.FindViewById(Resource.Id.vr_choice_id_chk);
			EditText editTextChoiceID = (EditText)rpcView.FindViewById(Resource.Id.vr_choice_id_et);

            CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.vr_result_code_chk);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.vr_perform_interaction_result_code_spn);

            var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = resultCodeAdapter;

			checkBoxChoiceID.CheckedChange += (sender, e) => editTextChoiceID.Enabled = e.IsChecked;
			resultCodeCheckbox.CheckedChange += (sender, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction();
            tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.PerformInteraction>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());

                if(tmpObj.getChoiceID()!=null)
                    editTextChoiceID.Text = tmpObj.getChoiceID().ToString();
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
				 rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				int? choiceId = null;

				if (checkBoxChoiceID.Checked)
                if(editTextChoiceID.Text != null && editTextChoiceID.Text.Length > 0)
				    choiceId = Java.Lang.Integer.ParseInt(editTextChoiceID.Text);

				RpcResponse rpcResponse = BuildRpc.buildVrPerformInteractionResponse(BuildRpc.getNextId(), choiceId, rsltCode);
				AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

			rpcAlertDialog.Show();
		}

		private void CreateVRResponseDeleteCommand()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(VRResponseDeleteCommand);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (sender, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.DeleteCommand>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
			  rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{

			   HmiApiLib.Common.Enums.Result? rsltCode = null;
			   if (resultCodeCheckbox.Checked)
			   {
			       rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
			   }

			   RpcResponse rpcResponse = null;
			   rpcResponse = BuildRpc.buildVrDeleteCommandResponse(BuildRpc.getNextId(), rsltCode);
			   AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateVRResponseChangeRegistration()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(VRResponseChangeRegistration);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (sender, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.ChangeRegistration>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
			  rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
			   HmiApiLib.Common.Enums.Result? rsltCode = null;
			   if (resultCodeCheckbox.Checked)
			   {
			       rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
			   }

			   RpcResponse rpcResponse = BuildRpc.buildVrChangeRegistrationResponse(BuildRpc.getNextId(), rsltCode);
			   AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateVRResponseAddCommand()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(VRResponseAddCommand);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (sender, e) => resultCodeSpinner.Enabled = e.IsChecked;

			HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand tmpObj = new HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand();
			tmpObj = (HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VR.OutgoingResponses.AddCommand>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
			  rpcAlertDialog.Dispose();
            });

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
			   HmiApiLib.Common.Enums.Result? rsltCode = null;
			   if (resultCodeCheckbox.Checked)
			   {
			       rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
			   }

			   RpcResponse rpcResponse = null;
			   rpcResponse = BuildRpc.buildVrAddCommandResponse(BuildRpc.getNextId(), rsltCode);
			   AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
            });

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseAddCommand()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseAddCommand);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (sender, e) => spnGeneric.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.AddCommand>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcMessage rpcMessage = BuildRpc.buildUiAddCommandResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(Activity, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseAddSubMenu()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UIResponseAddSubMenu);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.AddSubMenu>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCheckbox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
                }

                RpcMessage rpcMessage = BuildRpc.buildUiAddSubMenuResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(Activity, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseAlert()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Context);
			View rpcView = layoutInflater.Inflate(Resource.Layout.alert_ui_response, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UIResponseAlert);

			CheckBox tryAgainTimeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.alert_try_again_time_check);
			EditText tryAgainTimeEditText = (EditText)rpcView.FindViewById(Resource.Id.alert_try_again_time_et);

			CheckBox resultCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.alert_resultcode_check);
			Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.alert_resultcode_spn);

			tryAgainTimeEditText.Text = "0";

			var appExitReasonAdapter = new ArrayAdapter<String>(Context, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpn.Adapter = appExitReasonAdapter;

            tryAgainTimeCheckBox.CheckedChange += (s, e) => tryAgainTimeEditText.Enabled = e.IsChecked;
            resultCodeCheckBox.CheckedChange += (s, e) => resultCodeSpn.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.Alert tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Alert();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Alert)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Alert>(Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				resultCodeSpn.SetSelection((int)tmpObj.getResultCode());

				if (tmpObj.getTryAgainTime() != null)
					tryAgainTimeEditText.Text = tmpObj.getTryAgainTime().ToString();
			}

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                int? tryAgainTime = null;

                if (tryAgainTimeCheckBox.Checked && tryAgainTimeEditText.Text != null && tryAgainTimeEditText.Text.Length > 0)
                    tryAgainTime =Java.Lang.Integer.ParseInt(tryAgainTimeEditText.Text);

                if (resultCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;

				RpcResponse rpcMessage = BuildRpc.buildUiAlertResponse(BuildRpc.getNextId(), rsltCode, tryAgainTime);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});
			rpcAlertDialog.Show();
		}

        private void CreateUIResponseChangeRegistration()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle(UIResponseChangeRegistration);

            CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnGeneric.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ChangeRegistration>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnGeneric.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
           {
               HmiApiLib.Common.Enums.Result? resltCode = null;
               if (resultCodeCheckbox.Checked)
               {
                   resltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
               }

               RpcMessage rpcMessage = BuildRpc.buildUiChangeRegistrationResponse(BuildRpc.getNextId(), resltCode);
               AppUtils.savePreferenceValueForRpc(Activity, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
           });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

		private void CreateUIResponseClosePopUp()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseClosePopUp);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ClosePopUp>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? resltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					resltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiClosePopUpResponse(BuildRpc.getNextId(), resltCode);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseDeleteCommand()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseDeleteCommand);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.DeleteCommand>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? resltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					resltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiDeleteCommandResponse(BuildRpc.getNextId(), resltCode);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseDeleteSubMenu()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseDeleteSubMenu);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) =>spnGeneric.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.DeleteSubMenu>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{

				HmiApiLib.Common.Enums.Result? resltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					resltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiDeleteSubMenuResponse(BuildRpc.getNextId(), resltCode);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseEndAudioPassThru()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseEndAudioPassThru);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.EndAudioPassThru>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? resltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					resltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiEndAudioPassThruResponse(BuildRpc.getNextId(), resltCode);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponsePerformAudioPassThru()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UIResponsePerformAudioPassThru);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.PerformAudioPassThru>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiPerformAudioPassThruResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponsePerformInteraction()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.perform_interaction_response, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UIResponsePerformInteraction);

            CheckBox choiceIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.choice_id_cb);
			EditText choiceIDEditText = (EditText)rpcView.FindViewById(Resource.Id.choice_id_et);

			CheckBox manualTextEntryCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.manual_text_entry_cb);
			EditText manualTextEntryEditText = (EditText)rpcView.FindViewById(Resource.Id.manual_text_entry_et);

			CheckBox resultCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.perform_interaction_result_code_spn);

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = resultCodeAdapter;

            choiceIDCheckBox.CheckedChange += (s, e) => choiceIDEditText.Enabled = e.IsChecked;
            manualTextEntryCheckBox.CheckedChange += (s, e) => manualTextEntryEditText.Enabled = e.IsChecked;
            resultCodeCheckBox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.PerformInteraction>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());

                if (tmpObj.getChoiceID() != null)
				    choiceIDEditText.Text = tmpObj.getChoiceID().ToString();

                if (tmpObj.getManualTextEntry() != null)
					manualTextEntryEditText.Text = tmpObj.getManualTextEntry();
		    }

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                int? choiceID = null;
                String manualTextEntry = null;
                HmiApiLib.Common.Enums.Result? rsltCode = null;

                if (choiceIDCheckBox.Checked && manualTextEntryEditText.Text != null && choiceIDEditText.Text.Length > 0)
                    choiceID = Java.Lang.Integer.ParseInt(choiceIDEditText.Text);

                if (manualTextEntryCheckBox.Checked)
                    manualTextEntry = manualTextEntryEditText.Text;

                if (resultCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
                
                RpcResponse rpcMessage = BuildRpc.buildUiPerformInteractionResponse(BuildRpc.getNextId(), choiceID, manualTextEntry, rsltCode);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
            });

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseScrollableMessage()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseScrollableMessage);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Context, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ScrollableMessage>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				RpcMessage rpcMessage = null;
				rpcMessage = BuildRpc.buildUiScrollableMessageResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseSetAppIcon()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Context);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseSetAppIcon);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Context, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

            resultCodeCheckbox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetAppIcon>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiSetAppIconResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseSetDisplayLayout()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.set_display_layout, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UIResponseSetDisplayLayout);

			DisplayCapabilities dspCap = null;
            CheckBox displayCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.display_capabilities_add_display_capabilities_chk);
			Button addDisplayCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.set_display_capabilities_add_display_capabilities_btn);

            displayCapabilitiesCheckBox.CheckedChange += (sen, evn) => addDisplayCapabilitiesButton.Enabled = evn.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout();
            tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetDisplayLayout>(Activity, tmpObj.getMethod());
			
			if (tmpObj != null)
			{
				dspCap = tmpObj.getDisplayCapabilities();
			}

			if (dspCap == null)
			{
				dspCap = new DisplayCapabilities();
			}
            addDisplayCapabilitiesButton.Click += delegate
         {
             Android.Support.V7.App.AlertDialog.Builder displayCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
             View displayCapabilitiesView = layoutInflater.Inflate(Resource.Layout.display_capabilities, null);
             displayCapabilitiesAlertDialog.SetView(displayCapabilitiesView);
             displayCapabilitiesAlertDialog.SetTitle("Display Capabilities");

             CheckBox checkBoxDisplayType = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_type_checkbox);
             Spinner spnButtonDisplayType = (Spinner)displayCapabilitiesView.FindViewById(Resource.Id.display_type_spinner);

             string[] displayType = Enum.GetNames(typeof(DisplayType));
             var displayTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, displayType);
             spnButtonDisplayType.Adapter = displayTypeAdapter;

             spnButtonDisplayType.SetSelection((int)dspCap.getDisplayType());

             List<TextField> textFieldsList = new List<TextField>();
             ListView textFieldsListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_text_fields_listview);
             Button textFieldsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_text_fields_btn);
             CheckBox addTextFieldsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_text_fields_chk);
             if (dspCap.getTextFields() != null)
             {
                 textFieldsList.AddRange(dspCap.getTextFields());
             }
             var textFieldAdapter = new TextFieldAdapter(Activity, textFieldsList);
             textFieldsListView.Adapter = textFieldAdapter;
             Utility.setListViewHeightBasedOnChildren(textFieldsListView);

             List<ImageField> imageFieldsList = new List<ImageField>();
             ListView imageFieldsListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_image_fields_listview);
             Button imageFieldsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_fields_btn);
             CheckBox imageFieldsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_fields_chk);
             if (dspCap.getImageFields() != null)
             {
                 imageFieldsList.AddRange(dspCap.getImageFields());
             }
             var imageFieldAdapter = new ImageFieldAdapter(Activity, imageFieldsList);
             imageFieldsListView.Adapter = imageFieldAdapter;
             Utility.setListViewHeightBasedOnChildren(imageFieldsListView);

             List<MediaClockFormat> mediaClockFormatsList = new List<MediaClockFormat>();
             ListView mediaClockFormatsListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_media_clock_formats_listview);
             Button mediaClockFormatsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_media_clock_formats_btn);
             CheckBox mediaClockFormatsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_media_clock_formats_chk);
             if (dspCap.getMediaClockFormats() != null)
             {
                 mediaClockFormatsList.AddRange(dspCap.getMediaClockFormats());
             }
             var mediaClockFormatAdapter = new MediaClockFormatAdapter(Activity, mediaClockFormatsList);
             mediaClockFormatsListView.Adapter = mediaClockFormatAdapter;
             Utility.setListViewHeightBasedOnChildren(mediaClockFormatsListView);

             List<ImageType> imageTypeList = new List<ImageType>();
             ListView imageTypeListView = (ListView)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_image_types_listview);
             CheckBox imageTypeCheckbox = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_types_chk);
             Button addImageTypeButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_image_types_btn);
             if (dspCap.getImageCapabilities() != null)
             {
                 imageTypeList.AddRange(dspCap.getImageCapabilities());
             }
             var imageTypeAdapter = new ImageTypeAdapter(Activity, imageTypeList);
             imageTypeListView.Adapter = imageTypeAdapter;
             Utility.setListViewHeightBasedOnChildren(imageTypeListView);

             CheckBox screenParamsChk = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_screen_params_chk);
             Button screenParamsButton = (Button)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_add_screen_params_btn);

             CheckBox checkBoxGraphicSupported = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_graphic_supported_checkbox);
             checkBoxGraphicSupported.Checked = dspCap.getGraphicSupported();

             CheckBox checkBoxTemplatesAvailable = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_templates_available_checkbox);
             EditText editTextTemplatesAvailable = (EditText)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_templates_available_edittext);
             if (dspCap.getTemplatesAvailable() != null)
             {
                 String availableTemplates = null;
                 for (int i = 0; i < dspCap.getTemplatesAvailable().Count; i++)
                 {
                     if (i == 0)
                     {
                         availableTemplates = dspCap.getTemplatesAvailable()[i];
                     }
                     else
                     {
                         availableTemplates += availableTemplates + "," + dspCap.getTemplatesAvailable()[i];
                     }
                 }
                 editTextTemplatesAvailable.Text = availableTemplates;
             }

             checkBoxTemplatesAvailable.CheckedChange += (s, e) => editTextTemplatesAvailable.Enabled = e.IsChecked;
             screenParamsChk.CheckedChange += (s, e) => screenParamsButton.Enabled = e.IsChecked;
             checkBoxDisplayType.CheckedChange += (s, e) => spnButtonDisplayType.Enabled = e.IsChecked;
             addTextFieldsChk.CheckedChange += (s, e) =>
             {
                 textFieldsButton.Enabled = e.IsChecked;
                 textFieldsListView.Enabled = e.IsChecked;
             };
             imageFieldsChk.CheckedChange += (s, e) =>
             {
                 imageFieldsButton.Enabled = e.IsChecked;
                 imageFieldsListView.Enabled = e.IsChecked;
             };
             mediaClockFormatsChk.CheckedChange += (sender, e) =>
             {
                 mediaClockFormatsButton.Enabled = e.IsChecked;
                 mediaClockFormatsListView.Enabled = e.IsChecked;
             };

			 imageTypeCheckbox.CheckedChange += (sender, e) =>
			  {
				  addImageTypeButton.Enabled = e.IsChecked;
				  imageTypeListView.Enabled = e.IsChecked;
			  };


             textFieldsButton.Click += delegate
             {
                 Android.Support.V7.App.AlertDialog.Builder textFieldsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
                 View textFieldsView = layoutInflater.Inflate(Resource.Layout.text_field, null);
                 textFieldsAlertDialog.SetView(textFieldsView);
                 textFieldsAlertDialog.SetTitle("TextField");

                 CheckBox checkBoxName = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_name_checkbox);
                 Spinner spnName = (Spinner)textFieldsView.FindViewById(Resource.Id.text_field_name_spinner);

                 string[] textFieldNames = Enum.GetNames(typeof(TextFieldName));
                 var textFieldNamesAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, textFieldNames);
                 spnName.Adapter = textFieldNamesAdapter;

                 CheckBox checkBoxCharacterSet = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_characterSet_checkbox);
                 Spinner spnCharacterSet = (Spinner)textFieldsView.FindViewById(Resource.Id.text_field_characterSet_spinner);

                 string[] characterSet = Enum.GetNames(typeof(CharacterSet));
                 var characterSetAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, characterSet);
                 spnCharacterSet.Adapter = characterSetAdapter;

                 CheckBox checkBoxWidth = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_width_checkbox);
                 EditText editTextWidth = (EditText)textFieldsView.FindViewById(Resource.Id.text_field_width_edittext);

                 CheckBox checkBoxRow = (CheckBox)textFieldsView.FindViewById(Resource.Id.text_field_row_checkbox);
                 EditText editTextRow = (EditText)textFieldsView.FindViewById(Resource.Id.text_field_row_edittext);

                 checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
                 checkBoxCharacterSet.CheckedChange += (s, e) => spnCharacterSet.Enabled = e.IsChecked;
                 checkBoxWidth.CheckedChange += (s, e) => editTextWidth.Enabled = e.IsChecked;
                 checkBoxRow.CheckedChange += (s, e) => editTextRow.Enabled = e.IsChecked;

                 textFieldsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                 {
                     TextField txtField = new TextField();

                     if (checkBoxName.Checked)
                     {
                         txtField.name = (TextFieldName)spnName.SelectedItemPosition;
                     }
                     if (checkBoxCharacterSet.Checked)
                     {
                         txtField.characterSet = (CharacterSet)spnCharacterSet.SelectedItemPosition;
                     }
                     if (checkBoxWidth.Checked)
                     {
                         if (editTextWidth.Text.Equals(""))
                             txtField.width = 0;
                         else
                             txtField.width = Java.Lang.Integer.ParseInt(editTextWidth.Text);
                     }
                     if (checkBoxRow.Checked)
                     {
                         if (editTextRow.Text.Equals(""))
                             txtField.rows = 0;
                         else
                             txtField.rows = Java.Lang.Integer.ParseInt(editTextRow.Text);
                     }
                     textFieldsList.Add(txtField);
                     textFieldAdapter.NotifyDataSetChanged();
                     Utility.setListViewHeightBasedOnChildren(textFieldsListView);
                 });

                 textFieldsAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                 {
                     textFieldsAlertDialog.Dispose();
                 });
                 textFieldsAlertDialog.Show();
             };

             imageFieldsButton.Click += delegate
             {
                 Android.Support.V7.App.AlertDialog.Builder imageFieldsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
                 View imageFieldsView = layoutInflater.Inflate(Resource.Layout.image_field, null);
                 imageFieldsAlertDialog.SetView(imageFieldsView);
                 imageFieldsAlertDialog.SetTitle("ImageField");

                 CheckBox checkBoxName = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_name_checkbox);
                 Spinner spnName = (Spinner)imageFieldsView.FindViewById(Resource.Id.image_field_name_spinner);

                 string[] imageFieldNames = Enum.GetNames(typeof(ImageFieldName));
                 var imageFieldNamesAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, imageFieldNames);
                 spnName.Adapter = imageFieldNamesAdapter;

                 List<FileType> fileTypeList = new List<FileType>();
                 ListView fileTypeListView = (ListView)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_listview);
                 var fileTypeAdapter = new FileTypeAdapter(Activity, fileTypeList);
                 fileTypeListView.Adapter = fileTypeAdapter;

                 string[] fileTypes = Enum.GetNames(typeof(FileType));
                 bool[] fileTypeBoolArray = new bool[fileTypes.Length];

                 CheckBox fileTypeChk = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_chk);
                 Button fileTypeButton = (Button)imageFieldsView.FindViewById(Resource.Id.image_field_image_type_supported_btn);

                 checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
                 fileTypeChk.CheckedChange += (sender, e) => fileTypeButton.Enabled = e.IsChecked;

                 fileTypeButton.Click += (sender, e1) =>
                 {
                     Android.Support.V7.App.AlertDialog.Builder fileTypeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
                     fileTypeAlertDialog.SetTitle("FileType");

                     fileTypeAlertDialog.SetMultiChoiceItems(fileTypes, fileTypeBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => fileTypeBoolArray[e.Which] = e.IsChecked);

                     fileTypeAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                     {
                         fileTypeAlertDialog.Dispose();
                     });

                     fileTypeAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                     {
                         for (int i = 0; i < fileTypes.Length; i++)
                         {
                             if (fileTypeBoolArray[i])
                             {
                                 fileTypeList.Add(((FileType)typeof(FileType).GetEnumValues().GetValue(i)));
                             }
                         }
                         fileTypeAdapter.NotifyDataSetChanged();
                         Utility.setListViewHeightBasedOnChildren(fileTypeListView);
                     });
                     fileTypeAlertDialog.Show();
                 };

                 CheckBox checkBoxImageResolution = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_image_resolution_checkbox);

                 CheckBox checkBoxResolutionWidth = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_width_checkbox);
                 EditText editTextResolutionWidth = (EditText)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_width_edit_text);

                 CheckBox checkBoxResolutionHeight = (CheckBox)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_height_checkbox);
                 EditText editTextResolutionHeight = (EditText)imageFieldsView.FindViewById(Resource.Id.image_field_resolution_height_edit_text);

                 checkBoxImageResolution.CheckedChange += (sender, e) =>
                 {
                     checkBoxResolutionWidth.Enabled = e.IsChecked;
                     editTextResolutionWidth.Enabled = e.IsChecked;
                     checkBoxResolutionHeight.Enabled = e.IsChecked;
                     editTextResolutionHeight.Enabled = e.IsChecked;
                 };
                 checkBoxResolutionWidth.CheckedChange += (s, e) => editTextResolutionWidth.Enabled = e.IsChecked;
                 checkBoxResolutionHeight.CheckedChange += (s, e) => editTextResolutionHeight.Enabled = e.IsChecked;

                 imageFieldsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                 {
                     ImageField imgField = new ImageField();

                     if (checkBoxName.Checked)
                     {
                         imgField.name = (ImageFieldName)spnName.SelectedItemPosition;
                     }
                     if (fileTypeChk.Checked)
                     {
                         imgField.imageTypeSupported = fileTypeList;
                     }

                     ImageResolution resolution = null;
                     if (checkBoxImageResolution.Checked)
                     {
                         resolution = new ImageResolution();

                         if (checkBoxResolutionWidth.Checked)
                         {
                             if (editTextResolutionWidth.Text.Equals(""))
                                 resolution.resolutionWidth = 0;
                             else
                                 resolution.resolutionWidth = Java.Lang.Integer.ParseInt(editTextResolutionWidth.Text);
                         }
                         if (checkBoxResolutionHeight.Checked)
                         {
                             if (editTextResolutionHeight.Text.Equals(""))
                                 resolution.resolutionHeight = 0;
                             else
                                 resolution.resolutionHeight = Java.Lang.Integer.ParseInt(editTextResolutionHeight.Text);
                         }
                     }
                     imgField.imageResolution = resolution;

                     imageFieldsList.Add(imgField);
                     imageFieldAdapter.NotifyDataSetChanged();
                     Utility.setListViewHeightBasedOnChildren(imageFieldsListView);
                 });

                 imageFieldsAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                 {
                     imageFieldsAlertDialog.Dispose();
                 });
                 imageFieldsAlertDialog.Show();
             };

             string[] mediaClockFormats = Enum.GetNames(typeof(MediaClockFormat));
             bool[] mediaClockFormatsBoolArray = new bool[mediaClockFormats.Length];

             mediaClockFormatsButton.Click += (sender, e1) =>
             {
                 Android.Support.V7.App.AlertDialog.Builder mediaClockFormatsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
                 mediaClockFormatsAlertDialog.SetTitle("MediaClockFormats");

                 mediaClockFormatsAlertDialog.SetMultiChoiceItems(mediaClockFormats, mediaClockFormatsBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => mediaClockFormatsBoolArray[e.Which] = e.IsChecked);

                 mediaClockFormatsAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                 {
                     mediaClockFormatsAlertDialog.Dispose();
                 });

                 mediaClockFormatsAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                 {
                     for (int i = 0; i < mediaClockFormats.Length; i++)
                     {
                         if (mediaClockFormatsBoolArray[i])
                         {
                             mediaClockFormatsList.Add(((MediaClockFormat)typeof(MediaClockFormat).GetEnumValues().GetValue(i)));
                         }
                     }
                     mediaClockFormatAdapter.NotifyDataSetChanged();
                     Utility.setListViewHeightBasedOnChildren(mediaClockFormatsListView);
                 });

                 mediaClockFormatsAlertDialog.Show();
             };

             string[] imageCapabilities = Enum.GetNames(typeof(ImageType));
             bool[] imageCapabilitiesBoolArray = new bool[imageCapabilities.Length];

             addImageTypeButton.Click += (sender, e1) =>
             {
                 Android.Support.V7.App.AlertDialog.Builder imageCapabilitiesAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(rpcAlertDialog.Context);
                 imageCapabilitiesAlertDialog.SetTitle("ImageCapabilities");

                 imageCapabilitiesAlertDialog.SetMultiChoiceItems(imageCapabilities, imageCapabilitiesBoolArray, (object sender1, DialogMultiChoiceClickEventArgs e) => imageCapabilitiesBoolArray[e.Which] = e.IsChecked);

                 imageCapabilitiesAlertDialog.SetNegativeButton("Cancel", (senderAlert, args) =>
                 {
                     imageCapabilitiesAlertDialog.Dispose();
                 });

                 imageCapabilitiesAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
                 {
                     for (int i = 0; i < imageCapabilities.Length; i++)
                     {
                         if (imageCapabilitiesBoolArray[i])
                         {
                             imageTypeList.Add(((ImageType)typeof(ImageType).GetEnumValues().GetValue(i)));
                         }
                     }
                     imageTypeAdapter.NotifyDataSetChanged();
                     Utility.setListViewHeightBasedOnChildren(imageTypeListView);
                 });

                 imageCapabilitiesAlertDialog.Show();
             };

             // fetch here from preferences
             ScreenParams scrnParam = new ScreenParams();
             screenParamsButton.Click += delegate
             {
                 Android.Support.V7.App.AlertDialog.Builder screenParamsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
                 View screenParamsView = layoutInflater.Inflate(Resource.Layout.screen_param, null);
                 screenParamsAlertDialog.SetView(screenParamsView);
                 screenParamsAlertDialog.SetTitle("ScreenParams");

                 CheckBox checkBoxImageResolution = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_image_resolution_checkbox);
                 CheckBox checkBoxResolutionWidth = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_width_checkbox);
                 EditText editTextResolutionWidth = (EditText)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_width_edit_text);

                 CheckBox checkBoxResolutionHeight = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_height_checkbox);
                 EditText editTextResolutionHeight = (EditText)screenParamsView.FindViewById(Resource.Id.screen_param_resolution_height_edit_text);

                 CheckBox checkTouchEventCapabilities = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_touch_event_capability_checkbox);
                 CheckBox checkBoxpressAvailable = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_press_available_checkbox);
                 CheckBox checkBoxMultiTouchAvailable = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_multi_touch_available_checkbox);
                 CheckBox checkBoxDoubleTouchAvailable = (CheckBox)screenParamsView.FindViewById(Resource.Id.screen_param_double_press_available_checkbox);

                 checkBoxResolutionWidth.CheckedChange += (s, e) => editTextResolutionWidth.Enabled = e.IsChecked;
                 checkBoxResolutionHeight.CheckedChange += (s, e) => editTextResolutionHeight.Enabled = e.IsChecked;

                 checkBoxImageResolution.CheckedChange += (sender, e) =>
                 {
                     checkBoxResolutionWidth.Enabled = e.IsChecked;
                     editTextResolutionWidth.Enabled = e.IsChecked;
                     checkBoxResolutionHeight.Enabled = e.IsChecked;
                     editTextResolutionHeight.Enabled = e.IsChecked;
                 };

                 checkTouchEventCapabilities.CheckedChange += (sender, e) =>
                 {
                     checkBoxpressAvailable.Enabled = e.IsChecked;
                     checkBoxMultiTouchAvailable.Enabled = e.IsChecked;
                     checkBoxDoubleTouchAvailable.Enabled = e.IsChecked;
                 };

                 if (scrnParam.getResolution() != null)
                 {
                     checkBoxImageResolution.Checked = true;
                     editTextResolutionWidth.Text = scrnParam.getResolution().getResolutionWidth().ToString();
                     editTextResolutionHeight.Text = scrnParam.getResolution().getResolutionHeight().ToString();
                 }
                 if (scrnParam.getTouchEventAvailable() != null)
                 {
                     checkTouchEventCapabilities.Checked = true;
                     checkBoxpressAvailable.Checked = scrnParam.getTouchEventAvailable().getPressAvailable();
                     checkBoxMultiTouchAvailable.Checked = scrnParam.getTouchEventAvailable().getMultiTouchAvailable();
                     checkBoxDoubleTouchAvailable.Checked = scrnParam.getTouchEventAvailable().getDoublePressAvailable();
                 }

                 screenParamsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                 {
                     ImageResolution imgResolution = null;
                     if (checkBoxImageResolution.Checked)
                     {
                         imgResolution = new ImageResolution();
                         if (checkBoxResolutionWidth.Checked)
                         {
                             if (editTextResolutionWidth.Text.Equals(""))
                                 imgResolution.resolutionWidth = 0;
                             else
                                 imgResolution.resolutionWidth = Java.Lang.Integer.ParseInt(editTextResolutionWidth.Text);
                         }
                         if (checkBoxResolutionHeight.Checked)
                         {
                             if (editTextResolutionHeight.Text.Equals(""))
                                 imgResolution.resolutionHeight = 0;
                             else
                                 imgResolution.resolutionHeight = Java.Lang.Integer.ParseInt(editTextResolutionHeight.Text);
                         }
                     }
                     scrnParam.resolution = imgResolution;

                     TouchEventCapabilities touchEventCapabilities = null;
                     if (checkTouchEventCapabilities.Checked)
                     {
                         touchEventCapabilities = new TouchEventCapabilities();

                         touchEventCapabilities.pressAvailable = checkBoxpressAvailable.Checked;
                         touchEventCapabilities.pressAvailable = checkBoxpressAvailable.Checked;
                         touchEventCapabilities.pressAvailable = checkBoxpressAvailable.Checked;
                     }
                     scrnParam.touchEventAvailable = touchEventCapabilities;
                 });

                 screenParamsAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                 {
                     screenParamsAlertDialog.Dispose();
                 });
                 screenParamsAlertDialog.Show();
             };

             CheckBox checkBoxNumCustomPresetsAvailable = (CheckBox)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_num_custom_presets_available_checkbox);
             EditText editTextNumCustomPresetsAvailable = (EditText)displayCapabilitiesView.FindViewById(Resource.Id.display_capabilities_num_custom_presets_available_edittext);

             checkBoxNumCustomPresetsAvailable.CheckedChange += (s, e) => editTextNumCustomPresetsAvailable.Enabled = e.IsChecked;

             editTextNumCustomPresetsAvailable.Text = dspCap.getNumCustomPresetsAvailable().ToString();

             displayCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
             {
                 if (checkBoxDisplayType.Checked)
                 {
                     dspCap.displayType = (DisplayType)spnButtonDisplayType.SelectedItemPosition;
                 }
                 if (addTextFieldsChk.Checked)
                 {
                     dspCap.textFields = textFieldsList;
                 }
                 if (imageFieldsChk.Checked)
                 {
                     dspCap.imageFields = imageFieldsList;
                 }
                 if (mediaClockFormatsChk.Checked)
                 {
                     dspCap.mediaClockFormats = mediaClockFormatsList;
                 }
                 if (imageTypeCheckbox.Checked)
                 {
                     dspCap.imageCapabilities = imageTypeList;
                 }
                 dspCap.graphicSupported = checkBoxGraphicSupported.Checked;
                 if (checkBoxTemplatesAvailable.Checked)
                 {
                     List<String> templatesAvailable = new List<string>();
                     templatesAvailable.AddRange(editTextTemplatesAvailable.Text.Split(','));
                     dspCap.templatesAvailable = templatesAvailable;
                 }
                 if (screenParamsChk.Checked)
                 {
                     dspCap.screenParams = scrnParam;
                 }
                 if (checkBoxNumCustomPresetsAvailable.Checked)
                 {
                     if (editTextNumCustomPresetsAvailable.Text.Equals(""))
                         dspCap.numCustomPresetsAvailable = 0;
                     else
                         dspCap.numCustomPresetsAvailable = Java.Lang.Integer.ParseInt(editTextNumCustomPresetsAvailable.Text);
                 }
             });

             displayCapabilitiesAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
             {
                 displayCapabilitiesAlertDialog.Dispose();
             });
             displayCapabilitiesAlertDialog.Show();
         };



            CheckBox buttonCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.display_capabilities_add_button_capabilities_chk);
			Button addButtonCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.set_display_capabilities_add_button_capabilities_btn);
            ListView buttonCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.set_display_capabilities_button_capabilities_listview);
            List<ButtonCapabilities> btnCapList = new List<ButtonCapabilities>();

            if (tmpObj != null && tmpObj.getButtonCapabilities() != null)
			{
			  btnCapList.AddRange(tmpObj.getButtonCapabilities());
			}
			var buttonCapabilitiesAdapter = new ButtonCapabilitiesAdapter(Activity, btnCapList);
			buttonCapabilitiesListView.Adapter = buttonCapabilitiesAdapter;
			Utility.setListViewHeightBasedOnChildren(buttonCapabilitiesListView);

			buttonCapabilitiesCheckBox.CheckedChange += (sen, evn) => addButtonCapabilitiesButton.Enabled = evn.IsChecked;

			addButtonCapabilitiesButton.Click += delegate
			{
				AlertDialog.Builder btnCapabilitiesAlertDialog = new AlertDialog.Builder(Activity);
				View btnCapabilitiesView = (View)layoutInflater.Inflate(Resource.Layout.button_capabilities, null);
				btnCapabilitiesAlertDialog.SetView(btnCapabilitiesView);
				btnCapabilitiesAlertDialog.SetTitle("ButtonCapabilities");

				TextView textViewButtonName = (TextView)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_tv);

				Spinner spnButtonNames = (Spinner)btnCapabilitiesView.FindViewById(Resource.Id.get_capabilities_button_name_spn);
				string[] btnCapabilitiesButtonName = Enum.GetNames(typeof(HmiApiLib.ButtonName));
				var btnCapabilitiesButtonNameAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, btnCapabilitiesButtonName);
				spnButtonNames.Adapter = btnCapabilitiesButtonNameAdapter;


				CheckBox checkBoxShortPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.short_press_available_cb);

				CheckBox checkBoxLongPressAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.long_press_available_cb);

				CheckBox checkBoxUpDownAvailable = (CheckBox)btnCapabilitiesView.FindViewById(Resource.Id.up_down_available_cb);


				btnCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
			  {

				  ButtonCapabilities btn = new ButtonCapabilities();
				  btn.name = (ButtonName)spnButtonNames.SelectedItemPosition;

				  if (checkBoxShortPressAvailable.Checked)
					  btn.shortPressAvailable = true;
				  else
					  btn.shortPressAvailable = false;

				  if (checkBoxLongPressAvailable.Checked)
					  btn.longPressAvailable = true;
				  else
					  btn.longPressAvailable = false;

				  if (checkBoxUpDownAvailable.Checked)
					  btn.upDownAvailable = true;
				  else
					  btn.upDownAvailable = false;

                    btnCapList.Add(btn);
                    buttonCapabilitiesAdapter.NotifyDataSetChanged();

			  });

				btnCapabilitiesAlertDialog.SetPositiveButton(CANCEL, (senderAlert, args) =>
			 {
				 btnCapabilitiesAlertDialog.Dispose();
			 });
				btnCapabilitiesAlertDialog.Show();

				//  var namesAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, buttonNames);

				//  spnButtonNames.Adapter = namesAdapter;
			};

			

            CheckBox softButtonCapabilitiesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.set_display_capabilities_add_soft_button_capabilities_chk);
			Button addSoftButtonCapabilitiesButton = (Button)rpcView.FindViewById(Resource.Id.set_display_capabilities_add_soft_button_capabilities_btn);
            List<SoftButtonCapabilities> btnSoftButtonCapList = new List<SoftButtonCapabilities>();
			ListView softButtonCapabilitiesListView = (ListView)rpcView.FindViewById(Resource.Id.set_display_capabilities_soft_button_capabilities_listview);

			if (tmpObj != null && tmpObj.getSoftButtonCapabilities() != null)
			{
				btnSoftButtonCapList.AddRange(tmpObj.getSoftButtonCapabilities());
			}
			var softButtonCapabilitiesAdapter = new SoftButtonCapabilitiesAdapter(Activity, btnSoftButtonCapList);
			softButtonCapabilitiesListView.Adapter = softButtonCapabilitiesAdapter;
			Utility.setListViewHeightBasedOnChildren(softButtonCapabilitiesListView);


			softButtonCapabilitiesCheckBox.CheckedChange += (sen, evn) => addSoftButtonCapabilitiesButton.Enabled = evn.IsChecked;

			addSoftButtonCapabilitiesButton.Click += delegate
			 {
				 AlertDialog.Builder SoftButtonCapabilitiesAlertDialog = new AlertDialog.Builder(Activity);
				 View SoftButtonCapabilitiesView = (View)layoutInflater.Inflate(Resource.Layout.soft_button_capabilities, null);
				 SoftButtonCapabilitiesAlertDialog.SetView(SoftButtonCapabilitiesView);
				 SoftButtonCapabilitiesAlertDialog.SetTitle("SoftButtonCapabilities");

				 CheckBox checkBoxShortPressAvailable = (CheckBox)SoftButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_short_press_available_checkbox);

				 CheckBox checkBoxLongPressAvailable = (CheckBox)SoftButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_long_press_available_checkbox);

				 CheckBox checkBoxUpDownAvailable = (CheckBox)SoftButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_up_down_available_checkbox);

				 CheckBox checkBoxImageSupported = (CheckBox)SoftButtonCapabilitiesView.FindViewById(Resource.Id.soft_button_capabilities_image_supported_checkbox);


				 SoftButtonCapabilitiesAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
				 {

					 SoftButtonCapabilities btn = new SoftButtonCapabilities();

					 if (checkBoxShortPressAvailable.Checked)
						 btn.shortPressAvailable = true;
					 else
						 btn.shortPressAvailable = false;

					 if (checkBoxLongPressAvailable.Checked)
						 btn.longPressAvailable = true;
					 else
						 btn.longPressAvailable = false;

					 if (checkBoxUpDownAvailable.Checked)
						 btn.upDownAvailable = true;
					 else
						 btn.upDownAvailable = false;

					 if (checkBoxImageSupported.Checked)
						 btn.imageSupported = true;
					 else
						 btn.imageSupported = false;

					 btnSoftButtonCapList.Add(btn);
                     softButtonCapabilitiesAdapter.NotifyDataSetChanged();

				 });

				 SoftButtonCapabilitiesAlertDialog.SetPositiveButton(CANCEL, (senderAlert, args) =>
			  {
				  SoftButtonCapabilitiesAlertDialog.Dispose();
			  });
				 SoftButtonCapabilitiesAlertDialog.Show();

			 };




			CheckBox checkBoxOnScreenPresetsAvailable = (CheckBox)rpcView.FindViewById(Resource.Id.set_display_capabilities_preset_bank_capabilities_cb);

			PresetBankCapabilities prstCap = new PresetBankCapabilities();
			


			CheckBox resultCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.display_capabilities_result_code_chk);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.set_display_capabilities_result_code_spn);

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = resultCodeAdapter;


			if (tmpObj != null)
			{
				spnResultCode.SetSelection((int)tmpObj.getResultCode());
                if (tmpObj.getPresetBankCapabilities() != null)
                    checkBoxOnScreenPresetsAvailable.Checked = tmpObj.getPresetBankCapabilities().onScreenPresetsAvailable;
                else
                    checkBoxOnScreenPresetsAvailable.Checked = false;
			}

            resultCodeCheckBox.CheckedChange += (sen, evn) => spnResultCode.Enabled = evn.IsChecked;


			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			 {
				 rpcAlertDialog.Dispose();
			 });

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
		    {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                prstCap.onScreenPresetsAvailable = checkBoxOnScreenPresetsAvailable.Checked;

				if (resultCodeCheckBox.Checked)
				rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

                if (!displayCapabilitiesCheckBox.Checked)
                    dspCap = null;

                if (!buttonCapabilitiesCheckBox.Checked)
                    btnCapList = null;

				if (!softButtonCapabilitiesCheckBox.Checked)
				    btnSoftButtonCapList = null;
				if (!checkBoxOnScreenPresetsAvailable.Checked)
				    prstCap = null;
            

                RpcMessage rpcMessage = null;
                rpcMessage = BuildRpc.buildUiSetDisplayLayoutResponse(BuildRpc.getNextId(), rsltCode, dspCap, btnCapList, btnSoftButtonCapList, prstCap);
                AppUtils.savePreferenceValueForRpc(Activity, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
            });

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});
			rpcAlertDialog.Show();
		}

		private void CreateUIResponseSetGlobalProperties()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseSetGlobalProperties);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Context, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetGlobalProperties>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiSetGlobalPropertiesResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		void CreateUIResponseSetMediaClockTimer()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseSetMediaClockTimer);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Context, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.SetMediaClockTimer>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiSetMediaClockTimerResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseShow()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(UIResponseShow);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner resultCodeSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Context, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpinner.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (s, e) => resultCodeSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.Show tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Show();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Show)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Show>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				resultCodeSpinner.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpinner.SelectedItemPosition;
				}

				RpcResponse rpcMessage = BuildRpc.buildUiShowResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}


		private void CreateUIResponseShowCustomForm()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.alert_ui_response, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UIResponseShowCustomForm);

			CheckBox infoCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.alert_try_again_time_check);			
			EditText infoEditText = (EditText)rpcView.FindViewById(Resource.Id.alert_try_again_time_et);			

			CheckBox resultCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.alert_resultcode_check);
			Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.alert_resultcode_spn);

            infoCheckBox.Text = "Info";
            infoEditText.InputType = Android.Text.InputTypes.ClassText;
            infoEditText.Text = "DEFAULT";
			resultCodeCheckBox.Text = "ResultCode";

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpn.Adapter = adapter;

            infoCheckBox.CheckedChange += (s, e) => infoEditText.Enabled = e.IsChecked;
			resultCodeCheckBox.CheckedChange += (s, e) => resultCodeSpn.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.ShowCustomForm>(Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				resultCodeSpn.SetSelection((int)tmpObj.getResultCode());

				if (tmpObj.getInfo() != null)
                    infoEditText.Text = tmpObj.getInfo();
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				String info = null;

				if (infoCheckBox.Checked && infoEditText.Text != null && infoEditText.Text.Length > 0)
					info = infoEditText.Text;

				if (resultCodeCheckBox.Checked)
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;

				RpcResponse rpcMessage = BuildRpc.buildUiShowCustomFormResponse(BuildRpc.getNextId(), rsltCode, info);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateUIResponseSlider()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.slider_response, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UIResponseSlider);

			CheckBox slider_position_checkbox = (CheckBox)rpcView.FindViewById(Resource.Id.slider_position_checkbox);
			EditText slider_position_edittext = (EditText)rpcView.FindViewById(Resource.Id.slider_position_et);
            slider_position_edittext.Text = "1";

			CheckBox resultCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.slider_result_code_checkbox);
			Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.slider_result_code_spinner);

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			resultCodeSpn.Adapter = resultCodeAdapter;

			slider_position_checkbox.CheckedChange += (s, e) => slider_position_edittext.Enabled = e.IsChecked;
			resultCodeCheckBox.CheckedChange += (s, e) => resultCodeSpn.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutgoingResponses.Slider tmpObj = new HmiApiLib.Controllers.UI.OutgoingResponses.Slider();
			tmpObj = (HmiApiLib.Controllers.UI.OutgoingResponses.Slider)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutgoingResponses.Slider>(Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				resultCodeSpn.SetSelection((int)tmpObj.getResultCode());

                if (tmpObj.getSliderPosition() != null)
					slider_position_edittext.Text = tmpObj.getSliderPosition().ToString();
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				int? sliderPosition = null;

				if (slider_position_checkbox.Checked && slider_position_edittext.Text != null && slider_position_edittext.Text.Length > 0)
					sliderPosition = Java.Lang.Integer.ParseInt(slider_position_edittext.Text);

				if (resultCodeCheckBox.Checked)
					rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;

				RpcResponse rpcMessage = BuildRpc.buildUiSliderResponse(BuildRpc.getNextId(), rsltCode, sliderPosition);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});
			rpcAlertDialog.Show();
		}

        private void CreateUINotificationOnCommand()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_command_notification, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UINotificationOnCommand);

            CheckBox cmdIdCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_command_cmd_id_check);
            Spinner spnCmdId = (Spinner)rpcView.FindViewById(Resource.Id.on_command_cmd_id_spinner);

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_check);

            EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_et);
            Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_spinner);
            registerdAppIdSpn.Enabled = false;

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.sdl_on_command_app_id_manual_radioGroup);

            RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_command_app_id_manual);
            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_command_app_id_registered_apps);

            cmdIdCheck.CheckedChange += (s, e) => spnCmdId.Enabled = e.IsChecked;

            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
                if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };


            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
                manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
                registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };

            List<int?> cmdIDList = new List<int?>();

            List<int> appIds = new List<int>();
            foreach (AppItem item in AppInstanceManager.appList)
            {
                appIds.Add(item.getAppID());
            }

            int[] appIDArray = appIds.ToArray();

            for (int i = 0; i < AppInstanceManager.commandIdList.Count; i++)
            {
                List<int?> commandIdList = AppInstanceManager.commandIdList[appIDArray[i]];
                cmdIDList.AddRange(commandIdList);
            }

            var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
            registerdAppIdSpn.Adapter = appIDAdapter;

            var cmdIdAdapter = new ArrayAdapter<int?>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, cmdIDList);
            spnCmdId.Adapter = cmdIdAdapter;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnCommand tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnCommand();
            tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnCommand)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnCommand>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getAppId() != null)
                    manualAppIdEditText.Text = tmpObj.getAppId().ToString();
                if (tmpObj.getCmdID() != null)
                    spnCmdId.SetSelection((int)tmpObj.getCmdID());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null, cmdID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))

                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = appIds[registerdAppIdSpn.SelectedItemPosition];

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }

                 if (cmdIdCheck.Checked)
                     cmdID = (int)spnCmdId.SelectedItem;

                 RequestNotifyMessage rpcMessage = BuildRpc.buildUIOnCommand(cmdID, selectedAppID);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

		private void CreateUINotificationOnDriverDistraction()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UINotificationOnDriverDistraction);

			CheckBox driverDistractionStateCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner driverDistractionStateSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);
            driverDistractionStateCheckbox.Text = "Driver Distraction State";

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, driverDistractionState);
			driverDistractionStateSpinner.Adapter = adapter;

            driverDistractionStateCheckbox.CheckedChange += (s, e) => driverDistractionStateSpinner.Enabled = e.IsChecked;

			HmiApiLib.Controllers.UI.OutGoingNotifications.OnDriverDistraction tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnDriverDistraction();
			tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnDriverDistraction)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnDriverDistraction>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null && tmpObj.getDriverDistractionState() != null)
			{
				driverDistractionStateSpinner.SetSelection((int)tmpObj.getDriverDistractionState());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});					
			
			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
                DriverDistractionState? driverDistractionsState = null;
				if (driverDistractionStateCheckbox.Checked)
				{
                    driverDistractionsState = (DriverDistractionState)driverDistractionStateSpinner.SelectedItemPosition;
				}

				RequestNotifyMessage rpcMessage = BuildRpc.buildUIOnDriverDistraction(driverDistractionsState);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});
			rpcAlertDialog.Show();
		}

		private void CreateUINotificationOnKeyboardInput()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.slider_response, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UINotificationOnKeyboardInput);

			CheckBox dataCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.slider_position_checkbox);
			EditText dataEditText = (EditText)rpcView.FindViewById(Resource.Id.slider_position_et);

			CheckBox keyboardEventCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.slider_result_code_checkbox);
			Spinner keyboardEventSpinner = (Spinner)rpcView.FindViewById(Resource.Id.slider_result_code_spinner);

            dataCheckbox.Text = "Data";
            dataEditText.Text = "Data";
            dataEditText.InputType = Android.Text.InputTypes.ClassText;
            			
			var keyBoardEventAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, keyBoardEvent);
			keyboardEventSpinner.Adapter = keyBoardEventAdapter;

            dataCheckbox.CheckedChange += (s, e) => dataEditText.Enabled = e.IsChecked;
            keyboardEventCheckBox.CheckedChange += (s, e) => keyboardEventSpinner.Enabled = e.IsChecked;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnKeyboardInput tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnKeyboardInput();
            tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnKeyboardInput)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnKeyboardInput>(Context, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getKeyboardEvent() != null)
                    keyboardEventSpinner.SetSelection((int)tmpObj.getKeyboardEvent());

                if (tmpObj.getData() != null)
                    dataEditText.Text = tmpObj.getData();
            }

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{

				KeyboardEvent? keyBoardEvent = null;
				String data = "";

				if (dataCheckbox.Checked && dataEditText.Text != null && dataEditText.Text.Length > 0)
					data = dataEditText.Text;

				if (keyboardEventCheckBox.Checked)
					keyBoardEvent = (KeyboardEvent)keyboardEventSpinner.SelectedItemPosition;

				RequestNotifyMessage rpcMessage = BuildRpc.buildUIOnKeyboardInput(keyBoardEvent, data);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateUINotificationOnLanguageChange()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UINotificationOnLanguageChange);

			CheckBox languageCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			languageCheckBox.Text = "Language";
			Spinner spnLanguage = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			languageCheckBox.CheckedChange += (sender, e) => spnLanguage.Enabled = e.IsChecked;

			string[] language = Enum.GetNames(typeof(Language));
			var languageAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, language);
			spnLanguage.Adapter = languageAdapter;

			HmiApiLib.Controllers.UI.OutGoingNotifications.OnLanguageChange tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnLanguageChange();
			tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnLanguageChange)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnLanguageChange>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				if (tmpObj.getLanguage() != null)
					spnLanguage.SetSelection((int)tmpObj.getLanguage());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				Language? lang = null;
				if (languageCheckBox.Checked)
				{
					lang = (Language)spnLanguage.SelectedItemPosition;
				}

				RequestNotifyMessage rpcMessage = BuildRpc.buildUIOnLanguageChange(lang);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});
            rpcAlertDialog.Show();
		}

		private void CreateUINotificationOnRecordStart()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Context);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UINotificationOnRecordStart);

			CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner appIDSpinner = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            appIDCheckBox.Text = "App ID";

            appIDCheckBox.CheckedChange += (sender, e) => appIDSpinner.Enabled = e.IsChecked;

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			appIDSpinner.Adapter = appIDAdapter;
			
			HmiApiLib.Controllers.UI.OutGoingNotifications.OnRecordStart tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnRecordStart();
			tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnRecordStart)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnRecordStart>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
                if(tmpObj.getAppId() != null)
				    appIDSpinner.SetSelection((int)tmpObj.getAppId());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				int? appId = null;
				if (appIDCheckBox.Checked)
				{
					appId = appIDSpinner.SelectedItemPosition;
				}

                RequestNotifyMessage rpcMessage = BuildRpc.buildUIOnRecordStart(appId);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});
			rpcAlertDialog.Show();
		}

        private void CreateUINotificationOnResetTimeout()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_reset_timeout, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UINotificationOnResetTimeout);

            CheckBox CheckBoxMethodName = (CheckBox)rpcView.FindViewById(Resource.Id.tts_notification_method_name_cb);
            EditText editTextMethodName = (EditText)rpcView.FindViewById(Resource.Id.tts_notification_method_name_et);
            editTextMethodName.Text = "Method Name";

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_notification_app_id_checkbox);

            EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.tts_notification_app_id_et);
            Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.tts_notification_app_id_spinner);
            registerdAppIdSpn.Enabled = false;

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.tts_notification_app_id_manual_radioGroup);

            RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.tts_notification_app_id_manual);
            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.tts_notification_app_id_registered_apps);

            CheckBoxMethodName.CheckedChange += (sender, e) => editTextMethodName.Enabled = e.IsChecked;
            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
                if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };

            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
                manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
                registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };

            var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
            registerdAppIdSpn.Adapter = appIDAdapter;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnResetTimeout tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnResetTimeout();
            tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnResetTimeout)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnResetTimeout>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getAppId() != null)
                    manualAppIdEditText.Text = tmpObj.getAppId().ToString();

                if (tmpObj.getMethodName() != null)
                    editTextMethodName.Text = tmpObj.getMethodName();
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int selectedAppID = 0;
                 String methodName = null;

                 if (CheckBoxMethodName.Checked && editTextMethodName.Text != null && editTextMethodName.Text.Length > 0)
                     methodName = editTextMethodName.Text;

                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))

                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }

                 RequestNotifyMessage rpcMessage = BuildRpc.buildUIOnResetTimeout(selectedAppID, methodName);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });

            rpcAlertDialog.Show();
        }


        private void CreateUINotificationOnSystemContext()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_command_notification, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(UINotificationOnSystemContext);

            CheckBox systemContextCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_command_cmd_id_check);
            Spinner spnSystemContext = (Spinner)rpcView.FindViewById(Resource.Id.on_command_cmd_id_spinner);
            systemContextCheck.Text = "System Context";

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_check);
            EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_et);
            Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.sdl_on_command_app_id_spinner);
            registerdAppIdSpn.Enabled = false;

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.sdl_on_command_app_id_manual_radioGroup);
            RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_command_app_id_manual);
            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_command_app_id_registered_apps);

            systemContextCheck.CheckedChange += (sender, e) => spnSystemContext.Enabled = e.IsChecked;
            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
                if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };


            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
                manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
                registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };

            var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
            registerdAppIdSpn.Adapter = appIDAdapter;

            var systemContextAdapter = new ArrayAdapter<string>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, systemContext);
            spnSystemContext.Adapter = systemContextAdapter;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnSystemContext tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnSystemContext();
            tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnSystemContext)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnSystemContext>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getAppId() != null)
                    manualAppIdEditText.Text = tmpObj.getAppId().ToString();

                spnSystemContext.SetSelection((int)tmpObj.getSystemContext());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null;
                 SystemContext? systemContext = null;

                 if (systemContextCheck.Checked)
                     systemContext = (SystemContext)spnSystemContext.SelectedItemPosition;

                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))

                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text.Equals(""))
                             selectedAppID = 0;
                         else
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }
                 RequestNotifyMessage rpcMessage = BuildRpc.buildUiOnSystemContext(systemContext, selectedAppID);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

		private void CreateUINotificationOnTouchEvent()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_touch_event_notification, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(UINotificationOnTouchEvent);

			CheckBox touchTypeCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_touch_event_touch_type_checkbox);
			Spinner spnTouchType = (Spinner)rpcView.FindViewById(Resource.Id.on_touch_event_touch_type_spinner);

            CheckBox touchEventCheck = (CheckBox)rpcView.FindViewById(Resource.Id.on_touch_event_button_checkbox);
            Button createTouchEvent = (Button)rpcView.FindViewById(Resource.Id.on_touch_event_create_touch_event);

			ListView listViewTouchEvent = (ListView)rpcView.FindViewById(Resource.Id.touch_event_listview);

			touchTypeCheck.CheckedChange += (sender, e) => spnTouchType.Enabled = e.IsChecked;
			touchEventCheck.CheckedChange += (sender, e) => createTouchEvent.Enabled = e.IsChecked;

			string[] touchType = Enum.GetNames(typeof(TouchType));
			var touchTypeAdapter = new ArrayAdapter<string>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, touchType);
			spnTouchType.Adapter = touchTypeAdapter;

            List<TouchEvent> touchEventsList = new List<TouchEvent>();
            var touchEventAdapter = new TouchEventAdapter(Activity, touchEventsList);
			listViewTouchEvent.Adapter = touchEventAdapter;

            HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent tmpObj = new HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent();
			tmpObj = (HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.UI.OutGoingNotifications.OnTouchEvent>(Activity, tmpObj.getMethod());
             
            if(tmpObj != null)
            {
                if(tmpObj.getTouchType() != null)
                    spnTouchType.SetSelection((int)tmpObj.getTouchType());

                if (tmpObj.getTouchEvent() != null)
				{
                    touchEventsList.AddRange(tmpObj.getTouchEvent());
					touchEventAdapter.NotifyDataSetChanged();
				}
            }

			createTouchEvent.Click += (sender, e) =>
			{
				List<TouchCoord> touchCoordList = new List<TouchCoord>();
				AlertDialog.Builder touchEventAlertDialog = new AlertDialog.Builder(rpcAlertDialog.Context);
				View touchEventView = layoutInflater.Inflate(Resource.Layout.touch_event, null);
				touchEventAlertDialog.SetView(touchEventView);
				touchEventAlertDialog.SetTitle("Touch Event");

				CheckBox touchEventIdCheckbox = (CheckBox)touchEventView.FindViewById(Resource.Id.touch_event_id_checkbox);
				EditText touchEventIdEditText = (EditText)touchEventView.FindViewById(Resource.Id.touch_event_id_edit_text);

				CheckBox touchEventTsCheckbox = (CheckBox)touchEventView.FindViewById(Resource.Id.touch_event_ts_checkbox);
				EditText touchEventTsEditText = (EditText)touchEventView.FindViewById(Resource.Id.touch_event_ts_edittext);

				CheckBox touchEventCordCheckbox = (CheckBox)touchEventView.FindViewById(Resource.Id.touch_event_cord_checkbox);				
				Button createTouchCordButton = (Button)touchEventView.FindViewById(Resource.Id.create_touch_cord_button);

                ListView touchCordListView = (ListView)touchEventView.FindViewById(Resource.Id.touch_cord_list_view);

				var touchCoordAdapter = new TouchCoordAdapter(Activity, touchCoordList);
				touchCordListView.Adapter = touchCoordAdapter;
                Utility.setListViewHeightBasedOnChildren(touchCordListView);

				touchEventIdCheckbox.CheckedChange += (send, e1) => touchEventIdEditText.Enabled = e1.IsChecked;
				touchEventTsCheckbox.CheckedChange += (send, e1) => touchEventTsEditText.Enabled = e1.IsChecked;
				touchEventCordCheckbox.CheckedChange += (send, e1) => createTouchCordButton.Enabled = e1.IsChecked;

				createTouchCordButton.Click += (sender1, e1) =>
				{
					AlertDialog.Builder touchCoordAlertDialog = new AlertDialog.Builder(rpcAlertDialog.Context);
					View touchCoordView = layoutInflater.Inflate(Resource.Layout.touch_cord, null);
					touchCoordAlertDialog.SetView(touchCoordView);
					touchCoordAlertDialog.SetTitle("Touch Coord");

					CheckBox xCheckBox = (CheckBox)touchCoordView.FindViewById(Resource.Id.touch_cord_x_checkbox);
					EditText xEditText = (EditText)touchCoordView.FindViewById(Resource.Id.touch_cord_x_edittext);

					CheckBox yCheckBox = (CheckBox)touchCoordView.FindViewById(Resource.Id.touch_cord_y_checkbox);
					EditText yEditText = (EditText)touchCoordView.FindViewById(Resource.Id.touch_cord_y_edittext);

					xCheckBox.CheckedChange += (send, e2) => xEditText.Enabled = e2.IsChecked;
					yCheckBox.CheckedChange += (send, e2) => yEditText.Enabled = e2.IsChecked;

					touchCoordAlertDialog.SetNegativeButton(CANCEL, (senderAlert, args) =>
					{
						touchCoordAlertDialog.Dispose();
					});

					touchCoordAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
					{
						TouchCoord coord = new TouchCoord();
						try
						{
                            if(xCheckBox.Checked)
							    coord.x = Int32.Parse(xEditText.Text);

                            if(yCheckBox.Checked)
							    coord.y = Int32.Parse(yEditText.Text);
						}
						catch (Exception e11)
						{

						}
						touchCoordList.Add(coord);
						touchCoordAdapter.NotifyDataSetChanged();
					});

					touchCoordAlertDialog.Show();
				};


				touchEventAlertDialog.SetNegativeButton(CANCEL, (senderAlert, args) =>
				{
					touchEventAlertDialog.Dispose();
				});

				touchEventAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
				{
					TouchEvent touchEvent = new TouchEvent();
					try
					{
                        if(touchEventIdCheckbox.Checked)
						    touchEvent.id = Int32.Parse(touchEventIdEditText.Text);
					}
					catch (Exception e2)
					{
						touchEvent.id = 0;
					}

                    if (touchEventTsCheckbox.Checked)
                    {
                        List<int> tsList = new List<int>();
                        string[] t = touchEventTsEditText.Text.Split(',');
                        foreach (string ts in t)
                        {
                            try
                            {
                                tsList.Add(Int32.Parse(ts));
                            }
                            catch (Exception e3)
                            {

                            }
                        }
                        touchEvent.ts = tsList;
                    }

					touchEvent.c = touchCoordList;

					touchEventsList.Add(touchEvent);
					touchEventAdapter.NotifyDataSetChanged();
				});

				touchEventAlertDialog.Show();
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			 {
                HmiApiLib.Common.Enums.TouchType? tchType = null;

				 if (touchTypeCheck.Checked)
                    tchType = (HmiApiLib.Common.Enums.TouchType)spnTouchType.SelectedItemPosition;

				 if (!touchEventCheck.Checked)
					 touchEventsList = null;

				 RequestNotifyMessage rpcMessage = null;
				 rpcMessage = BuildRpc.buildUiOnTouchEvent(tchType, touchEventsList);
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
			 });

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

        private void CreateTTSResponseChangeRegistration()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle(TTSResponseChangeRegistration);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner resultCodeSpn = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            resultCodeSpn.Adapter = adapter;

            rsltCodeCheckBox.CheckedChange += (s, e) =>
           {
               resultCodeSpn.Enabled = e.IsChecked;
           };

            HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.ChangeRegistration>(adapter.Context, tmpObj.getMethod());
            if (tmpObj != null)
            {
                resultCodeSpn.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (rsltCodeCheckBox.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)resultCodeSpn.SelectedItemPosition;

                RpcResponse rpcMessage = BuildRpc.buildTTSChangeRegistrationResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        private void CreateTTSResponseSetGlobalProperties()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
            rpcAlertDialog.SetView(rpcView);

            rpcAlertDialog.SetTitle(TTSResponseSetGlobalProperties);

            CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnResultCode.Adapter = adapter;

            rsltCodeCheckBox.CheckedChange += (s, e) =>
            {
                spnResultCode.Enabled = e.IsChecked;
            };

            HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties();
            tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.SetGlobalProperties>(adapter.Context, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (rsltCodeCheckBox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                RpcResponse rpcMessage = BuildRpc.buildTTSSetGlobalPropertiesResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

		private void CreateTTSResponseSpeak()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(TTSResponseSpeak);

			CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = adapter;

			rsltCodeCheckBox.CheckedChange += (s, e) =>
		    {
                spnResultCode.Enabled = e.IsChecked;
		    };

			HmiApiLib.Controllers.TTS.OutgoingResponses.Speak tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.Speak();
			tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.Speak)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.Speak>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnResultCode.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (rsltCodeCheckBox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildTtsSpeakResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateTTSResponseStopSpeaking()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(TTSResponseStopSpeaking);

			CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = adapter;

			rsltCodeCheckBox.CheckedChange += (s, e) =>
		    {
                spnResultCode.Enabled = e.IsChecked;
		    };

			HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking tmpObj = new HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking();
			tmpObj = (HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutgoingResponses.StopSpeaking>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnResultCode.SetSelection((int)tmpObj.getResultCode());
			}

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (rsltCodeCheckBox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildTtsStopSpeakingResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

        private void CreateTTSNotificationOnLanguageChange()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox onLanuageChangeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner onLanguageChangeSpn = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, languages);
            onLanguageChangeSpn.Adapter = adapter;

            onLanuageChangeCheckBox.CheckedChange += (s, e) =>
           {
               onLanguageChangeSpn.Enabled = e.IsChecked;
           };

            HmiApiLib.Controllers.TTS.OutGoingNotifications.OnLanguageChange tmpObj = new HmiApiLib.Controllers.TTS.OutGoingNotifications.OnLanguageChange();
            tmpObj = (HmiApiLib.Controllers.TTS.OutGoingNotifications.OnLanguageChange)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutGoingNotifications.OnLanguageChange>(Activity, tmpObj.getMethod());
            if (tmpObj != null && null != tmpObj.getLanguage())
            {
                onLanguageChangeSpn.SetSelection((int)tmpObj.getLanguage());
            }

            onLanuageChangeCheckBox.Text = "Language";

            rpcAlertDialog.SetTitle(TTSNotificationOnLanguageChange);
            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
                Language? languages = null;
                if (onLanuageChangeCheckBox.Checked)
                {
                    languages = (Language)onLanguageChangeSpn.SelectedItemPosition;
                }

                RequestNotifyMessage rpcResponse = BuildRpc.buildTtsOnLanguageChange(languages);
                AppUtils.savePreferenceValueForRpc(Activity, rpcResponse.getMethod(), rpcResponse);
                AppInstanceManager.Instance.sendRpc(rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.Show();
        }

        private void CreateTTSNotificationOnResetTimeout()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_reset_timeout, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.tts_notification_app_id_checkbox);

            EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.tts_notification_app_id_et);
            Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.tts_notification_app_id_spinner);
            registerdAppIdSpn.Enabled = false;

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.tts_notification_app_id_manual_radioGroup);
            RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.tts_notification_app_id_manual);
            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.tts_notification_app_id_registered_apps);

            CheckBox checkBoxMethodName = (CheckBox)rpcView.FindViewById(Resource.Id.tts_notification_method_name_cb);
            EditText editTextMethodName = (EditText)rpcView.FindViewById(Resource.Id.tts_notification_method_name_et);
            editTextMethodName.Text = "Method Name";

            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
                if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };

            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
                manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
                registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };

            checkBoxMethodName.CheckedChange += (s, e) =>
            {
                editTextMethodName.Enabled = e.IsChecked;
            };

            var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
            registerdAppIdSpn.Adapter = appIDAdapter;

            rpcAlertDialog.SetTitle(TTSNotificationOnResetTimeout);

            HmiApiLib.Controllers.TTS.OutGoingNotifications.OnResetTimeout tmpObj = new HmiApiLib.Controllers.TTS.OutGoingNotifications.OnResetTimeout();
            tmpObj = (HmiApiLib.Controllers.TTS.OutGoingNotifications.OnResetTimeout)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.TTS.OutGoingNotifications.OnResetTimeout>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (null != tmpObj.getAppId())
                {
                    manualAppIdEditText.Text = tmpObj.getAppId().ToString();
                }
                editTextMethodName.Text = tmpObj.getMethodName();
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))

                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }
                 String methodName = null;
                 if (checkBoxMethodName.Checked)
                 {
                     methodName = editTextMethodName.Text;
                 }
                 RequestNotifyMessage rpcMessage = BuildRpc.buildTtsOnResetTimeout(selectedAppID, methodName);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });

            rpcAlertDialog.Show();
        }

        void CreateTTSNotificationStarted()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(TTSNotificationStarted);

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RequestNotifyMessage rpcMessage = BuildRpc.buildTtsStartedNotification();
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {

             });

            rpcAlertDialog.Show();
        }

        void CreateTTSNotificationStopped()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(TTSNotificationStopped);

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RequestNotifyMessage rpcMessage = BuildRpc.buildTtsStoppedNotification();
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {

             });

            rpcAlertDialog.Show();
        }


        private void CreateSDLRequestActivateApp()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_app_activated, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_check);

            EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_et);
            Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_spinner);
            registerdAppIdSpn.Enabled = false;

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.bc_on_activated_app_id_manual_radioGroup);
            RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_manual);
            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_registered_apps);

            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
                if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };

            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
                manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
                registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };

            var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
            registerdAppIdSpn.Adapter = appIDAdapter;

            HmiApiLib.Controllers.SDL.OutgoingRequests.ActivateApp tmpObj = new HmiApiLib.Controllers.SDL.OutgoingRequests.ActivateApp();
            tmpObj = (HmiApiLib.Controllers.SDL.OutgoingRequests.ActivateApp)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutgoingRequests.ActivateApp>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (null != tmpObj.getAppId())
                {
                    manualAppIdEditText.Text = tmpObj.getAppId().ToString();
                }
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetTitle(SDLRequestActivateApp);

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))

                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }
                 RpcRequest rpcMessage = BuildRpc.buildSdlActivateAppRequest(BuildRpc.getNextId(), selectedAppID);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });

            rpcAlertDialog.Show();
        }

        private void CreateSDLRequestGetListOfPermissions()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_app_activated, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_check);
            EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_et);
            Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_spinner);
            registerdAppIdSpn.Enabled = false;

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.bc_on_activated_app_id_manual_radioGroup);
            RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_manual);
            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_registered_apps);

            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
                if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };

            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
                manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
                registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };

            var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
            registerdAppIdSpn.Adapter = appIDAdapter;

            HmiApiLib.Controllers.SDL.OutgoingRequests.GetListOfPermissions tmpObj = new HmiApiLib.Controllers.SDL.OutgoingRequests.GetListOfPermissions();
            tmpObj = (HmiApiLib.Controllers.SDL.OutgoingRequests.GetListOfPermissions)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutgoingRequests.GetListOfPermissions>(Activity, tmpObj.getMethod());
            if (tmpObj != null && tmpObj.getAppId() != null)
            {
                manualAppIdEditText.Text = tmpObj.getAppId().ToString();
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetTitle(SDLRequestGetListOfPermissions);

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))
                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }
                 RpcRequest rpcMessage = BuildRpc.buildSDLGetListOfPermissionsRequest(BuildRpc.getNextId(), selectedAppID);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });

            rpcAlertDialog.Show();
        }

        private void CreateSDLRequestGetStatusUpdate()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(SDLRequestGetStatusUpdate);

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
             {
				 RpcRequest rpcMessage = BuildRpc.buildSDLGetStatusUpdateRequest(BuildRpc.getNextId());
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {

             });

            rpcAlertDialog.Show();
        }

        private void CreateSDLRequestGetURLS()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.sdl_get_urls, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(SDLRequestGetURLS);

            CheckBox serviceCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.sdl_get_url_check);
            EditText editTextService = (EditText)rpcView.FindViewById(Resource.Id.sdl_get_urls_et);

            serviceCheckBox.CheckedChange += (s, e) => editTextService.Enabled = e.IsChecked;

            HmiApiLib.Controllers.SDL.OutgoingRequests.GetURLS tmpObj = new HmiApiLib.Controllers.SDL.OutgoingRequests.GetURLS();
            tmpObj = (HmiApiLib.Controllers.SDL.OutgoingRequests.GetURLS)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutgoingRequests.GetURLS>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getService() != null)
                    editTextService.Text = tmpObj.getService().ToString();
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
             {
                 int? service = null;
                if (serviceCheckBox.Checked && editTextService.Text != null && editTextService.Text.Length > 0)
                 {
                     service = Java.Lang.Integer.ParseInt(editTextService.Text);
                 }
                 RpcRequest rpcMessage = BuildRpc.buildSDLGetURLSRequest(BuildRpc.getNextId(), service);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
             });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });

            rpcAlertDialog.Show();
        }

        private void CreateSDLRequestGetUserFriendlyMessage()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.alert_ui_response, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(SDLRequestGetUserFriendlyMessage);

            CheckBox checkBoxMessageCode = (CheckBox)rpcView.FindViewById(Resource.Id.alert_try_again_time_check);
            checkBoxMessageCode.Text = "MessageCode";
            EditText editTextMessageCode = (EditText)rpcView.FindViewById(Resource.Id.alert_try_again_time_et);
            editTextMessageCode.InputType = Android.Text.InputTypes.ClassText;
            editTextMessageCode.Text = "Message 1, Message 2";
            rpcView.FindViewById(Resource.Id.alert_text_hint).Visibility = ViewStates.Visible;

            CheckBox checkBoxlanguage = (CheckBox)rpcView.FindViewById(Resource.Id.alert_resultcode_check);
            checkBoxlanguage.Text = "Language";

            Spinner spnlanguage = (Spinner)rpcView.FindViewById(Resource.Id.alert_resultcode_spn);
            string[] language = Enum.GetNames(typeof(Language));
            var languageAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, language);
            spnlanguage.Adapter = languageAdapter;

            checkBoxMessageCode.CheckedChange += (s, e) => editTextMessageCode.Enabled = e.IsChecked;
            checkBoxlanguage.CheckedChange += (s, e) => spnlanguage.Enabled = e.IsChecked;

            HmiApiLib.Controllers.SDL.OutgoingRequests.GetUserFriendlyMessage tmpObj = new HmiApiLib.Controllers.SDL.OutgoingRequests.GetUserFriendlyMessage();
            tmpObj = (HmiApiLib.Controllers.SDL.OutgoingRequests.GetUserFriendlyMessage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutgoingRequests.GetUserFriendlyMessage>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getMessageCodes() != null)
                {
                    List<string> messageCodeList = tmpObj.getMessageCodes();

                    string[] messageDataRsltArray = messageCodeList.ToArray();

                    for (int i = 0; i < messageCodeList.Count; i++)
                    {
                        if (i == messageCodeList.Count - 1)
                        {
                            editTextMessageCode.Append(messageDataRsltArray[i]);
                            break;
                        }
                        editTextMessageCode.Append(messageDataRsltArray[i] + ",");
                    }
                }

                if (tmpObj.getLanguage() != null)
                    spnlanguage.SetSelection((int)tmpObj.getLanguage());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                List<String> messageCodes = null;
                Language? selectedLanguage = null;
                if (checkBoxMessageCode.Checked)
                {
                    messageCodes = new List<string>();
                    if (editTextMessageCode.Text != null && editTextMessageCode.Text.Length > 0)
                    {
                        messageCodes.AddRange(editTextMessageCode.Text.Split(','));
                    }
                }
                if (checkBoxlanguage.Checked)
                {
                    selectedLanguage = (HmiApiLib.Common.Enums.Language)spnlanguage.SelectedItemPosition;
                }
                RpcRequest rpcMessage = BuildRpc.buildSDLGetUserFriendlyMessageRequest(BuildRpc.getNextId(), messageCodes, selectedLanguage);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });

            rpcAlertDialog.Show();
        }

        private void CreateSDLRequestUpdateSDL()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(SDLRequestUpdateSDL);

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
             {
                 RpcRequest rpcMessage = BuildRpc.buildSDLUpdateSDLRequest(BuildRpc.getNextId());
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {

             });

            rpcAlertDialog.Show();
        }

        private void CreateSDLNotificationOnAllowSDLFunctionality()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_allow_sdl_functionality, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(SDLNotificationOnAllowSDLFunctionality);

            CheckBox checkBoxDeviceInfo = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_device_info_chk);
            CheckBox checkBoxDeviceName = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_device_name_check);
            EditText editTextDeviceName = (EditText)rpcView.FindViewById(Resource.Id.device_name_sdl_et);

            CheckBox checkBoxDeviceId = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_device_id_sdl_check);
            EditText editTextDeviceId = (EditText)rpcView.FindViewById(Resource.Id.device_id_sdl_et);

            CheckBox checkBoxTransportType = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_transport_type_sdl_check);
            Spinner spnTransportType = (Spinner)rpcView.FindViewById(Resource.Id.transport_type_sdl_spn);

            CheckBox checkBoxIsSDLAllowed = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_is_sdl_allowed_check);
            CheckBox checkBoxAllowed = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_allowed_check);

            CheckBox checkBoxConsentedSource = (CheckBox)rpcView.FindViewById(Resource.Id.on_allow_sdl_consent_source_check);
            Spinner spnConsentSource = (Spinner)rpcView.FindViewById(Resource.Id.consent_source_spn);

            string[] transportType = Enum.GetNames(typeof(HmiApiLib.Common.Enums.TransportType));
            var transportTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, transportType);
            spnTransportType.Adapter = transportTypeAdapter;

            string[] consentSource = Enum.GetNames(typeof(HmiApiLib.Common.Enums.ConsentSource));
            var consentSourceAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, consentSource);
            spnConsentSource.Adapter = consentSourceAdapter;

            HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAllowSDLFunctionality tmpObj = new HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAllowSDLFunctionality();
            tmpObj = (HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAllowSDLFunctionality)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAllowSDLFunctionality>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getDevice() != null)
                {
                    if (tmpObj.getDevice().getName() != null)
                        editTextDeviceName.Text = tmpObj.getDevice().getName();

                    if (tmpObj.getDevice().getId() != null)
                        editTextDeviceId.Text = tmpObj.getDevice().getId();

                    spnTransportType.SetSelection((int)tmpObj.getDevice().getTransportType());
                    checkBoxIsSDLAllowed.Checked = tmpObj.getDevice().getIsSDLAllowed();
                }

                if (tmpObj.getAllowed() != null)
                    checkBoxAllowed.Checked = (bool)tmpObj.getAllowed();

                if (tmpObj.getSource() != null)
                    spnConsentSource.SetSelection((int)tmpObj.getSource());
            }

            checkBoxDeviceInfo.CheckedChange += (sender, e) =>
            {
                checkBoxDeviceName.Enabled = e.IsChecked;
                editTextDeviceName.Enabled = e.IsChecked;
                checkBoxDeviceId.Enabled = e.IsChecked;
                editTextDeviceId.Enabled = e.IsChecked;
                checkBoxTransportType.Enabled = e.IsChecked;
                spnTransportType.Enabled = e.IsChecked;
                checkBoxIsSDLAllowed.Enabled = e.IsChecked;
            };
            checkBoxDeviceName.CheckedChange += (s, e) => editTextDeviceName.Enabled = e.IsChecked;
            checkBoxDeviceId.CheckedChange += (s, e) => editTextDeviceId.Enabled = e.IsChecked;
            checkBoxTransportType.CheckedChange += (s, e) => spnTransportType.Enabled = e.IsChecked;
            checkBoxConsentedSource.CheckedChange += (s, e) => spnConsentSource.Enabled = e.IsChecked;

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
                DeviceInfo devInfo = null;
                if (checkBoxDeviceInfo.Checked)
                {
                    devInfo = new DeviceInfo();
                    if (checkBoxDeviceName.Checked)
                        devInfo.name = editTextDeviceName.Text;
                    if (checkBoxDeviceId.Checked)
                        devInfo.id = editTextDeviceId.Text;
                    if (checkBoxTransportType.Checked)
                        devInfo.transportType = (HmiApiLib.Common.Enums.TransportType)spnTransportType.SelectedItemPosition;
                    devInfo.isSDLAllowed = checkBoxIsSDLAllowed.Checked;
                }
                ConsentSource? source = null;
                if (checkBoxConsentedSource.Checked)
                    source = (ConsentSource)spnConsentSource.SelectedItemPosition;

                RequestNotifyMessage rpcMessage = BuildRpc.buildSDLOnAllowSDLFunctionality(devInfo, checkBoxAllowed.Checked, source);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

        private void CreateSDLNotificationOnAppPermissionConsent()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.on_app_permission_consent, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(SDLNotificationOnAppPermissionConsent);

            CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.sdl_on_app_permisssion_consent_app_id_check);
            EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.sdl_on_app_permisssion_consent_app_id_et);
            Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.sdl_on_app_permisssion_consent_app_id_spn);
            registerdAppIdSpn.Enabled = false;

            RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.sdl_on_app_permisssion_consent_app_id_manual_radioGroup);
            RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_app_permisssion_consent_app_id_manual);
            RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.sdl_on_app_permisssion_consent_app_id_registered_apps);

            CheckBox checkBoxConsentSource = (CheckBox)rpcView.FindViewById(Resource.Id.consented_source__sdl_check);
            Spinner spnConsentSource = (Spinner)rpcView.FindViewById(Resource.Id.consented_source_sdl_spn);

            checkBoxConsentSource.CheckedChange += (s, e) =>
            {
                spnConsentSource.Enabled = e.IsChecked;
            };

            appIDCheckBox.CheckedChange += (s, e) =>
            {
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
                if (e.IsChecked)
                {
                    if (manualAppIdRadioButton.Checked)
                    {
                        manualAppIdEditText.Enabled = true;
                        registerdAppIdSpn.Enabled = false;
                    }
                    else
                    {
                        registerdAppIdSpn.Enabled = true;
                        manualAppIdEditText.Enabled = false;
                    }
                }
                else
                {
                    registerdAppIdSpn.Enabled = false;
                    manualAppIdEditText.Enabled = false;
                }
            };

            manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
            {
                manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
                registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
            };

            var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
            registerdAppIdSpn.Adapter = appIDAdapter;

            string[] consentSource = Enum.GetNames(typeof(ConsentSource));
            var consentSourceAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, consentSource);
            spnConsentSource.Adapter = consentSourceAdapter;

            CheckBox addPermissionCheck = (CheckBox)rpcView.FindViewById(Resource.Id.sdl_on_app_permission_consent_add_permission_chk);
            ListView ListViewConsentedFunctions = (ListView)rpcView.FindViewById(Resource.Id.consented_functions_sdl_lv);
            List<PermissionItem> consentedFunctions = new List<PermissionItem>();
            var permissionItemAdapter = new PermissionItemAdapter(Activity, consentedFunctions);
            ListViewConsentedFunctions.Adapter = permissionItemAdapter;

            Button consentFunctionsButton = (Button)rpcView.FindViewById(Resource.Id.consented_functions_sdl_btn);

            addPermissionCheck.CheckedChange += (sender, e) =>
            {
                ListViewConsentedFunctions.Enabled = e.IsChecked;
                consentFunctionsButton.Enabled = e.IsChecked;
            };

            consentFunctionsButton.Click += delegate
            {
                AlertDialog.Builder consentFunctionsAlertDialog = new AlertDialog.Builder(Activity);
                View consentFunctionsView = layoutInflater.Inflate(Resource.Layout.permissison_item, null);
                consentFunctionsAlertDialog.SetView(consentFunctionsView);
                consentFunctionsAlertDialog.SetTitle("Add Permission Item");

                CheckBox checkBoxName = (CheckBox)consentFunctionsView.FindViewById(Resource.Id.permission_item_name_check);
                EditText editTextName = (EditText)consentFunctionsView.FindViewById(Resource.Id.permission_item_name_et);

                CheckBox checkBoxID = (CheckBox)consentFunctionsView.FindViewById(Resource.Id.permission_item_id_check);
                EditText editTextID = (EditText)consentFunctionsView.FindViewById(Resource.Id.permission_item_id_et);

                CheckBox checkBoxAllowed = (CheckBox)consentFunctionsView.FindViewById(Resource.Id.permission_item_allowed_cb);

                checkBoxName.CheckedChange += (s, e) => editTextName.Enabled = !editTextName.Enabled;
                checkBoxID.CheckedChange += (s, e) => editTextID.Enabled = !editTextID.Enabled;

                consentFunctionsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
               {
                   PermissionItem item = new PermissionItem();
                   if (checkBoxName.Checked)
                   {
                       item.name = editTextName.Text;
                   }
                   if (checkBoxID.Checked)
                   {
                       if (editTextID.Text != null && editTextID.Text.Length > 0)
                           item.id = Java.Lang.Integer.ParseInt(editTextID.Text);
                   }
                   item.allowed = checkBoxAllowed.Checked;
                   consentedFunctions.Add(item);
                   permissionItemAdapter.NotifyDataSetChanged();
               });

                consentFunctionsAlertDialog.SetPositiveButton(CANCEL, (senderAlert, args) =>
               {
                   consentFunctionsAlertDialog.Dispose();
               });
                consentFunctionsAlertDialog.Show();
            };

            HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAppPermissionConsent tmpObj = new HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAppPermissionConsent();
            tmpObj = (HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAppPermissionConsent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutGoingNotifications.OnAppPermissionConsent>(Activity, tmpObj.getMethod());
            if (null != tmpObj)
            {
                if (tmpObj.getAppId() != null)
                {
                    manualAppIdEditText.Text = tmpObj.getAppId().ToString();
                }
                if (tmpObj.getConsentedFunctions() != null)
                {
                    consentedFunctions.AddRange(tmpObj.getConsentedFunctions());
                    permissionItemAdapter.NotifyDataSetChanged();
                }
                spnConsentSource.SetSelection((int)tmpObj.getSource());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                string name = manualRegisterdSelectedOptionRadioButton.Text;

                int? selectedAppID = null;
                if (appIDCheckBox.Checked)
                {
                    if (name.Equals("Registered Apps"))
                        if (registerdAppIdSpn.Adapter.Count == 0)
                        {
                            Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                            return;
                        }
                        else
                            selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                    else if (name.Equals("Manual"))
                        if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                            selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                }
                List<PermissionItem> list = null;
                if (addPermissionCheck.Checked)
                {
                    list = consentedFunctions;
                }
                ConsentSource? selectedSource = null;
                if (checkBoxConsentSource.Checked)
                {
                    selectedSource = (ConsentSource)spnConsentSource.SelectedItemPosition;
                }
                RequestNotifyMessage rpcMessage = BuildRpc.buildSDLOnAppPermissionConsent(selectedAppID, list, selectedSource);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
           {
               if (tmpObj != null)
               {
                   AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
               }
           });

            rpcAlertDialog.Show();
        }

        private void CreateSDLNotificationOnPolicyUpdate()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(SDLNotificationOnPolicyUpdate);

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
				RequestNotifyMessage rpcMessage = BuildRpc.buildSDLOnPolicyUpdate();
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {

             });

            rpcAlertDialog.Show();
        }

        void CreateSDLNotificationOnReceivedPolicyUpdate()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = (View)layoutInflater.Inflate(Resource.Layout.on_received_policy_update, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox policyFileCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.policy_file_check);
            EditText policyFileEditText = (EditText)rpcView.FindViewById(Resource.Id.policy_file_et);
            policyFileEditText.Text = "Policy File";

            rpcAlertDialog.SetTitle(SDLNotificationOnReceivedPolicyUpdate);

            policyFileCheckBox.CheckedChange += (s, e) =>
            {
               policyFileEditText.Enabled = e.IsChecked;
            };

            HmiApiLib.Controllers.SDL.OutGoingNotifications.OnReceivedPolicyUpdate tmpObj = new HmiApiLib.Controllers.SDL.OutGoingNotifications.OnReceivedPolicyUpdate();
            tmpObj = (HmiApiLib.Controllers.SDL.OutGoingNotifications.OnReceivedPolicyUpdate)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.SDL.OutGoingNotifications.OnReceivedPolicyUpdate>(Activity, tmpObj.getMethod());
            if (null != tmpObj)
            {
                policyFileEditText.Text = tmpObj.getPolicyfile();
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
                String file = null;
                if (policyFileCheckBox.Checked)
                {
                    file = policyFileEditText.Text;
                }
                RequestNotifyMessage rpcMessage = BuildRpc.buildSDLOnReceivedPolicyUpdate(file);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.Show();
        }

		void CreateNavigationResponseAlertManeuver()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(NavigationResponseAlertManeuver);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.AlertManeuver>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rsltCode.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

			rsltCode.Text = "Result Code";

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
                HmiApiLib.Common.Enums.Result? resultCode = null;
                if (rsltCode.Checked)
                {
                    resultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
                }
				RpcResponse rpcResponse = BuildRpc.buildNavAlertManeuverResponse(BuildRpc.getNextId(), resultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});
			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseGetWayPoints()
		{
			AlertDialog.Builder getSystemInfoRpcAlertDialog = new AlertDialog.Builder(Activity);
			View getSystemInfoRpcView = layoutInflater.Inflate(Resource.Layout.get_way_points, null);
			getSystemInfoRpcAlertDialog.SetView(getSystemInfoRpcView);
			getSystemInfoRpcAlertDialog.SetTitle(NavigationResponseGetWayPoints);

            CheckBox resultCodeChk = (CheckBox)getSystemInfoRpcView.FindViewById(Resource.Id.get_way_points_result_code_tv);
			Spinner spnResultCode = (Spinner)getSystemInfoRpcView.FindViewById(Resource.Id.get_way_points_result_code_spn);

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = resultCodeAdapter;

			ListView locationListview = (ListView)getSystemInfoRpcView.FindViewById(Resource.Id.location_listview);

			List<LocationDetails> wayPoints = new List<LocationDetails>();
            var adapter = new LocationDetailAdapter(Activity, wayPoints);
            locationListview.Adapter = adapter;

			Button addLocationDetailsButton = (Button)getSystemInfoRpcView.FindViewById(Resource.Id.add_location_details);
            CheckBox addLocationDetailsChk = (CheckBox)getSystemInfoRpcView.FindViewById(Resource.Id.get_way_points_location_chk);

            addLocationDetailsChk.CheckedChange += (sender, e) => 
            {
                addLocationDetailsButton.Enabled = e.IsChecked;
                locationListview.Enabled = e.IsChecked;
            };
			resultCodeChk.CheckedChange += (sender, e) =>
			{
				spnResultCode.Enabled = e.IsChecked;
			};

            HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.GetWayPoints>(Context, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
                if (null != tmpObj.getWayPoints())
                {
					wayPoints.AddRange(tmpObj.getWayPoints());
					adapter.NotifyDataSetChanged();
                }
            }

			addLocationDetailsButton.Click += delegate
			{
				AlertDialog.Builder locationDetailsAlertDialog = new AlertDialog.Builder(Activity);
				View locationDetailsView = layoutInflater.Inflate(Resource.Layout.location_details, null);
				locationDetailsAlertDialog.SetView(locationDetailsView);
				locationDetailsAlertDialog.SetTitle("Add Location Details");

                CheckBox chkCoordinate = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_coordinate);
                CheckBox chkLatitudeDegree = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_latitude_degree_tv);
				EditText editTextLatitudeDegree = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_latitude_degree_et);

				CheckBox chkLongitudeDegree = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_longitude_degree_tv);
				EditText editTextLongitudeDegree = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_longitude_degree_et);

				CheckBox chkLocationName = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_location_name_tv);
				EditText editTextLocationName = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_location_name_et);

				CheckBox chkAddress = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_address_tv);
				EditText editTextAddressLine1 = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_address_line1_et);
				EditText editTextAddressLine2 = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_address_line2_et);
				EditText editTextAddressLine3 = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_address_line3_et);
                EditText editTextAddressLine4 = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_address_line4_et);

				CheckBox chkLocationDescription = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_location_description_tv);
				EditText editTextLocationDescription = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_location_description_et);

				CheckBox chkPhoneNumber = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_phone_number_tv);
				EditText editTextPhoneNumber = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_phone_number_et);

                CheckBox chkLocationImage = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_location_image);
				CheckBox chkImageValue = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_image_value_tv);
				EditText editTextImageValue = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_image_value_et);

				CheckBox chkImageType = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_image_type_tv);
				Spinner spnImageType = (Spinner)locationDetailsView.FindViewById(Resource.Id.get_way_points_image_type_spn);
				var imageTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, imageType);
				spnImageType.Adapter = imageTypeAdapter;

                CheckBox chkOASISAddress = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_oasis_address_chk);
                LinearLayout layoutOASISAddress = (LinearLayout)locationDetailsView.FindViewById(Resource.Id.get_way_points_oasis_address_layout);

				CheckBox chkCountryName = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_country_name_tv);
				EditText editTextCountryName = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_country_name_et);

				CheckBox chkCountryCode = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_country_code_tv);
				EditText editTextCountryCode = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_country_code_et);

				CheckBox chkPostalCode = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_postal_code_tv);
				EditText editTextPostalCode = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_postal_code_et);

				CheckBox chkAdministrativeArea = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_administrative_area_tv);
				EditText editTextAdministrativeArea = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_administrative_area_et);

				CheckBox chkSubAdministrativeArea = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_sub_administrative_area_tv);
				EditText editTextSubAdministrativeArea = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_sub_administrative_area_et);

				CheckBox chkLocality = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_locality_tv);
				EditText editTextLocality = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_locality_et);

				CheckBox chkSubLocality = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_sub_locality_tv);
				EditText editTextSubLocality = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_sub_locality_et);

				CheckBox chkThoruoghFare = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_thorough_fare_tv);
				EditText editTextThoruoghFare = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_thorough_fare_et);

				CheckBox chkSubThoruoghFare = (CheckBox)locationDetailsView.FindViewById(Resource.Id.get_way_points_sub_thorough_fare_tv);
				EditText editTextSubThoruoghFare = (EditText)locationDetailsView.FindViewById(Resource.Id.get_way_points_sub_thorough_fare_et);

                chkCoordinate.CheckedChange += (sender, e) => 
                {
                    chkLatitudeDegree.Enabled = e.IsChecked;
                    editTextLatitudeDegree.Enabled = (e.IsChecked && chkLatitudeDegree.Checked);
                    chkLongitudeDegree.Enabled = e.IsChecked;
                    editTextLongitudeDegree.Enabled = (e.IsChecked && chkLongitudeDegree.Checked);
                };

                chkLatitudeDegree.CheckedChange += (sender, e) => 
                {
                    editTextLatitudeDegree.Enabled = e.IsChecked;
                };

				chkLongitudeDegree.CheckedChange += (sender, e) =>
				{
					editTextLongitudeDegree.Enabled = e.IsChecked;
				};

				chkLocationName.CheckedChange += (sender, e) =>
				{
					editTextLocationName.Enabled = e.IsChecked;
				};

				chkAddress.CheckedChange += (sender, e) =>
				{
					editTextAddressLine1.Enabled = e.IsChecked;
                    editTextAddressLine2.Enabled = e.IsChecked;
                    editTextAddressLine3.Enabled = e.IsChecked;
                    editTextAddressLine4.Enabled = e.IsChecked;
				};

				chkLocationDescription.CheckedChange += (sender, e) =>
				{
					editTextLocationDescription.Enabled = e.IsChecked;
				};

				chkPhoneNumber.CheckedChange += (sender, e) =>
				{
					editTextPhoneNumber.Enabled = e.IsChecked;
				};

				chkLocationImage.CheckedChange += (sender, e) =>
				{
                    chkImageValue.Enabled = e.IsChecked;
					editTextImageValue.Enabled = (e.IsChecked && chkImageValue.Checked);
                    chkImageType.Enabled = e.IsChecked;
                    spnImageType.Enabled = (e.IsChecked && chkImageType.Checked);
				};

				chkImageValue.CheckedChange += (sender, e) =>
				{
					editTextImageValue.Enabled = e.IsChecked;
				};

				chkImageType.CheckedChange += (sender, e) =>
				{
					spnImageType.Enabled = e.IsChecked;
				};

				chkOASISAddress.CheckedChange += (sender, e) =>
				{
                    chkCountryName.Enabled = e.IsChecked;
                    chkCountryCode.Enabled = e.IsChecked;
                    chkPostalCode.Enabled = e.IsChecked;
                    chkAdministrativeArea.Enabled = e.IsChecked;
                    chkSubAdministrativeArea.Enabled = e.IsChecked;
                    chkLocality.Enabled = e.IsChecked;
                    chkSubLocality.Enabled = e.IsChecked;
                    chkThoruoghFare.Enabled = e.IsChecked;
                    chkSubThoruoghFare.Enabled = e.IsChecked;
                    editTextCountryName.Enabled = (e.IsChecked && chkCountryName.Checked);
                    editTextCountryCode.Enabled = (e.IsChecked && chkCountryCode.Checked);
                    editTextPostalCode.Enabled = (e.IsChecked && chkPostalCode.Checked);
                    editTextAdministrativeArea.Enabled = (e.IsChecked && chkAdministrativeArea.Checked);
                    editTextSubAdministrativeArea.Enabled = (e.IsChecked && chkSubAdministrativeArea.Checked);
                    editTextLocality.Enabled = (e.IsChecked && chkLocality.Checked);
                    editTextSubLocality.Enabled = (e.IsChecked && chkSubLocality.Checked);
                    editTextThoruoghFare.Enabled = (e.IsChecked && chkThoruoghFare.Checked);
                    editTextSubThoruoghFare.Enabled = (e.IsChecked && chkSubThoruoghFare.Checked);
				};

				chkCountryName.CheckedChange += (sender, e) =>
				{
					editTextCountryName.Enabled = e.IsChecked;
				};

				chkCountryCode.CheckedChange += (sender, e) =>
				{
					editTextCountryCode.Enabled = e.IsChecked;
				};

				chkPostalCode.CheckedChange += (sender, e) =>
				{
					editTextPostalCode.Enabled = e.IsChecked;
				};

				chkAdministrativeArea.CheckedChange += (sender, e) =>
				{
					editTextAdministrativeArea.Enabled = e.IsChecked;
				};

				chkSubAdministrativeArea.CheckedChange += (sender, e) =>
				{
					editTextSubAdministrativeArea.Enabled = e.IsChecked;
				};

				chkLocality.CheckedChange += (sender, e) =>
				{
					editTextLocality.Enabled = e.IsChecked;
				};

				chkSubLocality.CheckedChange += (sender, e) =>
				{
					editTextSubLocality.Enabled = e.IsChecked;
				};

				chkThoruoghFare.CheckedChange += (sender, e) =>
				{
					editTextThoruoghFare.Enabled = e.IsChecked;
				};

				chkSubThoruoghFare.CheckedChange += (sender, e) =>
				{
					editTextSubThoruoghFare.Enabled = e.IsChecked;
				};

				locationDetailsAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
			   {
				   Coordinate coordinate = new Coordinate();

                    if (chkLatitudeDegree.Checked) 
                    {
                        if (editTextLatitudeDegree.Text != null && editTextLatitudeDegree.Text.Length > 0)
					        coordinate.latitudeDegrees = Java.Lang.Float.ParseFloat(editTextLatitudeDegree.Text);
                    }
                    if (chkLongitudeDegree.Checked)
                    {
					   if (editTextLongitudeDegree.Text != null && editTextLongitudeDegree.Text.Length > 0)
						   coordinate.longitudeDegrees = Java.Lang.Float.ParseFloat(editTextLongitudeDegree.Text);
                    }

				   List<string> addressLines = new List<string>();
                    if (chkAddress.Checked) 
                    {
					   addressLines.Add(editTextAddressLine1.Text);
					   addressLines.Add(editTextAddressLine2.Text);
					   addressLines.Add(editTextAddressLine3.Text);
                        addressLines.Add(editTextAddressLine4.Text);
                    }

				   Image locationImage = new Image();
                    if (chkImageType.Checked)
                    {
                        locationImage.imageType = (ImageType)spnResultCode.SelectedItemPosition;
                    }
                    if (chkImageValue.Checked)
                    {
                        locationImage.value = editTextImageValue.Text;
                    }

				   OASISAddress searchAddress = new OASISAddress();
                    if (chkCountryName.Checked)
                    {
                        searchAddress.countryName = editTextCountryName.Text;
                    }
                    if (chkCountryCode.Checked)
                    {
                        searchAddress.countryCode = editTextCountryCode.Text;
                    }
                    if (chkPostalCode.Checked)
                    {
                        searchAddress.postalCode = editTextPostalCode.Text;
                    }
                    if (chkAdministrativeArea.Checked)
                    {
                        searchAddress.administrativeArea = editTextAdministrativeArea.Text;
                    }
                    if (chkSubAdministrativeArea.Checked)
                    {
                        searchAddress.subAdministrativeArea = editTextSubAdministrativeArea.Text;
                    }
                    if (chkLocality.Checked)
				   {
                        searchAddress.locality = editTextLocality.Text;
				   }
                    if (chkSubLocality.Checked)
				   {
                        searchAddress.subLocality = editTextSubLocality.Text;
				   }
                    if (chkThoruoghFare.Checked)
				   {
                        searchAddress.thoroughfare = editTextThoruoghFare.Text;
				   }
                    if (chkSubThoruoghFare.Checked)
				   {
                        searchAddress.subThoroughfare = editTextSubThoruoghFare.Text;
				   }
				   
				   LocationDetails lctnDetails = new LocationDetails();
                    if (chkCoordinate.Checked)
                    {
                        lctnDetails.coordinate = coordinate;
                    }
                    if (chkLocationName.Checked)
                    {
                        lctnDetails.locationName = editTextLocationName.Text;
                    }
                    if (chkAddress.Checked)
                    {
                        lctnDetails.addressLines = addressLines;
                    }
                    if (chkLocationDescription.Checked)
                    {
                        lctnDetails.locationDescription = editTextLocationDescription.Text;
                    }
                    if (chkPhoneNumber.Checked)
                    {
                        lctnDetails.phoneNumber = editTextPhoneNumber.Text;
                    }
                    if (chkLocationImage.Checked)
                    {
                        lctnDetails.locationImage = locationImage;
                    }
                    if (chkOASISAddress.Checked)
                    {
                        lctnDetails.searchAddress = searchAddress;
                    }
				   
				   wayPoints.Add(lctnDetails);
                    adapter.NotifyDataSetChanged();
			   });

                locationDetailsAlertDialog.SetPositiveButton(CANCEL, (senderAlert, args) =>
			   {
				   locationDetailsAlertDialog.Dispose();
			   });

				locationDetailsAlertDialog.Show();

			};

            getSystemInfoRpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				getSystemInfoRpcAlertDialog.Dispose();
			});

            getSystemInfoRpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
                HmiApiLib.Common.Enums.Result? resltCode = null;
				if (resultCodeChk.Checked)
				{
					resltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
				}
				List<LocationDetails> finalWayPoints = null;
				if (addLocationDetailsChk.Checked)
				{
					finalWayPoints = wayPoints;
				}
				RpcMessage rpcMessage = BuildRpc.buildNavGetWayPointsResponse(BuildRpc.getNextId(), resltCode, finalWayPoints);
				AppUtils.savePreferenceValueForRpc(Context, ((RpcResponse)rpcMessage).getMethod(), rpcMessage);
			});

            getSystemInfoRpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
		   {
                AppUtils.removeSavedPreferenceValueForRpc(Context, tmpObj.getMethod());
		   });

			getSystemInfoRpcAlertDialog.Show();
		}

		private void CreateNavigationResponseSendLocation()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseSendLocation);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.SendLocation>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

            rsltCode.CheckedChange += (sender, e) => 
            {
                spnGeneric.Enabled = e.IsChecked;
            };

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
                HmiApiLib.Common.Enums.Result? selectedResultCode = null;
                if (rsltCode.Checked)
                {
                    selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
                }
				rpcResponse = BuildRpc.buildNavSendLocationResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseShowConstantTBT()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseShowConstantTBT);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.ShowConstantTBT>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

            rsltCode.CheckedChange += (sender, e) => 
            {
                spnGeneric.Enabled = e.IsChecked;
            };

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
                RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
                rpcResponse = BuildRpc.buildNavShowConstantTBTResponse(BuildRpc.getNextId(), selectedResultCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
   			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseStartAudioStream()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseStartAudioStream);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StartAudioStream>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				rpcResponse = BuildRpc.buildNavStartAudioStreamResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseStartStream()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseStartStream);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StartStream>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}
			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				rpcResponse = BuildRpc.buildNavStartStreamResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseStopAudioStream()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseStopAudioStream);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StopAudioStream>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}
			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};
			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				rpcResponse = BuildRpc.buildNavStopAudioStreamResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseStopStream()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseStopStream);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.StopStream>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				rpcResponse = BuildRpc.buildNavStopStreamResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseSubscribeWayPoints()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseSubscribeWayPoints);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.SubscribeWayPoints>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				rpcResponse = BuildRpc.buildNavSubscribeWayPointsResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseUnsubscribeWayPoints()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseUnsubscribeWayPoints);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints();
            tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints>(adapter.Context, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnGeneric.SetSelection((int)tmpObj.getResultCode());
            }

			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				RpcResponse rpcResponse = BuildRpc.buildNavUnsubscribeWayPointsResponse(BuildRpc.getNextId(), selectedResultCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationResponseUpdateTurnList()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationResponseUpdateTurnList);

			CheckBox rsltCode = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList tmpObj = new HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutgoingResponses.UpdateTurnList>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

			rsltCode.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				RpcResponse rpcResponse = null;
				HmiApiLib.Common.Enums.Result? selectedResultCode = null;
				if (rsltCode.Checked)
				{
					selectedResultCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}
				rpcResponse = BuildRpc.buildNavUpdateTurnListResponse(BuildRpc.getNextId(), (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateNavigationNotificationOnTBTClientState()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			rpcAlertDialog.SetTitle(NavigationNotificationOnTBTClientState);

			CheckBox tbtStateCheck = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, tbtStateArray);
			spnGeneric.Adapter = adapter;

            tbtStateCheck.Text = "TBT State";

            HmiApiLib.Controllers.Navigation.OutGoingNotifications.OnTBTClientState tmpObj = new HmiApiLib.Controllers.Navigation.OutGoingNotifications.OnTBTClientState();
			tmpObj = (HmiApiLib.Controllers.Navigation.OutGoingNotifications.OnTBTClientState)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Navigation.OutGoingNotifications.OnTBTClientState>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null && tmpObj.getState() != null)
			{
                spnGeneric.SetSelection((int)tmpObj.getState());
			}

			tbtStateCheck.CheckedChange += (sender, e) =>
			{
				spnGeneric.Enabled = e.IsChecked;
			};

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
                TBTState? state = null;
                if (tbtStateCheck.Checked)
                {
                    state = (TBTState)spnGeneric.SelectedItemPosition;
                }
				RequestNotifyMessage rpcMessage = BuildRpc.buildNavigationOnTBTClientState(state);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateButtonsNotificationOnButtonPress()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.on_button, null);
			rpcAlertDialog.SetView(rpcView);

            CheckBox buttonNameChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_button_name_tv);
			Spinner spnButtonName = (Spinner)rpcView.FindViewById(Resource.Id.on_button_button_name_spn);

			CheckBox buttonModeChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_mode_tv);
			Spinner spnButtonMode = (Spinner)rpcView.FindViewById(Resource.Id.on_button_mode_spn);

			CheckBox customButtonIdChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_custom_button_id_tv);
			EditText editFieldCustomButton = (EditText)rpcView.FindViewById(Resource.Id.on_button_custom_button_spn);

			CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_activated_app_id_check);

			EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.on_button_app_id_et);
			Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.on_button_app_id_spinner);
			registerdAppIdSpn.Enabled = false;

			RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.on_button_app_id_manual_radioGroup);
			RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.on_button_app_id_manual);
			RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.on_button_app_id_registered_apps);

			buttonNameChk.CheckedChange += (sender, e) => spnButtonName.Enabled = e.IsChecked;
			buttonModeChk.CheckedChange += (sender, e) => spnButtonMode.Enabled = e.IsChecked;
			customButtonIdChk.CheckedChange += (sender, e) => editFieldCustomButton.Enabled = e.IsChecked;

			appIDCheckBox.CheckedChange += (s, e) =>
			{
				manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
				manualAppIdRadioButton.Enabled = e.IsChecked;
				registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
				{
					if (manualAppIdRadioButton.Checked)
					{
						manualAppIdEditText.Enabled = true;
						registerdAppIdSpn.Enabled = false;
					}
					else
					{
						registerdAppIdSpn.Enabled = true;
						manualAppIdEditText.Enabled = false;
					}
				}
				else
				{
					registerdAppIdSpn.Enabled = false;
					manualAppIdEditText.Enabled = false;
				}
			};

			manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
			{
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
			};

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

			rpcAlertDialog.SetTitle(ButtonsNotificationOnButtonPress);
			buttonModeChk.Text = "Button Press Mode";

			var buttonNamesAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, buttonNames);
			spnButtonName.Adapter = buttonNamesAdapter;

			var buttonPressModeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, buttonPressMode);
			spnButtonMode.Adapter = buttonPressModeAdapter;

			HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonPress tmpObj = new HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonPress();
			tmpObj = (HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonPress)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonPress>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				if (tmpObj.getName() != null)
				{
					spnButtonName.SetSelection((int)tmpObj.getName());
				}
				if (null != tmpObj.getMode())
				{
					spnButtonMode.SetSelection((int)tmpObj.getMode());
				}
				if (null != tmpObj.getCustomButtonID())
				{
					editFieldCustomButton.Text = tmpObj.getCustomButtonID().ToString();
				}
				if (null != tmpObj.getAppId())
				{
					manualAppIdEditText.Text = tmpObj.getAppId().ToString();
				}
			}

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
				string name = manualRegisterdSelectedOptionRadioButton.Text;

				int? selectedAppID = null;
                if (appIDCheckBox.Checked)
                {
                    if (name.Equals("Registered Apps"))
                        if (registerdAppIdSpn.Adapter.Count == 0)
                        {
                            Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                            return;
                        }
                        else
                            selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                    else if (name.Equals("Manual"))
                        if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                            selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
				}
				ButtonName? buttonName = null;
				if (buttonNameChk.Checked)
				{
					buttonName = (ButtonName)spnButtonName.SelectedItemPosition;
				}
				ButtonPressMode? mode = null;
				if (buttonModeChk.Checked)
				{
					mode = (ButtonPressMode)spnButtonMode.SelectedItemPosition;
				}
				int? customButtonID = null;
				if (customButtonIdChk.Checked)
				{
					if (editFieldCustomButton.Text != null && editFieldCustomButton.Text.Length > 0)
					{
						customButtonID = Java.Lang.Integer.ParseInt(editFieldCustomButton.Text);
					}
				}
				RequestNotifyMessage rpcMessage = BuildRpc.buildButtonsOnButtonPress(buttonName, mode, customButtonID, selectedAppID);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
				    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});
            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.Show();
		}

		private void CreateButtonsNotificationOnButtonEvent()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_button, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(ButtonsNotificationOnButtonEvent);

            CheckBox buttonNameChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_button_name_tv);
			Spinner spnButtonName = (Spinner)rpcView.FindViewById(Resource.Id.on_button_button_name_spn);

			CheckBox eventModeChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_mode_tv);
			Spinner spnButtonMode = (Spinner)rpcView.FindViewById(Resource.Id.on_button_mode_spn);

            CheckBox customButtonIdChk = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_custom_button_id_tv);
			EditText editFieldCustomButton = (EditText)rpcView.FindViewById(Resource.Id.on_button_custom_button_spn);

			CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.on_button_activated_app_id_check);

			EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.on_button_app_id_et);
			Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.on_button_app_id_spinner);
			registerdAppIdSpn.Enabled = false;

			RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.on_button_app_id_manual_radioGroup);
			RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.on_button_app_id_manual);
			RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.on_button_app_id_registered_apps);

            buttonNameChk.CheckedChange += (sender, e) => spnButtonName.Enabled = e.IsChecked;
			eventModeChk.CheckedChange += (sender, e) => spnButtonMode.Enabled = e.IsChecked;
			customButtonIdChk.CheckedChange += (sender, e) => editFieldCustomButton.Enabled = e.IsChecked;
			appIDCheckBox.CheckedChange += (s, e) =>
			{
				manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
				manualAppIdRadioButton.Enabled = e.IsChecked;
				registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
				{
					if (manualAppIdRadioButton.Checked)
					{
						manualAppIdEditText.Enabled = true;
						registerdAppIdSpn.Enabled = false;
					}
					else
					{
						registerdAppIdSpn.Enabled = true;
						manualAppIdEditText.Enabled = false;
					}
				}
				else
				{
					registerdAppIdSpn.Enabled = false;
					manualAppIdEditText.Enabled = false;
				}
			};

			manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
			{
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
			};

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

			var buttonEventModeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, buttonEventMode);
			spnButtonMode.Adapter = buttonEventModeAdapter;

			var buttonNamesAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, buttonNames);
			spnButtonName.Adapter = buttonNamesAdapter;

            HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonEvent tmpObj = new HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonEvent();
			tmpObj = (HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonEvent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutGoingNotifications.OnButtonEvent>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
                if (tmpObj.getName() != null)
                {
                    spnButtonName.SetSelection((int)tmpObj.getName());
                }
                if (null != tmpObj.getMode())
                {
                    spnButtonMode.SetSelection((int)tmpObj.getMode());
                }
                if (null != tmpObj.getCustomButtonID())
                {
                    editFieldCustomButton.Text = tmpObj.getCustomButtonID().ToString();
                }
                if (null != tmpObj.getAppId())
                {
                    manualAppIdEditText.Text = tmpObj.getAppId().ToString();
                }			
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
				string name = manualRegisterdSelectedOptionRadioButton.Text;

				int? selectedAppID = null;
                if (appIDCheckBox.Checked)
                {
                    if (name.Equals("Registered Apps"))
                        if (registerdAppIdSpn.Adapter.Count == 0)
                        {
                            Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                            return;
                        }
                        else
                            selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                    else if (name.Equals("Manual"))
                        if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                            selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
				}
                ButtonName? buttonName = null;
                if (buttonNameChk.Checked)
                {
                    buttonName = (ButtonName)spnButtonName.SelectedItemPosition;
                }
                ButtonEventMode? mode = null;
                if (eventModeChk.Checked)
                {
                    mode = (ButtonEventMode)spnButtonMode.SelectedItemPosition;
                }
                int? customButtonID = null;
                if (customButtonIdChk.Checked)
                {
                    if (editFieldCustomButton.Text != null && editFieldCustomButton.Text.Length > 0)
                    {
                        customButtonID = Java.Lang.Integer.ParseInt(editFieldCustomButton.Text);
                    }
				}
                RequestNotifyMessage rpcMessage = BuildRpc.buildButtonsOnButtonEvent(buttonName, mode, customButtonID, selectedAppID);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

        void CreateBCNotificationOnSystemInfoChanged()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
            rpcAlertDialog.SetView(rpcView);

            CheckBox languageCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);

            Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);
            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, languages);
            spnGeneric.Adapter = adapter;

            languageCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemInfoChanged tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemInfoChanged();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemInfoChanged)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemInfoChanged>(Activity, tmpObj.getMethod());
            if (tmpObj != null && tmpObj.getLanguage() != null)
            {
                spnGeneric.SetSelection((int)tmpObj.getLanguage());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            languageCheckbox.Text = "Language";

            rpcAlertDialog.SetTitle(BCNotificationOnSystemInfoChanged);
            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Language? language = null;
                if (languageCheckbox.Checked)
                {
                    language = (Language)spnGeneric.SelectedItemPosition;
                }

                RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnSystemInfoChanged(language);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
            });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                 }
             });
            rpcAlertDialog.Show();
        }

		void CreateBCNotificationOnUpdateDeviceList()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(BCNotificationOnUpdateDeviceList);

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			 {
				 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnUpdateDeviceList();
				 AppInstanceManager.Instance.sendRpc(rpcMessage);
			 });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				
			});
			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnSystemRequest()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View getSystemRequestRpcView = layoutInflater.Inflate(Resource.Layout.on_system_request, null);

			rpcAlertDialog.SetView(getSystemRequestRpcView);
			rpcAlertDialog.SetTitle(BCNotificationOnSystemRequest);

            CheckBox checkBoxRequestType = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_request_request_type_check);
			Spinner spnRequestType = (Spinner)getSystemRequestRpcView.FindViewById(Resource.Id.request_type_spn);

			CheckBox checkBoxURL = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_request_url_check);
			EditText editTextURL = (EditText)getSystemRequestRpcView.FindViewById(Resource.Id.url_et);

			CheckBox checkBoxFileType = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_file_type_check);
			Spinner spnFileType = (Spinner)getSystemRequestRpcView.FindViewById(Resource.Id.file_type_spn);

			CheckBox checkBoxOffset = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_offset_check);
			EditText editTextOffset = (EditText)getSystemRequestRpcView.FindViewById(Resource.Id.offset_et);

			CheckBox checkBoxLength = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_length_check);
			EditText editTextLength = (EditText)getSystemRequestRpcView.FindViewById(Resource.Id.length_et);

			CheckBox checkBoxTimeOut = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_timeout_check);
			EditText editTextTimeOut = (EditText)getSystemRequestRpcView.FindViewById(Resource.Id.time_out_et);

			CheckBox checkBoxFileName = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_filename_check);
			EditText editTextFileName = (EditText)getSystemRequestRpcView.FindViewById(Resource.Id.file_name_et);

			CheckBox appIDCheckBox = (CheckBox)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_request_app_id_check);

			EditText manualAppIdEditText = (EditText)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_request_app_id_et);
			Spinner registerdAppIdSpn = (Spinner)getSystemRequestRpcView.FindViewById(Resource.Id.bc_on_system_request_app_id_spinner);
			registerdAppIdSpn.Enabled = false;

			RadioGroup manualRegisterdAppIdRadioGroup = getSystemRequestRpcView.FindViewById<RadioGroup>(Resource.Id.bc_on_system_request_app_id_manual_radioGroup);
			RadioButton manualAppIdRadioButton = getSystemRequestRpcView.FindViewById<RadioButton>(Resource.Id.bc_on_system_request_app_id_manual);
			RadioButton registerdAppIdRadioButton = getSystemRequestRpcView.FindViewById<RadioButton>(Resource.Id.bc_on_system_request_app_id_registered_apps);

            checkBoxRequestType.CheckedChange += (s, e) => spnRequestType.Enabled = e.IsChecked;
            checkBoxURL.CheckedChange += (s, e) => editTextURL.Enabled = e.IsChecked;
            checkBoxFileType.CheckedChange += (s, e) => spnFileType.Enabled = e.IsChecked;
            checkBoxOffset.CheckedChange += (s, e) => editTextOffset.Enabled = e.IsChecked;
            checkBoxLength.CheckedChange += (s, e) => editTextLength.Enabled = e.IsChecked;
            checkBoxTimeOut.CheckedChange += (s, e) => editTextTimeOut.Enabled = e.IsChecked;
            checkBoxFileName.CheckedChange += (s, e) => editTextFileName.Enabled = e.IsChecked;

			appIDCheckBox.CheckedChange += (s, e) =>
			{
				manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
				manualAppIdRadioButton.Enabled = e.IsChecked;
				registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
				{
					if (manualAppIdRadioButton.Checked)
					{
						manualAppIdEditText.Enabled = true;
						registerdAppIdSpn.Enabled = false;
					}
					else
					{
						registerdAppIdSpn.Enabled = true;
						manualAppIdEditText.Enabled = false;
					}
				}
				else
				{
					registerdAppIdSpn.Enabled = false;
					manualAppIdEditText.Enabled = false;
				}
			};

			manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
			{
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
			};

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

			var requsetTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, requestType);
			spnRequestType.Adapter = requsetTypeAdapter;

			var fileTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, fileType);
			spnFileType.Adapter = fileTypeAdapter;

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemRequest tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemRequest();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemRequest)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnSystemRequest>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
                spnRequestType.SetSelection((int)tmpObj.getRequestType());
                if(tmpObj.getUrl() != null)
				    editTextURL.Text = tmpObj.getUrl();		

				spnFileType.SetSelection((int)tmpObj.getFileType());

                if (tmpObj.getOffset() != null)
				    editTextOffset.Text = tmpObj.getOffset().ToString();
                
                if (tmpObj.getLength() != null)
				    editTextLength.Text = tmpObj.getLength().ToString();

                if (tmpObj.getTimeout() != null)
				    editTextTimeOut.Text = tmpObj.getTimeout().ToString();

                if (tmpObj.getFileName() != null)
				    editTextFileName.Text = tmpObj.getFileName();

                if (tmpObj.getAppId() != null)
				    manualAppIdEditText.Text = tmpObj.getAppId().ToString();
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = getSystemRequestRpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))
                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }
                 String url = null;
                 if (checkBoxURL.Checked)
                     if (editTextURL.Text != null && editTextURL.Text.Length > 0)
                         url = editTextURL.Text;

                 RequestType? selectedRequestType = null;
                 if (checkBoxRequestType.Checked)
                 {
                     selectedRequestType = (RequestType)spnRequestType.SelectedItemPosition;
                 }

                 FileType? selectedFileType = null;
                 if (checkBoxFileType.Checked)
                 {
                     selectedFileType = (FileType)spnFileType.SelectedItemPosition;
                 }

                 int? offset = null, length = null, timeOut = null;
                 if (checkBoxOffset.Checked)
                     if (editTextOffset.Text != null && editTextOffset.Text.Length > 0)
                         offset = Java.Lang.Integer.ParseInt(editTextOffset.Text);
                 if (checkBoxLength.Checked)
                     if (editTextLength.Text != null && editTextLength.Text.Length > 0)
                         length = Java.Lang.Integer.ParseInt(editTextLength.Text);
                 if (checkBoxTimeOut.Checked)
                     if (editTextTimeOut.Text != null && editTextTimeOut.Text.Length > 0)
                         timeOut = Java.Lang.Integer.ParseInt(editTextTimeOut.Text);

                 RequestNotifyMessage rpcMessage = null;
                 rpcMessage = BuildRpc.buildBasicCommunicationOnSystemRequestNotification(selectedRequestType, url, selectedFileType, offset, length, timeOut, editTextFileName.Text, selectedAppID);

                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		void CreateBCNotificationOnStartDeviceDiscovery()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(BCNotificationOnStartDeviceDiscovery);

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnStartDeviceDiscovery tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnStartDeviceDiscovery();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnStartDeviceDiscovery)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnStartDeviceDiscovery>(Activity, tmpObj.getMethod());

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnStartDeviceDiscovery();
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		void CreateBCNotificationOnReady()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(BCNotificationOnReady);

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnReady tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnReady();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnReady)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnReady>(Activity, tmpObj.getMethod());

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnReady();
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnPhoneCall()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_deactivate_HMI, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox checkBoxIsActive = (CheckBox)rpcView.FindViewById(Resource.Id.is_deactivated);
			rpcAlertDialog.SetTitle(BCNotificationOnPhoneCall);
			checkBoxIsActive.Text = "Active";

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnPhoneCall tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnPhoneCall();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnPhoneCall)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnPhoneCall>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{				
                checkBoxIsActive.Checked = (bool)tmpObj.getActive();
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});		

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			 {
				 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnPhoneCallNotification(checkBoxIsActive.Checked);
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				 AppInstanceManager.Instance.sendRpc(rpcMessage);
			 });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnIgnitionCycleOver()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            rpcAlertDialog.SetTitle(BCNotificationOnIgnitionCycleOver);

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnIgnitionCycleOver tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnIgnitionCycleOver();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnIgnitionCycleOver)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnIgnitionCycleOver>(Activity, tmpObj.getMethod());

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnIgnitionCycleOver();
				AppUtils.savePreferenceValueForRpc(Activity, ((RequestNotifyMessage)rpcMessage).getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnFindApplications()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_device_chosen, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCNotificationOnFindApplications);

            CheckBox checkboxDeviceInfo = (CheckBox)rpcView.FindViewById(Resource.Id.device_info_chk);
			CheckBox checkboxDeviceName = (CheckBox)rpcView.FindViewById(Resource.Id.device_name_cb);
			EditText editFieldDeviceName = (EditText)rpcView.FindViewById(Resource.Id.device_name);

			CheckBox checkboxDeviceId = (CheckBox)rpcView.FindViewById(Resource.Id.device_id_cb);
			EditText editFieldDeviceId = (EditText)rpcView.FindViewById(Resource.Id.device_id);

			CheckBox checkboxTransportType = (CheckBox)rpcView.FindViewById(Resource.Id.transport_type_cb);
			Spinner spnTransportType = (Spinner)rpcView.FindViewById(Resource.Id.transport_type);

			CheckBox checkBoxIsSDLAllowed = (CheckBox)rpcView.FindViewById(Resource.Id.is_sdl_allowed);

			checkboxDeviceInfo.CheckedChange += (sender, e) =>
			{
				checkboxDeviceName.Enabled = e.IsChecked;
				editFieldDeviceName.Enabled = e.IsChecked;
				checkboxDeviceId.Enabled = e.IsChecked;
				editFieldDeviceId.Enabled = e.IsChecked;
				checkboxTransportType.Enabled = e.IsChecked;
				spnTransportType.Enabled = e.IsChecked;
				checkBoxIsSDLAllowed.Enabled = e.IsChecked;
			};
            checkboxDeviceName.CheckedChange += (s, e) => editFieldDeviceName.Enabled = e.IsChecked;
            checkboxDeviceId.CheckedChange += (s, e) => editFieldDeviceId.Enabled = e.IsChecked;
            checkboxTransportType.CheckedChange += (s, e) => spnTransportType.Enabled = e.IsChecked;

			var transportTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, transportType);
			spnTransportType.Adapter = transportTypeAdapter;

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnFindApplications tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnFindApplications();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnFindApplications)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnFindApplications>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				editFieldDeviceName.Text = tmpObj.getDeviceInfo().getName();
				editFieldDeviceId.Text = tmpObj.getDeviceInfo().getId();
				spnTransportType.SetSelection((int)tmpObj.getDeviceInfo().getTransportType());
				checkBoxIsSDLAllowed.Checked = (bool)tmpObj.getDeviceInfo().getIsSDLAllowed();
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			 {
				 DeviceInfo devInfo = new DeviceInfo();

				 if (checkboxDeviceName.Checked)
					 devInfo.name = editFieldDeviceName.Text;

				 if (checkboxDeviceId.Checked)
					 devInfo.id = editFieldDeviceId.Text;

				 if (checkboxTransportType.Checked)
					 devInfo.transportType = (TransportType)spnTransportType.SelectedItemPosition;

				 devInfo.isSDLAllowed = checkBoxIsSDLAllowed.Checked;

				 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnFindApplicationsNotification(devInfo);
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				 AppInstanceManager.Instance.sendRpc(rpcMessage);
			 });


            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnExitApplication()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_exit_application, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_exit_application_app_id_check);

			EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.bc_on_exit_application_app_id_et);
			Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.bc_on_exit_application_app_id_spinner);
			registerdAppIdSpn.Enabled = false;

			RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.bc_on_exit_application_app_id_manual_radioGroup);
			RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_exit_application_app_id_manual);
			RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_exit_application_app_id_registered_apps);

			appIDCheckBox.CheckedChange += (s, e) =>
			{
				manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
				manualAppIdRadioButton.Enabled = e.IsChecked;
				registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
				{
					if (manualAppIdRadioButton.Checked)
					{
						manualAppIdEditText.Enabled = true;
						registerdAppIdSpn.Enabled = false;
					}
					else
					{
						registerdAppIdSpn.Enabled = true;
						manualAppIdEditText.Enabled = false;
					}
				}
				else
				{
					registerdAppIdSpn.Enabled = false;
					manualAppIdEditText.Enabled = false;
				}
			};

			manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
			{
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
			};

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

            CheckBox textViewAppExitReason = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_exit_application_app_exit_reason_cb);

			Spinner spnAppExitReason = (Spinner)rpcView.FindViewById(Resource.Id.bc_on_exit_application_app_exit_reason);
			var appExitReasonAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, appsExitReason);
			spnAppExitReason.Adapter = appExitReasonAdapter;

            textViewAppExitReason.CheckedChange += (s, e) => spnAppExitReason.Enabled = e.IsChecked;

			rpcAlertDialog.SetTitle("OnExitApplication");

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitApplication tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitApplication();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitApplication)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitApplication>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                if (tmpObj.getApplicationExitReason() != null)
                {
                    spnAppExitReason.SetSelection((int)tmpObj.getApplicationExitReason());
                }
                if (tmpObj.getAppId() != null)
                {
                    manualAppIdEditText.Text = tmpObj.getAppId().ToString();
                }
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{

				RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
				string name = manualRegisterdSelectedOptionRadioButton.Text;

				int? selectedAppID = null;
                if (appIDCheckBox.Checked)
                {
                    if (name.Equals("Registered Apps"))
                        if (registerdAppIdSpn.Adapter.Count == 0)
                        {
                            Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                            return;
                        }
                        else
                            selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                    else if (name.Equals("Manual"))
                        if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                            selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                }
				ApplicationExitReason? applicationExitReason = null;
				if (textViewAppExitReason.Checked)
					applicationExitReason = (ApplicationExitReason)spnAppExitReason.SelectedItemPosition;

				RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnExitApplication(applicationExitReason, selectedAppID);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
				    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		void CreateBCNotificationOnExitAllApplications()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox appsCloseReasonCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);

			Spinner appsCloseReasonSpn = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);
			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, appsCloseReason);
			appsCloseReasonSpn.Adapter = adapter;

            appsCloseReasonCheckBox.CheckedChange += (s, e) => appsCloseReasonSpn.Enabled = e.IsChecked;

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitAllApplications tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitAllApplications();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitAllApplications)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnExitAllApplications>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null && tmpObj.getApplicationsCloseReason() != null)
			{
				 appsCloseReasonSpn.SetSelection((int)tmpObj.getApplicationsCloseReason());
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			appsCloseReasonCheckBox.Text = "App Close Reason";

			rpcAlertDialog.SetTitle(BCNotificationOnExitAllApplications);

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{				
				ApplicationsCloseReason? applicationCloseReason = null;
				if (appsCloseReasonCheckBox.Checked)
					applicationCloseReason = (ApplicationsCloseReason)appsCloseReasonSpn.SelectedItemPosition;

				RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnExitAllApplications(applicationCloseReason);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
				AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                 }
			 });

			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnEventChanged()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(BCNotificationOnEventChanged);

			CheckBox checkBoxAllow = (CheckBox)rpcView.FindViewById(Resource.Id.allow);

            CheckBox eventTypesCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
			Spinner spnEventType = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

            eventTypesCheckBox.CheckedChange += (s, e) => spnEventType.Enabled = e.IsChecked;

			checkBoxAllow.Text = ("IsActive");

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, eventTypes);
			spnEventType.Adapter = adapter;

			eventTypesCheckBox.Text = "Event Types";

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEventChanged tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEventChanged();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEventChanged)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEventChanged>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				checkBoxAllow.Checked = (bool)tmpObj.getActive();
				spnEventType.SetSelection((int)tmpObj.getEventName());
			}

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
		   {
			   HmiApiLib.Common.Enums.EventTypes? eventTypes = null;
			   if (eventTypesCheckBox.Checked)
				   eventTypes = (HmiApiLib.Common.Enums.EventTypes)spnEventType.SelectedItemPosition;
                
			   RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnEventChangedNotification(eventTypes, checkBoxAllow.Checked);
			   AppUtils.savePreferenceValueForRpc(adapter.Context, rpcMessage.getMethod(), rpcMessage);
			   AppInstanceManager.Instance.sendRpc(rpcMessage);

		   });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				 }
			 });

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			 {
				 rpcAlertDialog.Dispose();
			 });
			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnEmergencyEvent()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_deactivate_HMI, null);
			rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(BCNotificationOnEmergencyEvent);

			CheckBox checkBoxEnabled = (CheckBox)rpcView.FindViewById(Resource.Id.is_deactivated);
            checkBoxEnabled.Text = "Enabled";

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEmergencyEvent tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEmergencyEvent();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEmergencyEvent)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnEmergencyEvent>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{				
                checkBoxEnabled.Checked = (bool)tmpObj.getEnabled();
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			 {
				 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnEmergencyEventNotification(checkBoxEnabled.Checked);
				 AppUtils.savePreferenceValueForRpc(Activity, ((RequestNotifyMessage)rpcMessage).getMethod(), rpcMessage);
				 AppInstanceManager.Instance.sendRpc(rpcMessage);
			 });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnDeviceChosen()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_device_chosen, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCNotificationOnDeviceChosen);

            CheckBox checkboxDeviceInfo = (CheckBox)rpcView.FindViewById(Resource.Id.device_info_chk);
            CheckBox checkboxDeviceName = (CheckBox)rpcView.FindViewById(Resource.Id.device_name_cb);
			EditText editFieldDeviceName = (EditText)rpcView.FindViewById(Resource.Id.device_name);

			CheckBox checkboxDeviceId = (CheckBox)rpcView.FindViewById(Resource.Id.device_id_cb);
			EditText editFieldDeviceId = (EditText)rpcView.FindViewById(Resource.Id.device_id);

			CheckBox checkboxTransportType = (CheckBox)rpcView.FindViewById(Resource.Id.transport_type_cb);
			Spinner spnTransportType = (Spinner)rpcView.FindViewById(Resource.Id.transport_type);

			CheckBox checkBoxIsSDLAllowed = (CheckBox)rpcView.FindViewById(Resource.Id.is_sdl_allowed);

            checkboxDeviceInfo.CheckedChange += (sender, e) => 
            {
                checkboxDeviceName.Enabled = e.IsChecked;
                editFieldDeviceName.Enabled = e.IsChecked;
                checkboxDeviceId.Enabled = e.IsChecked;
                editFieldDeviceId.Enabled = e.IsChecked;
                checkboxTransportType.Enabled = e.IsChecked;
                spnTransportType.Enabled = e.IsChecked;
                checkBoxIsSDLAllowed.Enabled = e.IsChecked;
            };
            checkboxDeviceName.CheckedChange += (s, e) => editFieldDeviceName.Enabled = e.IsChecked;
            checkboxDeviceId.CheckedChange += (s, e) => editFieldDeviceId.Enabled = e.IsChecked;
            checkboxTransportType.CheckedChange += (s, e) => spnTransportType.Enabled = e.IsChecked;
		
			var transportTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, transportType);
			spnTransportType.Adapter = transportTypeAdapter;

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeviceChosen tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeviceChosen();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeviceChosen)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeviceChosen>(Activity, tmpObj.getMethod());
            if (tmpObj != null && null != tmpObj.getDeviceInfo())
            {
                editFieldDeviceName.Text = tmpObj.getDeviceInfo().getName();
                editFieldDeviceId.Text = tmpObj.getDeviceInfo().getId();
                spnTransportType.SetSelection((int)tmpObj.getDeviceInfo().getTransportType());
                checkBoxIsSDLAllowed.Checked = (bool)tmpObj.getDeviceInfo().getIsSDLAllowed();
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
                DeviceInfo devInfo = null;
                if (checkboxDeviceInfo.Checked)
                {
                    devInfo = new DeviceInfo();

					 if (checkboxDeviceName.Checked)
						 devInfo.name = editFieldDeviceName.Text;

					 if (checkboxDeviceId.Checked)
						 devInfo.id = editFieldDeviceId.Text;

					 if (checkboxTransportType.Checked)
						 devInfo.transportType = (TransportType)spnTransportType.SelectedItemPosition;

					 devInfo.isSDLAllowed = checkBoxIsSDLAllowed.Checked;
                }

				 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnDeviceChosenNotification(devInfo);
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				 AppInstanceManager.Instance.sendRpc(rpcMessage);
			 });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });
			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnDeactivateHMI()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_deactivate_HMI, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox checkBoxIsDeactivated = (CheckBox)rpcView.FindViewById(Resource.Id.is_deactivated);

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeactivateHMI tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeactivateHMI();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeactivateHMI)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnDeactivateHMI>(Activity, tmpObj.getMethod());
			if (tmpObj != null && null != tmpObj.getDeactivatedStatus())
			{
                checkBoxIsDeactivated.Checked = (bool)tmpObj.getDeactivatedStatus();
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetTitle(BCNotificationOnDeactivateHMI);

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			 {
				 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnDeactivateHMINotification(checkBoxIsDeactivated.Checked);
				 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
				 AppInstanceManager.Instance.sendRpc(rpcMessage);
			 });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

        void CreateBCNotificationOnAwakeSDL()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);

            HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAwakeSDL tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAwakeSDL();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAwakeSDL)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAwakeSDL>(Activity, tmpObj.getMethod());

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetTitle(BCNotificationOnAwakeSDL);

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnAwakeSDL();
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
             {
                 if (tmpObj != null)
                 {
                     AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                 }
             });
            rpcAlertDialog.Show();
        }

		private void CreateBCNotificationOnAppDeactivated()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_app_activated, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_check);

			EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_et);
			Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_spinner);
			registerdAppIdSpn.Enabled = false;

			RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.bc_on_activated_app_id_manual_radioGroup);
			RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_manual);
			RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_registered_apps);


			appIDCheckBox.CheckedChange += (s, e) =>
			{
				manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
				manualAppIdRadioButton.Enabled = e.IsChecked;
				registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
				{
					if (manualAppIdRadioButton.Checked)
					{
						manualAppIdEditText.Enabled = true;
						registerdAppIdSpn.Enabled = false;
					}
					else
					{
						registerdAppIdSpn.Enabled = true;
						manualAppIdEditText.Enabled = false;
					}
				}
				else
				{
					registerdAppIdSpn.Enabled = false;
					manualAppIdEditText.Enabled = false;
				}
			};

			manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
			{
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
			};

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppDeactivated tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppDeactivated(); 
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppDeactivated)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppDeactivated>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				manualAppIdEditText.Text = tmpObj.getAppId().ToString();
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetTitle(BCNotificationOnAppDeactivated);

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			 {
				 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
				 string name = manualRegisterdSelectedOptionRadioButton.Text;

				 int? selectedAppID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))
                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                             selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();

                     else if (name.Equals("Manual"))
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                 }

                RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnAppDeactivated(selectedAppID);
                AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);

			 });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
					 AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}

		private void CreateBCNotificationOnAppActivated()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.on_app_activated, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox appIDCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_check);

			EditText manualAppIdEditText = (EditText)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_et);
			Spinner registerdAppIdSpn = (Spinner)rpcView.FindViewById(Resource.Id.bc_on_activated_app_id_spinner);
			registerdAppIdSpn.Enabled = false;

			RadioGroup manualRegisterdAppIdRadioGroup = rpcView.FindViewById<RadioGroup>(Resource.Id.bc_on_activated_app_id_manual_radioGroup);
			RadioButton manualAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_manual);
			RadioButton registerdAppIdRadioButton = rpcView.FindViewById<RadioButton>(Resource.Id.bc_on_activated_app_id_registered_apps);

			appIDCheckBox.CheckedChange += (s, e) =>
			{
                manualRegisterdAppIdRadioGroup.Enabled = e.IsChecked;
                manualAppIdRadioButton.Enabled = e.IsChecked;
                registerdAppIdRadioButton.Enabled = e.IsChecked;
				if (e.IsChecked)
				{
					if (manualAppIdRadioButton.Checked)
					{
						manualAppIdEditText.Enabled = true;
						registerdAppIdSpn.Enabled = false;
					}
					else
					{
						registerdAppIdSpn.Enabled = true;
						manualAppIdEditText.Enabled = false;
					}
				}
				else
				{
					registerdAppIdSpn.Enabled = false;
					manualAppIdEditText.Enabled = false;
				}
			};

			manualRegisterdAppIdRadioGroup.CheckedChange += (s, e) =>
			{
				manualAppIdEditText.Enabled = manualAppIdRadioButton.Checked;
				registerdAppIdSpn.Enabled = !manualAppIdRadioButton.Checked;
			};

			var appIDAdapter = new AppIDAdapter(Activity, AppInstanceManager.appList);
			registerdAppIdSpn.Adapter = appIDAdapter;

			HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppActivated tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppActivated();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppActivated)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications.OnAppActivated>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				manualAppIdEditText.Text = tmpObj.getAppId().ToString();
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetTitle("OnAppActivated");

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
             {
                 RadioButton manualRegisterdSelectedOptionRadioButton = rpcView.FindViewById<RadioButton>(manualRegisterdAppIdRadioGroup.CheckedRadioButtonId);
                 string name = manualRegisterdSelectedOptionRadioButton.Text;

                 int? selectedAppID = null;
                 if (appIDCheckBox.Checked)
                 {
                     if (name.Equals("Registered Apps"))
                     {
                         if (registerdAppIdSpn.Adapter.Count == 0)
                         {
                             Toast.MakeText(Application.Context, "No App Registered", ToastLength.Short).Show();
                             return;
                         }
                         else
                         {
                             selectedAppID = AppInstanceManager.appList[registerdAppIdSpn.SelectedItemPosition].getAppID();
                         }
                     }
                     else if (name.Equals("Manual"))
                     {
                         if (manualAppIdEditText.Text != null && manualAppIdEditText.Text.Length > 0)
                             selectedAppID = Java.Lang.Integer.ParseInt(manualAppIdEditText.Text);
                     }
                 }
                 RequestNotifyMessage rpcMessage = BuildRpc.buildBasicCommunicationOnAppActivated(selectedAppID);
                 AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                 AppInstanceManager.Instance.sendRpc(rpcMessage);
             });
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			 {
				 if (tmpObj != null)
				 {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				 }
			 });

			rpcAlertDialog.Show();
		}


        private void CreateVIResponseUnSubscribeVehicleData()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.subscribe_vehicle_data_response, null);
            rpcAlertDialog.SetView(rpcView);
            rpcAlertDialog.SetTitle(VIResponseUnsubscribeVehicleData);

            var resultCode_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_result_code_chk);
            var gps_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_gps);
            var speed_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_speed);
            var rpm_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_rpm);
            var fuelLevel_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level);
            var fuelLevel_State_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_state);
            var instantFuelConsumption_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_instant_fuel_consumption);
            var externalTemperature_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_external_temperature);
            var prndl_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_prndl);
            var tirePressure_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_tire_pressure);
            var odometer_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_odometer);
            var beltStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_belt_status);
            var bodyInformation_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_body_info);
            var deviceStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_device_status);
            var driverBraking_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_driver_braking);
            var wiperStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_wiper_status);
            var headLampStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_head_lamp_status);
            var engineTorque_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_torque);
            var accPedalPosition_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_acc_pedal_pos);
            var steeringWheelAngle_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_steering_whel_angle);
            var eCallInfo_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_ecall_info);
            var airbagStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_airbag_status);
            var emergencyEvent_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_emergency_event);
            var clusterModes_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_cluster_modes);
            var myKey_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_my_key);
			var electronicParkBrakeStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_electronic_park_brake_status);
            var engineOilLife_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_oil_life);
            var fuelRange_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_range);

			var gps_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_gps_data_type_chk);
            var speed_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_speed_data_type_chk);
            var rpm_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_rpm_date_type_chk);
            var fuelLevel_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_data_type_chk);
            var fuelLevel_State_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_state_data_type_chk);
            var instantFuelConsumption_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_data_type_chk);
            var externalTemperature_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_external_temperature_data_type_chk);
            var prndl_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_prndl_data_type_chk);
            var tirePressure_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_tire_pressure_data_type_chk);
            var odometer_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_odometer_data_type_chk);
            var beltStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_belt_status_data_type_chk);
            var bodyInformation_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_body_info_data_type_chk);
            var deviceStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_device_status_data_type_chk);
            var driverBraking_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_driver_braking_data_type_chk);
            var wiperStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_wiper_status_data_type_chk);
            var headLampStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_head_lamp_status_data_type_chk);
            var engineTorque_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_torque_data_type_chk);
            var accPedalPosition_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_acc_pedal_pos_data_type_chk);
            var steeringWheelAngle_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_steering_whel_angle_data_type_chk);
            var eCallInfo_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_ecall_info_data_type_chk);
            var airbagStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_airbag_status_data_type_chk);
            var emergencyEvent_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_emergency_event_data_type_chk);
            var clusterModes_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_cluster_modes_data_type_chk);
            var myKey_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_my_key_data_type_chk);
			var electronicParkBrakeStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_data_type_chk);
            var engineOilLife_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_oil_life_data_type_chk);
            var fuelRange_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_range_data_type_chk);

			var gps_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_gps_result_code_chk);
            var speed_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_speed_result_code_chk);
            var rpm_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_rpm_result_code_chk);
            var fuelLevel_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_result_code_chk);
            var fuelLevel_State_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_state_result_code_chk);
            var instantFuelConsumption_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_result_code_chk);
            var externalTemperature_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_external_temperature_result_code_chk);
            var prndl_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_prndl_result_code_chk);
            var tirePressure_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_tire_pressure_result_code_chk);
            var odometer_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_odometer_result_code_chk);
            var beltStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_belt_status_result_code_chk);
            var bodyInformation_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_body_info_result_code_chk);
            var deviceStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_device_status_result_code_chk);
            var driverBraking_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_driver_braking_result_code_chk);
            var wiperStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_wiper_status_result_code_chk);
            var headLampStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_head_lamp_status_result_code_chk);
            var engineTorque_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_torque_result_code_chk);
            var accPedalPosition_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_acc_pedal_pos_result_code_chk);
            var steeringWheelAngle_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_steering_whel_angle_result_code_chk);
            var eCallInfo_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_ecall_info_result_code_chk);
            var airbagStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_airbag_status_result_code_chk);
            var emergencyEvent_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_emergency_event_result_code_chk);
            var clusterModes_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_cluster_modes_result_code_chk);
            var myKey_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_my_key_result_code_chk);
			var electronicParkBrakeStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_result_code_chk);
            var engineOilLife_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_oil_life_result_code_chk);
            var fuelRange_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_range_result_code_chk);

			var gps_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_gps_data_type_spinner);
            var speed_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_speed_data_type_spinner);
            var rpm_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_rpm_date_type_spinner);
            var fuelLevel_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_data_type_spinner);
            var fuelLevel_State_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_state_data_type_spinner);
            var instantFuelConsumption_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_data_type_spinner);
            var externalTemperature_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_external_temperature_data_type_spinner);
            var prndl_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_prndl_data_type_spinner);
            var tirePressure_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_tire_pressure_data_type_spinner);
            var odometer_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_odometer_data_type_spinner);
            var beltStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_belt_status_data_type_spinner);
            var bodyInformation_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_body_info_data_type_spinner);
            var deviceStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_device_status_data_type_spinner);
            var driverBraking_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_driver_braking_data_type_spinner);
            var wiperStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_wiper_status_data_type_spinner);
            var headLampStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_head_lamp_status_data_type_spinner);
            var engineTorque_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_torque_data_type_spinner);
            var accPedalPosition_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_acc_pedal_pos_data_type_spinner);
            var steeringWheelAngle_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_steering_whel_angle_data_type_spinner);
            var eCallInfo_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_ecall_info_data_type_spinner);
            var airbagStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_airbag_status_data_type_spinner);
            var emergencyEvent_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_emergency_event_data_type_spinner);
            var clusterModes_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_cluster_modes_data_type_spinner);
            var myKey_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_my_key_data_type_spinner);
			var electronicParkBrakeStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_data_type_spinner);
            var engineOilLife_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_oil_life_data_type_spinner);
            var fuelRange_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_range_data_type_spinner);

			var result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_result_code_spinner);
            var gps_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_gps_result_code_spinner);
            var speed_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_speed_result_code_spinner);
            var rpm_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_rpm_result_code_spinner);
            var fuelLevel_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_result_code_spinner);
            var fuelLevel_State_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_state_result_code_spinner);
            var instantFuelConsumption_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_result_code_spinner);
            var externalTemperature_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_external_temperature_result_code_spinner);
            var prndl_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_prndl_result_code_spinner);
            var tirePressure_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_tire_pressure_result_code_spinner);
            var odometer_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_odometer_result_code_spinner);
            var beltStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_belt_status_result_code_spinner);
            var bodyInformation_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_body_info_result_code_spinner);
            var deviceStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_device_status_result_code_spinner);
            var driverBraking_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_driver_braking_result_code_spinner);
            var wiperStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_wiper_status_result_code_spinner);
            var headLampStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_head_lamp_status_result_code_spinner);
            var engineTorque_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_torque_result_code_spinner);
            var accPedalPosition_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_acc_pedal_pos_result_code_spinner);
            var steeringWheelAngle_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_steering_whel_angle_result_code_spinner);
            var eCallInfo_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_ecall_info_result_code_spinner);
            var airbagStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_airbag_status_result_code_spinner);
            var emergencyEvent_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_emergency_event_result_code_spinner);
            var clusterModes_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_cluster_modes_result_code_spinner);
            var myKey_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_my_key_result_code_spinner);
			var electronicParkBrakeStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_result_code_spinner);
            var engineOilLife_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_oil_life_result_code_spinner);
            var fuelRange_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_range_result_code_spinner);

			resultCode_chk.CheckedChange += (sender, e) => result_code_spinner.Enabled = e.IsChecked;

            gps_chk.CheckedChange += (sender, e) =>
            {
                gps_data_type_chk.Enabled = e.IsChecked;
                gps_result_code_chk.Enabled = e.IsChecked;
                gps_data_type_spinner.Enabled = e.IsChecked;
                gps_result_code_spinner.Enabled = e.IsChecked;
            };

            speed_chk.CheckedChange += (sender, e) =>
            {
                speed_data_type_chk.Enabled = e.IsChecked;
                speed_result_code_chk.Enabled = e.IsChecked;
                speed_data_type_spinner.Enabled = e.IsChecked;
                speed_result_code_spinner.Enabled = e.IsChecked;
            };

            rpm_chk.CheckedChange += (sender, e) =>
            {
                rpm_data_type_chk.Enabled = e.IsChecked;
                rpm_result_code_chk.Enabled = e.IsChecked;
                rpm_data_type_spinner.Enabled = e.IsChecked;
                rpm_result_code_spinner.Enabled = e.IsChecked;
            };

            fuelLevel_chk.CheckedChange += (sender, e) =>
            {
                fuelLevel_data_type_chk.Enabled = e.IsChecked;
                fuelLevel_result_code_chk.Enabled = e.IsChecked;
                fuelLevel_data_type_spinner.Enabled = e.IsChecked;
                fuelLevel_result_code_spinner.Enabled = e.IsChecked;
            };

            fuelLevel_State_chk.CheckedChange += (sender, e) =>
            {
                fuelLevel_State_data_type_chk.Enabled = e.IsChecked;
                fuelLevel_State_result_code_chk.Enabled = e.IsChecked;
                fuelLevel_State_data_type_spinner.Enabled = e.IsChecked;
                fuelLevel_State_result_code_spinner.Enabled = e.IsChecked;
            };

            instantFuelConsumption_chk.CheckedChange += (sender, e) =>
            {
                instantFuelConsumption_data_type_chk.Enabled = e.IsChecked;
                instantFuelConsumption_result_code_chk.Enabled = e.IsChecked;
                instantFuelConsumption_data_type_spinner.Enabled = e.IsChecked;
                instantFuelConsumption_result_code_spinner.Enabled = e.IsChecked;
            };

            externalTemperature_chk.CheckedChange += (sender, e) =>
            {
                externalTemperature_data_type_chk.Enabled = e.IsChecked;
                externalTemperature_result_code_chk.Enabled = e.IsChecked;
                externalTemperature_data_type_spinner.Enabled = e.IsChecked;
                externalTemperature_result_code_spinner.Enabled = e.IsChecked;
            };

            prndl_chk.CheckedChange += (sender, e) =>
            {
                prndl_data_type_chk.Enabled = e.IsChecked;
                prndl_result_code_chk.Enabled = e.IsChecked;
                prndl_data_type_spinner.Enabled = e.IsChecked;
                prndl_result_code_spinner.Enabled = e.IsChecked;
            };

            tirePressure_chk.CheckedChange += (sender, e) =>
            {
                tirePressure_data_type_chk.Enabled = e.IsChecked;
                tirePressure_result_code_chk.Enabled = e.IsChecked;
                tirePressure_data_type_spinner.Enabled = e.IsChecked;
                tirePressure_result_code_spinner.Enabled = e.IsChecked;
            };

            odometer_chk.CheckedChange += (sender, e) =>
            {
                odometer_data_type_chk.Enabled = e.IsChecked;
                odometer_result_code_chk.Enabled = e.IsChecked;
                odometer_data_type_spinner.Enabled = e.IsChecked;
                odometer_result_code_spinner.Enabled = e.IsChecked;
            };

            beltStatus_chk.CheckedChange += (sender, e) =>
            {
                beltStatus_data_type_chk.Enabled = e.IsChecked;
                beltStatus_result_code_chk.Enabled = e.IsChecked;
                beltStatus_data_type_spinner.Enabled = e.IsChecked;
                beltStatus_result_code_spinner.Enabled = e.IsChecked;
            };

            bodyInformation_chk.CheckedChange += (sender, e) =>
            {
                bodyInformation_data_type_chk.Enabled = e.IsChecked;
                bodyInformation_result_code_chk.Enabled = e.IsChecked;
                bodyInformation_data_type_spinner.Enabled = e.IsChecked;
                bodyInformation_result_code_spinner.Enabled = e.IsChecked;
            };

            deviceStatus_chk.CheckedChange += (sender, e) =>
            {
                deviceStatus_data_type_chk.Enabled = e.IsChecked;
                deviceStatus_result_code_chk.Enabled = e.IsChecked;
                deviceStatus_data_type_spinner.Enabled = e.IsChecked;
                deviceStatus_result_code_spinner.Enabled = e.IsChecked;
            };

            driverBraking_chk.CheckedChange += (sender, e) =>
            {
                driverBraking_data_type_chk.Enabled = e.IsChecked;
                driverBraking_result_code_chk.Enabled = e.IsChecked;
                driverBraking_data_type_spinner.Enabled = e.IsChecked;
                driverBraking_result_code_spinner.Enabled = e.IsChecked;
            };

            wiperStatus_chk.CheckedChange += (sender, e) =>
            {
                wiperStatus_data_type_chk.Enabled = e.IsChecked;
                wiperStatus_result_code_chk.Enabled = e.IsChecked;
                wiperStatus_data_type_spinner.Enabled = e.IsChecked;
                wiperStatus_result_code_spinner.Enabled = e.IsChecked;
            };

            headLampStatus_chk.CheckedChange += (sender, e) =>
            {
                headLampStatus_data_type_chk.Enabled = e.IsChecked;
                headLampStatus_result_code_chk.Enabled = e.IsChecked;
                headLampStatus_data_type_spinner.Enabled = e.IsChecked;
                headLampStatus_result_code_spinner.Enabled = e.IsChecked;
            };

            engineTorque_chk.CheckedChange += (sender, e) =>
            {
                engineTorque_data_type_chk.Enabled = e.IsChecked;
                engineTorque_result_code_chk.Enabled = e.IsChecked;
                engineTorque_data_type_spinner.Enabled = e.IsChecked;
                engineTorque_result_code_spinner.Enabled = e.IsChecked;
            };

            accPedalPosition_chk.CheckedChange += (sender, e) =>
            {
                accPedalPosition_data_type_chk.Enabled = e.IsChecked;
                accPedalPosition_result_code_chk.Enabled = e.IsChecked;
                accPedalPosition_data_type_spinner.Enabled = e.IsChecked;
                accPedalPosition_result_code_spinner.Enabled = e.IsChecked;
            };

            steeringWheelAngle_chk.CheckedChange += (sender, e) =>
            {
                steeringWheelAngle_data_type_chk.Enabled = e.IsChecked;
                steeringWheelAngle_result_code_chk.Enabled = e.IsChecked;
                steeringWheelAngle_data_type_spinner.Enabled = e.IsChecked;
                steeringWheelAngle_result_code_spinner.Enabled = e.IsChecked;
            };

            eCallInfo_chk.CheckedChange += (sender, e) =>
            {
                eCallInfo_data_type_chk.Enabled = e.IsChecked;
                eCallInfo_result_code_chk.Enabled = e.IsChecked;
                eCallInfo_data_type_spinner.Enabled = e.IsChecked;
                eCallInfo_result_code_spinner.Enabled = e.IsChecked;
            };

            airbagStatus_chk.CheckedChange += (sender, e) =>
            {
                airbagStatus_data_type_chk.Enabled = e.IsChecked;
                airbagStatus_result_code_chk.Enabled = e.IsChecked;
                airbagStatus_data_type_spinner.Enabled = e.IsChecked;
                airbagStatus_result_code_spinner.Enabled = e.IsChecked;
            };

            emergencyEvent_chk.CheckedChange += (sender, e) =>
            {
                emergencyEvent_data_type_chk.Enabled = e.IsChecked;
                emergencyEvent_result_code_chk.Enabled = e.IsChecked;
                emergencyEvent_data_type_spinner.Enabled = e.IsChecked;
                emergencyEvent_result_code_spinner.Enabled = e.IsChecked;
            };

            clusterModes_chk.CheckedChange += (sender, e) =>
            {
                clusterModes_data_type_chk.Enabled = e.IsChecked;
                clusterModes_result_code_chk.Enabled = e.IsChecked;
                clusterModes_data_type_spinner.Enabled = e.IsChecked;
                clusterModes_result_code_spinner.Enabled = e.IsChecked;
            };

            myKey_chk.CheckedChange += (sender, e) =>
            {
                myKey_data_type_chk.Enabled = e.IsChecked;
                myKey_result_code_chk.Enabled = e.IsChecked;
                myKey_data_type_spinner.Enabled = e.IsChecked;
                myKey_result_code_spinner.Enabled = e.IsChecked;
            };

			electronicParkBrakeStatus_chk.CheckedChange += (sender, e) =>
			{
				electronicParkBrakeStatus_data_type_chk.Enabled = e.IsChecked;
				electronicParkBrakeStatus_result_code_chk.Enabled = e.IsChecked;
				electronicParkBrakeStatus_data_type_spinner.Enabled = e.IsChecked;
				electronicParkBrakeStatus_result_code_spinner.Enabled = e.IsChecked;
			};

            engineOilLife_chk.CheckedChange += (sender, e) =>
            {
                engineOilLife_data_type_chk.Enabled = e.IsChecked;
                engineOilLife_result_code_chk.Enabled = e.IsChecked;
                engineOilLife_data_type_spinner.Enabled = e.IsChecked && engineOilLife_data_type_chk.Checked;
                engineOilLife_result_code_spinner.Enabled = e.IsChecked && engineOilLife_result_code_chk.Checked;
            };

            fuelRange_chk.CheckedChange += (sender, e) =>
            {
                fuelRange_data_type_chk.Enabled = e.IsChecked;
                fuelRange_result_code_chk.Enabled = e.IsChecked;
                fuelRange_data_type_spinner.Enabled = e.IsChecked && fuelRange_data_type_chk.Checked;
                fuelRange_result_code_spinner.Enabled = e.IsChecked && fuelRange_result_code_chk.Checked;
            };


			gps_data_type_chk.CheckedChange += (sender, e) => gps_data_type_spinner.Enabled = e.IsChecked;
            speed_data_type_chk.CheckedChange += (sender, e) => speed_data_type_spinner.Enabled = e.IsChecked;
            rpm_data_type_chk.CheckedChange += (sender, e) => rpm_data_type_spinner.Enabled = e.IsChecked;
            fuelLevel_data_type_chk.CheckedChange += (sender, e) => fuelLevel_data_type_spinner.Enabled = e.IsChecked;
            fuelLevel_State_data_type_chk.CheckedChange += (sender, e) => fuelLevel_State_data_type_spinner.Enabled = e.IsChecked;
            instantFuelConsumption_data_type_chk.CheckedChange += (sender, e) => instantFuelConsumption_data_type_spinner.Enabled = e.IsChecked;
            externalTemperature_data_type_chk.CheckedChange += (sender, e) => externalTemperature_data_type_spinner.Enabled = e.IsChecked;
            prndl_data_type_chk.CheckedChange += (sender, e) => prndl_data_type_spinner.Enabled = e.IsChecked;
            tirePressure_data_type_chk.CheckedChange += (sender, e) => tirePressure_data_type_spinner.Enabled = e.IsChecked;
            odometer_data_type_chk.CheckedChange += (sender, e) => odometer_data_type_spinner.Enabled = e.IsChecked;
            beltStatus_data_type_chk.CheckedChange += (sender, e) => beltStatus_data_type_spinner.Enabled = e.IsChecked;
            bodyInformation_data_type_chk.CheckedChange += (sender, e) => bodyInformation_data_type_spinner.Enabled = e.IsChecked;
            deviceStatus_data_type_chk.CheckedChange += (sender, e) => deviceStatus_data_type_spinner.Enabled = e.IsChecked;
            driverBraking_data_type_chk.CheckedChange += (sender, e) => driverBraking_data_type_spinner.Enabled = e.IsChecked;
            wiperStatus_data_type_chk.CheckedChange += (sender, e) => wiperStatus_data_type_spinner.Enabled = e.IsChecked;
            headLampStatus_data_type_chk.CheckedChange += (sender, e) => headLampStatus_data_type_spinner.Enabled = e.IsChecked;
            engineTorque_data_type_chk.CheckedChange += (sender, e) => engineTorque_data_type_spinner.Enabled = e.IsChecked;
            accPedalPosition_data_type_chk.CheckedChange += (sender, e) => accPedalPosition_data_type_spinner.Enabled = e.IsChecked;
            steeringWheelAngle_data_type_chk.CheckedChange += (sender, e) => steeringWheelAngle_data_type_spinner.Enabled = e.IsChecked;
            eCallInfo_data_type_chk.CheckedChange += (sender, e) => eCallInfo_data_type_spinner.Enabled = e.IsChecked;
            airbagStatus_data_type_chk.CheckedChange += (sender, e) => airbagStatus_data_type_spinner.Enabled = e.IsChecked;
            emergencyEvent_data_type_chk.CheckedChange += (sender, e) => emergencyEvent_data_type_spinner.Enabled = e.IsChecked;
            clusterModes_data_type_chk.CheckedChange += (sender, e) => clusterModes_data_type_spinner.Enabled = e.IsChecked;
            myKey_data_type_chk.CheckedChange += (sender, e) => myKey_data_type_spinner.Enabled = e.IsChecked;
			electronicParkBrakeStatus_data_type_chk.CheckedChange += (sender, e) => electronicParkBrakeStatus_data_type_spinner.Enabled = e.IsChecked;
            engineOilLife_data_type_chk.CheckedChange += (sender, e) => engineOilLife_data_type_spinner.Enabled = e.IsChecked;
            fuelRange_data_type_chk.CheckedChange += (sender, e) => fuelRange_data_type_spinner.Enabled = e.IsChecked;

			gps_result_code_chk.CheckedChange += (sender, e) => gps_result_code_spinner.Enabled = e.IsChecked;
            speed_result_code_chk.CheckedChange += (sender, e) => speed_result_code_spinner.Enabled = e.IsChecked;
            rpm_result_code_chk.CheckedChange += (sender, e) => rpm_result_code_spinner.Enabled = e.IsChecked;
            fuelLevel_result_code_chk.CheckedChange += (sender, e) => fuelLevel_result_code_spinner.Enabled = e.IsChecked;
            fuelLevel_State_result_code_chk.CheckedChange += (sender, e) => fuelLevel_State_result_code_spinner.Enabled = e.IsChecked;
            instantFuelConsumption_result_code_chk.CheckedChange += (sender, e) => instantFuelConsumption_result_code_spinner.Enabled = e.IsChecked;
            externalTemperature_result_code_chk.CheckedChange += (sender, e) => externalTemperature_result_code_spinner.Enabled = e.IsChecked;
            prndl_result_code_chk.CheckedChange += (sender, e) => prndl_result_code_spinner.Enabled = e.IsChecked;
            tirePressure_result_code_chk.CheckedChange += (sender, e) => tirePressure_result_code_spinner.Enabled = e.IsChecked;
            odometer_result_code_chk.CheckedChange += (sender, e) => odometer_result_code_spinner.Enabled = e.IsChecked;
            beltStatus_result_code_chk.CheckedChange += (sender, e) => beltStatus_result_code_spinner.Enabled = e.IsChecked;
            bodyInformation_result_code_chk.CheckedChange += (sender, e) => bodyInformation_result_code_spinner.Enabled = e.IsChecked;
            deviceStatus_result_code_chk.CheckedChange += (sender, e) => deviceStatus_result_code_spinner.Enabled = e.IsChecked;
            driverBraking_result_code_chk.CheckedChange += (sender, e) => driverBraking_result_code_spinner.Enabled = e.IsChecked;
            wiperStatus_result_code_chk.CheckedChange += (sender, e) => wiperStatus_result_code_spinner.Enabled = e.IsChecked;
            headLampStatus_result_code_chk.CheckedChange += (sender, e) => headLampStatus_result_code_spinner.Enabled = e.IsChecked;
            engineTorque_result_code_chk.CheckedChange += (sender, e) => engineTorque_result_code_spinner.Enabled = e.IsChecked;
            accPedalPosition_result_code_chk.CheckedChange += (sender, e) => accPedalPosition_result_code_spinner.Enabled = e.IsChecked;
            steeringWheelAngle_result_code_chk.CheckedChange += (sender, e) => steeringWheelAngle_result_code_spinner.Enabled = e.IsChecked;
            eCallInfo_result_code_chk.CheckedChange += (sender, e) => eCallInfo_result_code_spinner.Enabled = e.IsChecked;
            airbagStatus_result_code_chk.CheckedChange += (sender, e) => airbagStatus_result_code_spinner.Enabled = e.IsChecked;
            emergencyEvent_result_code_chk.CheckedChange += (sender, e) => emergencyEvent_result_code_spinner.Enabled = e.IsChecked;
            clusterModes_result_code_chk.CheckedChange += (sender, e) => clusterModes_result_code_spinner.Enabled = e.IsChecked;
            myKey_result_code_chk.CheckedChange += (sender, e) => myKey_result_code_spinner.Enabled = e.IsChecked;
			electronicParkBrakeStatus_result_code_chk.CheckedChange += (sender, e) => electronicParkBrakeStatus_result_code_spinner.Enabled = e.IsChecked;
            engineOilLife_result_code_chk.CheckedChange += (sender, e) => engineOilLife_result_code_spinner.Enabled = e.IsChecked;
            fuelRange_result_code_chk.CheckedChange += (sender, e) => fuelRange_result_code_spinner.Enabled = e.IsChecked;

			var vehicleDataTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataType);
            gps_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            speed_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            rpm_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            fuelLevel_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            fuelLevel_State_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            instantFuelConsumption_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            externalTemperature_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            prndl_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            tirePressure_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            odometer_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            beltStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            bodyInformation_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            deviceStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            driverBraking_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            wiperStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            headLampStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            engineTorque_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            accPedalPosition_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            steeringWheelAngle_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            eCallInfo_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            airbagStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            emergencyEvent_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            clusterModes_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			myKey_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			electronicParkBrakeStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            engineOilLife_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            fuelRange_data_type_spinner.Adapter = vehicleDataTypeAdapter;

            var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			var vehicleDataResultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataResultCode);
            result_code_spinner.Adapter = resultCodeAdapter;
            gps_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            speed_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            rpm_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            fuelLevel_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            fuelLevel_State_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            instantFuelConsumption_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            externalTemperature_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            prndl_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            tirePressure_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            odometer_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            beltStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            bodyInformation_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            deviceStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            driverBraking_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            wiperStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            headLampStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            engineTorque_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            accPedalPosition_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            steeringWheelAngle_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            eCallInfo_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            airbagStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            emergencyEvent_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            clusterModes_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			myKey_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			electronicParkBrakeStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            engineOilLife_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            fuelRange_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
				result_code_spinner.SetSelection((int)tmpObj.getResultCode());

				gps_data_type_spinner.SetSelection((int)tmpObj.getGps().getDataType());
				gps_result_code_spinner.SetSelection((int)tmpObj.getGps().getResultCode());

				speed_data_type_spinner.SetSelection((int)tmpObj.getSpeed().getDataType());
				speed_result_code_spinner.SetSelection((int)tmpObj.getSpeed().getResultCode());

				rpm_data_type_spinner.SetSelection((int)tmpObj.getRpm().getDataType());
				rpm_result_code_spinner.SetSelection((int)tmpObj.getRpm().getResultCode());

				fuelLevel_data_type_spinner.SetSelection((int)tmpObj.getFuelLevel().getDataType());
				fuelLevel_result_code_spinner.SetSelection((int)tmpObj.getFuelLevel_State().getResultCode());

				fuelLevel_State_data_type_spinner.SetSelection((int)tmpObj.getFuelLevel_State().getDataType());
				fuelLevel_State_result_code_spinner.SetSelection((int)tmpObj.getFuelLevel_State().getResultCode());

				instantFuelConsumption_data_type_spinner.SetSelection((int)tmpObj.getInstantFuelConsumption().getDataType());
				instantFuelConsumption_result_code_spinner.SetSelection((int)tmpObj.getInstantFuelConsumption().getResultCode());

				externalTemperature_data_type_spinner.SetSelection((int)tmpObj.getExternalTemperature().getDataType());
				externalTemperature_result_code_spinner.SetSelection((int)tmpObj.getExternalTemperature().getResultCode());

				prndl_data_type_spinner.SetSelection((int)tmpObj.getPrndl().getDataType());
				prndl_result_code_spinner.SetSelection((int)tmpObj.getPrndl().getResultCode());

				tirePressure_data_type_spinner.SetSelection((int)tmpObj.getTirePressure().getDataType());
				tirePressure_result_code_spinner.SetSelection((int)tmpObj.getTirePressure().getResultCode());

				odometer_data_type_spinner.SetSelection((int)tmpObj.getOdometer().getDataType());
				odometer_result_code_spinner.SetSelection((int)tmpObj.getOdometer().getResultCode());

				beltStatus_data_type_spinner.SetSelection((int)tmpObj.getBeltStatus().getDataType());
				beltStatus_result_code_spinner.SetSelection((int)tmpObj.getBeltStatus().getResultCode());

				bodyInformation_data_type_spinner.SetSelection((int)tmpObj.getBodyInformation().getDataType());
				bodyInformation_result_code_spinner.SetSelection((int)tmpObj.getBodyInformation().getResultCode());

				deviceStatus_data_type_spinner.SetSelection((int)tmpObj.getDeviceStatus().getDataType());
				deviceStatus_result_code_spinner.SetSelection((int)tmpObj.getDeviceStatus().getResultCode());

				driverBraking_data_type_spinner.SetSelection((int)tmpObj.getdriverBraking().getDataType());
				driverBraking_result_code_spinner.SetSelection((int)tmpObj.getdriverBraking().getResultCode());

				wiperStatus_data_type_spinner.SetSelection((int)tmpObj.getWiperStatus().getDataType());
				wiperStatus_result_code_spinner.SetSelection((int)tmpObj.getWiperStatus().getResultCode());

				headLampStatus_data_type_spinner.SetSelection((int)tmpObj.getHeadLampStatus().getDataType());
				headLampStatus_result_code_spinner.SetSelection((int)tmpObj.getHeadLampStatus().getResultCode());

				engineTorque_data_type_spinner.SetSelection((int)tmpObj.getEngineTorque().getDataType());
				engineTorque_result_code_spinner.SetSelection((int)tmpObj.getEngineTorque().getResultCode());

				accPedalPosition_data_type_spinner.SetSelection((int)tmpObj.getAccPedalPosition().getDataType());
				accPedalPosition_result_code_spinner.SetSelection((int)tmpObj.getAccPedalPosition().getResultCode());

				steeringWheelAngle_data_type_spinner.SetSelection((int)tmpObj.getSteeringWheelAngle().getDataType());
				steeringWheelAngle_result_code_spinner.SetSelection((int)tmpObj.getSteeringWheelAngle().getResultCode());

				eCallInfo_data_type_spinner.SetSelection((int)tmpObj.getECallInfo().getDataType());
				eCallInfo_result_code_spinner.SetSelection((int)tmpObj.getECallInfo().getResultCode());

				airbagStatus_data_type_spinner.SetSelection((int)tmpObj.getAirbagStatus().getDataType());
				airbagStatus_result_code_spinner.SetSelection((int)tmpObj.getAirbagStatus().getResultCode());

				emergencyEvent_data_type_spinner.SetSelection((int)tmpObj.getEmergencyEvent().getDataType());
				emergencyEvent_result_code_spinner.SetSelection((int)tmpObj.getEmergencyEvent().getResultCode());

				clusterModes_data_type_spinner.SetSelection((int)tmpObj.getClusterModes().getDataType());
				clusterModes_result_code_spinner.SetSelection((int)tmpObj.getClusterModes().getResultCode());

				myKey_data_type_spinner.SetSelection((int)tmpObj.getMyKey().getDataType());
				myKey_result_code_spinner.SetSelection((int)tmpObj.getMyKey().getResultCode());

				electronicParkBrakeStatus_data_type_spinner.SetSelection((int)tmpObj.getElectronicParkBrakeStatus().getDataType());
				electronicParkBrakeStatus_result_code_spinner.SetSelection((int)tmpObj.getElectronicParkBrakeStatus().getResultCode());

                engineOilLife_data_type_spinner.SetSelection((int)tmpObj.getEngineOilLife().getDataType());
                engineOilLife_result_code_spinner.SetSelection((int)tmpObj.getEngineOilLife().getResultCode());

                fuelRange_data_type_spinner.SetSelection((int)tmpObj.getFuelRange().getDataType());
                fuelRange_result_code_spinner.SetSelection((int)tmpObj.getFuelRange().getResultCode());

			}

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCode_chk.Checked)
                    rsltCode = (HmiApiLib.Common.Enums.Result)result_code_spinner.SelectedItemPosition;


                VehicleDataResult gps = new VehicleDataResult(), speed = new VehicleDataResult(), rpm = new VehicleDataResult(), fuelLevel = new VehicleDataResult(), fuelLevelState = new VehicleDataResult(), instantFuel = new VehicleDataResult(), externalTemperature = new VehicleDataResult(),
                                 prndl = new VehicleDataResult(), tirePressure = new VehicleDataResult(), odometer = new VehicleDataResult(), beltStatus = new VehicleDataResult(), bodyInformation = new VehicleDataResult(), deviceStatus = new VehicleDataResult(), driverBraking = new VehicleDataResult(),
                                 wiperStatus = new VehicleDataResult(), headLampStatus = new VehicleDataResult(), engineTorque = new VehicleDataResult(), accPedalPosition = new VehicleDataResult(), steeringWheelAngle = new VehicleDataResult(), eCallInfo = new VehicleDataResult(),
                airbagStatus = new VehicleDataResult(), emergencyEvent = new VehicleDataResult(), clusterModes = new VehicleDataResult(), myKey = new VehicleDataResult(), electronicParkBrakeStatus = new VehicleDataResult(), engineOilLife = new VehicleDataResult(), fuelRange = new VehicleDataResult();


                VehicleDataType gpsDataType = new VehicleDataType(), speedDataType = new VehicleDataType(), rpmDataType = new VehicleDataType(), fuelLevelDataType = new VehicleDataType(), fuelLevelStateDataType = new VehicleDataType(), instantFuelConsumptionDataType = new VehicleDataType(), externalTemperatureDataType = new VehicleDataType(),
                                 prndlDataType = new VehicleDataType(), tirePressureDataType = new VehicleDataType(), odometerDataType = new VehicleDataType(), beltStatusDataType = new VehicleDataType(), bodyInformationDataType = new VehicleDataType(), deviceStatusDataType = new VehicleDataType(), driverBrakingDataType = new VehicleDataType(),
                                 wiperStatusDataType = new VehicleDataType(), headLampStatusDataType = new VehicleDataType(), engineTorqueDataType = new VehicleDataType(), accPedalPositionDataType = new VehicleDataType(), steeringWheelAngleDataType = new VehicleDataType(), eCallInfoDataType = new VehicleDataType(),
                airbagStatusDataType = new VehicleDataType(), emergencyEventDataType = new VehicleDataType(), clusterModesDataType = new VehicleDataType(), myKeyDataType = new VehicleDataType(), electronicParkBrakeStatusDataType = new VehicleDataType(), engineOilLifeDataType = new VehicleDataType(), fuelRangeDataType = new VehicleDataType();

                VehicleDataResultCode gpsRsltCode = new VehicleDataResultCode(), speedRsltCode = new VehicleDataResultCode(), rpmRsltCode = new VehicleDataResultCode(), fuelLevelRsltCode = new VehicleDataResultCode(), fuelLevelStateRsltCode = new VehicleDataResultCode(), instantFuelConsumptionRsltCode = new VehicleDataResultCode(), externalTemperatureRsltCode = new VehicleDataResultCode(),
                                 prndlRsltCode = new VehicleDataResultCode(), tirePressureRsltCode = new VehicleDataResultCode(), odometerRsltCode = new VehicleDataResultCode(), beltStatusRsltCode = new VehicleDataResultCode(), bodyInformationRsltCode = new VehicleDataResultCode(), deviceStatusRsltCode = new VehicleDataResultCode(), driverBrakingRsltCode = new VehicleDataResultCode(),
                                 wiperStatusRsltCode = new VehicleDataResultCode(), headLampStatusRsltCode = new VehicleDataResultCode(), engineTorqueRsltCode = new VehicleDataResultCode(), accPedalPositionRsltCode = new VehicleDataResultCode(), steeringWheelAngleRsltCode = new VehicleDataResultCode(), eCallInfoRsltCode = new VehicleDataResultCode(),
                airbagStatusRsltCode = new VehicleDataResultCode(), emergencyEventRsltCode = new VehicleDataResultCode(), clusterModesRsltCode = new VehicleDataResultCode(), myKeyRsltCode = new VehicleDataResultCode(), electronicParkBrakeStatusRsltCode = new VehicleDataResultCode(), engineOilLifeRsltCode = new VehicleDataResultCode(), fuelRangeRsltCode = new VehicleDataResultCode();



                if (gps_data_type_chk.Checked)
                    gpsDataType = (VehicleDataType)gps_data_type_spinner.SelectedItemPosition;

                if (gps_result_code_chk.Checked)
                    gpsRsltCode = (VehicleDataResultCode)gps_result_code_spinner.SelectedItemPosition;

                if (gps_chk.Checked)
                {
                    gps.dataType = gpsDataType;
                    gps.resultCode = gpsRsltCode;
                }


                if (speed_data_type_chk.Checked)
                    speedDataType = (VehicleDataType)speed_data_type_spinner.SelectedItemPosition;

                if (speed_result_code_chk.Checked)
                    speedRsltCode = (VehicleDataResultCode)speed_result_code_spinner.SelectedItemPosition;

                if (speed_chk.Checked)
                {
                    speed.dataType = speedDataType;
                    speed.resultCode = speedRsltCode;
                }


                if (rpm_data_type_chk.Checked)
                    rpmDataType = (VehicleDataType)rpm_data_type_spinner.SelectedItemPosition;

                if (rpm_result_code_chk.Checked)
                    rpmRsltCode = (VehicleDataResultCode)rpm_result_code_spinner.SelectedItemPosition;

                if (rpm_chk.Checked)
                {
                    rpm.dataType = rpmDataType;
                    rpm.resultCode = rpmRsltCode;
                }


                if (fuelLevel_data_type_chk.Checked)
                    fuelLevelDataType = (VehicleDataType)fuelLevel_data_type_spinner.SelectedItemPosition;

                if (fuelLevel_result_code_chk.Checked)
                    fuelLevelRsltCode = (VehicleDataResultCode)fuelLevel_result_code_spinner.SelectedItemPosition;

                if (fuelLevel_chk.Checked)
                {
                    fuelLevel.dataType = fuelLevelDataType;
                    fuelLevel.resultCode = fuelLevelRsltCode;
                }


                if (fuelLevel_State_data_type_chk.Checked)
                    fuelLevelStateDataType = (VehicleDataType)fuelLevel_State_data_type_spinner.SelectedItemPosition;

                if (fuelLevel_State_result_code_chk.Checked)
                    fuelLevelStateRsltCode = (VehicleDataResultCode)fuelLevel_State_result_code_spinner.SelectedItemPosition;

                if (fuelLevel_State_chk.Checked)
                {
                    fuelLevelState.dataType = fuelLevelStateDataType;
                    fuelLevelState.resultCode = fuelLevelStateRsltCode;
                }


                if (instantFuelConsumption_data_type_chk.Checked)
                    instantFuelConsumptionDataType = (VehicleDataType)instantFuelConsumption_data_type_spinner.SelectedItemPosition;

                if (instantFuelConsumption_result_code_chk.Checked)
                    instantFuelConsumptionRsltCode = (VehicleDataResultCode)instantFuelConsumption_result_code_spinner.SelectedItemPosition;

                if (instantFuelConsumption_chk.Checked)
                {
                    instantFuel.dataType = instantFuelConsumptionDataType;
                    instantFuel.resultCode = instantFuelConsumptionRsltCode;
                }


                if (externalTemperature_data_type_chk.Checked)
                    externalTemperatureDataType = (VehicleDataType)externalTemperature_data_type_spinner.SelectedItemPosition;

                if (externalTemperature_result_code_chk.Checked)
                    externalTemperatureRsltCode = (VehicleDataResultCode)externalTemperature_result_code_spinner.SelectedItemPosition;

                if (externalTemperature_chk.Checked)
                {
                    externalTemperature.dataType = gpsDataType;
                    externalTemperature.resultCode = gpsRsltCode;
                }


                if (prndl_data_type_chk.Checked)
                    prndlDataType = (VehicleDataType)prndl_data_type_spinner.SelectedItemPosition;

                if (prndl_result_code_chk.Checked)
                    prndlRsltCode = (VehicleDataResultCode)prndl_result_code_spinner.SelectedItemPosition;

                if (prndl_chk.Checked)
                {
                    prndl.dataType = prndlDataType;
                    prndl.resultCode = prndlRsltCode;
                }


                if (tirePressure_data_type_chk.Checked)
                    tirePressureDataType = (VehicleDataType)tirePressure_data_type_spinner.SelectedItemPosition;

                if (tirePressure_result_code_chk.Checked)
                    tirePressureRsltCode = (VehicleDataResultCode)tirePressure_result_code_spinner.SelectedItemPosition;

                if (tirePressure_chk.Checked)
                {
                    tirePressure.dataType = tirePressureDataType;
                    tirePressure.resultCode = tirePressureRsltCode;
                }


                if (odometer_data_type_chk.Checked)
                    odometerDataType = (VehicleDataType)odometer_data_type_spinner.SelectedItemPosition;

                if (odometer_result_code_chk.Checked)
                    odometerRsltCode = (VehicleDataResultCode)odometer_result_code_spinner.SelectedItemPosition;

                if (odometer_chk.Checked)
                {
                    odometer.dataType = odometerDataType;
                    odometer.resultCode = odometerRsltCode;
                }


                if (beltStatus_data_type_chk.Checked)
                    beltStatusDataType = (VehicleDataType)beltStatus_data_type_spinner.SelectedItemPosition;

                if (beltStatus_result_code_chk.Checked)
                    beltStatusRsltCode = (VehicleDataResultCode)beltStatus_result_code_spinner.SelectedItemPosition;

                if (beltStatus_chk.Checked)
                {
                    beltStatus.dataType = beltStatusDataType;
                    beltStatus.resultCode = beltStatusRsltCode;
                }


                if (bodyInformation_data_type_chk.Checked)
                    bodyInformationDataType = (VehicleDataType)bodyInformation_data_type_spinner.SelectedItemPosition;

                if (bodyInformation_result_code_chk.Checked)
                    bodyInformationRsltCode = (VehicleDataResultCode)bodyInformation_result_code_spinner.SelectedItemPosition;

                if (bodyInformation_chk.Checked)
                {
                    bodyInformation.dataType = bodyInformationDataType;
                    bodyInformation.resultCode = bodyInformationRsltCode;
                }


                if (deviceStatus_data_type_chk.Checked)
                    deviceStatusDataType = (VehicleDataType)deviceStatus_data_type_spinner.SelectedItemPosition;

                if (deviceStatus_result_code_chk.Checked)
                    deviceStatusRsltCode = (VehicleDataResultCode)deviceStatus_result_code_spinner.SelectedItemPosition;

                if (deviceStatus_chk.Checked)
                {
                    deviceStatus.dataType = deviceStatusDataType;
                    deviceStatus.resultCode = deviceStatusRsltCode;
                }


                if (driverBraking_data_type_chk.Checked)
                    driverBrakingDataType = (VehicleDataType)driverBraking_data_type_spinner.SelectedItemPosition;

                if (driverBraking_result_code_chk.Checked)
                    driverBrakingRsltCode = (VehicleDataResultCode)driverBraking_result_code_spinner.SelectedItemPosition;

                if (driverBraking_chk.Checked)
                {
                    driverBraking.dataType = driverBrakingDataType;
                    driverBraking.resultCode = driverBrakingRsltCode;
                }


                if (wiperStatus_data_type_chk.Checked)
                    wiperStatusDataType = (VehicleDataType)wiperStatus_data_type_spinner.SelectedItemPosition;

                if (wiperStatus_result_code_chk.Checked)
                    wiperStatusRsltCode = (VehicleDataResultCode)wiperStatus_result_code_spinner.SelectedItemPosition;

                if (wiperStatus_chk.Checked)
                {
                    wiperStatus.dataType = wiperStatusDataType;
                    wiperStatus.resultCode = wiperStatusRsltCode;
                }


                if (headLampStatus_data_type_chk.Checked)
                    headLampStatusDataType = (VehicleDataType)headLampStatus_data_type_spinner.SelectedItemPosition;

                if (headLampStatus_result_code_chk.Checked)
                    headLampStatusRsltCode = (VehicleDataResultCode)headLampStatus_result_code_spinner.SelectedItemPosition;

                if (headLampStatus_chk.Checked)
                {
                    headLampStatus.dataType = headLampStatusDataType;
                    headLampStatus.resultCode = headLampStatusRsltCode;
                }


                if (engineTorque_data_type_chk.Checked)
                    engineTorqueDataType = (VehicleDataType)engineTorque_data_type_spinner.SelectedItemPosition;

                if (engineTorque_result_code_chk.Checked)
                    engineTorqueRsltCode = (VehicleDataResultCode)engineTorque_result_code_spinner.SelectedItemPosition;

                if (engineTorque_chk.Checked)
                {
                    engineTorque.dataType = engineTorqueDataType;
                    engineTorque.resultCode = engineTorqueRsltCode;
                }


                if (accPedalPosition_data_type_chk.Checked)
                    accPedalPositionDataType = (VehicleDataType)accPedalPosition_data_type_spinner.SelectedItemPosition;

                if (accPedalPosition_result_code_chk.Checked)
                    accPedalPositionRsltCode = (VehicleDataResultCode)accPedalPosition_result_code_spinner.SelectedItemPosition;

                if (accPedalPosition_chk.Checked)
                {
                    accPedalPosition.dataType = accPedalPositionDataType;
                    accPedalPosition.resultCode = accPedalPositionRsltCode;
                }

                if (steeringWheelAngle_data_type_chk.Checked)
                    steeringWheelAngleDataType = (VehicleDataType)steeringWheelAngle_data_type_spinner.SelectedItemPosition;

                if (steeringWheelAngle_result_code_chk.Checked)
                    steeringWheelAngleRsltCode = (VehicleDataResultCode)steeringWheelAngle_result_code_spinner.SelectedItemPosition;

                if (steeringWheelAngle_chk.Checked)
                {
                    steeringWheelAngle.dataType = steeringWheelAngleDataType;
                    steeringWheelAngle.resultCode = steeringWheelAngleRsltCode;
                }


                if (eCallInfo_data_type_chk.Checked)
                    eCallInfoDataType = (VehicleDataType)eCallInfo_data_type_spinner.SelectedItemPosition;

                if (eCallInfo_result_code_chk.Checked)
                    eCallInfoRsltCode = (VehicleDataResultCode)eCallInfo_result_code_spinner.SelectedItemPosition;

                if (eCallInfo_chk.Checked)
                {
                    eCallInfo.dataType = eCallInfoDataType;
                    eCallInfo.resultCode = eCallInfoRsltCode;
                }

                if (airbagStatus_data_type_chk.Checked)
                    airbagStatusDataType = (VehicleDataType)airbagStatus_data_type_spinner.SelectedItemPosition;

                if (airbagStatus_result_code_chk.Checked)
                    airbagStatusRsltCode = (VehicleDataResultCode)airbagStatus_result_code_spinner.SelectedItemPosition;

                if (airbagStatus_chk.Checked)
                {
                    airbagStatus.dataType = airbagStatusDataType;
                    airbagStatus.resultCode = airbagStatusRsltCode;
                }

                if (emergencyEvent_data_type_chk.Checked)
                    emergencyEventDataType = (VehicleDataType)emergencyEvent_data_type_spinner.SelectedItemPosition;

                if (emergencyEvent_result_code_chk.Checked)
                    emergencyEventRsltCode = (VehicleDataResultCode)emergencyEvent_result_code_spinner.SelectedItemPosition;

                if (emergencyEvent_chk.Checked)
                {
                    emergencyEvent.dataType = emergencyEventDataType;
                    emergencyEvent.resultCode = emergencyEventRsltCode;
                }

                if (clusterModes_data_type_chk.Checked)
                    clusterModesDataType = (VehicleDataType)clusterModes_data_type_spinner.SelectedItemPosition;

                if (clusterModes_result_code_chk.Checked)
                    clusterModesRsltCode = (VehicleDataResultCode)clusterModes_result_code_spinner.SelectedItemPosition;

                if (clusterModes_chk.Checked)
                {
                    clusterModes.dataType = clusterModesDataType;
                    clusterModes.resultCode = clusterModesRsltCode;
                }

                if (myKey_data_type_chk.Checked)
                    myKeyDataType = (VehicleDataType)myKey_data_type_spinner.SelectedItemPosition;

                if (myKey_result_code_chk.Checked)
                    myKeyRsltCode = (VehicleDataResultCode)myKey_result_code_spinner.SelectedItemPosition;

                if (myKey_chk.Checked)
                {
                    myKey.dataType = myKeyDataType;
                    myKey.resultCode = myKeyRsltCode;
                }

                if (electronicParkBrakeStatus_data_type_chk.Checked)
                    electronicParkBrakeStatusDataType = (VehicleDataType)electronicParkBrakeStatus_data_type_spinner.SelectedItemPosition;

                if (electronicParkBrakeStatus_result_code_chk.Checked)
                    electronicParkBrakeStatusRsltCode = (VehicleDataResultCode)electronicParkBrakeStatus_result_code_spinner.SelectedItemPosition;

                if (electronicParkBrakeStatus_chk.Checked)
                {
                    electronicParkBrakeStatus.dataType = electronicParkBrakeStatusDataType;
                    electronicParkBrakeStatus.resultCode = electronicParkBrakeStatusRsltCode;
                }


                if (engineOilLife_data_type_chk.Checked)
                    engineOilLifeDataType = (VehicleDataType)engineOilLife_data_type_spinner.SelectedItemPosition;

                if (engineOilLife_result_code_chk.Checked)
                    engineOilLifeRsltCode = (VehicleDataResultCode)engineOilLife_result_code_spinner.SelectedItemPosition;

                if (engineOilLife_chk.Checked)
                {
                    engineOilLife.dataType = engineOilLifeDataType;
                    engineOilLife.resultCode = engineOilLifeRsltCode;
                }


                if (fuelRange_data_type_chk.Checked)
                    fuelRangeDataType = (VehicleDataType)fuelRange_data_type_spinner.SelectedItemPosition;

                if (fuelRange_result_code_chk.Checked)
                    fuelRangeRsltCode = (VehicleDataResultCode)fuelRange_result_code_spinner.SelectedItemPosition;

                if (fuelRange_chk.Checked)
                {
                    fuelRange.dataType = fuelRangeDataType;
                    fuelRange.resultCode = fuelRangeRsltCode;
                }

                RpcResponse rpcResponse = BuildRpc.buildVehicleInfoUnsubscribeVehicleDataResponse(BuildRpc.getNextId(), rsltCode, gps, speed, rpm, fuelLevel, 
                                                                                                  fuelLevelState, instantFuel, externalTemperature, prndl, tirePressure, odometer, beltStatus,
                                                                                      bodyInformation, deviceStatus, driverBraking, wiperStatus, headLampStatus, engineTorque, 
                                                                                                  accPedalPosition, steeringWheelAngle, eCallInfo, airbagStatus, emergencyEvent, clusterModes, myKey, 
                                                                                                  electronicParkBrakeStatus, engineOilLife, fuelRange);

                AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, rpcResponse.getMethod(), rpcResponse);
            });


			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
                }
            });

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.Show();
        }

        void CreateVINotificationOnVehicleData()
        {
			Android.Support.V7.App.AlertDialog.Builder rpcAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.vi_get_vehicle_data, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(VINotificationOnVehicleData);

            rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_result_code_chk).Visibility = ViewStates.Gone;
			rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_result_code_spinner).Visibility = ViewStates.Gone;

			CheckBox gps_data_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_chk);
			Button gps_data_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_btn);

			CheckBox speed_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_speed_chk);
			EditText speed_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_speed_et);

			CheckBox rpm_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_rpm_chk);
			EditText rpm_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_rpm_et);

			CheckBox fuel_level_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_chk);
			EditText fuel_level_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_et);

			CheckBox fuel_level_state_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_state_chk);
			Spinner fuel_level_state_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_level_state_spinner);

			CheckBox instant_fuel_consumption_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_instant_fuel_consumption_chk);
			EditText instant_fuel_consumption_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_instant_fuel_consumption_et);

			CheckBox external_temp_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_external_temp_chk);
			EditText external_temp_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_external_temp_et);

			CheckBox vin_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_vin_chk);
			EditText vin_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_vin_et);

			CheckBox prndl_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_prndl_chk);
			Spinner prndl_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_prndl_spinner);

			CheckBox tire_pressure_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_tire_pressure_chk);
			Button tire_pressure_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_tire_pressure_btn);

			CheckBox odometer_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_odometer_chk);
			EditText odometer_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_odometer_et);

			CheckBox belt_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_belt_status_chk);
			Button belt_status_button = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_belt_status_button);

			CheckBox body_info_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_body_info_chk);
			Button body_info_button = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_body_info_button);

			CheckBox device_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_device_status_chk);
			Button device_status_button = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_device_status_button);

			CheckBox driver_braking_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_driver_braking_chk);
			Spinner driver_braking_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_driver_braking_spinner);

			CheckBox wiper_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_wiper_status_chk);
			Spinner wiper_status_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_wiper_status_spinner);

			CheckBox head_lamp_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_head_lamp_status_chk);
			Button head_lamp_status_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_head_lamp_status_btn);

			CheckBox engine_torque_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_torque_chk);
			EditText engine_torque_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_torque_et);

			CheckBox acc_padel_position_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_acc_padel_position_chk);
			EditText acc_padel_position_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_acc_padel_position_et);

			CheckBox steering_wheel_angle_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_steering_wheel_angle_chk);
			EditText steering_wheel_angle_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_steering_wheel_angle_et);

			CheckBox ecall_info_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_ecall_info_chk);
			Button ecall_info_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_ecall_info_btn);

			CheckBox airbag_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_airbag_status_chk);
			Button airbag_status_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_airbag_status_btn);

			CheckBox emergency_event_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_emergency_event_chk);
			Button emergency_event_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_emergency_event_btn);

			CheckBox cluster_modes_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_cluster_modes_chk);
			Button cluster_modes_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_cluster_modes_btn);

			CheckBox my_key_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_my_key_chk);
			Spinner my_key_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_my_key_spinner);

			CheckBox electronic_park_brake_status_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_electronic_park_brake_status_chk);
			Spinner electronic_park_brake_status_spinner = (Spinner)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_electronic_park_brake_status_spinner);

            CheckBox engine_oil_life_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_oil_life_chk);
            EditText engine_oil_life_et = (EditText)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_engine_oil_life_et);

            CheckBox fuel_range_chk = (CheckBox)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_range_chk);
            Button fuel_range_btn = (Button)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_range_btn);
            ListView fuel_range_lv = (ListView)rpcView.FindViewById(Resource.Id.vi_get_vehicle_data_fuel_range_lv);
            List<FuelRange> fuelRangeList = null;

			var ElectronicParkBrakeStatus = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, VIElectronicParkBrakeStatusArray);
			electronic_park_brake_status_spinner.Adapter = ElectronicParkBrakeStatus;

			var ComponentVolumeStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, ComponentVolumeStatusArray);
			fuel_level_state_spinner.Adapter = ComponentVolumeStatusAdapter;

			var PRNDLAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, PRNDLArray);
			prndl_spinner.Adapter = PRNDLAdapter;

			var VehicleDataEventStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, VehicleDataEventStatusArray);
			driver_braking_spinner.Adapter = VehicleDataEventStatusAdapter;

			var WiperStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, WiperStatusArray);
			wiper_status_spinner.Adapter = WiperStatusAdapter;

			var VehicleDataStatus = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, VehicleDataStatusArray);
			my_key_spinner.Adapter = VehicleDataStatus;

			gps_data_chk.CheckedChange += (s, e) => gps_data_btn.Enabled = e.IsChecked;
			speed_chk.CheckedChange += (s, e) => speed_et.Enabled = e.IsChecked;
			rpm_chk.CheckedChange += (s, e) => rpm_et.Enabled = e.IsChecked;
			fuel_level_chk.CheckedChange += (s, e) => fuel_level_et.Enabled = e.IsChecked;
			fuel_level_state_chk.CheckedChange += (s, e) => fuel_level_state_spinner.Enabled = e.IsChecked;
			instant_fuel_consumption_chk.CheckedChange += (s, e) => instant_fuel_consumption_et.Enabled = e.IsChecked;
			external_temp_chk.CheckedChange += (s, e) => external_temp_et.Enabled = e.IsChecked;
			vin_chk.CheckedChange += (s, e) => vin_et.Enabled = e.IsChecked;
			prndl_chk.CheckedChange += (s, e) => prndl_spinner.Enabled = e.IsChecked;
			tire_pressure_chk.CheckedChange += (s, e) => tire_pressure_btn.Enabled = e.IsChecked;
			odometer_chk.CheckedChange += (s, e) => odometer_et.Enabled = e.IsChecked;
			belt_status_chk.CheckedChange += (s, e) => belt_status_button.Enabled = e.IsChecked;
			body_info_chk.CheckedChange += (s, e) => body_info_button.Enabled = e.IsChecked;
			device_status_chk.CheckedChange += (s, e) => device_status_button.Enabled = e.IsChecked;
			driver_braking_chk.CheckedChange += (s, e) => driver_braking_spinner.Enabled = e.IsChecked;
			wiper_status_chk.CheckedChange += (s, e) => wiper_status_spinner.Enabled = e.IsChecked;
			head_lamp_status_chk.CheckedChange += (s, e) => head_lamp_status_btn.Enabled = e.IsChecked;
			engine_torque_chk.CheckedChange += (s, e) => engine_torque_et.Enabled = e.IsChecked;
			acc_padel_position_chk.CheckedChange += (s, e) => acc_padel_position_et.Enabled = e.IsChecked;
			steering_wheel_angle_chk.CheckedChange += (s, e) => steering_wheel_angle_et.Enabled = e.IsChecked;
			ecall_info_chk.CheckedChange += (s, e) => ecall_info_btn.Enabled = e.IsChecked;
			airbag_status_chk.CheckedChange += (s, e) => airbag_status_btn.Enabled = e.IsChecked;
			emergency_event_chk.CheckedChange += (s, e) => emergency_event_btn.Enabled = e.IsChecked;
			cluster_modes_chk.CheckedChange += (s, e) => cluster_modes_btn.Enabled = e.IsChecked;
			my_key_chk.CheckedChange += (s, e) => my_key_spinner.Enabled = e.IsChecked;
            electronic_park_brake_status_chk.CheckedChange += (s, e) => electronic_park_brake_status_spinner.Enabled = e.IsChecked;
            engine_oil_life_chk.CheckedChange += (s, e) => engine_oil_life_et.Enabled = e.IsChecked;
            fuel_range_chk.CheckedChange += (s, e) =>
            {
                fuel_range_btn.Enabled = e.IsChecked;
                fuel_range_lv.Enabled = e.IsChecked;
            };

            HmiApiLib.Controllers.VehicleInfo.OutGoingNotifications.OnVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutGoingNotifications.OnVehicleData();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutGoingNotifications.OnVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutGoingNotifications.OnVehicleData>(Activity, tmpObj.getMethod());

			GPSData viGPSData = null;
			TireStatus viTireStatus = null;
			BeltStatus viBeltStatus = null;
			BodyInformation viBodyInformation = null;
			DeviceStatus viDeviceStatus = null;
			HeadLampStatus viHeadLampStatus = null;
			ECallInfo viECallInfo = null;
			AirbagStatus viAirbagStatus = null;
			EmergencyEvent viEmergencyEvent = null;
			ClusterModeStatus viClusterModeStatus = null;

			if (tmpObj != null)
			{
				viGPSData = tmpObj.getGps();
				if (tmpObj.getSpeed() != null)
				{
					speed_et.Text = tmpObj.getSpeed().ToString();
				}
				if (tmpObj.getRpm() != null)
				{
					rpm_et.Text = tmpObj.getRpm().ToString();
				}
				if (tmpObj.getFuelLevel() != null)
				{
					fuel_level_et.Text = tmpObj.getFuelLevel().ToString();
				}
				if (tmpObj.getFuelLevel_State() != null)
				{
					fuel_level_state_spinner.SetSelection((int)tmpObj.getFuelLevel_State());
				}
				if (tmpObj.getInstantFuelConsumption() != null)
				{
					instant_fuel_consumption_et.Text = tmpObj.getInstantFuelConsumption().ToString();
				}
				if (tmpObj.getExternalTemperature() != null)
				{
					external_temp_et.Text = tmpObj.getExternalTemperature().ToString();
				}
				if (tmpObj.getVin() != null)
				{
					vin_et.Text = tmpObj.getVin();
				}

				if (tmpObj.getPrndl() != null)
				{
					prndl_spinner.SetSelection((int)tmpObj.getPrndl());
				}
				viTireStatus = tmpObj.getTirePressure();
				if (tmpObj.getOdometer() != null)
				{
					odometer_et.Text = tmpObj.getOdometer().ToString();
				}
				viBeltStatus = tmpObj.getBeltStatus();
				viBodyInformation = tmpObj.getBodyInformation();
				viDeviceStatus = tmpObj.getDeviceStatus();

				if (tmpObj.getDriverBraking() != null)
				{
					driver_braking_spinner.SetSelection((int)tmpObj.getDriverBraking());
				}

				if (tmpObj.getWiperStatus() != null)
				{
					wiper_status_spinner.SetSelection((int)tmpObj.getWiperStatus());
				}
				viHeadLampStatus = tmpObj.getHeadLampStatus();
				if (tmpObj.getEngineTorque() != null)
				{
					engine_torque_et.Text = tmpObj.getEngineTorque().ToString();
				}
				if (tmpObj.getAccPedalPosition() != null)
				{
					acc_padel_position_et.Text = tmpObj.getAccPedalPosition().ToString();
				}
				if (tmpObj.getSteeringWheelAngle() != null)
				{
					steering_wheel_angle_et.Text = tmpObj.getSteeringWheelAngle().ToString();
				}
				viECallInfo = tmpObj.getECallInfo();
				viAirbagStatus = tmpObj.getAirbagStatus();
				viEmergencyEvent = tmpObj.getEmergencyEvent();
				viClusterModeStatus = tmpObj.getClusterModes();
				if (tmpObj.getMyKey() != null)
				{
					my_key_spinner.SetSelection((int)tmpObj.getMyKey().getE911Override());
				}
                if (tmpObj.getElectronicParkBrakeStatus() != null)
				{
					electronic_park_brake_status_spinner.SetSelection((int)tmpObj.getElectronicParkBrakeStatus());
				}
                if (tmpObj.getEngineOilLife() != null)
                {
                    engine_oil_life_et.Text = ((float)tmpObj.getEngineOilLife()).ToString();
                }
                fuelRangeList = tmpObj.getFuelRange();
			}

            if (fuelRangeList == null)
                fuelRangeList = new List<FuelRange>();

            var fuelRangeAdapter = new FuelRangeAdapter(Activity, fuelRangeList);
            fuel_range_lv.Adapter = fuelRangeAdapter;
            Utility.setListViewHeightBasedOnChildren(fuel_range_lv);

			gps_data_btn.Click += (sender, e) =>
			{
                Android.Support.V7.App.AlertDialog.Builder gpsAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
                View gpsView = layoutInflater.Inflate(Resource.Layout.vi_gps_data, null);
				gpsAlertDialog.SetView(gpsView);
				gpsAlertDialog.SetTitle("GPS Data");

				CheckBox chkLongitude = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_longitude_chk);
				EditText etLongitude = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_longitude_et);

				CheckBox chkLatitude = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_latitude_chk);
				EditText etLatitude = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_latitude_et);

				CheckBox chkUtcYear = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_year_chk);
				EditText etUtcYear = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_year_et);

				CheckBox chkUtcMonth = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_month_chk);
				EditText etUtcMonth = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_month_et);

				CheckBox chkUtcDay = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_day_chk);
				EditText etUtcDay = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_day_et);

				CheckBox chkUtcHours = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_hours_chk);
				EditText etUtcHours = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_hours_et);

				CheckBox chkUtcMinutes = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_minutes_chk);
				EditText etUtcMinutes = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_minutes_et);

				CheckBox chkUtcSeconds = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_seconds_chk);
				EditText etUtcSeconds = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_utc_seconds_et);

				CheckBox chkCompassDirection = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_compass_direction_chk);
				Spinner spnCompassDirection = (Spinner)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_compass_direction_spinner);

				CheckBox chkpdop = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_pdop_chk);
				EditText etpdop = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_pdop_et);

				CheckBox chkhdop = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_hdop_chk);
				EditText ethdop = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_hdop_et);

				CheckBox chkvdop = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_vdop_chk);
				EditText etvdop = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_vdop_et);

				CheckBox chkActual = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_actual_chk);

				CheckBox chkSatellites = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_satellite_chk);
				EditText etSatellites = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_satellite_et);

				CheckBox chkDimension = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_dimension_chk);
				Spinner spnDimension = (Spinner)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_dimension_spinner);

				CheckBox chkAltitude = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_altitude_chk);
				EditText etAltitude = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_altitude_et);

				CheckBox chkHeading = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_heading_chk);
				EditText etHeading = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_heading_et);

				CheckBox chkSpeed = (CheckBox)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_speed_chk);
				EditText etSpeed = (EditText)gpsView.FindViewById(Resource.Id.vi_get_vehicle_data_gps_speed_et);

                var CompassDirectionAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, CompassDirectionArray);
				spnCompassDirection.Adapter = CompassDirectionAdapter;

				var DimensionAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, DimensionArray);
				spnDimension.Adapter = DimensionAdapter;

				chkLongitude.CheckedChange += (sender1, e1) => etLongitude.Enabled = e1.IsChecked;
				chkLatitude.CheckedChange += (sender1, e1) => etLatitude.Enabled = e1.IsChecked;
				chkUtcYear.CheckedChange += (sender1, e1) => etUtcYear.Enabled = e1.IsChecked;
				chkUtcMonth.CheckedChange += (sender1, e1) => etUtcMonth.Enabled = e1.IsChecked;
				chkUtcDay.CheckedChange += (sender1, e1) => etUtcDay.Enabled = e1.IsChecked;
				chkUtcHours.CheckedChange += (sender1, e1) => etUtcHours.Enabled = e1.IsChecked;
				chkUtcMinutes.CheckedChange += (sender1, e1) => etUtcMinutes.Enabled = e1.IsChecked;
				chkUtcSeconds.CheckedChange += (sender1, e1) => etUtcSeconds.Enabled = e1.IsChecked;
				chkCompassDirection.CheckedChange += (sender1, e1) => spnCompassDirection.Enabled = e1.IsChecked;
				chkpdop.CheckedChange += (sender1, e1) => etpdop.Enabled = e1.IsChecked;
				chkhdop.CheckedChange += (sender1, e1) => ethdop.Enabled = e1.IsChecked;
				chkvdop.CheckedChange += (sender1, e1) => etvdop.Enabled = e1.IsChecked;
				chkSatellites.CheckedChange += (sender1, e1) => etSatellites.Enabled = e1.IsChecked;
				chkDimension.CheckedChange += (sender1, e1) => spnDimension.Enabled = e1.IsChecked;
				chkAltitude.CheckedChange += (sender1, e1) => etAltitude.Enabled = e1.IsChecked;
				chkHeading.CheckedChange += (sender1, e1) => etHeading.Enabled = e1.IsChecked;
				chkSpeed.CheckedChange += (sender1, e1) => etSpeed.Enabled = e1.IsChecked;

				if (viGPSData != null)
				{
					etLongitude.Text = viGPSData.getLongitudeDegrees().ToString();
					etLatitude.Text = viGPSData.getLatitudeDegrees().ToString();
					etUtcYear.Text = viGPSData.getUtcYear().ToString();
					etUtcMonth.Text = viGPSData.getUtcMonth().ToString();
					etUtcDay.Text = viGPSData.getUtcDay().ToString();
					etUtcHours.Text = viGPSData.getUtcHours().ToString();
					etUtcMinutes.Text = viGPSData.getUtcMinutes().ToString();
					etUtcSeconds.Text = viGPSData.getUtcSeconds().ToString();
					spnCompassDirection.SetSelection((int)viGPSData.getCompassDirection());
					etpdop.Text = viGPSData.getPdop().ToString();
					ethdop.Text = viGPSData.getHdop().ToString();
					etvdop.Text = viGPSData.getVdop().ToString();
					chkActual.Checked = viGPSData.getActual();
					etSatellites.Text = viGPSData.getSatellites().ToString();
					spnDimension.SetSelection((int)viGPSData.getDimension());
					etAltitude.Text = viGPSData.getAltitude().ToString();
					etHeading.Text = viGPSData.getHeading().ToString();
					etSpeed.Text = viGPSData.getSpeed().ToString();
				}
				else
				{
					viGPSData = new GPSData();
				}
				gpsAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					gpsAlertDialog.Dispose();
				});
				gpsAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
					if (chkLongitude.Checked && etLongitude.Text != null && etLongitude.Text.Length > 0)
					{
						viGPSData.longitudeDegrees = Java.Lang.Float.ParseFloat(etLongitude.Text);
					}
					if (chkLatitude.Checked && etLatitude.Text != null && etLatitude.Text.Length > 0)
					{
						viGPSData.latitudeDegrees = Java.Lang.Float.ParseFloat(etLatitude.Text);
					}
					if (chkUtcYear.Checked && etUtcYear.Text != null && etUtcYear.Text.Length > 0)
					{
						viGPSData.utcYear = Java.Lang.Integer.ParseInt(etUtcYear.Text);
					}
					if (chkUtcMonth.Checked && etUtcMonth.Text != null && etUtcMonth.Text.Length > 0)
					{
						viGPSData.utcMonth = Java.Lang.Integer.ParseInt(etUtcMonth.Text);
					}
					if (chkUtcDay.Checked && etUtcDay.Text != null && etUtcDay.Text.Length > 0)
					{
						viGPSData.utcDay = Java.Lang.Integer.ParseInt(etUtcDay.Text);
					}
					if (chkUtcHours.Checked && etUtcHours.Text != null && etUtcHours.Text.Length > 0)
					{
						viGPSData.utcHours = Java.Lang.Integer.ParseInt(etUtcHours.Text);
					}
					if (chkUtcMinutes.Checked && etUtcMinutes.Text != null && etUtcMinutes.Text.Length > 0)
					{
						viGPSData.utcMinutes = Java.Lang.Integer.ParseInt(etUtcMinutes.Text);
					}
					if (chkUtcSeconds.Checked && etUtcSeconds.Text != null && etUtcSeconds.Text.Length > 0)
					{
						viGPSData.utcSeconds = Java.Lang.Integer.ParseInt(etUtcSeconds.Text);
					}
					if (chkCompassDirection.Checked)
					{
						viGPSData.compassDirection = (CompassDirection)spnCompassDirection.SelectedItemPosition;
					}
					if (chkpdop.Checked && etpdop.Text != null && etpdop.Text.Length > 0)
					{
						viGPSData.pdop = Java.Lang.Float.ParseFloat(etpdop.Text);
					}
					if (chkhdop.Checked && ethdop.Text != null && ethdop.Text.Length > 0)
					{
						viGPSData.hdop = Java.Lang.Float.ParseFloat(ethdop.Text);
					}
					if (chkvdop.Checked && etvdop.Text != null && etvdop.Text.Length > 0)
					{
						viGPSData.vdop = Java.Lang.Float.ParseFloat(etvdop.Text);
					}
					viGPSData.actual = chkActual.Checked;
					if (chkSatellites.Checked && etSatellites.Text != null && etSatellites.Text.Length > 0)
					{
						viGPSData.satellites = Java.Lang.Integer.ParseInt(etSatellites.Text);
					}
					if (chkDimension.Checked)
					{
						viGPSData.dimension = (Dimension)spnDimension.SelectedItemPosition;
					}
					if (chkAltitude.Checked && etAltitude.Text != null && etAltitude.Text.Length > 0)
					{
						viGPSData.altitude = Java.Lang.Float.ParseFloat(etAltitude.Text);
					}
					if (chkHeading.Checked && etHeading.Text != null && etHeading.Text.Length > 0)
					{
						viGPSData.heading = Java.Lang.Float.ParseFloat(etHeading.Text);
					}
					if (chkSpeed.Checked && etSpeed.Text != null && etSpeed.Text.Length > 0)
					{
						viGPSData.speed = Java.Lang.Float.ParseFloat(etSpeed.Text);
					}
				});
				gpsAlertDialog.Show();
			};

			tire_pressure_btn.Click += (sender, e) =>
			{
				Android.Support.V7.App.AlertDialog.Builder tirePressureAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
                View tirePressureView = layoutInflater.Inflate(Resource.Layout.vi_tire_status, null);
				tirePressureAlertDialog.SetView(tirePressureView);
				tirePressureAlertDialog.SetTitle("Tire Pressure");

				CheckBox pressure_tell_tale_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_tell_tale_chk);
				Spinner pressure_tell_tale_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_tell_tale_spinner);

				CheckBox left_front_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_front_chk);
				Spinner left_front_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_front_spinner);

				CheckBox right_front_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_front_chk);
				Spinner right_front_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_front_spinner);

				CheckBox left_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_rear_chk);
				Spinner left_rear_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_left_rear_spinner);

				CheckBox right_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_rear_chk);
				Spinner right_rear_spn = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_right_rear_spinner);

				CheckBox inner_left_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_left_rear_chk);
				Spinner inner_left_rear_spinner = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_left_rear_spinner);

				CheckBox inner_right_rear_chk = (CheckBox)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_right_rear_chk);
				Spinner inner_right_rear_spinner = (Spinner)tirePressureView.FindViewById(Resource.Id.tire_pressure_inner_right_rear_spinner);

				var WarningLightStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, WarningLightStatusArray);
				pressure_tell_tale_spn.Adapter = WarningLightStatusAdapter;

				var SingleTireStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, SingleTireStatusArray);
				left_front_spn.Adapter = SingleTireStatusAdapter;
				right_front_spn.Adapter = SingleTireStatusAdapter;
				left_rear_spn.Adapter = SingleTireStatusAdapter;
				right_rear_spn.Adapter = SingleTireStatusAdapter;
				inner_left_rear_spinner.Adapter = SingleTireStatusAdapter;
				inner_right_rear_spinner.Adapter = SingleTireStatusAdapter;

				pressure_tell_tale_chk.CheckedChange += (sender1, e1) => pressure_tell_tale_spn.Enabled = e1.IsChecked;
				left_front_chk.CheckedChange += (sender1, e1) => left_front_spn.Enabled = e1.IsChecked;
				right_front_chk.CheckedChange += (sender1, e1) => right_front_spn.Enabled = e1.IsChecked;
				left_rear_chk.CheckedChange += (sender1, e1) => left_rear_spn.Enabled = e1.IsChecked;
				right_rear_chk.CheckedChange += (sender1, e1) => right_rear_spn.Enabled = e1.IsChecked;
				inner_left_rear_chk.CheckedChange += (sender1, e1) => inner_left_rear_spinner.Enabled = e1.IsChecked;
				inner_right_rear_chk.CheckedChange += (sender1, e1) => inner_right_rear_spinner.Enabled = e1.IsChecked;

				if (viTireStatus != null)
				{
					pressure_tell_tale_spn.SetSelection((int)viTireStatus.getPressureTelltale());
					if (viTireStatus.getleftFront() != null)
					{
						left_front_spn.SetSelection((int)viTireStatus.getleftFront().getStatus());
					}
					if (viTireStatus.getRightFront() != null)
					{
						right_front_spn.SetSelection((int)viTireStatus.getRightFront().getStatus());
					}
					if (viTireStatus.getLeftRear() != null)
					{
						left_rear_spn.SetSelection((int)viTireStatus.getLeftRear().getStatus());
					}
					if (viTireStatus.getRightRear() != null)
					{
						right_rear_spn.SetSelection((int)viTireStatus.getRightRear().getStatus());
					}
					if (viTireStatus.getInnerLeftRear() != null)
					{
						inner_left_rear_spinner.SetSelection((int)viTireStatus.getInnerLeftRear().getStatus());
					}
					if (viTireStatus.getInnerRightRear() != null)
					{
						inner_right_rear_spinner.SetSelection((int)viTireStatus.getInnerRightRear().getStatus());
					}
				}
				else
				{
					viTireStatus = new TireStatus();
				}

				tirePressureAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					tirePressureAlertDialog.Dispose();
				});
				tirePressureAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
					if (pressure_tell_tale_chk.Checked)
					{
						viTireStatus.pressureTelltale = (WarningLightStatus)pressure_tell_tale_spn.SelectedItemPosition;
					}
					if (left_front_chk.Checked)
					{
						SingleTireStatus leftFront = new SingleTireStatus();
						leftFront.status = (ComponentVolumeStatus)left_front_spn.SelectedItemPosition;
						viTireStatus.leftFront = leftFront;
					}
					if (right_front_chk.Checked)
					{
						SingleTireStatus right_front = new SingleTireStatus();
						right_front.status = (ComponentVolumeStatus)right_front_spn.SelectedItemPosition;
						viTireStatus.rightFront = right_front;
					}
					if (left_rear_chk.Checked)
					{
						SingleTireStatus left_rear = new SingleTireStatus();
						left_rear.status = (ComponentVolumeStatus)left_rear_spn.SelectedItemPosition;
						viTireStatus.leftRear = left_rear;
					}
					if (right_rear_chk.Checked)
					{
						SingleTireStatus right_rear = new SingleTireStatus();
						right_rear.status = (ComponentVolumeStatus)right_rear_spn.SelectedItemPosition;
						viTireStatus.rightRear = right_rear;
					}
					if (inner_left_rear_chk.Checked)
					{
						SingleTireStatus inner_left_rear = new SingleTireStatus();
						inner_left_rear.status = (ComponentVolumeStatus)inner_left_rear_spinner.SelectedItemPosition;
						viTireStatus.innerLeftRear = inner_left_rear;
					}
					if (inner_right_rear_chk.Checked)
					{
						SingleTireStatus inner_right_rear = new SingleTireStatus();
						inner_right_rear.status = (ComponentVolumeStatus)inner_right_rear_spinner.SelectedItemPosition;
						viTireStatus.innerRightRear = inner_right_rear;
					}
				});
				tirePressureAlertDialog.Show();
			};

			belt_status_button.Click += (sender, e) =>
			{
                Android.Support.V7.App.AlertDialog.Builder beltStatusAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View beltStatusView = layoutInflater.Inflate(Resource.Layout.vi_belt_status, null);
				beltStatusAlertDialog.SetView(beltStatusView);
				beltStatusAlertDialog.SetTitle("Belt Status");

				CheckBox driver_belt_deployed_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_driver_belt_deployed_chk);
				Spinner driver_belt_deployed_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_driver_belt_deployed_spinner);

				CheckBox passenger_belt_deployed_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_belt_deployed_chk);
				Spinner passenger_belt_deployed_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_belt_deployed_spinner);

				CheckBox passenger_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_buckle_belted_chk);
				Spinner passenger_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_buckle_belted_spinner);

				CheckBox driver_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_driver_buckle_belted_chk);
				Spinner driver_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_driver_buckle_belted_spinner);

				CheckBox left_row_2_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_2_buckle_belted_chk);
				Spinner left_row_2_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_2_buckle_belted_spinner);

				CheckBox passenger_child_detected_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_child_detected_chk);
				Spinner passenger_child_detected_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_passenger_child_detected_spinner);

				CheckBox right_row_2_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_2_buckle_belted_chk);
				Spinner right_row_2_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_2_buckle_belted_spinner);

				CheckBox middle_row_2_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_2_buckle_belted_chk);
				Spinner middle_row_2_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_2_buckle_belted_spinner);

				CheckBox middle_row_3_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_3_buckle_belted_chk);
				Spinner middle_row_3_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_3_buckle_belted_spinner);

				CheckBox left_row_3_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_3_buckle_belted_chk);
				Spinner left_row_3_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_left_row_3_buckle_belted_spinner);

				CheckBox right_row_3_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_3_buckle_belted_chk);
				Spinner right_row_3_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_right_row_3_buckle_belted_spinner);

				CheckBox left_rear_inflatable_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_left_rear_inflatable_belted_chk);
				Spinner left_rear_inflatable_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_left_rear_inflatable_belted_spinner);

				CheckBox right_rear_inflatable_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_right_rear_inflatable_belted_chk);
				Spinner right_rear_inflatable_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_right_rear_inflatable_belted_spinner);

				CheckBox middle_row_1_belt_deployed_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_belt_deployed_chk);
				Spinner middle_row_1_belt_deployed_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_belt_deployed_spinner);

				CheckBox middle_row_1_buckle_belted_chk = (CheckBox)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_buckle_belted_chk);
				Spinner middle_row_1_buckle_belted_spinner = (Spinner)beltStatusView.FindViewById(Resource.Id.belt_status_middle_row_1_buckle_belted_spinner);

				driver_belt_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_belt_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				driver_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				left_row_2_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_child_detected_spinner.Adapter = VehicleDataEventStatusAdapter;
				right_row_2_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				middle_row_2_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				middle_row_3_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				left_row_3_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				right_row_3_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				left_rear_inflatable_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				right_rear_inflatable_belted_spinner.Adapter = VehicleDataEventStatusAdapter;
				middle_row_1_belt_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				middle_row_1_buckle_belted_spinner.Adapter = VehicleDataEventStatusAdapter;

				if (viBeltStatus != null)
				{
					driver_belt_deployed_spinner.SetSelection((int)viBeltStatus.getDriverBeltDeployed());
					passenger_belt_deployed_spinner.SetSelection((int)viBeltStatus.getPassengerBeltDeployed());
					passenger_buckle_belted_spinner.SetSelection((int)viBeltStatus.getPassengerBuckleBelted());
					driver_buckle_belted_spinner.SetSelection((int)viBeltStatus.getDriverBuckleBelted());
					left_row_2_buckle_belted_spinner.SetSelection((int)viBeltStatus.getLeftRow2BuckleBelted());
					passenger_child_detected_spinner.SetSelection((int)viBeltStatus.getPassengerChildDetected());
					right_row_2_buckle_belted_spinner.SetSelection((int)viBeltStatus.getRightRow2BuckleBelted());
					middle_row_2_buckle_belted_spinner.SetSelection((int)viBeltStatus.getMiddleRow2BuckleBelted());
					middle_row_3_buckle_belted_spinner.SetSelection((int)viBeltStatus.getMiddleRow3BuckleBelted());
					left_row_3_buckle_belted_spinner.SetSelection((int)viBeltStatus.getLeftRow3BuckleBelted());
					right_row_3_buckle_belted_spinner.SetSelection((int)viBeltStatus.getRightRow3BuckleBelted());
					left_rear_inflatable_belted_spinner.SetSelection((int)viBeltStatus.getLeftRearInflatableBelted());
					right_rear_inflatable_belted_spinner.SetSelection((int)viBeltStatus.getRightRearInflatableBelted());
					middle_row_1_belt_deployed_spinner.SetSelection((int)viBeltStatus.getMiddleRow1BeltDeployed());
					middle_row_1_buckle_belted_spinner.SetSelection((int)viBeltStatus.getMiddleRow1BuckleBelted());
				}
				else
				{
					viBeltStatus = new BeltStatus();
				}

				driver_belt_deployed_chk.CheckedChange += (sender1, e1) => driver_belt_deployed_spinner.Enabled = e1.IsChecked;
				passenger_belt_deployed_chk.CheckedChange += (sender1, e1) => passenger_belt_deployed_spinner.Enabled = e1.IsChecked;
				passenger_buckle_belted_chk.CheckedChange += (sender1, e1) => passenger_buckle_belted_spinner.Enabled = e1.IsChecked;
				driver_buckle_belted_chk.CheckedChange += (sender1, e1) => driver_buckle_belted_spinner.Enabled = e1.IsChecked;
				left_row_2_buckle_belted_chk.CheckedChange += (sender1, e1) => left_row_2_buckle_belted_spinner.Enabled = e1.IsChecked;
				passenger_child_detected_chk.CheckedChange += (sender1, e1) => passenger_child_detected_spinner.Enabled = e1.IsChecked;
				right_row_2_buckle_belted_chk.CheckedChange += (sender1, e1) => right_row_2_buckle_belted_spinner.Enabled = e1.IsChecked;
				middle_row_2_buckle_belted_chk.CheckedChange += (sender1, e1) => middle_row_2_buckle_belted_spinner.Enabled = e1.IsChecked;
				middle_row_3_buckle_belted_chk.CheckedChange += (sender1, e1) => middle_row_3_buckle_belted_spinner.Enabled = e1.IsChecked;
				left_row_3_buckle_belted_chk.CheckedChange += (sender1, e1) => left_row_3_buckle_belted_spinner.Enabled = e1.IsChecked;
				right_row_3_buckle_belted_chk.CheckedChange += (sender1, e1) => right_row_3_buckle_belted_spinner.Enabled = e1.IsChecked;
				left_rear_inflatable_belted_chk.CheckedChange += (sender1, e1) => left_rear_inflatable_belted_spinner.Enabled = e1.IsChecked;
				right_rear_inflatable_belted_chk.CheckedChange += (sender1, e1) => right_rear_inflatable_belted_spinner.Enabled = e1.IsChecked;
				middle_row_1_belt_deployed_chk.CheckedChange += (sender1, e1) => middle_row_1_belt_deployed_spinner.Enabled = e1.IsChecked;
				middle_row_1_buckle_belted_chk.CheckedChange += (sender1, e1) => middle_row_1_buckle_belted_spinner.Enabled = e1.IsChecked;

				beltStatusAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					beltStatusAlertDialog.Dispose();
				});
				beltStatusAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
					if (driver_belt_deployed_chk.Checked)
					{
						viBeltStatus.driverBeltDeployed = (VehicleDataEventStatus)driver_belt_deployed_spinner.SelectedItemPosition;
					}
					if (passenger_belt_deployed_chk.Checked)
					{
						viBeltStatus.passengerBeltDeployed = (VehicleDataEventStatus)passenger_belt_deployed_spinner.SelectedItemPosition;
					}
					if (passenger_buckle_belted_chk.Checked)
					{
						viBeltStatus.passengerBuckleBelted = (VehicleDataEventStatus)passenger_buckle_belted_spinner.SelectedItemPosition;
					}
					if (driver_buckle_belted_chk.Checked)
					{
						viBeltStatus.driverBuckleBelted = (VehicleDataEventStatus)driver_buckle_belted_spinner.SelectedItemPosition;
					}
					if (left_row_2_buckle_belted_chk.Checked)
					{
						viBeltStatus.leftRow2BuckleBelted = (VehicleDataEventStatus)left_row_2_buckle_belted_spinner.SelectedItemPosition;
					}
					if (passenger_child_detected_chk.Checked)
					{
						viBeltStatus.passengerChildDetected = (VehicleDataEventStatus)passenger_child_detected_spinner.SelectedItemPosition;
					}
					if (right_row_2_buckle_belted_chk.Checked)
					{
						viBeltStatus.rightRow2BuckleBelted = (VehicleDataEventStatus)right_row_2_buckle_belted_spinner.SelectedItemPosition;
					}
					if (middle_row_2_buckle_belted_chk.Checked)
					{
						viBeltStatus.middleRow2BuckleBelted = (VehicleDataEventStatus)middle_row_2_buckle_belted_spinner.SelectedItemPosition;
					}
					if (middle_row_3_buckle_belted_chk.Checked)
					{
						viBeltStatus.middleRow3BuckleBelted = (VehicleDataEventStatus)middle_row_3_buckle_belted_spinner.SelectedItemPosition;
					}
					if (left_row_3_buckle_belted_chk.Checked)
					{
						viBeltStatus.leftRow3BuckleBelted = (VehicleDataEventStatus)left_row_3_buckle_belted_spinner.SelectedItemPosition;
					}
					if (right_row_3_buckle_belted_chk.Checked)
					{
						viBeltStatus.rightRow3BuckleBelted = (VehicleDataEventStatus)right_row_3_buckle_belted_spinner.SelectedItemPosition;
					}
					if (left_rear_inflatable_belted_chk.Checked)
					{
						viBeltStatus.leftRearInflatableBelted = (VehicleDataEventStatus)left_rear_inflatable_belted_spinner.SelectedItemPosition;
					}
					if (right_rear_inflatable_belted_chk.Checked)
					{
						viBeltStatus.rightRearInflatableBelted = (VehicleDataEventStatus)right_rear_inflatable_belted_spinner.SelectedItemPosition;
					}
					if (middle_row_1_belt_deployed_chk.Checked)
					{
						viBeltStatus.middleRow1BeltDeployed = (VehicleDataEventStatus)middle_row_1_belt_deployed_spinner.SelectedItemPosition;
					}
					if (middle_row_1_buckle_belted_chk.Checked)
					{
						viBeltStatus.middleRow1BuckleBelted = (VehicleDataEventStatus)middle_row_1_buckle_belted_spinner.SelectedItemPosition;
					}
				});
				beltStatusAlertDialog.Show();
			};

			body_info_button.Click += (sender, e) =>
			{
				Android.Support.V7.App.AlertDialog.Builder bodyInfoAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View bodyInfoView = layoutInflater.Inflate(Resource.Layout.vi_body_information, null);
				bodyInfoAlertDialog.SetView(bodyInfoView);
				bodyInfoAlertDialog.SetTitle("Body Info");

				CheckBox park_brake_active = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_park_brake_active);

				CheckBox ignition_stable_status_chk = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_stable_status_chk);
				Spinner ignition_stable_status_spinner = (Spinner)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_stable_status_spinner);

				CheckBox ignition_status_chk = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_status_chk);
				Spinner ignition_status_spinner = (Spinner)bodyInfoView.FindViewById(Resource.Id.body_info_ignition_status_spinner);

				CheckBox driver_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_driver_door_ajar);
				CheckBox passenger_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_passenger_door_ajar);
				CheckBox rear_left_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_rear_left_door_ajar);
				CheckBox rear_right_door_ajar = (CheckBox)bodyInfoView.FindViewById(Resource.Id.body_info_rear_right_door_ajar);

				var IgnitionStableStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, IgnitionStableStatusArray);
				ignition_stable_status_spinner.Adapter = IgnitionStableStatusAdapter;

				var IgnitionStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, IgnitionStatusArray);
				ignition_status_spinner.Adapter = IgnitionStatusAdapter;

				ignition_stable_status_chk.CheckedChange += (sender1, e1) => ignition_stable_status_spinner.Enabled = e1.IsChecked;
				ignition_status_chk.CheckedChange += (sender1, e1) => ignition_status_spinner.Enabled = e1.IsChecked;

				if (viBodyInformation != null)
				{
					park_brake_active.Checked = viBodyInformation.getParkBrakeActive();
					ignition_stable_status_spinner.SetSelection((int)viBodyInformation.getIgnitionStableStatus());
					ignition_status_spinner.SetSelection((int)viBodyInformation.getIgnitionStatus());
					driver_door_ajar.Checked = viBodyInformation.getDriverDoorAjar();
					passenger_door_ajar.Checked = viBodyInformation.getPassengerDoorAjar();
					rear_left_door_ajar.Checked = viBodyInformation.getRearLeftDoorAjar();
					rear_right_door_ajar.Checked = viBodyInformation.getRearRightDoorAjar();
				}
				else
				{
					viBodyInformation = new BodyInformation();
				}

				bodyInfoAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					bodyInfoAlertDialog.Dispose();
				});
				bodyInfoAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
					viBodyInformation.parkBrakeActive = park_brake_active.Checked;
					if (ignition_stable_status_chk.Checked)
					{
						viBodyInformation.ignitionStableStatus = (IgnitionStableStatus)ignition_stable_status_spinner.SelectedItemPosition;
					}
					if (ignition_status_chk.Checked)
					{
						viBodyInformation.ignitionStatus = (IgnitionStatus)ignition_status_spinner.SelectedItemPosition;
					}
					viBodyInformation.driverDoorAjar = driver_door_ajar.Checked;
					viBodyInformation.passengerDoorAjar = passenger_door_ajar.Checked;
					viBodyInformation.rearLeftDoorAjar = rear_left_door_ajar.Checked;
					viBodyInformation.rearRightDoorAjar = rear_right_door_ajar.Checked;
				});
				bodyInfoAlertDialog.Show();
			};

			device_status_button.Click += (sender, e) =>
			{
				Android.Support.V7.App.AlertDialog.Builder deviceStatusAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View deviceStatusView = layoutInflater.Inflate(Resource.Layout.vi_device_status, null);
				deviceStatusAlertDialog.SetView(deviceStatusView);
				deviceStatusAlertDialog.SetTitle("Device Status");

				CheckBox voice_rec_on = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_voice_rec_on);
				CheckBox bt_icon_on = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_bt_icon_on);
				CheckBox call_active = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_call_active);
				CheckBox phone_roaming = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_phone_roaming);
				CheckBox text_msg_available = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_text_msg_available);

				CheckBox batt_level_status_chk = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_batt_level_status_chk);
				Spinner batt_level_status_spinner = (Spinner)deviceStatusView.FindViewById(Resource.Id.device_status_batt_level_status_spinner);

				CheckBox stereo_audio_output_muted = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_stereo_audio_output_muted);
				CheckBox mono_audio_output_muted = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_mono_audio_output_muted);

				CheckBox signal_level_status_chk = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_signal_level_status_chk);
				Spinner signal_level_status_spinner = (Spinner)deviceStatusView.FindViewById(Resource.Id.device_status_signal_level_status_spinner);

				CheckBox primary_audio_source_chk = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_primary_audio_source_chk);
				Spinner primary_audio_source_spinner = (Spinner)deviceStatusView.FindViewById(Resource.Id.device_status_primary_audio_source_spinner);

				CheckBox ecall_event_active = (CheckBox)deviceStatusView.FindViewById(Resource.Id.device_status_ecall_event_active);

				var battLevelStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, DeviceLevelStatusArray);
				batt_level_status_spinner.Adapter = battLevelStatusAdapter;
				signal_level_status_spinner.Adapter = battLevelStatusAdapter;

				var PrimaryAudioSourceAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, PrimaryAudioSourceArray);
				primary_audio_source_spinner.Adapter = PrimaryAudioSourceAdapter;

				batt_level_status_chk.CheckedChange += (sender1, e1) => batt_level_status_spinner.Enabled = e1.IsChecked;
				signal_level_status_chk.CheckedChange += (sender1, e1) => signal_level_status_spinner.Enabled = e1.IsChecked;
				primary_audio_source_chk.CheckedChange += (sender1, e1) => primary_audio_source_spinner.Enabled = e1.IsChecked;

				if (viDeviceStatus != null)
				{
					voice_rec_on.Checked = viDeviceStatus.voiceRecOn;
					bt_icon_on.Checked = viDeviceStatus.btIconOn;
					call_active.Checked = viDeviceStatus.callActive;
					phone_roaming.Checked = viDeviceStatus.phoneRoaming;
					text_msg_available.Checked = viDeviceStatus.textMsgAvailable;
					batt_level_status_spinner.SetSelection((int)viDeviceStatus.getBattLevelStatus());
					stereo_audio_output_muted.Checked = viDeviceStatus.stereoAudioOutputMuted;
					mono_audio_output_muted.Checked = viDeviceStatus.monoAudioOutputMuted;
					signal_level_status_spinner.SetSelection((int)viDeviceStatus.getSignalLevelStatus());
					primary_audio_source_spinner.SetSelection((int)viDeviceStatus.getPrimaryAudioSource());
					ecall_event_active.Checked = viDeviceStatus.eCallEventActive;
				}
				else
				{
					viDeviceStatus = new DeviceStatus();
				}

				deviceStatusAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					deviceStatusAlertDialog.Dispose();
				});
				deviceStatusAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
					viDeviceStatus.voiceRecOn = voice_rec_on.Checked;
					viDeviceStatus.btIconOn = bt_icon_on.Checked;
					viDeviceStatus.callActive = call_active.Checked;
					viDeviceStatus.phoneRoaming = phone_roaming.Checked;
					viDeviceStatus.textMsgAvailable = text_msg_available.Checked;
					if (batt_level_status_chk.Checked)
					{
						viDeviceStatus.battLevelStatus = (DeviceLevelStatus)batt_level_status_spinner.SelectedItemPosition;
					}
					viDeviceStatus.stereoAudioOutputMuted = stereo_audio_output_muted.Checked;
					viDeviceStatus.monoAudioOutputMuted = mono_audio_output_muted.Checked;
					if (signal_level_status_chk.Checked)
					{
						viDeviceStatus.signalLevelStatus = (DeviceLevelStatus)signal_level_status_spinner.SelectedItemPosition;
					}
					if (primary_audio_source_chk.Checked)
					{
						viDeviceStatus.primaryAudioSource = (PrimaryAudioSource)primary_audio_source_spinner.SelectedItemPosition;
					}
					viDeviceStatus.eCallEventActive = ecall_event_active.Checked;
				});
				deviceStatusAlertDialog.Show();
			};

			head_lamp_status_btn.Click += (sender, e) =>
			{
				Android.Support.V7.App.AlertDialog.Builder headLampAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View headLampView = layoutInflater.Inflate(Resource.Layout.vi_head_lamp_status, null);
				headLampAlertDialog.SetView(headLampView);
				headLampAlertDialog.SetTitle("Head Lamp Status");

				CheckBox low_beams_on = (CheckBox)headLampView.FindViewById(Resource.Id.head_lamp_status_low_beams_on);
				CheckBox high_beams_on = (CheckBox)headLampView.FindViewById(Resource.Id.head_lamp_status_high_beams_on);

				CheckBox ambient_light_sensor_status_chk = (CheckBox)headLampView.FindViewById(Resource.Id.head_lamp_status_ambient_light_sensor_status_chk);
				Spinner ambient_light_sensor_status_spinner = (Spinner)headLampView.FindViewById(Resource.Id.head_lamp_status_ambient_light_sensor_status_spinner);

                var AmbientLightStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, AmbientLightStatusArray);
				ambient_light_sensor_status_spinner.Adapter = AmbientLightStatusAdapter;

				ambient_light_sensor_status_chk.CheckedChange += (sender1, e1) => ambient_light_sensor_status_spinner.Enabled = e1.IsChecked;

				if (viHeadLampStatus != null)
				{
					low_beams_on.Checked = viHeadLampStatus.lowBeamsOn;
					high_beams_on.Checked = viHeadLampStatus.highBeamsOn;
					ambient_light_sensor_status_spinner.SetSelection((int)viHeadLampStatus.getAmbientLightSensorStatus());
				}
				else
				{
					viHeadLampStatus = new HeadLampStatus();
				}

				headLampAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					headLampAlertDialog.Dispose();
				});
				headLampAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
					viHeadLampStatus.lowBeamsOn = low_beams_on.Checked;
					viHeadLampStatus.highBeamsOn = high_beams_on.Checked;
					if (ambient_light_sensor_status_chk.Checked)
					{
						viHeadLampStatus.ambientLightSensorStatus = (AmbientLightStatus)ambient_light_sensor_status_spinner.SelectedItemPosition;
					}
				});
				headLampAlertDialog.Show();
			};

			ecall_info_btn.Click += (sender, e) =>
			{
				Android.Support.V7.App.AlertDialog.Builder eCallInfoAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View eCallInfoView = layoutInflater.Inflate(Resource.Layout.vi_ecall_info, null);
				eCallInfoAlertDialog.SetView(eCallInfoView);
				eCallInfoAlertDialog.SetTitle("ECall Info");

				CheckBox ecall_notification_status_chk = (CheckBox)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_notification_status_chk);
				Spinner ecall_notification_status_spinner = (Spinner)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_notification_status_spinner);

				CheckBox aux_ecall_notification_status_chk = (CheckBox)eCallInfoView.FindViewById(Resource.Id.ecall_info_aux_ecall_notification_status_chk);
				Spinner aux_ecall_notification_status_spinner = (Spinner)eCallInfoView.FindViewById(Resource.Id.ecall_info_aux_ecall_notification_status_spinner);

				CheckBox ecall_confirmation_status_chk = (CheckBox)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_confirmation_status_chk);
				Spinner ecall_confirmation_status_spinner = (Spinner)eCallInfoView.FindViewById(Resource.Id.ecall_info_ecall_confirmation_status_spinner);

				ecall_notification_status_chk.CheckedChange += (sender1, e1) => ecall_notification_status_spinner.Enabled = e1.IsChecked;
				aux_ecall_notification_status_chk.CheckedChange += (sender1, e1) => aux_ecall_notification_status_spinner.Enabled = e1.IsChecked;
				ecall_confirmation_status_chk.CheckedChange += (sender1, e1) => ecall_confirmation_status_spinner.Enabled = e1.IsChecked;

				var vehicleDataNotificationStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataNotificationStatusArray);
				ecall_notification_status_spinner.Adapter = vehicleDataNotificationStatusAdapter;
				aux_ecall_notification_status_spinner.Adapter = vehicleDataNotificationStatusAdapter;

				var ECallConfirmationStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, ECallConfirmationStatusArray);
				ecall_confirmation_status_spinner.Adapter = ECallConfirmationStatusAdapter;

				if (null != viECallInfo)
				{
					ecall_notification_status_spinner.SetSelection((int)viECallInfo.getECallNotificationStatus());
					aux_ecall_notification_status_spinner.SetSelection((int)viECallInfo.getAuxECallNotificationStatus());
					ecall_confirmation_status_spinner.SetSelection((int)viECallInfo.getECallConfirmationStatus());
				}
				else
				{
					viECallInfo = new ECallInfo();
				}

				eCallInfoAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					eCallInfoAlertDialog.Dispose();
				});
				eCallInfoAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
					if (ecall_notification_status_chk.Checked)
					{
						viECallInfo.eCallNotificationStatus = (VehicleDataNotificationStatus)ecall_notification_status_spinner.SelectedItemPosition;
					}
					if (aux_ecall_notification_status_chk.Checked)
					{
						viECallInfo.auxECallNotificationStatus = (VehicleDataNotificationStatus)aux_ecall_notification_status_spinner.SelectedItemPosition;
					}
					if (ecall_confirmation_status_chk.Checked)
					{
						viECallInfo.eCallConfirmationStatus = (ECallConfirmationStatus)ecall_confirmation_status_spinner.SelectedItemPosition;
					}
				});
				eCallInfoAlertDialog.Show();
			};

			airbag_status_btn.Click += (sender, e) =>
			{
                Android.Support.V7.App.AlertDialog.Builder airbagStatusAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View airbagStatusView = layoutInflater.Inflate(Resource.Layout.vi_airbag_status, null);
				airbagStatusAlertDialog.SetView(airbagStatusView);
				airbagStatusAlertDialog.SetTitle("Airbag Status");

				CheckBox driver_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_airbag_deployed_chk);
				Spinner driver_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_airbag_deployed_spinner);

				CheckBox driver_side_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_side_airbag_deployed_chk);
				Spinner driver_side_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_side_airbag_deployed_spinner);

				CheckBox driver_curtain_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_curtain_airbag_deployed_chk);
				Spinner driver_curtain_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_curtain_airbag_deployed_spinner);

				CheckBox passenger_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_airbag_deployed_chk);
				Spinner passenger_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_airbag_deployed_spinner);

				CheckBox passenger_curtain_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_curtain_airbag_deployed_chk);
				Spinner passenger_curtain_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_curtain_airbag_deployed_spinner);

				CheckBox driver_knee_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_knee_airbag_deployed_chk);
				Spinner driver_knee_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_driver_knee_airbag_deployed_spinner);

				CheckBox passenger_side_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_side_airbag_deployed_chk);
				Spinner passenger_side_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_side_airbag_deployed_spinner);

				CheckBox passenger_knee_airbag_deployed_chk = (CheckBox)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_knee_airbag_deployed_chk);
				Spinner passenger_knee_airbag_deployed_spinner = (Spinner)airbagStatusView.FindViewById(Resource.Id.air_bag_status_passenger_knee_airbag_deployed_spinner);

				driver_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				driver_side_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				driver_curtain_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_curtain_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				driver_knee_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_side_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;
				passenger_knee_airbag_deployed_spinner.Adapter = VehicleDataEventStatusAdapter;

				driver_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_airbag_deployed_spinner.Enabled = e1.IsChecked;
				driver_side_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_side_airbag_deployed_spinner.Enabled = e1.IsChecked;
				driver_curtain_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_curtain_airbag_deployed_spinner.Enabled = e1.IsChecked;
				passenger_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_airbag_deployed_spinner.Enabled = e1.IsChecked;
				passenger_curtain_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_curtain_airbag_deployed_spinner.Enabled = e1.IsChecked;
				driver_knee_airbag_deployed_chk.CheckedChange += (sender1, e1) => driver_knee_airbag_deployed_spinner.Enabled = e1.IsChecked;
				passenger_side_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_side_airbag_deployed_spinner.Enabled = e1.IsChecked;
				passenger_knee_airbag_deployed_chk.CheckedChange += (sender1, e1) => passenger_knee_airbag_deployed_spinner.Enabled = e1.IsChecked;

				if (null != viAirbagStatus)
				{
					driver_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverAirbagDeployed());
					driver_side_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverSideAirbagDeployed());
					driver_curtain_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverCurtainAirbagDeployed());
					passenger_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerAirbagDeployed());
					passenger_curtain_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerCurtainAirbagDeployed());
					driver_knee_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getDriverKneeAirbagDeployed());
					passenger_side_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerSideAirbagDeployed());
					passenger_knee_airbag_deployed_spinner.SetSelection((int)viAirbagStatus.getPassengerKneeAirbagDeployed());
				}
				else
				{
					viAirbagStatus = new AirbagStatus();
				}

				airbagStatusAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					airbagStatusAlertDialog.Dispose();
				});
				airbagStatusAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
					if (driver_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.driverAirbagDeployed = (VehicleDataEventStatus)driver_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (driver_side_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.driverSideAirbagDeployed = (VehicleDataEventStatus)driver_side_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (driver_curtain_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.driverCurtainAirbagDeployed = (VehicleDataEventStatus)driver_curtain_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (passenger_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.passengerAirbagDeployed = (VehicleDataEventStatus)passenger_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (passenger_curtain_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.passengerCurtainAirbagDeployed = (VehicleDataEventStatus)passenger_curtain_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (driver_knee_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.driverKneeAirbagDeployed = (VehicleDataEventStatus)driver_knee_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (passenger_side_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.passengerSideAirbagDeployed = (VehicleDataEventStatus)passenger_side_airbag_deployed_spinner.SelectedItemPosition;
					}
					if (passenger_knee_airbag_deployed_chk.Checked)
					{
						viAirbagStatus.passengerKneeAirbagDeployed = (VehicleDataEventStatus)passenger_knee_airbag_deployed_spinner.SelectedItemPosition;
					}
				});
				airbagStatusAlertDialog.Show();
			};

			emergency_event_btn.Click += (sender, e) =>
			{
				Android.Support.V7.App.AlertDialog.Builder emergencyEventAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View emergencyEventView = layoutInflater.Inflate(Resource.Layout.vi_emergency_event, null);
				emergencyEventAlertDialog.SetView(emergencyEventView);
				emergencyEventAlertDialog.SetTitle("Emergency Event");

				CheckBox emergency_event_type_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_emergency_event_type_chk);
				Spinner emergency_event_type_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_emergency_event_type_spinner);

				CheckBox fuel_cutoff_status_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_fuel_cutoff_status_chk);
				Spinner fuel_cutoff_status_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_fuel_cutoff_status_spinner);

				CheckBox rollover_event_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_rollover_event_chk);
				Spinner rollover_event_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_rollover_event_spinner);

				CheckBox maximum_change_velocity_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_maximum_change_velocity_chk);
				Spinner maximum_change_velocity_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_maximum_change_velocity_spinner);

				CheckBox multiple_events_chk = (CheckBox)emergencyEventView.FindViewById(Resource.Id.emergency_event_multiple_events_chk);
				Spinner multiple_events_spinner = (Spinner)emergencyEventView.FindViewById(Resource.Id.emergency_event_multiple_events_spinner);

                var EmergencyEventTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, EmergencyEventTypeArray);
				emergency_event_type_spinner.Adapter = EmergencyEventTypeAdapter;

				var FuelCutoffStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, FuelCutoffStatusArray);
				fuel_cutoff_status_spinner.Adapter = FuelCutoffStatusAdapter;

				rollover_event_spinner.Adapter = VehicleDataEventStatusAdapter;
				maximum_change_velocity_spinner.Adapter = VehicleDataEventStatusAdapter;
				multiple_events_spinner.Adapter = VehicleDataEventStatusAdapter;

				if (null != viEmergencyEvent)
				{
					emergency_event_type_spinner.SetSelection((int)viEmergencyEvent.getEmergencyEventType());
					fuel_cutoff_status_spinner.SetSelection((int)viEmergencyEvent.getFuelCutoffStatus());
					rollover_event_spinner.SetSelection((int)viEmergencyEvent.getRolloverEvent());
					maximum_change_velocity_spinner.SetSelection((int)viEmergencyEvent.getMaximumChangeVelocity());
					multiple_events_spinner.SetSelection((int)viEmergencyEvent.getMultipleEvents());
				}
				else
				{
					viEmergencyEvent = new EmergencyEvent();
				}

				emergency_event_type_chk.CheckedChange += (sender1, e1) => emergency_event_type_spinner.Enabled = e1.IsChecked;
				fuel_cutoff_status_chk.CheckedChange += (sender1, e1) => fuel_cutoff_status_spinner.Enabled = e1.IsChecked;
				rollover_event_chk.CheckedChange += (sender1, e1) => rollover_event_spinner.Enabled = e1.IsChecked;
				maximum_change_velocity_chk.CheckedChange += (sender1, e1) => maximum_change_velocity_spinner.Enabled = e1.IsChecked;
				multiple_events_chk.CheckedChange += (sender1, e1) => multiple_events_spinner.Enabled = e1.IsChecked;

				emergencyEventAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					emergencyEventAlertDialog.Dispose();
				});
				emergencyEventAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
					if (emergency_event_type_chk.Checked)
					{
						viEmergencyEvent.emergencyEventType = (EmergencyEventType)emergency_event_type_spinner.SelectedItemPosition;
					}
					if (fuel_cutoff_status_chk.Checked)
					{
						viEmergencyEvent.fuelCutoffStatus = (FuelCutoffStatus)fuel_cutoff_status_spinner.SelectedItemPosition;
					}
					if (rollover_event_chk.Checked)
					{
						viEmergencyEvent.rolloverEvent = (VehicleDataEventStatus)rollover_event_spinner.SelectedItemPosition;
					}
					if (maximum_change_velocity_chk.Checked)
					{
						viEmergencyEvent.maximumChangeVelocity = (VehicleDataEventStatus)maximum_change_velocity_spinner.SelectedItemPosition;
					}
					if (multiple_events_chk.Checked)
					{
						viEmergencyEvent.multipleEvents = (VehicleDataEventStatus)multiple_events_spinner.SelectedItemPosition;
					}
				});
				emergencyEventAlertDialog.Show();
			};

			cluster_modes_btn.Click += (sender, e) =>
			{
                Android.Support.V7.App.AlertDialog.Builder clusterModeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
				View clusterModeView = layoutInflater.Inflate(Resource.Layout.vi_cluster_mode_status, null);
				clusterModeAlertDialog.SetView(clusterModeView);
				clusterModeAlertDialog.SetTitle("Cluster Mode Status");

				CheckBox power_mode_active = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_active);

				CheckBox power_mode_qualification_status_chk = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_qualification_status_chk);
				Spinner power_mode_qualification_status_spinner = (Spinner)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_qualification_status_spinner);

				CheckBox car_mode_status_status_chk = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_car_mode_status_status_chk);
				Spinner car_mode_status_status_spinner = (Spinner)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_car_mode_status_status_spinner);

				CheckBox power_mode_status_chk = (CheckBox)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_status_chk);
				Spinner power_mode_status_spinner = (Spinner)clusterModeView.FindViewById(Resource.Id.cluster_mode_status_power_mode_status_spinner);

				var PowerModeQualificationStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, PowerModeQualificationStatusArray);
				power_mode_qualification_status_spinner.Adapter = PowerModeQualificationStatusAdapter;

				var CarModeStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, CarModeStatusArray);
				car_mode_status_status_spinner.Adapter = CarModeStatusAdapter;

				var PowerModeStatusAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, PowerModeStatusArray);
				power_mode_status_spinner.Adapter = PowerModeStatusAdapter;

				if (null != viClusterModeStatus)
				{
					power_mode_active.Checked = viClusterModeStatus.powerModeActive;
					power_mode_qualification_status_spinner.SetSelection((int)viClusterModeStatus.getPowerModeQualificationStatus());
					car_mode_status_status_spinner.SetSelection((int)viClusterModeStatus.getCarModeStatus());
					power_mode_status_spinner.SetSelection((int)viClusterModeStatus.getPowerModeStatus());
				}
				else
				{
					viClusterModeStatus = new ClusterModeStatus();
				}

				power_mode_qualification_status_chk.CheckedChange += (sender1, e1) => power_mode_qualification_status_spinner.Enabled = e1.IsChecked;
				car_mode_status_status_chk.CheckedChange += (sender1, e1) => car_mode_status_status_spinner.Enabled = e1.IsChecked;
				power_mode_status_chk.CheckedChange += (sender1, e1) => power_mode_status_spinner.Enabled = e1.IsChecked;

				clusterModeAlertDialog.SetNeutralButton("Cancel", (senderAlert, args) =>
				{
					clusterModeAlertDialog.Dispose();
				});
				clusterModeAlertDialog.SetPositiveButton("OK", (senderAlert, args) =>
				{
					viClusterModeStatus.powerModeActive = power_mode_active.Checked;
					if (power_mode_qualification_status_chk.Checked)
					{
						viClusterModeStatus.powerModeQualificationStatus = (PowerModeQualificationStatus)power_mode_qualification_status_spinner.SelectedItemPosition;
					}
					if (car_mode_status_status_chk.Checked)
					{
						viClusterModeStatus.carModeStatus = (CarModeStatus)car_mode_status_status_spinner.SelectedItemPosition;
					}
					if (power_mode_qualification_status_chk.Checked)
					{
						viClusterModeStatus.powerModeStatus = (PowerModeStatus)power_mode_status_spinner.SelectedItemPosition;
					}
				});
				clusterModeAlertDialog.Show();
			};

            fuel_range_btn.Click += delegate
            {
                Android.Support.V7.App.AlertDialog.Builder fuelRangeAlertDialog = new Android.Support.V7.App.AlertDialog.Builder(Activity);
                View fuelRangeView = layoutInflater.Inflate(Resource.Layout.add_fuel_range, null);
                fuelRangeAlertDialog.SetView(fuelRangeView);
                fuelRangeAlertDialog.SetTitle("Add Fuel Range");

                CheckBox checkBoxName = (CheckBox)fuelRangeView.FindViewById(Resource.Id.fuel_type_check);
                Spinner spnName = (Spinner)fuelRangeView.FindViewById(Resource.Id.fuel_type_spinner);

                string[] fuelTypeNames = Enum.GetNames(typeof(FuelType));
                var fuelTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, fuelTypeNames);
                spnName.Adapter = fuelTypeAdapter;

                CheckBox rangeChk = (CheckBox)fuelRangeView.FindViewById(Resource.Id.range_check);
                EditText rangeET = (EditText)fuelRangeView.FindViewById(Resource.Id.range_et);

                checkBoxName.CheckedChange += (s, e) => spnName.Enabled = e.IsChecked;
                rangeChk.CheckedChange += (sender, e) => rangeET.Enabled = e.IsChecked;

                fuelRangeAlertDialog.SetNegativeButton("ok", (senderAlert, args) =>
                {
                    FuelRange fuelRange = new FuelRange();

                    if (checkBoxName.Checked)
                    {
                        fuelRange.type = (FuelType)spnName.SelectedItemPosition;
                    }
                    if (rangeChk.Checked)
                    {
                        try
                        {
                            fuelRange.range = Java.Lang.Float.ParseFloat(rangeET.Text);
                        }
                        catch (Exception e)
                        {
                            fuelRange.range = 0;
                        }
                    }

                    fuelRangeList.Add(fuelRange);
                    fuelRangeAdapter.NotifyDataSetChanged();
                    Utility.setListViewHeightBasedOnChildren(fuel_range_lv);
                });

                fuelRangeAlertDialog.SetPositiveButton("Cancel", (senderAlert, args) =>
                {
                    fuelRangeAlertDialog.Dispose();
                });
                fuelRangeAlertDialog.Show();
            };

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

            rpcAlertDialog.SetNegativeButton(TX_NOW, (senderAlert, args) =>
			{
				GPSData gpsdata = null;
				if (gps_data_chk.Checked)
				{
					gpsdata = viGPSData;
				}

				float? speed = null;
				if (speed_chk.Checked && speed_et.Text != null && speed_et.Text.Length > 0)
				{
					speed = Java.Lang.Float.ParseFloat(speed_et.Text);
				}

				int? rpm = null;
				if (rpm_chk.Checked && rpm_et.Text != null && rpm_et.Text.Length > 0)
				{
					rpm = Java.Lang.Integer.ParseInt(rpm_et.Text);
				}

				float? fuelLevel = null;
				if (fuel_level_chk.Checked && fuel_level_et.Text != null && fuel_level_et.Text.Length > 0)
				{
					fuelLevel = Java.Lang.Float.ParseFloat(fuel_level_et.Text);
				}

				ComponentVolumeStatus? fuelLevel_State = null;
				if (fuel_level_state_chk.Checked)
				{
					fuelLevel_State = (ComponentVolumeStatus)fuel_level_state_spinner.SelectedItemPosition;
				}

				float? instantFuelConsumption = null;
				if (instant_fuel_consumption_chk.Checked && instant_fuel_consumption_et.Text != null && instant_fuel_consumption_et.Text.Length > 0)
				{
					instantFuelConsumption = Java.Lang.Float.ParseFloat(instant_fuel_consumption_et.Text);
				}

				float? externalTemperature = null;
				if (external_temp_chk.Checked && external_temp_et.Text != null && external_temp_et.Text.Length > 0)
				{
					externalTemperature = Java.Lang.Float.ParseFloat(external_temp_et.Text);
				}

				string vin = null;
				if (vin_chk.Checked)
				{
					vin = vin_et.Text;
				}

				PRNDL? prndl = null;
				if (prndl_chk.Checked)
				{
					prndl = (PRNDL)prndl_spinner.SelectedItemPosition;
				}

				TireStatus tirePressure = null;
				if (tire_pressure_chk.Checked)
				{
					tirePressure = viTireStatus;
				}

				int? odometer = null;
				if (odometer_chk.Checked && odometer_et.Text != null && odometer_et.Text.Length > 0)
				{
					odometer = Java.Lang.Integer.ParseInt(odometer_et.Text);
				}

				BeltStatus beltStatus = null;
				if (belt_status_chk.Checked)
				{
					beltStatus = viBeltStatus;
				}

				BodyInformation bodyInformation = null;
				if (body_info_chk.Checked)
				{
					bodyInformation = viBodyInformation;
				}

				DeviceStatus deviceStatus = null;
				if (device_status_chk.Checked)
				{
					deviceStatus = viDeviceStatus;
				}

				VehicleDataEventStatus? driverBraking = null;
				if (driver_braking_chk.Checked)
				{
					driverBraking = (VehicleDataEventStatus)driver_braking_spinner.SelectedItemPosition;
				}

				WiperStatus? wiperStatus = null;
				if (wiper_status_chk.Checked)
				{
					wiperStatus = (WiperStatus)wiper_status_spinner.SelectedItemPosition;
				}

				HeadLampStatus headLampStatus = null;
				if (head_lamp_status_chk.Checked)
				{
					headLampStatus = viHeadLampStatus;
				}

				float? engineTorque = null;
				if (engine_torque_chk.Checked && engine_torque_et.Text != null && engine_torque_et.Text.Length > 0)
				{
					engineTorque = Java.Lang.Float.ParseFloat(engine_torque_et.Text);
				}

				float? accPedalPosition = null;
				if (acc_padel_position_chk.Checked && acc_padel_position_et.Text != null && acc_padel_position_et.Text.Length > 0)
				{
					accPedalPosition = Java.Lang.Float.ParseFloat(acc_padel_position_et.Text);
				}

				float? steeringWheelAngle = null;
				if (steering_wheel_angle_chk.Checked && steering_wheel_angle_et.Text != null && steering_wheel_angle_et.Text.Length > 0)
				{
					steeringWheelAngle = Java.Lang.Float.ParseFloat(steering_wheel_angle_et.Text);
				}

				ECallInfo eCallInfo = null;
				if (ecall_info_chk.Checked)
				{
					eCallInfo = viECallInfo;
				}

				AirbagStatus airbagStatus = null;
				if (airbag_status_chk.Checked)
				{
					airbagStatus = viAirbagStatus;
				}

				EmergencyEvent emergencyEvent = null;
				if (emergency_event_chk.Checked)
				{
					emergencyEvent = viEmergencyEvent;
				}

				ClusterModeStatus clusterModeStatus = null;
				if (cluster_modes_chk.Checked)
				{
					clusterModeStatus = viClusterModeStatus;
				}

				MyKey myKey = null;
				if (my_key_chk.Checked)
				{
					myKey = new MyKey();
					myKey.e911Override = (VehicleDataStatus)my_key_spinner.SelectedItemPosition;
				}

                ElectronicParkBrakeStatus? electronicParkBrakeStatus = null;

                if (electronic_park_brake_status_chk.Checked)
				{
					electronicParkBrakeStatus = (ElectronicParkBrakeStatus)electronic_park_brake_status_spinner.SelectedItemPosition;
				}

                float? engineOilLife = null;
                if (engine_oil_life_chk.Checked)
                {
                    try
                    {
                        engineOilLife = Java.Lang.Float.ParseFloat(engine_oil_life_et.Text);
                    }
                    catch (Exception e)
                    {
                        engineOilLife = null;
                    }
                }

                List<FuelRange> fuelRange = null;
                if (fuel_range_chk.Checked)
                {
                    fuelRange = fuelRangeList;
                }

				RequestNotifyMessage rpcMessage = BuildRpc.buildVehicleInfoOnVehicleData(gpsdata, speed, rpm, fuelLevel,
																						 fuelLevel_State, instantFuelConsumption, externalTemperature, vin,
																						prndl, tirePressure, odometer, beltStatus, bodyInformation,
																						deviceStatus, driverBraking, wiperStatus, headLampStatus,
																						engineTorque, accPedalPosition, steeringWheelAngle, eCallInfo, airbagStatus,
																						emergencyEvent, clusterModeStatus, myKey, electronicParkBrakeStatus,
                                                                                         engineOilLife, fuelRange);
				AppUtils.savePreferenceValueForRpc(Activity, rpcMessage.getMethod(), rpcMessage);
                AppInstanceManager.Instance.sendRpc(rpcMessage);
			});
            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
        }

        void CreateVIResponseSubscribeVehicleData()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.subscribe_vehicle_data_response, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(VIResponseSubscribeVehicleData);

            var resultCode_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_result_code_chk);
            var gps_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_gps);
			var speed_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_speed);
			var rpm_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_rpm);
			var fuelLevel_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level);
			var fuelLevel_State_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_state);
			var instantFuelConsumption_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_instant_fuel_consumption);
			var externalTemperature_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_external_temperature);
			var prndl_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_prndl);
			var tirePressure_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_tire_pressure);
			var odometer_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_odometer);
			var beltStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_belt_status);
			var bodyInformation_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_body_info);
			var deviceStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_device_status);
			var driverBraking_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_driver_braking);
			var wiperStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_wiper_status);
			var headLampStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_head_lamp_status);
			var engineTorque_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_torque);
			var accPedalPosition_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_acc_pedal_pos);
			var steeringWheelAngle_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_steering_whel_angle);
			var eCallInfo_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_ecall_info);
			var airbagStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_airbag_status);
			var emergencyEvent_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_emergency_event);
			var clusterModes_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_cluster_modes);
			var myKey_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_my_key);
			var electronicParkBrakeStatus_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_electronic_park_brake_status);
            var engineOilLife_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_oil_life);
            var fuelRange_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_range);

			var gps_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_gps_data_type_chk);
            var speed_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_speed_data_type_chk);
            var rpm_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_rpm_date_type_chk);
            var fuelLevel_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_data_type_chk);
            var fuelLevel_State_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_state_data_type_chk);
            var instantFuelConsumption_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_data_type_chk);
            var externalTemperature_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_external_temperature_data_type_chk);
            var prndl_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_prndl_data_type_chk);
            var tirePressure_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_tire_pressure_data_type_chk);
            var odometer_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_odometer_data_type_chk);
            var beltStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_belt_status_data_type_chk);
            var bodyInformation_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_body_info_data_type_chk);
            var deviceStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_device_status_data_type_chk);
            var driverBraking_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_driver_braking_data_type_chk);
            var wiperStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_wiper_status_data_type_chk);
            var headLampStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_head_lamp_status_data_type_chk);
            var engineTorque_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_torque_data_type_chk);
            var accPedalPosition_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_acc_pedal_pos_data_type_chk);
            var steeringWheelAngle_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_steering_whel_angle_data_type_chk);
            var eCallInfo_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_ecall_info_data_type_chk);
            var airbagStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_airbag_status_data_type_chk);
            var emergencyEvent_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_emergency_event_data_type_chk);
            var clusterModes_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_cluster_modes_data_type_chk);
            var myKey_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_my_key_data_type_chk);
			var electronicParkBrakeStatus_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_data_type_chk);
            var engineOilLife_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_oil_life_data_type_chk);
            var fuelRange_data_type_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_range_data_type_chk);

			var gps_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_gps_result_code_chk);
            var speed_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_speed_result_code_chk);
            var rpm_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_rpm_result_code_chk);
            var fuelLevel_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_result_code_chk);
            var fuelLevel_State_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_level_state_result_code_chk);
            var instantFuelConsumption_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_result_code_chk);
            var externalTemperature_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_external_temperature_result_code_chk);
            var prndl_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_prndl_result_code_chk);
            var tirePressure_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_tire_pressure_result_code_chk);
            var odometer_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_odometer_result_code_chk);
            var beltStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_belt_status_result_code_chk);
            var bodyInformation_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_body_info_result_code_chk);
            var deviceStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_device_status_result_code_chk);
            var driverBraking_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_driver_braking_result_code_chk);
            var wiperStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_wiper_status_result_code_chk);
            var headLampStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_head_lamp_status_result_code_chk);
            var engineTorque_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_torque_result_code_chk);
            var accPedalPosition_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_acc_pedal_pos_result_code_chk);
            var steeringWheelAngle_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_steering_whel_angle_result_code_chk);
            var eCallInfo_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_ecall_info_result_code_chk);
            var airbagStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_airbag_status_result_code_chk);
            var emergencyEvent_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_emergency_event_result_code_chk);
            var clusterModes_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_cluster_modes_result_code_chk);
            var myKey_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_my_key_result_code_chk);
			var electronicParkBrakeStatus_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_result_code_chk);
            var engineOilLife_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_engine_oil_life_result_code_chk);
            var fuelRange_result_code_chk = rpcView.FindViewById<CheckBox>(Resource.Id.subscribe_vehicle_fuel_range_result_code_chk);

			var gps_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_gps_data_type_spinner);
			var speed_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_speed_data_type_spinner);
			var rpm_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_rpm_date_type_spinner);
			var fuelLevel_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_data_type_spinner);
			var fuelLevel_State_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_state_data_type_spinner);
			var instantFuelConsumption_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_data_type_spinner);
			var externalTemperature_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_external_temperature_data_type_spinner);
			var prndl_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_prndl_data_type_spinner);
			var tirePressure_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_tire_pressure_data_type_spinner);
			var odometer_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_odometer_data_type_spinner);
			var beltStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_belt_status_data_type_spinner);
			var bodyInformation_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_body_info_data_type_spinner);
			var deviceStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_device_status_data_type_spinner);
			var driverBraking_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_driver_braking_data_type_spinner);
			var wiperStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_wiper_status_data_type_spinner);
			var headLampStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_head_lamp_status_data_type_spinner);
			var engineTorque_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_torque_data_type_spinner);
			var accPedalPosition_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_acc_pedal_pos_data_type_spinner);
			var steeringWheelAngle_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_steering_whel_angle_data_type_spinner);
			var eCallInfo_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_ecall_info_data_type_spinner);
			var airbagStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_airbag_status_data_type_spinner);
			var emergencyEvent_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_emergency_event_data_type_spinner);
			var clusterModes_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_cluster_modes_data_type_spinner);
			var myKey_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_my_key_data_type_spinner);
            var electronicParkBrakeStatus_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_data_type_spinner);
            var engineOilLife_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_oil_life_data_type_spinner);
            var fuelRange_data_type_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_range_data_type_spinner);

			var result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_result_code_spinner);
			var gps_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_gps_result_code_spinner);
			var speed_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_speed_result_code_spinner);
			var rpm_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_rpm_result_code_spinner);
			var fuelLevel_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_result_code_spinner);
			var fuelLevel_State_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_level_state_result_code_spinner);
			var instantFuelConsumption_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_instant_fuel_consumption_result_code_spinner);
			var externalTemperature_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_external_temperature_result_code_spinner);
			var prndl_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_prndl_result_code_spinner);
			var tirePressure_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_tire_pressure_result_code_spinner);
			var odometer_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_odometer_result_code_spinner);
			var beltStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_belt_status_result_code_spinner);
			var bodyInformation_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_body_info_result_code_spinner);
			var deviceStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_device_status_result_code_spinner);
			var driverBraking_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_driver_braking_result_code_spinner);
			var wiperStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_wiper_status_result_code_spinner);
			var headLampStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_head_lamp_status_result_code_spinner);
			var engineTorque_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_torque_result_code_spinner);
			var accPedalPosition_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_acc_pedal_pos_result_code_spinner);
			var steeringWheelAngle_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_steering_whel_angle_result_code_spinner);
			var eCallInfo_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_ecall_info_result_code_spinner);
			var airbagStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_airbag_status_result_code_spinner);
			var emergencyEvent_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_emergency_event_result_code_spinner);
			var clusterModes_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_cluster_modes_result_code_spinner);
			var myKey_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_my_key_result_code_spinner);
			var electronicParkBrakeStatus_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_electronic_park_brake_status_result_code_spinner);
            var engineOilLife_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_engine_oil_life_result_code_spinner);
            var fuelRange_result_code_spinner = rpcView.FindViewById<Spinner>(Resource.Id.subscribe_vehicle_fuel_range_result_code_spinner);

			resultCode_chk.CheckedChange += (sender, e) => result_code_spinner.Enabled = e.IsChecked;

            gps_chk.CheckedChange += (sender, e) =>
            {
                gps_data_type_chk.Enabled = e.IsChecked;
                gps_result_code_chk.Enabled = e.IsChecked;
                gps_data_type_spinner.Enabled = e.IsChecked;
                gps_result_code_spinner.Enabled = e.IsChecked;
            };

            speed_chk.CheckedChange += (sender, e) =>
            {
                speed_data_type_chk.Enabled = e.IsChecked;
                speed_result_code_chk.Enabled = e.IsChecked;
                speed_data_type_spinner.Enabled = e.IsChecked;
                speed_result_code_spinner.Enabled = e.IsChecked;
            };

            rpm_chk.CheckedChange += (sender, e) =>
            {
                rpm_data_type_chk.Enabled = e.IsChecked;
                rpm_result_code_chk.Enabled = e.IsChecked;
                rpm_data_type_spinner.Enabled = e.IsChecked;
                rpm_result_code_spinner.Enabled = e.IsChecked;
            };

            fuelLevel_chk.CheckedChange += (sender, e) =>
            {
                fuelLevel_data_type_chk.Enabled = e.IsChecked;
                fuelLevel_result_code_chk.Enabled = e.IsChecked;
                fuelLevel_data_type_spinner.Enabled = e.IsChecked;
                fuelLevel_result_code_spinner.Enabled = e.IsChecked;
            };

            fuelLevel_State_chk.CheckedChange += (sender, e) =>
            {
                fuelLevel_State_data_type_chk.Enabled = e.IsChecked;
                fuelLevel_State_result_code_chk.Enabled = e.IsChecked;
                fuelLevel_State_data_type_spinner.Enabled = e.IsChecked;
                fuelLevel_State_result_code_spinner.Enabled = e.IsChecked;
            };

            instantFuelConsumption_chk.CheckedChange += (sender, e) =>
            {
                instantFuelConsumption_data_type_chk.Enabled = e.IsChecked;
                instantFuelConsumption_result_code_chk.Enabled = e.IsChecked;
                instantFuelConsumption_data_type_spinner.Enabled = e.IsChecked;
                instantFuelConsumption_result_code_spinner.Enabled = e.IsChecked;
            };

            externalTemperature_chk.CheckedChange += (sender, e) =>
            {
                externalTemperature_data_type_chk.Enabled = e.IsChecked;
                externalTemperature_result_code_chk.Enabled = e.IsChecked;
                externalTemperature_data_type_spinner.Enabled = e.IsChecked;
                externalTemperature_result_code_spinner.Enabled = e.IsChecked;
            };

            prndl_chk.CheckedChange += (sender, e) =>
            {
                prndl_data_type_chk.Enabled = e.IsChecked;
                prndl_result_code_chk.Enabled = e.IsChecked;
                prndl_data_type_spinner.Enabled = e.IsChecked;
                prndl_result_code_spinner.Enabled = e.IsChecked;
            };

            tirePressure_chk.CheckedChange += (sender, e) =>
            {
                tirePressure_data_type_chk.Enabled = e.IsChecked;
                tirePressure_result_code_chk.Enabled = e.IsChecked;
                tirePressure_data_type_spinner.Enabled = e.IsChecked;
                tirePressure_result_code_spinner.Enabled = e.IsChecked;
            };

            odometer_chk.CheckedChange += (sender, e) =>
            {
                odometer_data_type_chk.Enabled = e.IsChecked;
                odometer_result_code_chk.Enabled = e.IsChecked;
                odometer_data_type_spinner.Enabled = e.IsChecked;
                odometer_result_code_spinner.Enabled = e.IsChecked;
            };

            beltStatus_chk.CheckedChange += (sender, e) =>
            {
                beltStatus_data_type_chk.Enabled = e.IsChecked;
                beltStatus_result_code_chk.Enabled = e.IsChecked;
                beltStatus_data_type_spinner.Enabled = e.IsChecked;
                beltStatus_result_code_spinner.Enabled = e.IsChecked;
            };

            bodyInformation_chk.CheckedChange += (sender, e) =>
            {
                bodyInformation_data_type_chk.Enabled = e.IsChecked;
                bodyInformation_result_code_chk.Enabled = e.IsChecked;
                bodyInformation_data_type_spinner.Enabled = e.IsChecked;
                bodyInformation_result_code_spinner.Enabled = e.IsChecked;
            };

            deviceStatus_chk.CheckedChange += (sender, e) =>
            {
                deviceStatus_data_type_chk.Enabled = e.IsChecked;
                deviceStatus_result_code_chk.Enabled = e.IsChecked;
                deviceStatus_data_type_spinner.Enabled = e.IsChecked;
                deviceStatus_result_code_spinner.Enabled = e.IsChecked;
            };

            driverBraking_chk.CheckedChange += (sender, e) =>
            {
                driverBraking_data_type_chk.Enabled = e.IsChecked;
                driverBraking_result_code_chk.Enabled = e.IsChecked;
                driverBraking_data_type_spinner.Enabled = e.IsChecked;
                driverBraking_result_code_spinner.Enabled = e.IsChecked;
            };

            wiperStatus_chk.CheckedChange += (sender, e) =>
            {
                wiperStatus_data_type_chk.Enabled = e.IsChecked;
                wiperStatus_result_code_chk.Enabled = e.IsChecked;
                wiperStatus_data_type_spinner.Enabled = e.IsChecked;
                wiperStatus_result_code_spinner.Enabled = e.IsChecked;
            };

            headLampStatus_chk.CheckedChange += (sender, e) =>
            {
                headLampStatus_data_type_chk.Enabled = e.IsChecked;
                headLampStatus_result_code_chk.Enabled = e.IsChecked;
                headLampStatus_data_type_spinner.Enabled = e.IsChecked;
                headLampStatus_result_code_spinner.Enabled = e.IsChecked;
            };

            engineTorque_chk.CheckedChange += (sender, e) =>
            {
                engineTorque_data_type_chk.Enabled = e.IsChecked;
                engineTorque_result_code_chk.Enabled = e.IsChecked;
                engineTorque_data_type_spinner.Enabled = e.IsChecked;
                engineTorque_result_code_spinner.Enabled = e.IsChecked;
            };

            accPedalPosition_chk.CheckedChange += (sender, e) =>
            {
                accPedalPosition_data_type_chk.Enabled = e.IsChecked;
                accPedalPosition_result_code_chk.Enabled = e.IsChecked;
                accPedalPosition_data_type_spinner.Enabled = e.IsChecked;
                accPedalPosition_result_code_spinner.Enabled = e.IsChecked;
            };

            steeringWheelAngle_chk.CheckedChange += (sender, e) =>
            {
                steeringWheelAngle_data_type_chk.Enabled = e.IsChecked;
                steeringWheelAngle_result_code_chk.Enabled = e.IsChecked;
                steeringWheelAngle_data_type_spinner.Enabled = e.IsChecked;
                steeringWheelAngle_result_code_spinner.Enabled = e.IsChecked;
            };

            eCallInfo_chk.CheckedChange += (sender, e) =>
            {
                eCallInfo_data_type_chk.Enabled = e.IsChecked;
                eCallInfo_result_code_chk.Enabled = e.IsChecked;
                eCallInfo_data_type_spinner.Enabled = e.IsChecked;
                eCallInfo_result_code_spinner.Enabled = e.IsChecked;
            };

            airbagStatus_chk.CheckedChange += (sender, e) =>
            {
                airbagStatus_data_type_chk.Enabled = e.IsChecked;
                airbagStatus_result_code_chk.Enabled = e.IsChecked;
                airbagStatus_data_type_spinner.Enabled = e.IsChecked;
                airbagStatus_result_code_spinner.Enabled = e.IsChecked;
            };

            emergencyEvent_chk.CheckedChange += (sender, e) =>
            {
                emergencyEvent_data_type_chk.Enabled = e.IsChecked;
                emergencyEvent_result_code_chk.Enabled = e.IsChecked;
                emergencyEvent_data_type_spinner.Enabled = e.IsChecked;
                emergencyEvent_result_code_spinner.Enabled = e.IsChecked;
            };

            clusterModes_chk.CheckedChange += (sender, e) =>
            {
                clusterModes_data_type_chk.Enabled = e.IsChecked;
                clusterModes_result_code_chk.Enabled = e.IsChecked;
                clusterModes_data_type_spinner.Enabled = e.IsChecked;
                clusterModes_result_code_spinner.Enabled = e.IsChecked;
            };

            myKey_chk.CheckedChange += (sender, e) =>
            {
                myKey_data_type_chk.Enabled = e.IsChecked;
                myKey_result_code_chk.Enabled = e.IsChecked;
                myKey_data_type_spinner.Enabled = e.IsChecked;
                myKey_result_code_spinner.Enabled = e.IsChecked;
            };

			electronicParkBrakeStatus_chk.CheckedChange += (sender, e) =>
			{
				electronicParkBrakeStatus_data_type_chk.Enabled = e.IsChecked;
				electronicParkBrakeStatus_result_code_chk.Enabled = e.IsChecked;
				electronicParkBrakeStatus_data_type_spinner.Enabled = e.IsChecked;
				electronicParkBrakeStatus_result_code_spinner.Enabled = e.IsChecked;
			};

            engineOilLife_chk.CheckedChange += (sender, e) =>
            {
                engineOilLife_data_type_chk.Enabled = e.IsChecked;
                engineOilLife_result_code_chk.Enabled = e.IsChecked;
                engineOilLife_data_type_spinner.Enabled = e.IsChecked;
                engineOilLife_result_code_spinner.Enabled = e.IsChecked;
            };

            fuelRange_chk.CheckedChange += (sender, e) =>
            {
                fuelRange_data_type_chk.Enabled = e.IsChecked;
                fuelRange_result_code_chk.Enabled = e.IsChecked;
                fuelRange_data_type_spinner.Enabled = e.IsChecked;
                fuelRange_result_code_spinner.Enabled = e.IsChecked;
            };

            gps_data_type_chk.CheckedChange += (sender, e) => gps_data_type_spinner.Enabled = e.IsChecked;
            speed_data_type_chk.CheckedChange += (sender, e) => speed_data_type_spinner.Enabled = e.IsChecked;
            rpm_data_type_chk.CheckedChange += (sender, e) => rpm_data_type_spinner.Enabled = e.IsChecked;
            fuelLevel_data_type_chk.CheckedChange += (sender, e) => fuelLevel_data_type_spinner.Enabled = e.IsChecked;
            fuelLevel_State_data_type_chk.CheckedChange += (sender, e) => fuelLevel_State_data_type_spinner.Enabled = e.IsChecked;
            instantFuelConsumption_data_type_chk.CheckedChange += (sender, e) => instantFuelConsumption_data_type_spinner.Enabled = e.IsChecked;
            externalTemperature_data_type_chk.CheckedChange += (sender, e) => externalTemperature_data_type_spinner.Enabled = e.IsChecked;
            prndl_data_type_chk.CheckedChange += (sender, e) => prndl_data_type_spinner.Enabled = e.IsChecked;
            tirePressure_data_type_chk.CheckedChange += (sender, e) => tirePressure_data_type_spinner.Enabled = e.IsChecked;
            odometer_data_type_chk.CheckedChange += (sender, e) => odometer_data_type_spinner.Enabled = e.IsChecked;
            beltStatus_data_type_chk.CheckedChange += (sender, e) => beltStatus_data_type_spinner.Enabled = e.IsChecked;
            bodyInformation_data_type_chk.CheckedChange += (sender, e) => bodyInformation_data_type_spinner.Enabled = e.IsChecked;
            deviceStatus_data_type_chk.CheckedChange += (sender, e) => deviceStatus_data_type_spinner.Enabled = e.IsChecked;
            driverBraking_data_type_chk.CheckedChange += (sender, e) => driverBraking_data_type_spinner.Enabled = e.IsChecked;
            wiperStatus_data_type_chk.CheckedChange += (sender, e) => wiperStatus_data_type_spinner.Enabled = e.IsChecked;
            headLampStatus_data_type_chk.CheckedChange += (sender, e) => headLampStatus_data_type_spinner.Enabled = e.IsChecked;
            engineTorque_data_type_chk.CheckedChange += (sender, e) => engineTorque_data_type_spinner.Enabled = e.IsChecked;
            accPedalPosition_data_type_chk.CheckedChange += (sender, e) => accPedalPosition_data_type_spinner.Enabled = e.IsChecked;
            steeringWheelAngle_data_type_chk.CheckedChange += (sender, e) => steeringWheelAngle_data_type_spinner.Enabled = e.IsChecked;
            eCallInfo_data_type_chk.CheckedChange += (sender, e) => eCallInfo_data_type_spinner.Enabled = e.IsChecked;
            airbagStatus_data_type_chk.CheckedChange += (sender, e) => airbagStatus_data_type_spinner.Enabled = e.IsChecked;
            emergencyEvent_data_type_chk.CheckedChange += (sender, e) => emergencyEvent_data_type_spinner.Enabled = e.IsChecked;
            clusterModes_data_type_chk.CheckedChange += (sender, e) => clusterModes_data_type_spinner.Enabled = e.IsChecked;
            myKey_data_type_chk.CheckedChange += (sender, e) => myKey_data_type_spinner.Enabled = e.IsChecked;
			electronicParkBrakeStatus_data_type_chk.CheckedChange += (sender, e) => electronicParkBrakeStatus_data_type_spinner.Enabled = e.IsChecked;
            engineOilLife_data_type_chk.CheckedChange += (sender, e) => engineOilLife_data_type_spinner.Enabled = e.IsChecked;
            fuelRange_data_type_chk.CheckedChange += (sender, e) => fuelRange_data_type_spinner.Enabled = e.IsChecked;

			gps_result_code_chk.CheckedChange += (sender, e) => gps_result_code_spinner.Enabled = e.IsChecked;
            speed_result_code_chk.CheckedChange += (sender, e) => speed_result_code_spinner.Enabled = e.IsChecked;
            rpm_result_code_chk.CheckedChange += (sender, e) => rpm_result_code_spinner.Enabled = e.IsChecked;
            fuelLevel_result_code_chk.CheckedChange += (sender, e) => fuelLevel_result_code_spinner.Enabled = e.IsChecked;
            fuelLevel_State_result_code_chk.CheckedChange += (sender, e) => fuelLevel_State_result_code_spinner.Enabled = e.IsChecked;
            instantFuelConsumption_result_code_chk.CheckedChange += (sender, e) => instantFuelConsumption_result_code_spinner.Enabled = e.IsChecked;
            externalTemperature_result_code_chk.CheckedChange += (sender, e) => externalTemperature_result_code_spinner.Enabled = e.IsChecked;
            prndl_result_code_chk.CheckedChange += (sender, e) => prndl_result_code_spinner.Enabled = e.IsChecked;
            tirePressure_result_code_chk.CheckedChange += (sender, e) => tirePressure_result_code_spinner.Enabled = e.IsChecked;
            odometer_result_code_chk.CheckedChange += (sender, e) => odometer_result_code_spinner.Enabled = e.IsChecked;
            beltStatus_result_code_chk.CheckedChange += (sender, e) => beltStatus_result_code_spinner.Enabled = e.IsChecked;
            bodyInformation_result_code_chk.CheckedChange += (sender, e) => bodyInformation_result_code_spinner.Enabled = e.IsChecked;
            deviceStatus_result_code_chk.CheckedChange += (sender, e) => deviceStatus_result_code_spinner.Enabled = e.IsChecked;
            driverBraking_result_code_chk.CheckedChange += (sender, e) => driverBraking_result_code_spinner.Enabled = e.IsChecked;
            wiperStatus_result_code_chk.CheckedChange += (sender, e) => wiperStatus_result_code_spinner.Enabled = e.IsChecked;
            headLampStatus_result_code_chk.CheckedChange += (sender, e) => headLampStatus_result_code_spinner.Enabled = e.IsChecked;
            engineTorque_result_code_chk.CheckedChange += (sender, e) => engineTorque_result_code_spinner.Enabled = e.IsChecked;
            accPedalPosition_result_code_chk.CheckedChange += (sender, e) => accPedalPosition_result_code_spinner.Enabled = e.IsChecked;
            steeringWheelAngle_result_code_chk.CheckedChange += (sender, e) => steeringWheelAngle_result_code_spinner.Enabled = e.IsChecked;
            eCallInfo_result_code_chk.CheckedChange += (sender, e) => eCallInfo_result_code_spinner.Enabled = e.IsChecked;
            airbagStatus_result_code_chk.CheckedChange += (sender, e) => airbagStatus_result_code_spinner.Enabled = e.IsChecked;
            emergencyEvent_result_code_chk.CheckedChange += (sender, e) => emergencyEvent_result_code_spinner.Enabled = e.IsChecked;
            clusterModes_result_code_chk.CheckedChange += (sender, e) => clusterModes_result_code_spinner.Enabled = e.IsChecked;
            myKey_result_code_chk.CheckedChange += (sender, e) => myKey_result_code_spinner.Enabled = e.IsChecked;
			electronicParkBrakeStatus_result_code_chk.CheckedChange += (sender, e) => electronicParkBrakeStatus_result_code_spinner.Enabled = e.IsChecked;
            engineOilLife_result_code_chk.CheckedChange += (sender, e) => engineOilLife_result_code_spinner.Enabled = e.IsChecked;
            fuelRange_result_code_chk.CheckedChange += (sender, e) => fuelRange_result_code_spinner.Enabled = e.IsChecked;

			var vehicleDataTypeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataType);
			gps_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			speed_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			rpm_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			fuelLevel_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			fuelLevel_State_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			instantFuelConsumption_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			externalTemperature_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			prndl_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			tirePressure_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			odometer_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			beltStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			bodyInformation_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			deviceStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			driverBraking_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			wiperStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			headLampStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			engineTorque_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			accPedalPosition_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			steeringWheelAngle_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			eCallInfo_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			airbagStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			emergencyEvent_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			clusterModes_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			myKey_data_type_spinner.Adapter = vehicleDataTypeAdapter;
			electronicParkBrakeStatus_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            engineOilLife_data_type_spinner.Adapter = vehicleDataTypeAdapter;
            fuelRange_data_type_spinner.Adapter = vehicleDataTypeAdapter;

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            var vehicleDataResultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataResultCode);
			result_code_spinner.Adapter = resultCodeAdapter;
			gps_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			speed_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			rpm_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			fuelLevel_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			fuelLevel_State_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			instantFuelConsumption_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			externalTemperature_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			prndl_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			tirePressure_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			odometer_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			beltStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			bodyInformation_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			deviceStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			driverBraking_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			wiperStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			headLampStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			engineTorque_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			accPedalPosition_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			steeringWheelAngle_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			eCallInfo_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			airbagStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			emergencyEvent_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			clusterModes_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			myKey_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
			electronicParkBrakeStatus_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            engineOilLife_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;
            fuelRange_result_code_spinner.Adapter = vehicleDataResultCodeAdapter;

			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				result_code_spinner.SetSelection((int)tmpObj.getResultCode());

                gps_data_type_spinner.SetSelection((int)tmpObj.getGps().getDataType());
                gps_result_code_spinner.SetSelection((int)tmpObj.getGps().getResultCode());

                speed_data_type_spinner.SetSelection((int)tmpObj.getSpeed().getDataType());
                speed_result_code_spinner.SetSelection((int)tmpObj.getSpeed().getResultCode());

                rpm_data_type_spinner.SetSelection((int)tmpObj.getRpm().getDataType());
                rpm_result_code_spinner.SetSelection((int)tmpObj.getRpm().getResultCode());

                fuelLevel_data_type_spinner.SetSelection((int)tmpObj.getFuelLevel().getDataType());
				fuelLevel_result_code_spinner.SetSelection((int)tmpObj.getFuelLevel_State().getResultCode());

				fuelLevel_State_data_type_spinner.SetSelection((int)tmpObj.getFuelLevel_State().getDataType());
				fuelLevel_State_result_code_spinner.SetSelection((int)tmpObj.getFuelLevel_State().getResultCode());

				instantFuelConsumption_data_type_spinner.SetSelection((int)tmpObj.getInstantFuelConsumption().getDataType());
				instantFuelConsumption_result_code_spinner.SetSelection((int)tmpObj.getInstantFuelConsumption().getResultCode());

				externalTemperature_data_type_spinner.SetSelection((int)tmpObj.getExternalTemperature().getDataType());
				externalTemperature_result_code_spinner.SetSelection((int)tmpObj.getExternalTemperature().getResultCode());

				prndl_data_type_spinner.SetSelection((int)tmpObj.getPrndl().getDataType());
				prndl_result_code_spinner.SetSelection((int)tmpObj.getPrndl().getResultCode());

				tirePressure_data_type_spinner.SetSelection((int)tmpObj.getTirePressure().getDataType());
				tirePressure_result_code_spinner.SetSelection((int)tmpObj.getTirePressure().getResultCode());

				odometer_data_type_spinner.SetSelection((int)tmpObj.getOdometer().getDataType());
				odometer_result_code_spinner.SetSelection((int)tmpObj.getOdometer().getResultCode());

				beltStatus_data_type_spinner.SetSelection((int)tmpObj.getBeltStatus().getDataType());
				beltStatus_result_code_spinner.SetSelection((int)tmpObj.getBeltStatus().getResultCode());

				bodyInformation_data_type_spinner.SetSelection((int)tmpObj.getBodyInformation().getDataType());
				bodyInformation_result_code_spinner.SetSelection((int)tmpObj.getBodyInformation().getResultCode());

				deviceStatus_data_type_spinner.SetSelection((int)tmpObj.getDeviceStatus().getDataType());
				deviceStatus_result_code_spinner.SetSelection((int)tmpObj.getDeviceStatus().getResultCode());

				driverBraking_data_type_spinner.SetSelection((int)tmpObj.getdriverBraking().getDataType());
				driverBraking_result_code_spinner.SetSelection((int)tmpObj.getdriverBraking().getResultCode());

				wiperStatus_data_type_spinner.SetSelection((int)tmpObj.getWiperStatus().getDataType());
				wiperStatus_result_code_spinner.SetSelection((int)tmpObj.getWiperStatus().getResultCode());

				headLampStatus_data_type_spinner.SetSelection((int)tmpObj.getHeadLampStatus().getDataType());
				headLampStatus_result_code_spinner.SetSelection((int)tmpObj.getHeadLampStatus().getResultCode());

				engineTorque_data_type_spinner.SetSelection((int)tmpObj.getEngineTorque().getDataType());
				engineTorque_result_code_spinner.SetSelection((int)tmpObj.getEngineTorque().getResultCode());

				accPedalPosition_data_type_spinner.SetSelection((int)tmpObj.getAccPedalPosition().getDataType());
				accPedalPosition_result_code_spinner.SetSelection((int)tmpObj.getAccPedalPosition().getResultCode());

				steeringWheelAngle_data_type_spinner.SetSelection((int)tmpObj.getSteeringWheelAngle().getDataType());
				steeringWheelAngle_result_code_spinner.SetSelection((int)tmpObj.getSteeringWheelAngle().getResultCode());

				eCallInfo_data_type_spinner.SetSelection((int)tmpObj.getECallInfo().getDataType());
				eCallInfo_result_code_spinner.SetSelection((int)tmpObj.getECallInfo().getResultCode());

				airbagStatus_data_type_spinner.SetSelection((int)tmpObj.getAirbagStatus().getDataType());
				airbagStatus_result_code_spinner.SetSelection((int)tmpObj.getAirbagStatus().getResultCode());

				emergencyEvent_data_type_spinner.SetSelection((int)tmpObj.getEmergencyEvent().getDataType());
				emergencyEvent_result_code_spinner.SetSelection((int)tmpObj.getEmergencyEvent().getResultCode());

				clusterModes_data_type_spinner.SetSelection((int)tmpObj.getClusterModes().getDataType());
				clusterModes_result_code_spinner.SetSelection((int)tmpObj.getClusterModes().getResultCode());

				myKey_data_type_spinner.SetSelection((int)tmpObj.getMyKey().getDataType());
				myKey_result_code_spinner.SetSelection((int)tmpObj.getMyKey().getResultCode());

				electronicParkBrakeStatus_data_type_spinner.SetSelection((int)tmpObj.getElectronicParkBrakeStatus().getDataType());
				electronicParkBrakeStatus_result_code_spinner.SetSelection((int)tmpObj.getElectronicParkBrakeStatus().getResultCode());

                if(tmpObj.getEngineOilLife() != null){
					engineOilLife_data_type_spinner.SetSelection((int)tmpObj.getEngineOilLife().getDataType());
					engineOilLife_result_code_spinner.SetSelection((int)tmpObj.getEngineOilLife().getResultCode());
                }

                if(tmpObj.getFuelRange() != null){
					fuelRange_data_type_spinner.SetSelection((int)tmpObj.getFuelRange().getDataType());
					fuelRange_result_code_spinner.SetSelection((int)tmpObj.getFuelRange().getResultCode());
                }

			}

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{

				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCode_chk.Checked)
					rsltCode = (HmiApiLib.Common.Enums.Result)result_code_spinner.SelectedItemPosition;


				VehicleDataResult gps = new VehicleDataResult(), speed = new VehicleDataResult(), rpm = new VehicleDataResult(), fuelLevel = new VehicleDataResult(), fuelLevelState = new VehicleDataResult(), instantFuel = new VehicleDataResult(), externalTemperature = new VehicleDataResult(),
								 prndl = new VehicleDataResult(), tirePressure = new VehicleDataResult(), odometer = new VehicleDataResult(), beltStatus = new VehicleDataResult(), bodyInformation = new VehicleDataResult(), deviceStatus = new VehicleDataResult(), driverBraking = new VehicleDataResult(),
								 wiperStatus = new VehicleDataResult(), headLampStatus = new VehicleDataResult(), engineTorque = new VehicleDataResult(), accPedalPosition = new VehicleDataResult(), steeringWheelAngle = new VehicleDataResult(), eCallInfo = new VehicleDataResult(),
								 airbagStatus = new VehicleDataResult(), emergencyEvent = new VehicleDataResult(), clusterModes = new VehicleDataResult(), myKey = new VehicleDataResult(), electronicParkBrakeStatus = new VehicleDataResult(),
                                 engineOilLife = new VehicleDataResult(), fuelRange = new VehicleDataResult();


				VehicleDataType gpsDataType = new VehicleDataType(), speedDataType = new VehicleDataType(), rpmDataType = new VehicleDataType(), fuelLevelDataType = new VehicleDataType(), fuelLevelStateDataType = new VehicleDataType(), instantFuelConsumptionDataType = new VehicleDataType(), externalTemperatureDataType = new VehicleDataType(),
								 prndlDataType = new VehicleDataType(), tirePressureDataType = new VehicleDataType(), odometerDataType = new VehicleDataType(), beltStatusDataType = new VehicleDataType(), bodyInformationDataType = new VehicleDataType(), deviceStatusDataType = new VehicleDataType(), driverBrakingDataType = new VehicleDataType(),
								 wiperStatusDataType = new VehicleDataType(), headLampStatusDataType = new VehicleDataType(), engineTorqueDataType = new VehicleDataType(), accPedalPositionDataType = new VehicleDataType(), steeringWheelAngleDataType = new VehicleDataType(), eCallInfoDataType = new VehicleDataType(),
                                airbagStatusDataType = new VehicleDataType(), emergencyEventDataType = new VehicleDataType(), clusterModesDataType = new VehicleDataType(), myKeyDataType = new VehicleDataType(), electronicParkBrakeStatusDataType = new VehicleDataType(), engineOilLifeDataType = new VehicleDataType(),
                                fuelRangeDataType = new VehicleDataType();

				VehicleDataResultCode gpsRsltCode = new VehicleDataResultCode(), speedRsltCode = new VehicleDataResultCode(), rpmRsltCode = new VehicleDataResultCode(), fuelLevelRsltCode = new VehicleDataResultCode(), fuelLevelStateRsltCode = new VehicleDataResultCode(), instantFuelConsumptionRsltCode = new VehicleDataResultCode(), externalTemperatureRsltCode = new VehicleDataResultCode(),
								 prndlRsltCode = new VehicleDataResultCode(), tirePressureRsltCode = new VehicleDataResultCode(), odometerRsltCode = new VehicleDataResultCode(), beltStatusRsltCode = new VehicleDataResultCode(), bodyInformationRsltCode = new VehicleDataResultCode(), deviceStatusRsltCode = new VehicleDataResultCode(), driverBrakingRsltCode = new VehicleDataResultCode(),
								 wiperStatusRsltCode = new VehicleDataResultCode(), headLampStatusRsltCode = new VehicleDataResultCode(), engineTorqueRsltCode = new VehicleDataResultCode(), accPedalPositionRsltCode = new VehicleDataResultCode(), steeringWheelAngleRsltCode = new VehicleDataResultCode(), eCallInfoRsltCode = new VehicleDataResultCode(),
                                airbagStatusRsltCode = new VehicleDataResultCode(), emergencyEventRsltCode = new VehicleDataResultCode(), clusterModesRsltCode = new VehicleDataResultCode(), myKeyRsltCode = new VehicleDataResultCode(), electronicParkBrakeStatusRsltCode = new VehicleDataResultCode(), engineOilLifeRsltCode = new VehicleDataResultCode(),
                                fuelRangeRsltCode = new VehicleDataResultCode();



				if (gps_data_type_chk.Checked)
					gpsDataType = (VehicleDataType)gps_data_type_spinner.SelectedItemPosition;

				if (gps_result_code_chk.Checked)
					gpsRsltCode = (VehicleDataResultCode)gps_result_code_spinner.SelectedItemPosition;

				if (gps_chk.Checked)
				{
					gps.dataType = gpsDataType;
					gps.resultCode = gpsRsltCode;
				}


				if (speed_data_type_chk.Checked)
					speedDataType = (VehicleDataType)speed_data_type_spinner.SelectedItemPosition;

				if (speed_result_code_chk.Checked)
					speedRsltCode = (VehicleDataResultCode)speed_result_code_spinner.SelectedItemPosition;

				if (speed_chk.Checked)
				{
					speed.dataType = speedDataType;
					speed.resultCode = speedRsltCode;
				}


				if (rpm_data_type_chk.Checked)
					rpmDataType = (VehicleDataType)rpm_data_type_spinner.SelectedItemPosition;

				if (rpm_result_code_chk.Checked)
					rpmRsltCode = (VehicleDataResultCode)rpm_result_code_spinner.SelectedItemPosition;

				if (rpm_chk.Checked)
				{
					rpm.dataType = rpmDataType;
					rpm.resultCode = rpmRsltCode;
				}


				if (fuelLevel_data_type_chk.Checked)
					fuelLevelDataType = (VehicleDataType)fuelLevel_data_type_spinner.SelectedItemPosition;

				if (fuelLevel_result_code_chk.Checked)
					fuelLevelRsltCode = (VehicleDataResultCode)fuelLevel_result_code_spinner.SelectedItemPosition;

				if (fuelLevel_chk.Checked)
				{
					fuelLevel.dataType = fuelLevelDataType;
					fuelLevel.resultCode = fuelLevelRsltCode;
				}


				if (fuelLevel_State_data_type_chk.Checked)
					fuelLevelStateDataType = (VehicleDataType)fuelLevel_State_data_type_spinner.SelectedItemPosition;

				if (fuelLevel_State_result_code_chk.Checked)
					fuelLevelStateRsltCode = (VehicleDataResultCode)fuelLevel_State_result_code_spinner.SelectedItemPosition;

				if (fuelLevel_State_chk.Checked)
				{
					fuelLevelState.dataType = fuelLevelStateDataType;
					fuelLevelState.resultCode = fuelLevelStateRsltCode;
				}


				if (instantFuelConsumption_data_type_chk.Checked)
					instantFuelConsumptionDataType = (VehicleDataType)instantFuelConsumption_data_type_spinner.SelectedItemPosition;

				if (instantFuelConsumption_result_code_chk.Checked)
					instantFuelConsumptionRsltCode = (VehicleDataResultCode)instantFuelConsumption_result_code_spinner.SelectedItemPosition;

				if (instantFuelConsumption_chk.Checked)
				{
                    instantFuel.dataType = instantFuelConsumptionDataType;
                    instantFuel.resultCode = instantFuelConsumptionRsltCode;
				}


				if (externalTemperature_data_type_chk.Checked)
					externalTemperatureDataType = (VehicleDataType)externalTemperature_data_type_spinner.SelectedItemPosition;

				if (externalTemperature_result_code_chk.Checked)
					externalTemperatureRsltCode = (VehicleDataResultCode)externalTemperature_result_code_spinner.SelectedItemPosition;

				if (externalTemperature_chk.Checked)
				{
					externalTemperature.dataType = gpsDataType;
					externalTemperature.resultCode = gpsRsltCode;
				}


				if (prndl_data_type_chk.Checked)
					prndlDataType = (VehicleDataType)prndl_data_type_spinner.SelectedItemPosition;

				if (prndl_result_code_chk.Checked)
					prndlRsltCode = (VehicleDataResultCode)prndl_result_code_spinner.SelectedItemPosition;

				if (prndl_chk.Checked)
				{
					prndl.dataType = prndlDataType;
					prndl.resultCode = prndlRsltCode;
				}


				if (tirePressure_data_type_chk.Checked)
					tirePressureDataType = (VehicleDataType)tirePressure_data_type_spinner.SelectedItemPosition;

				if (tirePressure_result_code_chk.Checked)
					tirePressureRsltCode = (VehicleDataResultCode)tirePressure_result_code_spinner.SelectedItemPosition;

				if (tirePressure_chk.Checked)
				{
					tirePressure.dataType = tirePressureDataType;
					tirePressure.resultCode = tirePressureRsltCode;
				}


				if (odometer_data_type_chk.Checked)
					odometerDataType = (VehicleDataType)odometer_data_type_spinner.SelectedItemPosition;

				if (odometer_result_code_chk.Checked)
					odometerRsltCode = (VehicleDataResultCode)odometer_result_code_spinner.SelectedItemPosition;

				if (odometer_chk.Checked)
				{
					odometer.dataType = odometerDataType;
					odometer.resultCode = odometerRsltCode;
				}


				if (beltStatus_data_type_chk.Checked)
					beltStatusDataType = (VehicleDataType)beltStatus_data_type_spinner.SelectedItemPosition;

				if (beltStatus_result_code_chk.Checked)
					beltStatusRsltCode = (VehicleDataResultCode)beltStatus_result_code_spinner.SelectedItemPosition;

				if (beltStatus_chk.Checked)
				{
					beltStatus.dataType = beltStatusDataType;
					beltStatus.resultCode = beltStatusRsltCode;
				}


				if (bodyInformation_data_type_chk.Checked)
					bodyInformationDataType = (VehicleDataType)bodyInformation_data_type_spinner.SelectedItemPosition;

				if (bodyInformation_result_code_chk.Checked)
					bodyInformationRsltCode = (VehicleDataResultCode)bodyInformation_result_code_spinner.SelectedItemPosition;

				if (bodyInformation_chk.Checked)
				{
					bodyInformation.dataType = bodyInformationDataType;
					bodyInformation.resultCode = bodyInformationRsltCode;
				}


				if (deviceStatus_data_type_chk.Checked)
					deviceStatusDataType = (VehicleDataType)deviceStatus_data_type_spinner.SelectedItemPosition;

				if (deviceStatus_result_code_chk.Checked)
					deviceStatusRsltCode = (VehicleDataResultCode)deviceStatus_result_code_spinner.SelectedItemPosition;

				if (deviceStatus_chk.Checked)
				{
					deviceStatus.dataType = deviceStatusDataType;
					deviceStatus.resultCode = deviceStatusRsltCode;
				}


				if (driverBraking_data_type_chk.Checked)
					driverBrakingDataType = (VehicleDataType)driverBraking_data_type_spinner.SelectedItemPosition;

				if (driverBraking_result_code_chk.Checked)
					driverBrakingRsltCode = (VehicleDataResultCode)driverBraking_result_code_spinner.SelectedItemPosition;

				if (driverBraking_chk.Checked)
				{
					driverBraking.dataType = driverBrakingDataType;
					driverBraking.resultCode = driverBrakingRsltCode;
				}


				if (wiperStatus_data_type_chk.Checked)
					wiperStatusDataType = (VehicleDataType)wiperStatus_data_type_spinner.SelectedItemPosition;

				if (wiperStatus_result_code_chk.Checked)
					wiperStatusRsltCode = (VehicleDataResultCode)wiperStatus_result_code_spinner.SelectedItemPosition;

				if (wiperStatus_chk.Checked)
				{
					wiperStatus.dataType = wiperStatusDataType;
					wiperStatus.resultCode = wiperStatusRsltCode;
				}


				if (headLampStatus_data_type_chk.Checked)
					headLampStatusDataType = (VehicleDataType)headLampStatus_data_type_spinner.SelectedItemPosition;

				if (headLampStatus_result_code_chk.Checked)
					headLampStatusRsltCode = (VehicleDataResultCode)headLampStatus_result_code_spinner.SelectedItemPosition;

				if (headLampStatus_chk.Checked)
				{
					headLampStatus.dataType = headLampStatusDataType;
					headLampStatus.resultCode = headLampStatusRsltCode;
				}


				if (engineTorque_data_type_chk.Checked)
					engineTorqueDataType = (VehicleDataType)engineTorque_data_type_spinner.SelectedItemPosition;

				if (engineTorque_result_code_chk.Checked)
					engineTorqueRsltCode = (VehicleDataResultCode)engineTorque_result_code_spinner.SelectedItemPosition;

				if (engineTorque_chk.Checked)
				{
					engineTorque.dataType = engineTorqueDataType;
					engineTorque.resultCode = engineTorqueRsltCode;
				}


				if (accPedalPosition_data_type_chk.Checked)
					accPedalPositionDataType = (VehicleDataType)accPedalPosition_data_type_spinner.SelectedItemPosition;

				if (accPedalPosition_result_code_chk.Checked)
					accPedalPositionRsltCode = (VehicleDataResultCode)accPedalPosition_result_code_spinner.SelectedItemPosition;

				if (accPedalPosition_chk.Checked)
				{
					accPedalPosition.dataType = accPedalPositionDataType;
					accPedalPosition.resultCode = accPedalPositionRsltCode;
				}


				if (steeringWheelAngle_data_type_chk.Checked)
					steeringWheelAngleDataType = (VehicleDataType)steeringWheelAngle_data_type_spinner.SelectedItemPosition;

				if (steeringWheelAngle_result_code_chk.Checked)
					steeringWheelAngleRsltCode = (VehicleDataResultCode)steeringWheelAngle_result_code_spinner.SelectedItemPosition;

				if (steeringWheelAngle_chk.Checked)
				{
					steeringWheelAngle.dataType = steeringWheelAngleDataType;
					steeringWheelAngle.resultCode = steeringWheelAngleRsltCode;
				}


				if (eCallInfo_data_type_chk.Checked)
					eCallInfoDataType = (VehicleDataType)eCallInfo_data_type_spinner.SelectedItemPosition;

				if (eCallInfo_result_code_chk.Checked)
					eCallInfoRsltCode = (VehicleDataResultCode)eCallInfo_result_code_spinner.SelectedItemPosition;

				if (eCallInfo_chk.Checked)
				{
					eCallInfo.dataType = eCallInfoDataType;
					eCallInfo.resultCode = eCallInfoRsltCode;
				}


				if (airbagStatus_data_type_chk.Checked)
					airbagStatusDataType = (VehicleDataType)airbagStatus_data_type_spinner.SelectedItemPosition;

				if (airbagStatus_result_code_chk.Checked)
					airbagStatusRsltCode = (VehicleDataResultCode)airbagStatus_result_code_spinner.SelectedItemPosition;

				if (airbagStatus_chk.Checked)
				{
					airbagStatus.dataType = airbagStatusDataType;
					airbagStatus.resultCode = airbagStatusRsltCode;
				}


				if (emergencyEvent_data_type_chk.Checked)
					emergencyEventDataType = (VehicleDataType)emergencyEvent_data_type_spinner.SelectedItemPosition;

				if (emergencyEvent_result_code_chk.Checked)
					emergencyEventRsltCode = (VehicleDataResultCode)emergencyEvent_result_code_spinner.SelectedItemPosition;

				if (emergencyEvent_chk.Checked)
				{
					emergencyEvent.dataType = emergencyEventDataType;
					emergencyEvent.resultCode = emergencyEventRsltCode;
				}


				if (clusterModes_data_type_chk.Checked)
					clusterModesDataType = (VehicleDataType)clusterModes_data_type_spinner.SelectedItemPosition;

				if (clusterModes_result_code_chk.Checked)
					clusterModesRsltCode = (VehicleDataResultCode)clusterModes_result_code_spinner.SelectedItemPosition;

				if (clusterModes_chk.Checked)
				{
					clusterModes.dataType = clusterModesDataType;
					clusterModes.resultCode = clusterModesRsltCode;
				}


				if (myKey_data_type_chk.Checked)
					myKeyDataType = (VehicleDataType)myKey_data_type_spinner.SelectedItemPosition;

				if (myKey_result_code_chk.Checked)
					myKeyRsltCode = (VehicleDataResultCode)myKey_result_code_spinner.SelectedItemPosition;

				if (myKey_chk.Checked)
				{
					myKey.dataType = myKeyDataType;
					myKey.resultCode = myKeyRsltCode;
				}


				if (electronicParkBrakeStatus_data_type_chk.Checked)
					electronicParkBrakeStatusDataType = (VehicleDataType)electronicParkBrakeStatus_data_type_spinner.SelectedItemPosition;

				if (electronicParkBrakeStatus_result_code_chk.Checked)
					electronicParkBrakeStatusRsltCode = (VehicleDataResultCode)electronicParkBrakeStatus_result_code_spinner.SelectedItemPosition;

				if (electronicParkBrakeStatus_chk.Checked)
				{
					electronicParkBrakeStatus.dataType = electronicParkBrakeStatusDataType;
					electronicParkBrakeStatus.resultCode = electronicParkBrakeStatusRsltCode;
				}


                if (engineOilLife_data_type_chk.Checked)
                    engineOilLifeDataType = (VehicleDataType)engineOilLife_data_type_spinner.SelectedItemPosition;

                if (engineOilLife_result_code_chk.Checked)
                    engineOilLifeRsltCode = (VehicleDataResultCode)engineOilLife_result_code_spinner.SelectedItemPosition;

                if (engineOilLife_chk.Checked)
                {
                    engineOilLife.dataType = engineOilLifeDataType;
                    engineOilLife.resultCode = engineOilLifeRsltCode;
                }


                if (fuelRange_data_type_chk.Checked)
                    fuelRangeDataType = (VehicleDataType)fuelRange_data_type_spinner.SelectedItemPosition;

                if (fuelRange_result_code_chk.Checked)
                    fuelRangeRsltCode = (VehicleDataResultCode)fuelRange_result_code_spinner.SelectedItemPosition;

                if (fuelRange_chk.Checked)
                {
                    fuelRange.dataType = fuelRangeDataType;
                    fuelRange.resultCode = fuelRangeRsltCode;
                }


				RpcResponse rpcResponse = null;
				rpcResponse = BuildRpc.buildVehicleInfoSubscribeVehicleDataResponse(BuildRpc.getNextId(), rsltCode, gps, speed, rpm, fuelLevel, fuelLevelState, instantFuel, externalTemperature, prndl, tirePressure, odometer, beltStatus,
                                                                                      bodyInformation, deviceStatus, driverBraking, wiperStatus, headLampStatus, engineTorque, accPedalPosition, steeringWheelAngle, eCallInfo, airbagStatus
                                                                                    , emergencyEvent, clusterModes, myKey, electronicParkBrakeStatus, engineOilLife, fuelRange);
                
				AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
                    AppUtils.removeSavedPreferenceValueForRpc(Activity, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.Show();
		}

		private void CreateVIResponseReadDID()
		{			
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.read_did_response, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox did_result_cb = (CheckBox)rpcView.FindViewById(Resource.Id.read_did_result_cb);
			Button createDidResultBtn = (Button)rpcView.FindViewById(Resource.Id.create_did_result_button);
			ListView didResultListView = (ListView)rpcView.FindViewById(Resource.Id.read_did_result_listview);

			CheckBox result_code_spinner = (CheckBox)rpcView.FindViewById(Resource.Id.read_did_result_code_cb);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.read_did_result_code_spinner);

            List<DIDResult> didResultList = new List<DIDResult>();
			var didResultAdapter = new DIDResultAdapter(Activity, didResultList);
			didResultListView.Adapter = didResultAdapter;

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = resultCodeAdapter;

            result_code_spinner.CheckedChange += (sender, e) => spnResultCode.Enabled = e.IsChecked;
            did_result_cb.CheckedChange += (sender, e) => createDidResultBtn.Enabled = e.IsChecked;

            rpcAlertDialog.SetTitle(VIResponseReadDID);

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID();
            tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.ReadDID>(resultCodeAdapter.Context, tmpObj.getMethod());
            
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());

                if (tmpObj.getDidResult() != null)
                {
                    didResultList.AddRange(tmpObj.getDidResult());
                    didResultAdapter.NotifyDataSetChanged();
                }
            }

            DIDResult didRslt = new DIDResult();
			createDidResultBtn.Click += (sender, e) =>
			{
				AlertDialog.Builder didResultAlertDialog = new AlertDialog.Builder(rpcAlertDialog.Context);
				View didResultView = layoutInflater.Inflate(Resource.Layout.did_result_response, null);
				didResultAlertDialog.SetView(didResultView);
				didResultAlertDialog.SetTitle("DID Result");

				CheckBox vehicleDataResultCodeCB = (CheckBox)didResultView.FindViewById(Resource.Id.vehicle_data_result_code_cb);
				Spinner vehicleDataResultCodeSpinner = (Spinner)didResultView.FindViewById(Resource.Id.vehicle_data_result_code_spinner);
				CheckBox didLocationCB = (CheckBox)didResultView.FindViewById(Resource.Id.did_location_cb);
				EditText didLocationET = (EditText)didResultView.FindViewById(Resource.Id.did_location_et);
				CheckBox dataCB = (CheckBox)didResultView.FindViewById(Resource.Id.data_cb);
				EditText dataET = (EditText)didResultView.FindViewById(Resource.Id.data_et);

				string[] vehicleDataResultCodeEnum = Enum.GetNames(typeof(VehicleDataResultCode));
				var vehicleDataResultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, vehicleDataResultCodeEnum);
				vehicleDataResultCodeSpinner.Adapter = vehicleDataResultCodeAdapter;

                vehicleDataResultCodeCB.CheckedChange += (sndr, evnt) => vehicleDataResultCodeSpinner.Enabled = evnt.IsChecked;
                didLocationCB.CheckedChange += (sndr, evnt) => didLocationET.Enabled = evnt.IsChecked;
                dataCB.CheckedChange += (sndr, evnt) => dataET.Enabled = evnt.IsChecked;

                if(didRslt != null)
                {
                    vehicleDataResultCodeSpinner.SetSelection((int)didRslt.getResultCode());
                    didLocationET.Text = didRslt.getDidLocation().ToString();
                    dataET.Text = didRslt.getData();
                }

				didResultAlertDialog.SetNegativeButton(CANCEL, (senderAlert, args) =>
				{
					didResultAlertDialog.Dispose();
				});

				didResultAlertDialog.SetPositiveButton("Add", (senderAlert, args) =>
				{
					didRslt.data = dataET.Text;
					didRslt.resultCode = (VehicleDataResultCode)vehicleDataResultCodeSpinner.SelectedItemPosition;
					try
					{
						didRslt.didLocation = Int32.Parse(didLocationET.Text.ToString());
					}
					catch (Exception e1)
					{

					}

					didResultList.Add(didRslt);
					didResultAdapter.NotifyDataSetChanged();
				});

				didResultAlertDialog.Show();
			};

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (result_code_spinner.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                if (!did_result_cb.Checked)
                    didResultList = null;

                RpcResponse rpcResponse = null;
				rpcResponse = BuildRpc.buildVehicleInfoReadDIDResponse(BuildRpc.getNextId(), rsltCode, didResultList);
				AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(resultCodeAdapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.Show();
		}

        private void CreateVIResponseGetDTCs()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.get_dtc_response, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox ecuHeaderCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.dtc_ecu_header_checkbox);
			EditText ecuHeaderEdittext = (EditText)rpcView.FindViewById(Resource.Id.dtc_ecu_header_edittext);

			CheckBox dtcCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.dtc_checkbox);
			EditText dtcEdittext = (EditText)rpcView.FindViewById(Resource.Id.dtc_edittext);

			CheckBox resultCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.dtc_result_code_checkbox);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.dtc_result_code_spinner);

			rpcAlertDialog.SetTitle(VIResponseGetDTCs);

			var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = resultCodeAdapter;

            ecuHeaderCheckbox.CheckedChange += (s, e) => ecuHeaderEdittext.Enabled = e.IsChecked;
            dtcCheckbox.CheckedChange += (s, e) => dtcEdittext.Enabled = e.IsChecked;
            resultCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;

            HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.GetDTCs>(resultCodeAdapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnResultCode.SetSelection((int)tmpObj.getResultCode());

                if(tmpObj.getEcuHeader() != null)
                    ecuHeaderEdittext.Text = tmpObj.getEcuHeader().ToString();

                List<string> dtcList = tmpObj.getDtc();
                if (null != dtcList)
                {
					string[] messageDataRsltArray = dtcList.ToArray();

					for (int i = 0; i < dtcList.Count; i++)
					{
						if (i == dtcList.Count - 1)
						{
							dtcEdittext.Append(messageDataRsltArray[i]);
							break;
						}
						dtcEdittext.Append(messageDataRsltArray[i] + ",");
					}
                }
            }

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				List<string> dtcList = null;

                int? ecuHeader = null;
                if(ecuHeaderCheckbox.Checked && ecuHeaderEdittext.Text != null && ecuHeaderEdittext.Text.Length > 0)
				    ecuHeader = Int32.Parse(ecuHeaderEdittext.Text);

                if (dtcCheckbox.Checked)
                {
                    string[] dtc = dtcEdittext.Text.Split(',');
                    dtcList = new List<string>(dtc);
                }

                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCheckBox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }

                RpcResponse rpcResponse = BuildRpc.buildVehicleInfoGetDTCsResponse(BuildRpc.getNextId(), rsltCode,ecuHeader, dtcList);
				AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(resultCodeAdapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.Show();
		}

		private void CreateVIResponseDiagnosticMessage()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.diagnostic_message, null);
			rpcAlertDialog.SetView(rpcView);

			CheckBox messageDataCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.diagnostic_messgae_data_check);
			EditText messageDataEdittext = (EditText)rpcView.FindViewById(Resource.Id.diagnostic_messgae_data__et);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.diagnostic_messgae_result_code_checkbox);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.diagnostic_messgae_result_code_spinner);

			rpcAlertDialog.SetTitle(VIResponseDiagnosticMessage);
			
            var resultCodeAdapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = resultCodeAdapter;

            messageDataCheckbox.CheckedChange += (s, e) => messageDataEdittext.Enabled = e.IsChecked;
			resultCodeCheckbox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;
            		
			HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage tmpObj = new HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage();
			tmpObj = (HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage>(resultCodeAdapter.Context, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnResultCode.SetSelection((int)tmpObj.getResultCode());

                List<int> messageDataRsltList = tmpObj.getMessageDataResult();
                if (messageDataRsltList != null)
                {
                    int[] messageDataRsltArray = messageDataRsltList.ToArray();

                    for (int i = 0; i < messageDataRsltList.Count; i++)
                    {
                        if (i == messageDataRsltList.Count - 1)
                        {
                            messageDataEdittext.Append(messageDataRsltArray[i].ToString());
                            break;
                        }
                        messageDataEdittext.Append(messageDataRsltArray[i].ToString() + ",");
                    }
                }
            }

			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				List<int> messageDataResultList = null;
				RpcResponse rpcResponse = null;

				if (messageDataCheckbox.Checked)
				{
					messageDataResultList = new List<int>();
					string[] t = messageDataEdittext.Text.Split(',');
					foreach (string ts in t)
					{
						try
						{
							messageDataResultList.Add(Int32.Parse(ts));
						}
						catch (Exception e)
						{

						}
					}
				}

                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCheckbox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;
                }                

                rpcResponse = BuildRpc.buildVehicleInfoDiagnosticMessageResponse(BuildRpc.getNextId(), rsltCode, messageDataResultList);
                AppUtils.savePreferenceValueForRpc(resultCodeAdapter.Context, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);				
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(resultCodeAdapter.Context, tmpObj.getMethod());
				}
			});

			rpcAlertDialog.Show();
		}

		private void CreateBCResponseUpdateDeviceList()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseUpdateDeviceList);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);

			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);
			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList>(adapter.Context, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnGeneric.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});


            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationUpdateDeviceListResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateBCResponseUpdateAppList()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseUpdateAppList);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.UpdateAppList>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});
		

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationUpdateAppListResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateBCResponseSystemRequest()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseSystemRequest);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);

			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);
			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.SystemRequest>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});
		

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationSystemRequestResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		private void CreateBCResponsePolicyUpdate()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponsePolicyUpdate);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate>(adapter.Context, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});


            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationPolicyUpdateResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }
			});

			rpcAlertDialog.Show();
		}

		void CreateBCResponseDialNumber()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseDialNumber);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

            resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
            resultCodeCheckbox.Text = "ResultCode";

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DialNumber>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}
            	

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				} 
				RpcResponse rpcResponse = null;
                rpcResponse = BuildRpc.buildBasicCommunicationDialNumberResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }

			});

			rpcAlertDialog.Show();
		}


        void CreateBCResponseDecryptCertificate()
        {
            AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
            View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseDecryptCertificate);

            CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
            Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

            var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
            spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate();
            tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate>(Activity, tmpObj.getMethod());
            if (tmpObj != null)
            {
                spnGeneric.SetSelection((int)tmpObj.getResultCode());
            }

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
            {
                rpcAlertDialog.Dispose();
            });

            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
            {
                HmiApiLib.Common.Enums.Result? rsltCode = null;
                if (resultCodeCheckbox.Checked)
                {
                    rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
                }

                RpcResponse rpcResponse = null;
                rpcResponse = BuildRpc.buildBasicCommunicationDecryptCertificateResponse(BuildRpc.getNextId(), rsltCode);
                AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
            });

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
            {
                if (tmpObj != null)
                {
                    AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
                }

            });

            rpcAlertDialog.Show();
        }

		private void CreateBCResponseAllowDeviceToConnect()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = (View)layoutInflater.Inflate(Resource.Layout.allow_device_to_Connect, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseAllowDeviceToConnect);

			CheckBox checkBoxAllow = (CheckBox)rpcView.FindViewById(Resource.Id.allow);

			CheckBox rsltCodeCheckBox = (CheckBox)rpcView.FindViewById(Resource.Id.result_code_cb);
			Spinner spnResultCode = (Spinner)rpcView.FindViewById(Resource.Id.result_Code);

			rsltCodeCheckBox.CheckedChange += (s, e) => spnResultCode.Enabled = e.IsChecked;
			checkBoxAllow.Text = ("Allow");
			rsltCodeCheckBox.Text = "Result Code";

			var adapter1 = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnResultCode.Adapter = adapter1;

			HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				checkBoxAllow.Checked = (bool)tmpObj.getAllow();
                spnResultCode.SetSelection((int)tmpObj.getResultCode());
			}


            rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (rsltCodeCheckBox.Checked)
					rsltCode = (HmiApiLib.Common.Enums.Result)spnResultCode.SelectedItemPosition;

				RpcResponse rpcResponse = BuildRpc.buildBasicCommunicationAllowDeviceToConnectResponse(BuildRpc.getNextId(), rsltCode, checkBoxAllow.Checked);
                AppUtils.savePreferenceValueForRpc(adapter1.Context, ((RpcResponse)rpcResponse).getMethod(), rpcResponse);
			});

            rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter1.Context, tmpObj.getMethod());
				}
			});

            rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.Show();
		}


		void CreateBCResponseActivateApp()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(BCResponseActivateApp);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.ActivateApp tmpObj = new HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.ActivateApp();
			tmpObj = (HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.ActivateApp)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.BasicCommunication.OutgoingResponses.ActivateApp>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}


			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcResponse = null;
                rpcResponse = BuildRpc.buildBasicCommunicationActivateAppResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}

			});

			rpcAlertDialog.Show();
		}

		void CreateButtonsResponseButtonPress()
		{
			AlertDialog.Builder rpcAlertDialog = new AlertDialog.Builder(Activity);
			View rpcView = layoutInflater.Inflate(Resource.Layout.genericspinner, null);
			rpcAlertDialog.SetView(rpcView);
			rpcAlertDialog.SetTitle(ButtonsResponseButtonPress);

			CheckBox resultCodeCheckbox = (CheckBox)rpcView.FindViewById(Resource.Id.genericspinner_result_code_cb);
			Spinner spnGeneric = (Spinner)rpcView.FindViewById(Resource.Id.genericspinner_Spinner);

			resultCodeCheckbox.CheckedChange += (s, e) => spnGeneric.Enabled = e.IsChecked;
			resultCodeCheckbox.Text = "ResultCode";

			var adapter = new ArrayAdapter<String>(Activity, Android.Resource.Layout.SimpleSpinnerDropDownItem, resultCode);
			spnGeneric.Adapter = adapter;

            HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress tmpObj = new HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress();
            tmpObj = (HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress)AppUtils.getSavedPreferenceValueForRpc<HmiApiLib.Controllers.Buttons.OutgoingResponses.ButtonPress>(Activity, tmpObj.getMethod());
			if (tmpObj != null)
			{
				spnGeneric.SetSelection((int)tmpObj.getResultCode());
			}


			rpcAlertDialog.SetNeutralButton(CANCEL, (senderAlert, args) =>
			{
				rpcAlertDialog.Dispose();
			});

			rpcAlertDialog.SetNegativeButton(TX_LATER, (senderAlert, args) =>
			{
				HmiApiLib.Common.Enums.Result? rsltCode = null;
				if (resultCodeCheckbox.Checked)
				{
					rsltCode = (HmiApiLib.Common.Enums.Result)spnGeneric.SelectedItemPosition;
				}

				RpcResponse rpcResponse = null;
                rpcResponse = BuildRpc.buildButtonsButtonPressResponse(BuildRpc.getNextId(), rsltCode);
				AppUtils.savePreferenceValueForRpc(adapter.Context, rpcResponse.getMethod(), rpcResponse);
			});

			rpcAlertDialog.SetPositiveButton(RESET, (senderAlert, args) =>
			{
				if (tmpObj != null)
				{
					AppUtils.removeSavedPreferenceValueForRpc(adapter.Context, tmpObj.getMethod());
				}

			});

			rpcAlertDialog.Show();
		}
	}
}
