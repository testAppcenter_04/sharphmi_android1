﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Enums;

namespace SharpHmiAndroid
{
    public class SpeechCapabilitiesAdapter : BaseAdapter<SpeechCapabilities>
    {
        List<SpeechCapabilities> speechCapList;
        Activity context;

        public SpeechCapabilitiesAdapter(Activity act, List<SpeechCapabilities> list)
        {
            speechCapList = list;
            context = act;
        }

        public override SpeechCapabilities this[int position] => speechCapList[position];

        public override int Count => speechCapList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? context.LayoutInflater.Inflate(Resource.Layout.touch_event_item_adapter, parent, false);

            var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = speechCapList[position].ToString();
            return view;
        }
    }
}
