﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid
{
    internal class RadioControlCapabilitiesAdapter:BaseAdapter<RadioControlCapabilities>
    {
        private Activity context;
        private List<RadioControlCapabilities> radioControlCapabilitiesList;

        public RadioControlCapabilitiesAdapter(Activity activity, List<RadioControlCapabilities> radioControlCapabilitiesList)
        {
            this.context = activity;
            this.radioControlCapabilitiesList = radioControlCapabilitiesList;
        }

        public override RadioControlCapabilities this[int position] => radioControlCapabilitiesList[position];

        public override int Count => radioControlCapabilitiesList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
			var view = context.LayoutInflater.Inflate(Resource.Layout.touch_event_item_adapter, parent, false);

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = radioControlCapabilitiesList[position].getModuleName().ToString();
			return view;
        }
    }
}