﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Enums;

namespace SharpHmiAndroid
{
	public class VrCapabilitiesAdapter : BaseAdapter<VrCapabilities>
	{
		List<VrCapabilities> vrCapabilities;
		Activity context;

		public VrCapabilitiesAdapter(Activity act, List<VrCapabilities> list) : base()
		{
			vrCapabilities = list;
			context = act;
		}

		public override VrCapabilities this[int position] => vrCapabilities[position];

		public override int Count => vrCapabilities.Count;

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var view = convertView ?? context.LayoutInflater.Inflate(
				Resource.Layout.touch_event_item_adapter, parent, false);

			var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = vrCapabilities[position].ToString();

			return view;
		}
	}
}
